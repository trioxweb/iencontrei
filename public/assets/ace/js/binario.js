
// Load this script once the document is ready
$(document).ready(function () {

    // Get all the thumbnail
$('div.thumbnail-item_01_01, ' +
    'div.thumbnail-item_02_01, ' +
    'div.thumbnail-item_02_02, ' +
    'div.thumbnail-item_03_01, ' +
    'div.thumbnail-item_03_02, ' +
    'div.thumbnail-item_03_03, ' +
    'div.thumbnail-item_03_04, ' +
    'div.thumbnail-item_04_01, ' +
    'div.thumbnail-item_04_02, ' +
    'div.thumbnail-item_04_03, ' +
    'div.thumbnail-item_04_04, ' +
    'div.thumbnail-item_04_05, ' +
    'div.thumbnail-item_04_06, ' +
    'div.thumbnail-item_04_07, ' +
    'div.thumbnail-item_04_08, ' +
    'div.thumbnail-item_05_01, ' +
    'div.thumbnail-item_05_02, ' +
    'div.thumbnail-item_05_03, ' +
    'div.thumbnail-item_05_04, ' +
    'div.thumbnail-item_05_05, ' +
    'div.thumbnail-item_05_06, ' +
    'div.thumbnail-item_05_07, ' +
    'div.thumbnail-item_05_08, ' +
    'div.thumbnail-item_05_09, ' +
    'div.thumbnail-item_05_010, ' +
    'div.thumbnail-item_05_011, ' +
    'div.thumbnail-item_05_012, ' +
    'div.thumbnail-item_05_013, ' +
    'div.thumbnail-item_05_014, ' +
    'div.thumbnail-item_05_015, ' +
    'div.thumbnail-item_05_016').mouseenter(function (e) {
        // Calculate the position of the image tooltip
        x = e.pageX - $(this).offset().left;
        y = e.pageY - $(this).offset().top;

        // Set the z-index of the current item,
        // make sure it's greater than the rest of thumbnail items
        // Set the position and display the image tooltip
        $(this).css('z-index', '15')
            .children("div.tooltip_p")
            .css({'top':y + 0, 'left':x + 0, 'display':'block'});

}).mousemove(function (e) {

    // Calculate the position of the image tooltip
    x = e.pageX - $(this).offset().left;
    y = e.pageY - $(this).offset().top;

    // This line causes the tooltip will follow the mouse pointer
    $(this).children("div.tooltip_p").css({'top':y + -200, 'left':x = -210});

}).mouseleave(function () {

    // Reset the z-index and hide the image tooltip
    $(this).css('z-index', '1')
        .children("div.tooltip_p")
        .animate({"opacity":"hide"}, "fast");
});

});



    function Browser() {

        var ua, s, i;

        this.isIE = false;
        this.isNS = false;
        this.version = null;

        ua = navigator.userAgent;

        s = "MSIE";
        if ((i = ua.indexOf(s)) >= 0) {
            this.isIE = true;
            this.version = parseFloat(ua.substr(i + s.length));
            return;
        }

        s = "Netscape6/";
        if ((i = ua.indexOf(s)) >= 0) {
            this.isNS = true;
            this.version = parseFloat(ua.substr(i + s.length));
            return;
        }

// Treat any other "Gecko" browser as NS 6.1.

        s = "Gecko";
        if ((i = ua.indexOf(s)) >= 0) {
            this.isNS = true;
            this.version = 6.1;
            return;
        }
    }

var browser = new Browser();

// Global object to hold drag information.

var dragObj = new Object();
dragObj.zIndex = 0;

function dragStart(event, id) {

    var el;
    var x, y;

// If an element id was given, find it. Otherwise use the element being
// clicked on.

    if (id) {
        dragObj.elNode = document.getElementById(id);
    }
    else {
        if (browser.isIE) {
            dragObj.elNode = window.event.srcElement;
        }
        if (browser.isNS) {
            dragObj.elNode = event.target;
        }

// If this is a text node, use its parent element.

        if (dragObj.elNode.nodeType == 3) {
            dragObj.elNode = dragObj.elNode.parentNode;
        }
    }

// Get cursor position with respect to the page.

    if (browser.isIE) {
        x = window.event.clientX + document.documentElement.scrollLeft
            + document.body.scrollLeft;
        y = window.event.clientY + document.documentElement.scrollTop
            + document.body.scrollTop;
    }
    if (browser.isNS) {
        x = event.clientX + window.scrollX;
        y = event.clientY + window.scrollY;
    }

// Save starting positions of cursor and element.

    dragObj.cursorStartX = x;
    dragObj.cursorStartY = y;
    dragObj.elStartLeft = parseInt(dragObj.elNode.style.left, 10);
    dragObj.elStartTop = parseInt(dragObj.elNode.style.top, 10);

    if (isNaN(dragObj.elStartLeft)) dragObj.elStartLeft = 0;
    if (isNaN(dragObj.elStartTop))  dragObj.elStartTop = 0;

// Update element's z-index.

    dragObj.elNode.style.zIndex = ++dragObj.zIndex;

// Capture mousemove and mouseup events on the page.

    if (browser.isIE) {
        document.attachEvent("onmousemove", dragGo);
        document.attachEvent("onmouseup", dragStop);
        window.event.cancelBubble = true;
        window.event.returnValue = false;
    }
    if (browser.isNS) {
        document.addEventListener("mousemove", dragGo, true);
        document.addEventListener("mouseup", dragStop, true);
        event.preventDefault();
    }
}

function dragGo(event) {

    var x, y;

// Get cursor position with respect to the page.

    if (browser.isIE) {
        x = window.event.clientX + document.documentElement.scrollLeft
            + document.body.scrollLeft;
        y = window.event.clientY + document.documentElement.scrollTop
            + document.body.scrollTop;
    }
    if (browser.isNS) {
        x = event.clientX + window.scrollX;
        y = event.clientY + window.scrollY;
    }

// Move drag element by the same amount the cursor has moved.

    dragObj.elNode.style.left = (dragObj.elStartLeft + x - dragObj.cursorStartX) + "px";
    dragObj.elNode.style.top = (dragObj.elStartTop + y - dragObj.cursorStartY) + "px";

    if (browser.isIE) {
        window.event.cancelBubble = true;
        window.event.returnValue = false;
    }
    if (browser.isNS) {
        event.preventDefault();
    }
}

function dragStop(event) {

// Stop capturing mousemove and mouseup events.

    if (browser.isIE) {
        document.detachEvent("onmousemove", dragGo);
        document.detachEvent("onmouseup", dragStop);
    }
    if (browser.isNS) {
        document.removeEventListener("mousemove", dragGo, true);
        document.removeEventListener("mouseup", dragStop, true);
    }
}



    // Load this script once the document is ready

    $(document).ready(function () {



        // Get all the thumbnail

        $('div.thumbnail-item_01_01, div.thumbnail-item_02_01, div.thumbnail-item_02_02, div.thumbnail-item_03_01, div.thumbnail-item_03_02, div.thumbnail-item_03_03, div.thumbnail-item_03_04, div.thumbnail-item_04_01, div.thumbnail-item_04_02, div.thumbnail-item_04_03, div.thumbnail-item_04_04, div.thumbnail-item_04_05, div.thumbnail-item_04_06, div.thumbnail-item_04_07, div.thumbnail-item_04_08, div.thumbnail-item_05_01, div.thumbnail-item_05_02, div.thumbnail-item_05_03, div.thumbnail-item_05_04, div.thumbnail-item_05_05, div.thumbnail-item_05_06, div.thumbnail-item_05_07, div.thumbnail-item_05_08, div.thumbnail-item_05_09, div.thumbnail-item_05_010, div.thumbnail-item_05_011, div.thumbnail-item_05_012, div.thumbnail-item_05_013, div.thumbnail-item_05_014, div.thumbnail-item_05_015, div.thumbnail-item_05_016,div.avatar_item_01_01').mouseenter(function (e) {



            // Calculate the position of the image tooltip

            x = e.pageX - $(this).offset().right;

            y = e.pageY - $(this).offset().top;


            // Set the z-index of the current item,

            // make sure it's greater than the rest of thumbnail items

            // Set the position and display the image tooltip

            $(this).css('z-index', '15')

                .children("div.tooltip_p_r")

                .css({'top':y + 0, 'left':x + 0, 'display':'block'});


    }).mousemove(function (e) {



        // Calculate the position of the image tooltip

        x = e.pageX - $(this).offset().right;

        y = e.pageY - $(this).offset().top;


        // This line causes the tooltip will follow the mouse pointer

        $(this).children("div.tooltip_p_r").css({'top':y + 10, 'right':x = 45});


    }).mouseleave(function () {



        // Reset the z-index and hide the image tooltip

        $(this).css('z-index', '1')

            .children("div.tooltip_p_r")

            .animate({"opacity":"hide"}, "slow");

    });


    });


