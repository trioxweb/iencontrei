function usuario(){
	var username = $('input#username').val();
	
	$('em.loadUsuario').html('');
	
	if (username !=''){

		$('em.loadUsuario').html('...');

		$('em.loadUsuario').load('/register/loadusername/'+username);

	}

}

function patrocinador(){

	var parent = $('input#parent').val();
	if (parent !=''){
		$('em.loadPatrocinador').html('carregando..');
		$('em.loadPatrocinador').load('/register/loadparent/'+parent);
	}
}

function getEndereco(cep) {
    if($.trim(cep) != ""){
        $.getScript("http://cep.republicavirtual.com.br/web_cep.php?formato=javascript&cep="+cep, function(){            
            if (resultadoCEP["resultado"] != 0) {
            	$("#address").val(unescape(resultadoCEP["tipo_logradouro"]) + " " + unescape(resultadoCEP["logradouro"]) + ", ? - " + unescape(resultadoCEP["bairro"]));
                $("#city").val(unescape(resultadoCEP["cidade"]));
                $("#zone").val(unescape(resultadoCEP["uf"]));
                $("#country").val('Brasil');
            }     
        });
    }
}

function addNewTelephone(){
	id = window.idTelephone += 1;
	$('#newtelephone').before('<div class="form-group"><label for="telephone" class="col-sm-3 control-label"></label><div class="col-sm-4"><input type="text" class="form-control" id="telephone'+id+'" name="telephones['+id+'][0]" data-mask="(99) 999999999" value=""></div><div class="col-sm-3"><input type="text" class="form-control" id="provider" name="telephones['+id+'][1]" placeholder="Operadora" value=""></div><div class="col-sm-1"><a class="btn btn-default rmTelephone" onclick="rmTelephone(this);"><i class="entypo-minus"></i></a></div></div>');
	$('#telephone'+id).mask('(99) 999999999');
	return false;
}

function rmTelephone(element){
	$(element).parent().parent().remove();
	return false;
}

$(function(){
	/* Masks */
	$("input.data").mask("99/99/9999");
	$("#data").mask("99/99/9999");
	$('#nascimento').mask('99/99/9999');
	$('#cpf').mask('999.999.999-99');
	$('#cnpj').mask('99.999.999/9999-99');
	$('#postcode').mask('99999-999');
	$('#zone').mask('aa');

	/* \START Busca Endereco Automatico */
	$('#rootwizard-2 #postcode').change(function() {
		getEndereco($('#rootwizard-2 #postcode').val());
	});

	/* \END Busca Endereco Automatico */

	/*$('#tel_primario').mask('(99) 9999-9999');
	$('#tel_secundario').mask('(99) 9999-9999');
	/*$('#tel_celular').mask('(99) 9999-9999');*/

	$('input.reais').priceFormat({
		prefix: '',
		centsSeparator: ',',
		thousandsSeparator: '.'
	});
	
	$('input#username').blur(function(){ usuario(); });

	$('input#parent').blur(function(){ patrocinador(); });
});