jQuery.fn.monthPicker = function(options)
{
	this.each(function(){
		var months = options.months? options.months : ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
	
		if (options.start) {
			if (options.year < options.start) {
				options.year = (new Date()).getFullYear();
			}
		}
		
		options.original_year = options.year;
		
		if (!options.next) {
			options.next = 'Next';
		}
		
		if (!options.previous) {
			options.previous = 'Previous';
		}
		
		if (!options.close) {
			options.close = 'Close';
		}
		
		var _create = function(parent, options)
		{
			if($('div.month-picker').length == 0) {
				options.year = options.original_year;
			}
			
			var div = '<div class="month-picker">';
			var prev = options.year > options.start? '<div class="link-prev"><a href="javascript:;">' + options.previous + '</a></div>' : '';
			var next = (options.year < options.end || !options.end)? '<div class="link-next"><a href="javascript:;">' + options.next + '</a></div>' : '';
			div += '<div class="link-close"><a href="javascript:;">' + options.close + '</a></div>';
			div += '<h3>' + options.year + '</h3><table><tbody><tr>';
			
			for(var m in months) {
				var month = (parseInt(m)+1);
				var url = options.url.replace(/%y/, options.year);
				url = url.replace(/%m/, month);
				
				div += '<td class="m' + month + '"><a href="' + url + '"><span class="name">' + months[m] + '</span></a></td>';
				
				if (month % 4 == 0) {
					div += '</tr><tr>';
				}
			}
			
			div += '</tbody></table>' +
					prev + ' ' + next;
			
			$('div.month-picker', parent).remove();
			$(parent).append(div);
			_add_events(parent, options);
		}
		
		var _add_events = function(parent, options)
		{
			$('.link-next a', parent).click(function(){
				options.year++;
				_create(parent, options);
			});
			
			$('.link-prev a', parent).click(function(){
				options.year--;
				_create(parent, options);
			});
			
			$('.link-close a', parent).click(function(){
				$(this).parents('div.month-picker').remove();
			});
			
			$('div.month-picker', parent).click(function(e){
				e.stopPropagation();
				e.preventDefault();
			});
			
			$('table a', parent).click(function(e){
				location.href = this.href;
				$(this).parents('div.month-picker').remove();
				e.stopPropagation();
				e.preventDefault();
			});
		}
		
		$(this)
			.wrap('<div class="month-picker-block" />')
			.click(function(e){
				$(this).siblings('div.month-picker').remove();
				var parent = $(this).parent('div');
				
				_create(parent, options);
				
				$(document).click(function(){
					$('div.month-picker').remove();
				});
				
				e.stopPropagation();
				e.preventDefault();
			});
	});
	
	return this;
}