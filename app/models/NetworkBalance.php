<?php

class NetworkBalance extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'network_balance';

	public $timestamps = 'created_at';
    
}