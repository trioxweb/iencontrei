<?php

class Point extends \Eloquent {
	protected $fillable = [];

	/**
	 * Get the points by User.
	 *
	 * @return Point
	 */
	public static function getTotalPointsByUser($user_id=null, $date_initial=null, $date_final=null)
	{
		return Cache::remember('get_total_points_by_user_'.$user_id.'_'.$date_initial.'_'.$date_final, 60, function() use ($user_id, $date_initial, $date_final){
			$points = Point::select('user_id as id', (DB::raw('sum(`value`) as `value`')))
								->where('status','=',1);

			if($user_id){
				$points->where('user_id', $user_id);
			}else{
				$points->groupBy('user_id');
			}

			if($date_initial && $date_final){
				$points->whereRaw(
					"created_at BETWEEN
						'".$date_initial."' AND '".$date_final."'"
				);
			}

			return $points->get();
		});
	}

	/**
	 * Get the user.
	 *
	 * @return User
	 */
	public function user()
	{
		return $this->belongsTo('User');
	}
}