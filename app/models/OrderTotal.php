<?php

class OrderTotal extends \Eloquent {
	protected $fillable = ['order_id', 'code', 'title', 'text', 'value', 'sort_order'];
	public $timestamps = false;
	protected $table = 'order_total';

	/**
	 * Get the order sub-total model.
	 *
	 * @return OrderTotal
	 */
	function getOrderSubtotal()
	{
		return $this->where('code', 'sub_total');
	}

	/**
	 * Get the order model.
	 *
	 * @return Order
	 */
	public function order()
	{
		return $this->belongsTo('Order', 'id', 'order_id');
	}
}