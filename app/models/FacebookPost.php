<?php

class FacebookPost extends \Eloquent {

	protected $table = 'facebook_posts';

	/**
	 * Get the user.
	 *
	 * @return User
	 */
	public function user()
	{
		return $this->belongsTo('User');
	}

	public static function getShareThisWeekPostsCount($user_id)
	{
		$previous_date = new DateTime('now');
		$actual_date = new DateTime('now');

		$posts = FacebookPost::where('user_id', $user_id)
					->whereBetween('created_at', array(
						$previous_date->modify('-1 week')->format('Y-m-d').' 00:00:00', 
						$actual_date->format('Y-m-d H:i:s')))
					->get();
		$posts_valids = 0;
		foreach ($posts as $post) {
			if($post->isValid())
				$posts_valids += 1;
		}
		return $posts_valids;
	}

	public static function getSharePostsCount($user_id)
	{
		$previous_date = new DateTime('now');
		$actual_date = new DateTime('now');

		$posts = FacebookPost::where('user_id', $user_id)
					->whereBetween('created_at', array(
						$previous_date->modify('-1 day')->format('Y-m-d').' 00:00:00', 
						$actual_date->format('Y-m-d H:i:s')))
					->get();
		$posts_valids = 0;
		foreach ($posts as $post) {
			if($post->isValid())
				$posts_valids += 1;
		}
		return $posts_valids;
	}

	public function isValid()
	{
		$response = get_headers('https://www.facebook.com/'.$this->post_id);
		if($response[0] == 'HTTP/1.1 301 Moved Permanently'){
			return true;
		}
		return false;
	}

	public static function getTotalWeekPosts(){
		$arr = array();
		$final_arr = array();

		$previous_date = new DateTime('now');
		$previous_date = $previous_date->modify('-4 week');
		for ($i=1; $i <= 4; $i++) { 
			$posts = DB::table('facebook_posts')
				->whereBetween('created_at', array(
						$previous_date->format('Y-m-d').' 00:00:00', 
						$previous_date->modify('+7 day')->format('Y-m-d H:i:s')))
				->get();

	        foreach ($posts as $post) {
	        	@$arr[$post->user_id][$i] += 1;

	        	if($arr[$post->user_id][$i] == 6){
	        		@$final_arr[$post->user_id] += 1;
	        	}
	        }
		}

		return $final_arr;
	}
}