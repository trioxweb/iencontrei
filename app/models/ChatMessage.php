<?php

class ChatMessage extends \Eloquent {

	protected $table = 'chat_messages';
	protected $orderby = 'created_at';

	public static function getAllHistory($user_id){
		$chatHistory = ChatMessage::whereRaw("read_at BETWEEN '".date('Y-m-d H:i:s')."' - INTERVAL 2 DAY AND '".date('Y-m-d H:i:s')."'")
			->orWhere('read_at', null)
			->where('sender_user_id', $user_id)
			->orWhere('receiver_user_id', $user_id)
			->get();
		return $chatHistory;
	}

	public static function getHistory($sender_user_id, $receiver_user_id){
		$chatHistory = ChatMessage::whereRaw("read_at BETWEEN '".date('Y-m-d H:i:s')."' - INTERVAL 2 DAY AND '".date('Y-m-d H:i:s')."'")
			->orWhere('read_at', null)
			->where('sender_user_id', $sender_user_id)
			->orWhere('sender_user_id', $receiver_user_id)
			->where('receiver_user_id', $receiver_user_id)
			->orWhere('receiver_user_id', $sender_user_id)
			->get();
		return $chatHistory;
	}

	public function isRead(){
		if($this->read_at > 0)
			return true;
	}

	public function setHasRead(){
		$this->read_at = date('Y-m-d H:i:s');
		return $this->save();
	}

	public function getChatId($user_id)
    {
        if($this->receiver_user_id != $user_id){
        	return $this->receiver_user_id;
        }else{
        	return $this->sender_user_id;
        }
    }

    public function getFromName($user_id)
    {
    	return User::find($this->getChatId($user_id))->data->name;
    }
}