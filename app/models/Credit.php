<?php

class Credit extends \Eloquent {
	protected $fillable = ['id', 'status','description', 'value', 'user_id', 'created_at', 'updated_at'];

	public static function getBalance($user_id=null)
	{
		if(!$user_id)
			$user_id = Auth::user()->id;

		$balance = Credit::select(DB::raw('SUM(value) as balance'))
		->where('user_id', '=', $user_id)
		->where('status', '=', '1')
		->first()
		->balance;

		return ($balance) ? $balance : 0;
	}

	public static function getTotalGains($user_id=null)
	{
		if(!$user_id)
			$user_id = Auth::user()->id;

		$balance = Credit::select(DB::raw('SUM(value) as balance'))
		->where('user_id', '=', $user_id)
		->where('status', '=', '1')
		->where('value', 'NOT LIKE', '%-%')
		->first()
		->balance;

		return ($balance) ? $balance : 0;
	}

	public static function getBalanceTotal()
	{
		$balance = Credit::select(DB::raw('SUM(value) as balance'))
		->where('status', '=', '1')
		->first()
		->balance;

		return ($balance) ? $balance : 0;
	}

	public static function divideProfit($value, $network_plan_id = false, $network_title_id = false, $verify_post, $desc = 'Divide Profit')
	{
		if($network_plan_id){
			$usersMlm = UserMlm::where('network_plan_id', $network_plan_id)->where('network_in', 1)->get();
		}elseif($network_title_id){
			$usersMlm = UserMlm::where('network_title_id', $network_title_id)->where('network_in', 1)->get();
		}

		if($verify_post == 1){
			$total_week_by_id = FacebookPost::getTotalWeekPosts();
		}

		if($usersMlm->count() > 0){
			$divided_value = $value / $usersMlm->count();

			foreach ($usersMlm as $userMlm) {
				if($verify_post == 1){
					if(isset($total_week_by_id[$userMlm->user_id])){
						$divided_value = ($divided_value / 4) * $total_week_by_id[$userMlm->user_id];
					}else{
						$divided_value = 0;
					}
				}
				if($divided_value != 0){
					$db[] = array(
						'user_id'		      => $userMlm->user_id,
						'value'	  			  => $divided_value,
						'description'		  => $desc,
						'created_at' 		  => date('Y-m-d H:i:s'),
						'updated_at'		  => date('Y-m-d H:i:s'),
					);
				}
			}
			if(isset($db)){
				DB::table('credits')->insert($db);
				return true;
			}
		}
	}

	/**
	 * Get the user.
	 *
	 * @return User
	 */
	public function user()
	{
		return $this->belongsTo('User');
	}

	/**
	 * Get the credit type.
	 *
	 * @return CreditType
	 */
	public function type()
	{
		return $this->hasOne('CreditType', 'id', 'type_id');
	}

}