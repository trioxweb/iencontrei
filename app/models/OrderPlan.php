<?php

class OrderPlan extends \Eloquent {
	protected $fillable = ['order_id','plan_id', 'name', 'total'];
	public $timestamps = false;
	protected $table = 'order_plan';

	/**
	 * Get the order model.
	 *
	 * @return Order
	 */
	public function order()
	{
		return $this->belongsTo('Order', 'id', 'order_id');
	}
}