<?php

class OrderStatus extends \Eloquent {
	protected $fillable = [];
	public $timestamps = false;
	protected $table = 'order_status';

	/**
	 * Get the order model.
	 *
	 * @return Order
	 */
	public function order()
	{
		return $this->hasOne('Order', 'id', 'order_status_id');
	}
}