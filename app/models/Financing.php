<?php

class Financing extends Eloquent {
	protected $fillable = ['order_id', 'funder_user_id', 'funded_user_id', 'amount_financed', 'amount_paid'];
	protected $table = 'financings';

	public function Balance(){
		$financings = Financing::where('amount_paid','<', DB::raw("`amount_financed`"))->get();
		foreach ($financings as $financing) {
			$to_pay = $financing->amount_financed - $financing->amount_paid;
			$funded_balance = Credit::getBalance($financing->funded_user_id);
			if($funded_balance > 0){

				$amount_can_paid = ($funded_balance < $to_pay) ? $funded_balance : $to_pay;
				
				$rest_to_pay = Currency::format($to_pay - $amount_can_paid);

				// Of Funded
				$db[] = array(
					'user_id'		      => $financing->funded_user_id,
					'value'	  		  	  => '-'.$amount_can_paid,
					'description'		  => 'Financiamento pelo usuario '.$financing->funder->username.', Restando Pagar: '.$rest_to_pay,
					'created_at' 		  => date('Y-m-d H:i:s'),
					'updated_at' 		  => date('Y-m-d H:i:s'),
				);

				// To Funder
				$db[] = array(
					'user_id'		      => $financing->funder_user_id,
					'value'	  			  => $amount_can_paid,
					'description'		  => 'Financiamento do usuario '.$financing->funded->username.', Restando Receber: '.$rest_to_pay,
					'created_at' 		  => date('Y-m-d H:i:s'),
					'updated_at' 		  => date('Y-m-d H:i:s'),
				);

				$financing->amount_paid += $amount_can_paid;
				$financing->save();
			}
		}

		if(isset($db)){
			DB::table('credits')->insert($db);
		}
	}

	/**
	 * Get the funded_user_id.
	 *
	 * @return User
	 */
	public function funded()
	{
		return $this->belongsTo('User', 'funded_user_id', 'id');
	}

	/**
	 * Get the funder_user_id.
	 *
	 * @return User
	 */
	public function funder()
	{
		return $this->belongsTo('User', 'funder_user_id', 'id');
	}
}