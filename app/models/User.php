<?php

use Zizaco\Confide\ConfideUser;
use Zizaco\Confide\Confide;
use Zizaco\Confide\ConfideEloquentRepository;
use Zizaco\Entrust\HasRole;
use Carbon\Carbon;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;
use SammyK\LaravelFacebookSdk\FacebookableTrait;

class User extends ConfideUser implements UserInterface, RemindableInterface{  
    use HasRole;

    /**
     * Facebook Includes for Connect to SDK
     */
    use FacebookableTrait;
    //protected $hidden = ['access_token'];
    protected static $facebook_field_aliases = [
        'facebook_field_name' => 'database_column_name',
        'id' => 'facebook_user_id',
    ];

    public $rulesRegister = array(
        'username'  => 'alpha',
        'firstname' => 'required|min:3',
        'lastname'  => 'required|min:3',
        //'postcode'  => 'required',
        'parent'    => 'required|exists:users,username',
    );

    public $rulesDataUpdate = array(
        'firstname' => 'required|min:3',
        'lastname'  => 'required|min:3',
        'parent'    => 'exists:users,username',
    );

    public $rulesAddressUpdate = array(
        'address'  => 'required',
        //'postcode' => 'required'
    );

    public $rulesBankUpdate = array(
        'paypal' => 'email', 
        'skrill' => 'email'
    );

    public function getRulesRegister(){
        return $this->rulesRegister;
    }

    public function getRulesDataUpdate(){
        return $this->rulesDataUpdate;
    }

    public function getRulesAddressUpdate(){
        return $this->rulesAddressUpdate;
    }

    public function getRulesBankUpdate(){
        return $this->rulesBankUpdate;
    }

    public function getRulesAllUpdate(){
        return array_merge($this->getUpdateRules(), $this->rulesDataUpdate, 
            $this->rulesAddressUpdate, $this->rulesBankUpdate);
    }

    /**
     * Get user by username
     * @param $username
     * @return mixed
     */
    public function getUserByUsername( $username )
    {
        return $this->where('username', '=', $username)->first();
    }

    /**
     * Get the date the user was created.
     *
     * @return string
     */
    public function joined()
    {
        return String::date(Carbon::createFromFormat('Y-n-j G:i:s', $this->created_at));
    }

    /**
     * Save roles inputted from multiselect
     * @param $inputRoles
     */
    public function saveRoles($inputRoles)
    {
        if(! empty($inputRoles)) {
            $this->roles()->sync($inputRoles);
        } else {
            $this->roles()->detach();
        }
    }

    /**
     * Returns user's current role ids only.
     * @return array|bool
     */
    public function currentRoleIds()
    {
        $roles = $this->roles;
        $roleIds = false;
        if( !empty( $roles ) ) {
            $roleIds = array();
            foreach( $roles as &$role )
            {
                $roleIds[] = $role->id;
            }
        }
        return $roleIds;
    }

    /**
     * Redirect after auth.
     * If ifValid is set to true it will redirect a logged in user.
     * @param $redirect
     * @param bool $ifValid
     * @return mixed
     */
    public static function checkAuthAndRedirect($redirect, $ifValid=false)
    {
        // Get the user information
        $user = Auth::user();
        $redirectTo = false;

        if(empty($user->id) && ! $ifValid) // Not logged in redirect, set session.
        {
            Session::put('loginRedirect', $redirect);
            $redirectTo = Redirect::to('user/login')
                ->with( 'notice', Lang::get('user/user.login_first') );
        }
        elseif(!empty($user->id) && $ifValid) // Valid user, we want to redirect.
        {
            $redirectTo = Redirect::to($redirect);
        }

        return array($user, $redirectTo);
    }

    public function currentUser()
    {
        return (new Confide(new ConfideEloquentRepository()))->user();
    }

    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {
        return $this->email;
    }

    /**
     * Returns user's mail by role id only.
     * @return array|bool
     */
    public static function getMailUsersCan($permission_name)
    {
        $permission = Permission::where('name', $permission_name)->first();
        $permissions_roles = DB::table('permission_role')->where('permission_id', $permission->id)->get();

        $mails = [];
        foreach ($permissions_roles as $permission_role) {
             if($role = Role::find($permission_role->role_id)){
                if($role->users()->count() > 0){
                    foreach ($role->users()->get() as $user) {
                        $mails[] = [$user->email => $user->data->firstname];
                    }   
                }
            }
        }

        if(!empty($mails))
            return $mails;    
        
        return false;
    }

    /**
     * Get the user Orders.
     *
     * @return Order
     */
    public function orders()
    {
        return $this->hasMany('Order');
    }

    /**
     * Get the user Point.
     *
     * @return Point
     */
    public function point()
    {
        return $this->hasMany('Point');
    }

    /**
     * Get the user Credit.
     *
     * @return Credit
     */
    public function credit()
    {
        return $this->hasMany('Credit');
    }

    /**
     * Get the user WithDraw.
     *
     * @return UserWithdraw
     */
    public function withdraw()
    {
        return $this->hasMany('UserWithdraw');
    }

    /**
     * Get the user MLM.
     *
     * @return UserMlm
     */
    public function mlm()
    {
        return $this->hasOne('UserMlm');
    }

    /**
     * Get the user Refer.
     *
     * @return UserRefer
     */
    public function refer()
    {
        return $this->hasOne('UserRefer');
    }

    /**
     * Get the user Data.
     *
     * @return userData
     */
    public function data()
    {
        return $this->hasOne('UserData');
    }

    /**
     * Get the bank.
     *
     * @return UserBank
     */
    public function bank()
    {
        return $this->hasOne('UserBank');
    }

    /**
     * Get the user Address.
     *
     * @return UserAddress
     */
    public function address()
    {
        return $this->hasOne('UserAddress');
    }

    public function isActiveInNetwork(){
        if($this->mlm){
            return $this->mlm->network_in;
        }
    }
}
