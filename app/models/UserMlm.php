<?php

class UserMlm extends \Eloquent {

	protected $table = 'users_mlm';
	public $timestamps = false;

	public static function getUserMlmArray($returnUserObj=false)
    {
		$users_mlm = UserMLm::select('users_mlm.user_id', 'parent', 'users_data.firstname as name', 'network.id', 'network.side as side')
						->leftJoin('users_data','users_data.user_id', '=', 'users_mlm.user_id')
		                ->leftJoin('network','network.user_id', '=', 'users_mlm.user_id')
		                ->groupBy('users_mlm.user_id')
		                ->where('network_in','=',1)
		                ->get();

		$arr = [];

		foreach ($users_mlm AS $row){
			$arr['natural'][$row->parent][] = $row->user_id;

			if (!empty($row->rede_id)){ }

			if($returnUserObj){
				$arr['natural_network'][$row->user_id] = $row->user;
			}

			$side = ($row->side==1) ? 'A' : 'B';

			$arr['name'][$row->user_id]			= strtoupper($row->name);
			$arr['name_network'][$row->user_id]	= strtoupper($row->name).' ('.$side.')';
			$arr['parent'][$row->user_id]		= $row->parent;
			$arr['side'][$row->user_id]			= $row->side;
        }

		return (object)$arr;

	}

	/**
	 * Define Plan, Position, Approve and PayBonus.
	 * @param User $user, $order
	 * @return Bollean
	 */
	public static function definePlanPositionApprovePayBonus(User $user, $order = null)
	{
		if($order)
        {
            if($order_plan = $order->order_plan()->first()){
                $user->mlm->network_plan_id = $order_plan->plan_id;
                $user->mlm->save();
            }
        }

		if($user->mlm->network_plan_id != null){
			//Position
			Network::position($user->id, $user->mlm->parent, $user->mlm->getSide());

			//Approve
            if($order){
				 if($user->mlm->approved_at < $order->created_at){
				 	UserMlm::approve($user->id);
				 }
			}else{
				UserMlm::approve($user->id);
			}

			$user = User::where('id', $user->id)->first();

            //PayBonus
            if($user->mlm->isActiveContract()){
                UserMlm::payBonus($user);
            }
        }

        $user = User::find($user->id);

        if($user->mlm->network_in && $user->mlm->isActiveContract() && $user->mlm->bonus){
        	if($order){
        		if($order->order_status_id != 6){
	            	$order->updateHistoryStatus(6);
        		}
	        }
	        return true;
        }
	}

	/**
	 * Upgrade the user.
	 *
	 * @return Bollean
	 */
	public static function upgrade(User $user, $plan_upgrade_id)
	{
		if($user->mlm->network_plan_id < $plan_upgrade_id){
			$old_plan = Plan::find($user->mlm->network_plan_id);
			$user->mlm->network_plan_id = $plan_upgrade_id;
			$user->mlm->bonus = 0;
			if($user->mlm->save()){
				$bonus = new Bonus;
				if($bonus->BonusIndicationWhenUpgrade($user, $old_plan))
					return true;
			}
		}

	}

	/**
	 * Approve the user.
	 *
	 * @return Bollean
	 */
	public static function approve($user_id, $contract_time=null)
	{
		if(!$contract_time)
				$contract_time = Setting::getSetting('contract_time', 'mlm');

		$user_mlm = UserMlm::where('user_id', '=', $user_id)->first();
		$days_to_reactive_contract = $user_mlm->toReactivateContract();

		if($days_to_reactive_contract >= $contract_time || $user_mlm->contracted_at == null){
			$user_mlm->contracted_at = date('Y-m-d H:i:s');
		}else{
			$rest_days = $contract_time - $days_to_reactive_contract;
			$date = new DateTime( 'now' );
			$user_mlm->contracted_at = $date->modify($rest_days.' Days')->format('Y-m-d H-i-s');
		}	

		$user_mlm->approved_at = date('Y-m-d H:i:s');

		/* Manager Qualify Verification */
		if($user_mlm->parentuser){
			$user_mlm->parentuser->mlm->verifyManagerQualify();
		}
		return $user_mlm->save();
	}

	/**
	 * Active the user.
	 *
	 * @return Bollean
	 */
	public static function active($user_id)
	{
		$user_mlm = UserMlm::where('user_id', '=', $user_id)->first();
		$days_to_reactive = $user_mlm->toReactiveMonthly($user_id);
		
		if($days_to_reactive >= 30){
			$user_mlm->approved_at = date('Y-m-d H:i:s');
		}else{
			$rest_days = 30 - $days_to_reactive;
			$date = new DateTime( 'now' );
			$user_mlm->approved_at = $date->modify($rest_days.' Days')->format('Y-m-d H-i-s');
		}
		
		return $user_mlm->save();
	}

	/**
	 * Pay Bonus the user.
	 *
	 * @return Bollean
	 */
	public static function payBonus(User $user)
	{
		if($user->mlm->bonus || !$user->mlm->network_plan_id) return false;

		$bonus = new Bonus;
		return $bonus->BonusIndication($user);
	}

	/**
	 * Add User in Opencart.
	 *
	 * @return Bollean
	 */
	public static function addUserInOpencart($user){
		$affiliate = array(
			'affiliate_id' => $user->id,
			'firstname' => $user->data->firstname,
			'salt' => DB::getPdo()->quote(substr(md5(uniqid(rand(), true)), 0, 9)),
			'code' => $user->username,
			'commission' => "0.00",
			'status' => 1,
			'approved' => 1,
			'date_added' => date('Y-m-d H:i:s')
		);
		if(DB::connection('shop')->table('affiliate')->where('affiliate_id', '=', $user->id)->count() > 0){
			DB::connection('shop')->table('affiliate')->where('affiliate_id', '=', $user->id)->update($affiliate);
		}else{
			DB::connection('shop')->table('affiliate')->insert($affiliate);
		}

		$customer = array(
			'username' => $user->username,
			'user_id' => $user->id,
			'firstname' => $user->data->firstname,
			'email' => $user->email,
			'password' => (isset($user->password_md5)) ? $user->password_md5 : $user->password,
			'telephone' => $user->data->telephone,
			'salt' => DB::getPdo()->quote(substr(md5(uniqid(rand(), true)), 0, 9)),
			'wishlist' => 'a:0:{}',
			'cart' => 'a:0:{}',
			'newsletter' => 1,
			'status' => 1,
			'approved' => 1,
			'customer_group_id' => 3,
			'date_added' => date('Y-m-d H:i:s'),
			'affiliate_id' => $user->id
		);
		if(DB::connection('shop')->table('customer')->where('email', '=', $user->email)->count() > 0){
			DB::connection('shop')->table('customer')->where('email', '=', $user->email)->update($customer);
		}else{
			DB::connection('shop')->table('customer')->insert($customer);
		}
	}

	/**
	 * Edit Password in Opencart.
	 *
	 * @return Bollean
	 */
	public static function editUserInOpencart($user){
		UserMlm::addUserInOpencart($user);
	}

	/**
	 * Verify if User is Active Monthly.
	 *
	 * @return Bollean
	 */
	public function isActiveMonthly($last_monthly=false)
	{
		return true;

		if($this->approved_at == null){
			return false;
		}

		$date = new DateTime( $this->approved_at );
		
		if($date->diff( new DateTime( 'now' ) )->days <= 30){
			return true;
		}

		return false;
	}

	/**
	 * Get Days to Reactive Monthly.
	 *
	 * @return Integer
	 */
	public function toReactiveMonthly()
	{
		$date = new DateTime( $this->approved_at );
		return $date->diff( new DateTime( 'now' ) )->days;
	}

	/**
	 * Active Monthly Need Pay.
	 *
	 * @return Integer
	 */
	public function activeMonthlyNeedPay()
	{
		return false;

	    $active_active = $this->isActiveMonthly();
	    
	    if($this->isActiveContract()){
		    if(!$active_active || ($active_active && $this->toReactiveMonthly() >= 25)){
		    		return true;
		    }
	    }
	}

	/**
	 * Get Active Monthly Value
	 *
	 * @return Double
	 */
	public static function getActiveMonthlyValue($user_id=null)
	{
		return Setting::getSetting('active_monthly', 'mlm');
	}

	/**
	 * Verify if User is Active Contract.
	 *
	 * @return Bollean
	 */
	public function isActiveContract()
	{
		if($this->contracted_at == null){
			return false;
		}

		$date = new DateTime( $this->contracted_at );	

		if($date->diff( new DateTime( 'now' ) )->days <= Setting::getSetting('contract_time', 'mlm')){
			return true;
		}
		
		return false;
	}

	/**
	 * Get Days to Reactive Contract.
	 *
	 * @return Integer
	 */
	public function toReactivateContract()
	{
		$date = new DateTime( $this->contracted_at );
		return $date->diff( new DateTime( 'now' ) )->days;
	}

	/**
	 * Contract Need Pay.
	 *
	 * @return Integer
	 */
	public function contractNeedPay()
	{
	    $contract_active = $this->isActiveContract();

	    if(!$contract_active || 
	    	($contract_active && $this->toReactivateContract() >= (Setting::getSetting('contract_time', 'mlm') - 5))
	    ){
    		return true;
    	}
	}

	/**
	 * Verify if User is Active
	 *
	 * @return Bollean
	 */
	public function isActive($last_monthly=false)
	{
		if($this->isActiveContract())
			return true;
		return false;
	}

	public function isFirstMonthly($last_monthly=false){
		if(!$this->first_buy_at) return false;

		$date_first_buy = new DateTime( $this->first_buy_at );
		$date_actual = new DateTime('now');

		if($last_monthly){
			$date_actual->modify("-1 month");
		}

		if($date_first_buy->format('Y-m') == $date_actual->format('Y-m')){
			return true;
		}

		return false;
	}

	public function isQualifyToPerformanceBonus(){
		if(!$this->first_buy_at) return false;

		if($this->isFirstMonthly(true)) return false;

		return true;
	}

	public function getPersonalVolume($last_monthly=false, $last_last_monthly=false){
		$date = new DateTime('now');

		if($last_last_monthly){
			$points = Cache::remember('personal_volume_last_last_monthly_points_'.$this->user_id, 60, function() use ($date){
						return Point::getTotalPointsByUser(
							$this->user_id, 
							$date->modify("-2 month")->format('Y-m-01 00:00:00'),
							$date->modify("+1 month")->format('Y-m-01 00:00:00')
						)->first();
					});
		}elseif($last_monthly){
			$points = Cache::remember('personal_volume_last_monthly_points_'.$this->user_id, 60, function() use ($date){
						return Point::getTotalPointsByUser(
							$this->user_id, 
							$date->modify("-1 month")->format('Y-m-01 00:00:00'),
							$date->modify("+1 month")->format('Y-m-01 00:00:00')
						)->first();
					});
		}else{
			$points = Cache::remember('personal_volume_points_'.$this->user_id, 60, function() use ($date){
						return Point::getTotalPointsByUser(
							$this->user_id, 
							$date->format('Y-m-01 00:00:00'),
							$date->modify("+1 month")->format('Y-m-01 00:00:00')
						)->first();
					});
		}
		
		if($points){
			if($points->value > 0){
				return $points->value;
			}
		}

		return 0;
	}

	public function getGroupVolume($depth=false, $last_monthly=false, $last_last_monthly=false){
		$group_volume = 0;
		
		foreach ($this->getDirects() as $userMlm) {
			if(!is_bool($depth)){
				if($depth > 0){
					$group_volume += $userMlm->getGroupVolume($depth - 1, $last_monthly, $last_last_monthly);
				}
			}else{
				$group_volume += $userMlm->getGroupVolume($depth, $last_monthly, $last_last_monthly);
			}
		}

		return $group_volume += $this->getPersonalVolume($last_monthly, $last_last_monthly);
	}

	public function getVG6IncreasePorcent($last_last_monthly=false){
		if($last_last_monthly){
			$volume_actual = $this->getGroupVolume(6, true);
			$volume_past   = $this->getGroupVolume(6, true, $last_last_monthly);
		}else{
			$volume_actual = $this->getGroupVolume(6);
			$volume_past   = $this->getGroupVolume(6, true);
		}

		if($volume_past <= 0){
			return 100;
		}
		return round((($volume_actual - $volume_past) / $volume_past) * 100, 2);
	}

	public function getConstructVolume(){
		return;
	}

	public function getActiveVolume(){
		return;
	}

	public function getGroupFidelityVolume($last_monthly=false){
		$fidelity_volume = 0;
		
		foreach ($this->getDirects() as $userMlm) {
			$fidelity_volume += $userMlm->getGroupFidelityVolume($last_monthly);
		}

		$points = 0;

		if(!$this->isFirstMonthly($last_monthly)){
			if($this->isActive($last_monthly)){
				$points = $this->getPersonalVolume($last_monthly) - 150;
			}
		}

		return $fidelity_volume += (($points < 0) ? 0 : $points);
	}

	public function getFidelityVolume($last_monthly=false){
		$points = 0;

		if(!$this->isFirstMonthly($last_monthly)){
			if($this->isActive($last_monthly)){
				$points = $this->getPersonalVolume($last_monthly) - 150;
			}
		}

		return ($points < 0) ? 0 : $points;
	}

	public function getActiveTeams($last_monthly=false){
		$active_teams = 0;

		foreach ($this->getDirects() as $userMlm) {
			if($userMlm->isActiveTeam($last_monthly)){
				$active_teams++;
			}
		}

		return $active_teams;
	}

	public function isActiveTeam($last_monthly=false){
		if($this->isActive($last_monthly)) return true;

		foreach ($this->getDirects() as $userMlm) {
			if($userMlm->isActiveTeam($last_monthly)){
				return true;
			}
		}

		return false;
	}

	public function getQualifyTeamsByTitleAndUser($last_monthly=false){
		$qualify_teams = [];

		foreach ($this->getDirects() as $userMlm) {

			$qualify_team = $userMlm->getQualifyTeamByTitle($last_monthly);

			krsort($qualify_team);

			$qualify_team_title = array_keys($qualify_team)[0];
			$title_id = ($last_monthly) ? $userMlm->getLastTitle() : $userMlm->network_title_id;

			if($title_id != $qualify_team_title){
				$userMlmQualify = $userMlm->getUserQualifyTeam($qualify_team_title, $last_monthly);
			}else{
				$userMlmQualify = $userMlm;
			}

			@$qualify_teams[$qualify_team_title][$userMlmQualify->user_id] = $userMlmQualify;

		}
		return $qualify_teams;
	}

	public function getUserQualifyTeam($search_title_id, $last_monthly=false){
		$directs = $this->getDirects();
		foreach ($directs as $userMlm) {
			$title_id = ($last_monthly) ? $userMlm->getLastTitle() : $userMlm->network_title_id;
			if($title_id == $search_title_id){
				return $userMlm;
			}
		}
		foreach ($directs as $userMlm) {
			if($return = $userMlm->getUserQualifyTeam($search_title_id, $last_monthly)){
				return $return;
			}
		}
	}

	public function getQualifyTeamsByTitle($last_monthly=false){
		$qualify_teams = [];

		foreach ($this->getDirects() as $userMlm) {
			$qualify_team = $userMlm->getQualifyTeamByTitle($last_monthly);
			krsort($qualify_team);
			foreach ($qualify_team as $key => $value) {
				@$qualify_teams[$key]++;
				break;
			}
		}

		return $qualify_teams;
	}

	public function getQualifyTeamByTitle($last_monthly=false){
		$qualify_team = [];

		foreach ($this->getDirects() as $userMlm) {
			foreach ($userMlm->getQualifyTeamByTitle($last_monthly) as $key => $value) {
				@$qualify_team[$key] += $value;
			}
		}

		$title_id = ($last_monthly) ? $this->getLastTitle() : $this->network_title_id;

		@$qualify_team[$title_id]++;

		return $qualify_team;
	}

	public function getQualifyTeams($last_monthly=false){
		$qualify_teams = 0;

		foreach ($this->getDirects() as $userMlm) {
			if($userMlm->isQualifyTeam($last_monthly)){
				$qualify_teams++;
			}
		}

		return $qualify_teams;
	}

	public function isQualifyTeam($last_monthly=false){
		if($last_monthly){
			$title_id = $this->getLastTitle();
		}else{
			$title_id = $this->network_title_id;
		}

		if($title_id >= 2) return true;

		foreach ($this->getDirects() as $userMlm) {
			if($userMlm->isQualifyTeam($last_monthly)){
				return true;
			}
		}

		return false;
	}

	public function getUsersInNetwotk($this_monthly=false){
		$total_network = 0;

		foreach ($this->getDirects($this_monthly) as $userMlm) {
			if($this_monthly){
				$date = new DateTime('now');
				$date_initial = new DateTime($date->format('Y-m-01 00:00:00'));
				$date_final = new DateTime($date->modify("+1 month")->format('Y-m-01 00:00:00'));

				if($date_initial <= $userMlm->created_at && $userMlm->created_at <= $date_final){
					$total_network++;
				}
			}else{
				$total_network++;
			}

			$total_network += $userMlm->getUsersInNetwotk($this_monthly);
		}

		return $total_network;
	}

	public function getUsersActiveInNetwotk(){
		$total_network = 0;

		foreach ($this->getDirects() as $userMlm) {
			if($userMlm->isActive()){
				$total_network++;
			}

			$total_network += $userMlm->getUsersActiveInNetwotk();
		}

		return $total_network;
	}

	public function getDirects($this_monthly=false){
		$usersMlm = UserMlm::where('parent', $this->user_id);
						
		if($this_monthly){
			$usersMlm->leftJoin('users', 'users.id', '=', 'users_mlm.user_id');
		}

		return $usersMlm->get();
	}

	public function getUnivelArray($first=true){
		$arr = null;

		foreach ($this->getDirects() as $userMlm) {
			$arr_r = $userMlm->getUnivelArray(false);
			if($first){
				$arr[$this->user_id][$userMlm->user_id] = $arr_r;
			}else{
				$arr[$userMlm->user_id] = $arr_r;
			}
		}

		return $arr;
	}

	public function getLastTitle($last_last_monthly=false){
		$date = new DateTime('now');

		if($last_last_monthly){
			$date_initial = $date->modify("-2 month")->format('Y-m-01 00:00:00');
			$date_final   = $date->modify("+1 month")->format('Y-m-01 00:00:00');
		}else{
			$date_initial = $date->modify("-1 month")->format('Y-m-01 00:00:00');
			$date_final   = $date->modify("+1 month")->format('Y-m-01 00:00:00');
		}

		$title_historic = MlmTitleHistoric::whereRaw(
				"created_at BETWEEN
					'".$date_initial."' AND '".$date_final."'"
			)->where('user_id', $this->user_id)->orderBy('title_id', 'DES')->get();

		if($title_historic = $title_historic->first()){
			$title_id = $title_historic->title_id;
		}else{
			$title_id = null;
		}

		return $title_id;
	}

	public function getTitleMax($last_monthly=false, $last_last_monthly=false){
		$date = new DateTime('now');

		if($last_last_monthly){
			$date = $date->modify("-1 month")->format('Y-m-01 00:00:00');
		}elseif($last_monthly){
			$date = $date->format('Y-m-01 00:00:00');
		}else{
			$date = $date->modify("+1 month")->format('Y-m-01 00:00:00');
		}

		$title_historic = MlmTitleHistoric::whereRaw("created_at < '{$date}'")
							->where('user_id', $this->user_id)
							->orderBy('title_id', 'DES')
							->get();

		if($title_historic = $title_historic->first()){
			$title_id = $title_historic->title_id;
		}else{
			$title_id = null;
		}

		return MlmTitle::find($title_id);
	}

	public function createHistoricTitle($network_title_id){
		if($network_title_id != null){
			$this->titleshistoric()->create(['title_id' => $network_title_id]);
		}
	}

	/**
	 *	Get total of users Actives
	 * @return Integer
	 */
	public static function usersActives()
	{
		$users_actives = 0;
		foreach(UserMlm::select('user_id')->get() as $userMlm){
			if($userMlm->isActive()){
				$users_actives++;
			}
		}
		return $users_actives;
	}

	/**
	 *	Get total of users Inactives
	 * @return Integer
	 */
	public static function usersInactives()
	{
		$users_inactives = 0;
		foreach(UserMlm::select('user_id')->get() as $userMlm){
			if(!$userMlm->isActive()){
				$users_inactives++;
			}
		}
		return $users_inactives;
	}

	/**
	 * Get Side Pattern Network.
	 *
	 * @return Integer
	 */
	public function getSide($user_id=null)
	{	
		if($this->parent == 0 || !UserMlm::where('user_id', '=', $this->parent)->first()->network_in){
			return 1;
		}

		if($this->side_network > 0){
			return $this->side_network;
		}elseif($this->parentuser->mlm->side_pattern_network == 0){
			$network = new Network(true);
			$less_side = $network->getLessSide($this->parent);
			if($less_side == 1 || $less_side == 2){
				return $less_side;
			}
			return (@$network->network_array->side[$this->parent] == 1) ? 2 : 1;
		}
		return $this->parentuser->mlm->side_pattern_network;
	}

	/**
	 * Verify if User is Binary Active.
	 *
	 * @return Bollean
	 */
	public function isActiveToBinary()
	{
		$contract_time = Setting::getSetting('contract_time', 'mlm');

		$side_a = UserMLm::select('user_id', 'network_plan_id')
            ->where('network_in', 1)
            ->where('parent', $this->user_id)
            ->whereRaw("DATEDIFF(NOW(), `contracted_at`) <= ".$contract_time)
            ->where('side_network', 1)
            ->where('network_plan_id','>=', 2)
            ->where('bonus', 1)
            ->first();

        $side_b = UserMLm::select('user_id', 'network_plan_id')
            ->where('network_in', 1)
            ->where('parent', $this->user_id)
            ->whereRaw("DATEDIFF(NOW(), `contracted_at`) <= ".$contract_time)
            ->where('side_network', 2)
            ->where('network_plan_id','>=', 2)
            ->where('bonus', 1)
            ->first();

    	if($side_a && $side_b){
    		$active_binary = 3;
    	}elseif($side_a){
    		$active_binary = 1;
    	}elseif($side_b){
    		$active_binary = 2;
    	}else{
    		$active_binary = 0;
    	}
    	
    	if($active_binary > $this->active_binary && $this->active_binary != null){ 
    		$userMlm = UserMlm::where('user_id', $this->user_id)->first();
    		$userMlm->active_binary = $active_binary;
    		$userMlm->save();

    		$points_b = $points_a = 0;
    		
    		// Equalize Side Left
    		if($active_binary == 1){
    			$points_a = $side_a->plan->points;

    		// Equalize Side Right
    		}elseif($active_binary == 2){
    			$points_b = $side_b->plan->points;

    		// Equalize Side Left When Active Binary with Side Left
    		}elseif($this->active_binary == 2 && $active_binary == 3){
    			$points_a = $side_a->plan->points;

    		// Equalize Side Right When Active Binary with Side Right
    		}elseif($this->active_binary == 1 && $active_binary == 3){
    			$points_b = $side_b->plan->points;
    		}else{
    			$points_a = $side_a->plan->points;
    			$points_b = $side_b->plan->points;
    		}

    		DB::table('network_balance')->insert(array(
    			'user_id' 	 => $this->user_id, 
				'value_a' 	 => $points_a, 
				'value_b' 	 => $points_b, 
				'created_at' => date('Y-m-d H:i:s')
			));
    	}

    	if($active_binary == 3){
    		return true;
    	}else{
    		return false;
    	}

	}

	/**
	 * User is Manager Qualify (Gerente Qualificado).
	 *
	 * @return Bollean
	 */
	public function isManagerQualify(){
		if($this->manager_qualify){
			return true;
		}
	}

	/**
	 * Verify if User is Manager Qualify (Gerente Qualificado).
	 *
	 * @return Bollean
	 */
	public function verifyManagerQualify(){
		if(!$this->manager_qualify){
			if($this->toReactivateContract() <= 60){
				if(UserMlm::where('parent', $this->user_id)->where('network_plan_id', '>=', '3')->count() >= 10){
					$this->manager_qualify = true;
					return $this->save();
				}
			}
		}
	}

	/**
	 * Verify if user Has Share In Week.
	 *
	 * @return Bollean
	 */
	public function hasSharedInWeek(){
		return true;
		$required_shares = Setting::getSetting('required_shares', 'mlm');
		if(FacebookPost::getShareThisWeekPostsCount($this->user_id) > $required_shares || true){
			return true;
		}
	}

	/**
	 * Get the user.
	 *
	 * @return User
	 */
	public function user()
	{
		return $this->belongsTo('User');
	}

	/**
	 * Get the plan.
	 *
	 * @return Plan
	 */
	public function plan()
	{
		return $this->hasOne('Plan', 'id', 'network_plan_id');
	}

	/**
	 * Get the title.
	 *
	 * @return MlmTitle
	 */
	public function title()
	{
		return $this->hasOne('MlmTitle', 'id', 'network_title_id');
	}

	/**
	 * Get the title.
	 *
	 * @return MlmTitle
	 */
	public function titleshistoric()
	{
		return $this->hasMany('MlmTitleHistoric', 'user_id', 'user_id');
	}

	/**
	 * Get the user parent.
	 *
	 * @return User parent
	 */
	public function parent_user()
	{
		return $this->hasOne('User', 'id', 'parent');
	}

	/**
	 * Get the user parent.
	 *
	 * @return User parent
	 */
	public function parentuser()
	{
		return $this->hasOne('User', 'id', 'parent');
	}
}