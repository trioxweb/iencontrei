<?php

class UserAddress extends Eloquent {

	protected $table = 'users_address';
	public $timestamps = false;

	/**
	 * Get the user.
	 *
	 * @return User
	 */
	public function user()
	{
		return $this->belongsTo('User');
	}
}