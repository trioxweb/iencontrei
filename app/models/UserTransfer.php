<?php

class UserTransfer extends \Eloquent {

	protected $table = 'users_transfers';
	protected $fillable = ['id', 'status', 'user_id', 'negative_credit_id', 'credit_id'];


	public static function approveTransfer(UserTransfer $transfer)
	{
		if($transfer->status) return false;

		$transfer->status = $transfer->negativecredit->status = $transfer->credit->status = 1;

		$transfer->negativecredit->description = str_replace('Pending', '', $transfer->negativecredit->description);

		if($transfer->negativecredit->save() && $transfer->credit->save() && $transfer->save()){

			return true;
		}
	}

	public static function unapproveTransfer(UserTransfer $transfer)
	{
		if($transfer->status) return false;

		$id = $transfer->id;

		$credit = $transfer->credit;
		$negativecredit = $transfer->negativecredit;

		$transfer->delete();
		$credit->delete();
		$negativecredit->delete();

		// Was the transfer deleted?
        $transfer = UserTransfer::find($id);
        if(empty($transfer)) return true;
	}

	public static function isPending($user_id=null)
	{
		if(!$user_id) $user_id = Auth::user()->id;

		$transfer = UserTransfer::where('user_id', '=', $user_id)->where('status', '!=', '1')->first();

		if($transfer) return true;

		return false;
	}

	/**
	 * Get the credit.
	 *
	 * @return Credit
	 */
	public function credit()
	{
		return $this->belongsTo('Credit', 'credit_id');
	}

	/**
	 * Get the negative credit.
	 *
	 * @return Credit
	 */
	public function negativecredit()
	{
		return $this->belongsTo('Credit', 'negative_credit_id');
	}

	/**
	 * Get the user.
	 *
	 * @return User
	 */
	public function user()
	{
		return $this->belongsTo('User');
	}


}