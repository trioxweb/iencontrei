<?php

class UserWithdraw extends \Eloquent {

	protected $table = 'users_withdraws';
	protected $fillable = ['value', 'payment_method', 'credit_id'];


	public static function markAsPaid(UserWithdraw $withdraw)
	{
		if($withdraw->status) return false;

		$withdraw->credit->description = 'Withdraw per '.$withdraw->payment_method;

		if($withdraw->credit->save()){
			$withdraw->status = 1;
			$withdraw->save();
			return true;
		}
	}

	public static function isPending($user_id=null)
	{
		if(!$user_id)
			$user_id = Auth::user()->id;

		$balance = UserWithdraw::where('user_id', '=', $user_id)
		->where('status', '!=', '1')
		->first();

		if($balance)
			return true;
		return false;
	}

	public static function getBalancePayable()
	{
		$balance = UserWithdraw::select(DB::raw('SUM(value) as balance'))
		->where('status', '=', '0')
		->first()
		->balance;

		return ($balance) ? $balance : 0;
	}

	public static function getBalanceTotalPay()
	{
		$balance = UserWithdraw::select(DB::raw('SUM(value) as balance'))
		->where('status', '=', '1')
		->first()
		->balance;

		return ($balance) ? $balance : 0;
	}

	/**
	 * Get the credit.
	 *
	 * @return Credit
	 */
	public function credit()
	{
		return $this->belongsTo('Credit');
	}

	/**
	 * Get the user.
	 *
	 * @return User
	 */
	public function user()
	{
		return $this->belongsTo('User');
	}


}