<?php

class UserData extends Eloquent {

	protected $table = 'users_data';
	public $timestamps = false;

	public function __construct()
    {
        parent::__construct();
    }

    public function getNameAttribute($value)
    {
        return $this->firstname.' '.$this->lastname;     
    }

    public function getAnydocumentAttribute($value)
    {
        if(is_array($document = unserialize($this->document))){
            return (isset($document['anydocument'])) ? $document['anydocument'] : null;
        }
    }

    public function getCpfAttribute($value)
    {
    	if(is_array($document = unserialize($this->document)))
        	return $document['cpf'];
    }

    public function getCnpjAttribute($value)
    {
    	if(is_array($document = unserialize($this->document)))
        	return $document['cnpj'];
    }

    public function getInsAttribute($value)
    {
    	if(is_array($document = unserialize($this->document)))
        	return $document['ins'];
    }

    public function getRazaosocialAttribute($value)
    {
    	if(is_array($document = unserialize($this->document)))
        	return $document['razaosocial'];
    }

    public function getTelephonesAttribute($value)
    {
        if(is_array($telephones = @unserialize($this->telephone))){
            if(!empty($telephones)){
                return $telephones;
            }else{
                $this->telephone = ''; 
            }
        }

        return [ 0 => [ 0 => $this->telephone, 1 => null]];
    }

	/**
	 * Get the user.
	 *
	 * @return User
	 */
	public function user()
	{
		return $this->belongsTo('User');
	}
}