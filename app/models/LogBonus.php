<?php

class LogBonus extends \Eloquent {
	protected $fillable = ['id', 'user_id', 'bonus_name', 'log', 'created_at', 'updated_at'];
	protected $table = 'log_bonuses';

	public function user()
	{
		return $this->belongsTo('User', 'id', 'user_id');
	}
}