<?php

class CreditAdditional extends \Eloquent {
	protected $fillable = ['id', 'status','description', 'value', 'user_id', 'created_at', 'updated_at'];
	protected $table = 'credits_additional';

	public static function getBalance($user_id=null)
	{
		if(!$user_id)
			$user_id = Auth::user()->id;

		$balance = CreditAdditional::select(DB::raw('SUM(value) as balance'))
		->where('user_id', '=', $user_id)
		->where('status', '=', '1')
		->first()
		->balance;

		return ($balance) ? $balance : 0;
	}

	public static function getTotalGains($user_id=null)
	{
		if(!$user_id)
			$user_id = Auth::user()->id;

		$balance = CreditAdditional::select(DB::raw('SUM(value) as balance'))
		->where('user_id', '=', $user_id)
		->where('status', '=', '1')
		->where('value', 'NOT LIKE', '%-%')
		->first()
		->balance;

		return ($balance) ? $balance : 0;
	}

	public static function getBalanceTotal()
	{
		$balance = CreditAdditional::select(DB::raw('SUM(value) as balance'))
		->where('status', '=', '1')
		->first()
		->balance;

		return ($balance) ? $balance : 0;
	}

	/**
	 * Get the user.
	 *
	 * @return User
	 */
	public function user()
	{
		return $this->belongsTo('User');
	}

	/**
	 * Get the credit type.
	 *
	 * @return CreditType
	 */
	public function type()
	{
		return $this->hasOne('CreditType', 'id', 'type_id');
	}

}