<?php

class OrderHistory extends \Eloquent {
	protected $fillable = ['order_status_id','notify','comment','created_at'];
	public $timestamps = false;
	protected $table = 'order_history';

	/**
	 * Get the order model.
	 *
	 * @return Order
	 */
	public function order()
	{
		return $this->belongsTo('Order', 'id', 'order_id');
	}

	/**
	 * Get the order status model.
	 *
	 * @return OrderStatus
	 */
	public function order_status()
	{
		return $this->belongsTo('OrderStatus', 'order_status_id', 'id');
	}
}