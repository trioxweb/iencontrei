<?php

class Order extends \Eloquent {
	protected $fillable = [
		'user_id', 'firstname', 'lastname', 'email', 'telephone', 'email',
	 	'payment_firstname', 'payment_lastname', 'payment_address_1', 'payment_address_2', 'payment_city', 'payment_postcode', 'payment_postcode', 'payment_zone', 'payment_method', 'payment_code', 
	 	'shipping_firstname', 'shipping_lastname', 'shipping_address_1', 'shipping_address_2', 'shipping_city', 'shipping_postcode', 'shipping_postcode', 'shipping_zone', 'shipping_method', 'shipping_code', 
		'order_status_id', 'currency_code', 'total', 'updated_at'
	];
	protected $table = 'order';

	/*
	 *
	 * Create order
	 * 
	 * @return Order
	 */
	public function createOrder(User $user, $currency_code, $payment_method_code)
	{
		
		$order = new Order();

		$order->user_id   = $user->id;
		$order->firstname = $user->data->firstname;
		$order->lastname  = $user->data->lastname;
		$order->email     = $user->email;
		$order->telephone = $user->data->telephone;
		$order->lastname  = $user->data->lastname;
		$order->email     = $user->email;

		$order->payment_firstname = $user->data->firstname;
		$order->payment_lastname  = $user->data->lastname;
		$order->payment_address_1 = $user->address->address;
		$order->payment_address_2 = $user->address->address_2;
		$order->payment_city  	  = $user->address->city;
		$order->payment_postcode  = $user->address->postcode;
		$order->payment_country   = $user->address->country;
		$order->payment_zone      = $user->address->zone;

		$payment_methods = Setting::getSetting('payment_methods');
		$order->payment_method    = $payment_methods[$payment_method_code]['name'];
		$order->payment_code      = $payment_method_code;

		$order->shipping_firstname = $user->data->firstname;
		$order->shipping_lastname  = $user->data->lastname;
		$order->shipping_address_1 = $user->address->address;
		$order->shipping_address_2 = $user->address->address_2;
		$order->shipping_city  	   = $user->address->city;
		$order->shipping_postcode  = $user->address->postcode;
		$order->shipping_country   = $user->address->country;
		$order->shipping_zone      = $user->address->zone;

		$order->shipping_method    = 'sem frete';
		$order->shipping_code      = 'no_shipping';

		$order->order_status_id = 0;
		$order->currency_code   = $currency_code;

		$order->total = 0;

		if($order->save()){
			return $order;
		}

		return false;
 	}

 	/*
	 * Save Total Order Table
	 * @return Order
	 */
 	public function createOrderTotal()
	{
		$this->order_total()->save(new OrderTotal(array(
				'code'	=> 'sub_total',
				'title' => 'Sub-total',
				'text'  => Currency::format($this->total, $this->currency_code),
				'value' => $this->total,
				'sort_order' => 1
		)));

		// Value of Shipping
		$this->total += $shipping_value = 0;

		$this->order_total()->save(new OrderTotal(array(
				'code'	=> 'shipping',
				'title' => 'Nenhum frete será cobrado.',
				'text'  => Currency::format($shipping_value, $this->currency_code),
				'value' => $shipping_value,
				'sort_order' => 2
		)));

		$this->order_total()->save(new OrderTotal(array(
				'code'	=> 'total',
				'title' => 'Total',
				'text'  => Currency::format($this->total, $this->currency_code),
				'value' => $this->total,
				'sort_order' => 5
		)));

		if($this->save()) return $this;
	}

	/*
	 * Update Order History Status
	 * @return Order
	 */
 	public function updateHistoryStatus($order_status_id = 1, $notify = 0)
	{
		$this->order_history()->save(new OrderHistory(array(
            'order_status_id' => $order_status_id,
            'notify'          => $notify,
            'created_at'      => date("Y-m-d H:i:s")
        )));
        $this->order_status_id = $order_status_id;
        return $this->save();
	}

 	/*
	 * Create order of Plan
	 * @param Plan $plan
	 * @return Order
	 */
	public function createOrderPlan(Plan $plan)
	{
		$this->total += $plan_value = $plan->value;	

		$this->order_plan()->save(new OrderPlan(array(
			'plan_id' => $plan->id,
			'name'    => $plan->name,
			'total'   => $plan_value,
		)));

		if($this->save()) return $this;
		
	}

	/*
	 * Create order of Plan Upgrade
	 * @param Plan $plan
	 * @return Order
	 */
	public function createOrderPlanUpgrade(Plan $plan)
	{
		$user = User::find($this->user_id);

		$this->total += $upgrade_value = $plan->getValuePlanUpgrade($user->mlm->plan, $plan);
		
		$this->order_plan_upgrade()->save(new OrderPlanUpgrade(array(
			'plan_upgrade_id' => $plan->id,
			'name'    => 'Upgrade to '.$plan->name,
			'total'   => $upgrade_value,
		)));

		if($this->save()) return $this;
		
	}

	/*
	 * Create order of Active Monthly
	 * @return Order
	 */
	public function createOrderActiveMonthly()
	{
		$this->total += $active_value = UserMlm::getActiveMonthlyValue($this->user_id);

		$this->order_active()->save(new OrderActive(array(
			'name'    => 'Active Monthly',
			'total'   => $active_value,
		)));

		if($this->save()) return $this;
		
	}

	/**
	 * Get the count products/plans.
	 *
	 * @return Integer
	 */
	public function countItens()
	{
		return 1;
	}

	/**
     * Get the date the user was created.
     *
     * @return string
     */
    public function date()
    {
        return Carbon::createFromFormat('Y-n-j G:i:s', $this->created_at)->format('j F Y');
    }

	/**
	 * Get the user.
	 *
	 * @return User
	 */
	public function user()
	{
		return $this->belongsTo('User');
	}

	/**
	 * Get the order status model.
	 *
	 * @return OrderStatus
	 */
	public function order_status()
	{
		return $this->belongsTo('OrderStatus', 'order_status_id', 'id');
	}

	/**
	 * Get the order total
	 *
	 * @return OrderTotal
	 */
	public function order_total()
	{
		return $this->hasMany('OrderTotal', 'order_id', 'id');
	}

	/**
	 * Get the order plan
	 *
	 * @return OrderPlan
	 */
	public function order_plan()
	{
		return $this->hasOne('OrderPlan', 'order_id', 'id');
	}

	/**
	 * Get the order plan
	 *
	 * @return OrderPlan
	 */
	public function plan()
	{
		return $this->hasOne('OrderPlan', 'order_id', 'id');
	}

	/**
	 * Get the order plan upgrade
	 *
	 * @return OrderPlanUpgrade
	 */
	public function order_plan_upgrade()
	{
		return $this->hasOne('OrderPlanUpgrade', 'order_id', 'id');
	}

	/**
	 * Get the order plan upgrade
	 *
	 * @return OrderPlanUpgrade
	 */
	public function upgrade()
	{
		return $this->hasOne('OrderPlanUpgrade', 'order_id', 'id');
	}

	/**
	 * Get the order active
	 *
	 * @return OrderActive
	 */
	public function order_active()
	{
		return $this->hasOne('OrderActive', 'order_id', 'id');
	}

	/**
	 * Get the order active
	 *
	 * @return OrderActive
	 */
	public function active()
	{
		return $this->hasOne('OrderActive', 'order_id', 'id');
	}

	/**
	 * Get the order history
	 *
	 * @return OrderHistory
	 */
	public function order_history()
	{
		return $this->hasMany('OrderHistory', 'order_id', 'id');
	}
}