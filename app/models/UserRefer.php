<?php

class UserRefer extends \Eloquent {

	protected $table = 'users_refers';

	public $timestamps = false;

	/**
	 * Get the user.
	 *
	 * @return User
	 */
	public function user()
	{
		return $this->belongsTo('User');
	}
}