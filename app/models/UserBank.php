<?php

class UserBank extends Eloquent {

	protected $table = 'users_bank';
	public $timestamps = false;

	/**
	 * Get the user.
	 *
	 * @return User
	 */
	public function user()
	{
		return $this->belongsTo('User');
	}
}