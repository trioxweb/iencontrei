<?php

class Payment {

	public static function createPayment(Order $order, User $user, $desc)
	{
		$payment = new Payment();
		if($order->payment_code == 'bank_transfer'){
			return $payment->createBankTransfer($order);
		}elseif($order->payment_code == 'pay_with_balance'){
			return $payment->createPayWithBalance($order);
		}else{
			return (object) array('result' => 0, 'messages' => 'Erro ao criar pedido! Tente novamente mais tarde.');
		}
	}

	public static function getPayment(Order $order)
	{
		if($order->order_status_id != 1){
			return '<p>Não é mais possivel realizar pagamento.</p>';
		}

		if($order->payment_code == 'bank_transfer'){
			return '
				<b>Banco:</b> Caixa Econômica <br>
				<b>Agência:</b> 4376 <br>
				<b>Conta Corrente:</b> 0379-5 <br>
				<b>Tipo de Conta:</b> Conta Corrente OP 003 <br>
				<b>Favorecido:</b> iEncontei Publicidade e Tecnologia <br>
				<b>CNPJ:</b> 16.679.863/0001-65 <br><br>

				<p style="margin-top:5px;"><span class="badge badge-roundless">Enviar o comprovante de pagamento com o nº do pedido para o email: financeiro@iencontrei.com.br</span></p>
			';
		}elseif($order->payment_code == 'pay_with_balance'){
			$finance_button = '';
			if(Setting::getSetting('has_financings') && $order->user_id != Auth::user()->id){
				$finance_button = '<a style="margin-bottom:10px;" href="'.URL::action('OfficeOrdersController@getFinancewithbalance', $order->id).'" class="btn btn-info">Clique aqui para Financiar com Saldo</a><br>';
			}
			return $finance_button.'<a href="'.URL::route('pay_with_balance', $order->id).'" class="btn btn-success">Clique aqui para Pagar com Saldo</a>';
		}
	}

	/*
	 *  @param Order $order
	 *  @return Array('result')
	 */
	private function createPayWithBalance(Order $order){

        $order->updateHistoryStatus(1);

       	$return['result'] = 1;
        return (object) $return;
	}

	/*
	 *  @param Order $order
	 *  @return Array('result')
	 */
	private function createBankTransfer(Order $order){

        $order->updateHistoryStatus(1);

       	$return['result'] = 1;
        return (object) $return;
	}
}