<?php

class Bonus extends \Eloquent {

	protected $table = 'bonuses';
	protected $fillable = ['bonus', 'run_on'];
	public $timestamps = false;

	public function ExecuteBonuses(){

		// Bonus 1 - Bonus de Indication / Patrocinio
		//$this->BonusIndication(true);
		//$this->BonusIndicationWhenUpgrade(true);
		
		// Bonus 2 - Binario
		//$this->BonusBinary(true);

		// Bonus 3 - Bonus Fixo
		// $this->BonusFixed(true);

		// Bonus 4 - Bonus Divisao de Lucro
		// $this->BonusDivideProfit(false);

	    // Bonus 5 - Carreira
	    //$this->BonusCarrerPlan(true);
	}

	public function BonusIndication(User $user)
	{
		$point = new Point;
		$point->value = $user->mlm->plan->points;
		if($point->value > 0){
    		$user->point()->save($point);
    		$this->recursiveBonusPatrocinio($user, $point->value);
    	}

		$user->mlm->bonus = 1;
		if($user->mlm->save()){
			if($user->mlm->parentuser){
				$user->mlm->parentuser->mlm->isActiveToBinary();
			}
			return true;
		}
	}

	public function BonusIndicationWhenUpgrade(User $user, Plan $old_plan)
	{
		$point = new Point;
		$point->value = $user->mlm->plan->points - $old_plan->points;
		if($point->value > 0){
    		$user->point()->save($point);
    		$this->recursiveBonusPatrocinio($user, $point->value);
    	}

		$user->mlm->bonus = 1;
		if($user->mlm->save()){
			$user->mlm->parentuser->mlm->isActiveToBinary();
			return true;
		}
	}

	private function recursiveBonusPatrocinio(User $user, $points){
		$bonus_indication_porc = Setting::getSetting('bonus_indication', 'mlm');

		$username = $user->username;
		$level = 1; $limit = 3;
		while ($level <= $limit) {
			if($user = $user->mlm->parentuser){
				if(isset($bonus_indication_porc[$user->mlm->network_plan_id][$level])){
					if(($value = $bonus_indication_porc[$user->mlm->network_plan_id][$level] * $points) > 0){
						$db_credit[] = array(
							'user_id' 	  => $user->id, 
							'value'   	  => $value, 
							'description' => '#1 Bônus de Patrocínio do Usuário: '.$username,
							'created_at'  => date('Y-m-d H:i:s'), 
						);
					}
					$level++;
				}
			}else{
				break;
			}
		}

		if(isset($db_credit)) DB::table('credits')->insert($db_credit);
	}

	public function BonusBinary($save=false)
	{
		$network   = new Network(true);

		$cap_by_plan = Setting::getSetting('cap', 'mlm');
		$plan_porc   = Setting::getSetting('plan_porc', 'mlm');

		foreach ($this->getUsersForBonuses() as $userMlm) {
			if($userMlm->isActiveToBinary() && $userMlm->network_plan_id >= 2){
				// Busca Pontos do User com Balanco
				$points = $network->getPointsInNetwork($userMlm->user_id);
				$balance = $points['lower_side'];

				if($userMlm->isManagerQualify()){
					$porc = 0.5;
					$cap  = $cap_by_plan[$userMlm->network_plan_id];
				}elseif($userMlm->network_plan_id > 0){
					$porc = $plan_porc[$userMlm->network_plan_id];
					$cap  = $cap_by_plan[$userMlm->network_plan_id];
				}else{
					$porc = 0;
					$cap  = 0;
				}

				$bonus = $balance * $porc;
				echo "$userMlm->user_id -> ".$balance." pontos | R$ $bonus | With Cap R$ ".(($bonus <= $cap) ? $bonus : $cap)." <br>";
				
				if($bonus > 0){
					$db_balance[] = array(
						'user_id' 	 => $userMlm->user_id, 
						'value_a' 	 => $balance, 
						'value_b' 	 => $balance, 
						'created_at' => date('Y-m-d H:i:s')
					);

					$db_credit[] = array(
						'user_id' 	  => $userMlm->user_id, 
						'value'   	  => ($bonus <= $cap) ? $bonus : $cap, 
						'description' => '#2 Bônus Binário', 
						'created_at'  => date('Y-m-d H:i:s'),
					);

					$db_log[] = [
						'user_id' 	  => $userMlm->user_id, 
						'bonus_name'  => 'Bônus Binário', 
						'log' 		  => "{$balance} pontos | R$ ".($bonus <= $cap) ? $bonus : $cap, 
						'created_at'  => date('Y-m-d H:i:s'),
						'updated_at'  => date('Y-m-d H:i:s'),
					];
				}
			}
		}

		if(isset($db_log) && $save)
			DB::table('log_bonuses')->insert($db_log);
		if(isset($db_credit) && $save) 
			DB::table('credits')->insert($db_credit);
		if(isset($db_balance) && $save) 
			DB::table('network_balance')->insert($db_balance);
	}

	public function BonusFixed($save=false){
		$bonus_by_plan = Setting::getSetting('bonus_fixed', 'mlm');

		foreach ($this->getUsersWithPlanForBonuses() as $userMlm) {
			$date = new DateTime( $userMlm->contracted_at );
			$date_diff_days = $date->diff( new DateTime( 'now' ) )->days;
			if($userMlm->isActive() && $userMlm->network_plan_id >= 3 
				&& $date_diff_days <= 364
				&& $date_diff_days > 30
				&& ($date_diff_days)%7 == 0
				&& isset($bonus_by_plan[$userMlm->network_plan_id])
				&& $date->modify('+1 month')->format('Y') == date('Y')
			){
				if($bonus_by_plan[$userMlm->network_plan_id] > 0){
					if($userMlm->hasSharedInWeek()){
						$db_credit_additional[] = array(
							'user_id' 	  => $userMlm->user_id, 
							'value'   	  => $bonus_by_plan[$userMlm->network_plan_id], 
							'description' => '#3 Bônus Fixo', 
							'created_at'  => date('Y-m-d H:i:s'),
						);

						$log = "$userMlm->user_id -> R$ ".$bonus_by_plan[$userMlm->network_plan_id];
						echo $log." <br>";
					}
				}
			}
		}

		if(isset($db_credit_additional) && $save) 
			DB::table('credits_additional')->insert($db_credit_additional);
	}

	public function BonusDivideProfit($save=false){
		$users_by_plan = [4 => [], 5 => []];
		$users_by_title = [1 => [], 2 => [], 3 => [], 4 => []];

		foreach ($this->getUsersWithPlanForBonuses() as $userMlm) {
			if($userMlm->isActive()){
				if($userMlm->network_title_id > 0){
					@$users_by_title[$userMlm->network_title_id][] = $userMlm;
				}
				if($userMlm->network_plan_id >= 4){
					@$users_by_plan[$userMlm->network_plan_id][] = $userMlm;
				}
			}
		}

		$date = new DateTime('now');
		$points = Point::select(DB::raw('sum(`value`) as `value`'))
					->where('status','=',1)
					->whereRaw("created_at BETWEEN '".$date->modify("-1 month")->format('Y-m-01 00:00:00')."' AND '".$date->modify("+1 month")->format('Y-m-01 00:00:00')."'")
					->first();

		if(!$points->value){
		 	$points->value = 0;
		}

		@$bonus_to_plan[4]  = ($points->value * 0.04)  / ((($c = count($users_by_plan[4])) > 0)  ? $c : 1);
		@$bonus_to_plan[5]  = ($points->value * 0.03)  / ((($c = count($users_by_plan[5])) > 0)  ? $c : 1);
		@$bonus_to_title[1] = ($points->value * 0.015) / ((($c = count($users_by_title[1])) > 0) ? $c : 1);
		@$bonus_to_title[2] = ($points->value * 0.02)  / ((($c = count($users_by_title[2])) > 0) ? $c : 1);
		@$bonus_to_title[3] = ($points->value * 0.025) / ((($c = count($users_by_title[3])) > 0) ? $c : 1);
		@$bonus_to_title[4] = ($points->value * 0.02)  / ((($c = count($users_by_title[4])) > 0) ? $c : 1);

		foreach ($users_by_plan as $plan_id => $usersMlm) {
			if($bonus_to_plan[$plan_id] > 0){
				foreach ($usersMlm as $userMlm) {
					$db_credit[] = array(
						'user_id' 	  => $userMlm->user_id, 
						'value'   	  => $bonus_to_plan[$plan_id], 
						'description' => '#4 Bonus Divisão de Lucro', 
						'created_at'  => date('Y-m-d H:i:s'),
					);

					$log = "$userMlm->user_id -> R$ ".$bonus_to_plan[$plan_id]." | By Plan";
					echo $log." <br>";
				}
			}
		}

		foreach ($users_by_title as $title_id => $usersMlm) {
			if($bonus_to_title[$title_id] > 0){
				foreach ($usersMlm as $userMlm) {
					$db_credit[] = array(
						'user_id' 	  => $userMlm->user_id, 
						'value'   	  => $bonus_to_title[$title_id], 
						'description' => '#4 Bonus Divisão de Lucro', 
						'created_at'  => date('Y-m-d H:i:s'),
					);

					$log = "$userMlm->user_id -> R$ ".$bonus_to_title[$title_id]." | By Title";
					echo $log." <br>";
				}
			}
		}

		if(isset($db_credit) && $save) 
			DB::table('credits')->insert($db_credit);
	}

	public function BonusCarrerPlan($save=false)
	{
		$network = new Network(true);

		$carrer_plans = Setting::getSetting('carrer_plans', 'mlm');

		$this->admins_mails = User::getMailUsersCan('manage_users');

		foreach ($this->getUsersForBonuses() as $userMlm) {
			$points = $network->getPointsInNetwork($userMlm->user_id);
			$points_of_lower_side = $points['lower_side_balance'];

			$network_title_id = null;
			foreach ($carrer_plans as $title_id => $necessary_points) {
				if($points_of_lower_side >= $necessary_points)
					$network_title_id = $title_id;
			}

			if($network_title_id != null && $network_title_id > $userMlm->network_title_id){
				if($save){
					$userMlm->user->mlm->network_title_id = $network_title_id;
					if($userMlm->user->mlm->save()){
						$userMlm->createHistoricTitle($network_title_id);
						$this->user = $userMlm->user;
						try{
							// Send Mail to User
				            Mail::send('emails.titles.title_achieved_to_admin', ['user' => $this->user], function($message){
				                $message->subject('Parabens! Você Alcançou um Novo Titulo!');
				                $message->to($this->user->email);
				            });

				            // Send Mail to Users Admins
			                Mail::send('emails.titles.title_achieved_to_admin', array('user' => $this->user), function($message){
			                    $message->subject('Novo Titulo Conquistado');
			                    if($mails = $this->admins_mails){
			                        $message->to($mails[0]);
			                        unset($mails[0]);
			                    	foreach ($mails as $mail) {
			                            $message->cc($mail);
			                        }
			                    }
			                });
			            }catch(Exception $exception){
			                echo $exception->getMessage();
			            }
					}
				}
				echo "$userMlm->user_id -> $points_of_lower_side pontos | $network_title_id <br>";
			}
		}
	}

	private function getUsersForBonuses()
	{
		return UserMlm::select('users_mlm.user_id', 'parent', 'users_data.firstname as name', 'network.id', 'network.side as side', 'users_mlm.network_title_id', 'users_mlm.network_plan_id', 'approved_at', 'contracted_at', 'manager_qualify')
			->leftJoin('users_data','users_data.user_id', '=', 'users_mlm.user_id')
            ->leftJoin('network','network.user_id', '=', 'users_mlm.user_id')
            ->groupBy('users_mlm.user_id')
            ->where('network_in','=',1)
            ->get();
	}

	private function getUsersWithPlanForBonuses()
	{
		return UserMlm::select('users_mlm.user_id', 'parent', 'users_data.firstname as name', 'network.id', 'network.side as side', 'users_mlm.network_title_id', 'users_mlm.network_plan_id', 'users_mlm.active_binary', 'approved_at', 'contracted_at', 'manager_qualify')
			->leftJoin('users_data','users_data.user_id', '=', 'users_mlm.user_id')
            ->leftJoin('network','network.user_id', '=', 'users_mlm.user_id')
            ->groupBy('users_mlm.user_id')
            ->where('network_plan_id', '>=', 2)
            ->where('network_in', '=', 1)
            ->get();
	}

	private function getUsersDirects($parent_user_id)
	{
		return UserMlm::select('users_mlm.user_id', 'parent', 'users_data.firstname as firstname', 'network.id', 'network.side as side', 'users_mlm.network_title_id', 'users_mlm.network_plan_id', 'users_mlm.active_binary', 'approved_at', 'contracted_at', 'manager_qualify')
			->leftJoin('users_data','users_data.user_id', '=', 'users_mlm.user_id')
            ->leftJoin('network','network.user_id', '=', 'users_mlm.user_id')
            ->groupBy('users_mlm.user_id')
            ->where('network_in','=',1)
            ->where('parent','=', $parent_user_id)
            ->get();
	}
}