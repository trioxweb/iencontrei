<?php

class Network extends \Eloquent {

	protected $table = 'network';

	public $network_array;

    public function __construct($construct=false)
    {
        parent::__construct();
        if($construct){
        	$this->network_array = $this->getNetworkArray();
        }
    }

    public function multi_array_keys($ar)
	{
		foreach($ar as $k => $v){
            $keys[] = $k;
			if (is_array($ar[$k])){
				$keys = array_merge($keys, $this->multi_array_keys($ar[$k]));
			}
		}
        return $keys;
    }

	public static function position($user_id, $parent_user_id, $side)
	{
		if($parent_user_id == 0 && Network::where('parent_user_id', '=', 0)->first()) return false;
		if($parent_user_id != 0)
			if(!UserMlm::where('user_id', '=', $parent_user_id)->first()->network_in) return false;
		if(UserMlm::where('user_id', '=', $user_id)->first()->network_in) return false;

		$network = new Network;
	    $network->user_id = $user_id;
	    $network->parent_user_id = $network->overflow($parent_user_id, $side);
	    $network->side = $side;
	    if($network->save()){
	    	$network->user->mlm->network_in = 1;
	    	$network->user->mlm->side_network = $side;
        	$network->user->mlm->save();

        	if(Setting::getSetting('has_shop')){
                UserMlm::addUserInOpencart($network->user);
            }

            try{
	            // Send Mail to User
	            Mail::send('emails.status.registers.register_positioned_to_user', ['user' => $network->user], function($message) use ($network){
	                $message->subject('Você foi Posicionado na Rede');
	                $message->to($network->user->email);
	            });   

	            // Send Mail to Parent
	            $parent = $network->user->mlm->parentuser;
	            if(isset($parent->email)){
	                Mail::send('emails.status.registers.new_register_to_sponsor', ['user' => $network->user], function($message) use ($parent){
	                    $message->subject('Novo Cadastro em Sua Rede');
	                    $message->to($parent->email);
	                });
	            }
	        }catch(Exception $exception){
                Session::flash('warning', $exception->getMessage());
            }
        	
	    	return true;
	    }
	}

	/**
	 * Get the user.
	 *
	 * @return User
	 */
	public function user()
	{
		return $this->belongsTo('User');
	}

	/**
	 * Get the parent network.
	 *
	 * @return Network
	 */
	public function parent_network()
	{
		return $this->hasOne('Network', 'user_id', 'parent_user_id');
	}

	/**
	 * 1) Selecionar os diretos do ID
	 * 2) Verificar se o ID ja indicou uma pessoa
	 * 3) Se ainda n�o indicou ninguem o lado do transbordo � o mesmo lado de cadastro do associado
	 * 4) Se j� indicou o lado do transbordo � o lado escolhido pelo associado
	 * 5) Selecionar todos os IDs do mesmo lado de transbordo
	 * 6) Montar a network com os IDs selecionados
	 * 7) Retornar apenas o ultimo ID que ser� o ID do Transbordamento
	 */
	public function overflow($parent_user_id, $side)
	{
		return $this->getLastUserInNetwork($parent_user_id, $this->getNetworkInSide($side));
	}

	public static function getDirects($id)
	{
		$users = UserMlm::where('parent','=',$id)->get();
		$arr = [];
		foreach ($users as $row)
		{
			$arr[] = $row->user;
		}
		return $arr;
	}

	function getNetworkInSide($side)
	{
		$network = Network::select('user_id','parent_user_id')
		            ->where('side','=',$side)->get();
	              
	    $arr = [];
		foreach ($network as $row)
		{
			$arr[$row->parent_user_id][] = $row->user_id;
		}
		return $arr;
	}

	function getLastUserInNetwork($parent_user_id, $network_in_side)
	{
		if (isset($network_in_side[$parent_user_id]))
		{
			foreach ($network_in_side[$parent_user_id] AS $next_user_id)
			{
				$last_user_id = $next_user_id;
				if (isset($network_in_side[$next_user_id]))
				{
					$last_user_id = $this->getLastUserInNetwork($next_user_id, $network_in_side);
				}
			}
			return $last_user_id;
		}
		return $parent_user_id;
	}

	private function getNetworkArray()
	{	
		$network = Network::select('user_id', 'parent_user_id', 'side')
		            ->groupBy('user_id')->where('user_id','!=','parent_user_id')->get();
		
		if ($network->count() > 0){
			foreach ($network AS $row)
			{
				$arr['list'][$row->parent_user_id][$row->side] = $row->user_id;
				$arr['side'][$row->user_id] = $row->side;
			}
			return (object)$arr;
		}
	}

	private $binary_tree;

	public function getBinaryTree($id=0, $limit=4, $level=1)
	{

		$network = $this->network_array->list;
		$name = UserMlm::getUserMlmArray()->name;

		if (isset($network[$id]))
		{
			ksort($network[$id]);
			if (isset($network[$id][1]) AND !isset($network[$id][2]))
			{
				$this->binary_tree[$level][$id][1]['id'] = $network[$id][1];
				$this->binary_tree[$level][$id][1]['name'] = $name[$network[$id][1]];
				$this->binary_tree[$level][$id][2] = '';

				if ($limit==0 OR $limit>0 AND $level<$limit)
					$this->getBinaryTree($network[$id][1], $limit, $level+1);
			}
			elseif ( ! isset($network[$id][1]) AND isset($network[$id][2]))
			{
				$this->binary_tree[$level][$id][1] = '';
				$this->binary_tree[$level][$id][2]['id'] = $network[$id][2];
				$this->binary_tree[$level][$id][2]['name'] = $name[$network[$id][2]];

				if ($limit==0 OR $limit>0 AND $level<$limit)
					$this->getBinaryTree($network[$id][2], $limit, $level+1);
			}
			else
			{
				foreach ($network[$id] AS $key=>$value)
				{
					$this->binary_tree[$level][$id][$key]['id'] = $value;
					$this->binary_tree[$level][$id][$key]['name'] = $name[$value];
                    
					if (isset($network[$value]) AND $limit==0 OR
						isset($network[$value]) AND $limit>0 AND $level<$limit)
					{
						$this->getBinaryTree($value, $limit, $level+1);
					}
				}
			}
			return $this->binary_tree;
		}
	}

	public function getTreeArray($id=0,$limit=0,$level=1)
	{
		$network = $this->network_array->list;

		if (isset($network[$id]))
		{
			foreach ($network[$id] AS $key=>$value)
			{
				$list[$value] = $value;
				if (isset($network[$value]) AND $limit==0 OR
					isset($network[$value]) AND $limit>0 AND $level<$limit)
				{
					$list[$value] = $this->getTreeArray($value, $limit, $level+1);
				}
			}
			return $list;
		}
	}

	function getPointsInNetwork($id)
	{
		$network = $this->network_array->list;

		$points = Point::select('user_id as id',(DB::raw('sum(`value`) as `value`')))
		                    ->groupBy('user_id')->where('status','=',1)->get();
		
		$total_points = array();
		if ($points->count() > 0){
			foreach ($points as $row){
				@$total_points[$row->id] += $row->value;
			}
		}
		
        $balance_a = NetworkBalance::select('user_id as id',(DB::raw('sum(`value_a`) as `value`')))
		                    ->groupBy('user_id')->where('user_id','=',$id)->get();
		$balance_b = NetworkBalance::select('user_id as id',(DB::raw('sum(`value_b`) as `value`')))
		                    ->groupBy('user_id')->where('user_id','=',$id)->get();
		
		$total_balance_a = 0;
		if ($balance_a->count() > 0){
			$total_balance_a = $balance_a[0]['value'];
		}

		$total_balance_b = 0;
		if ($balance_b->count() > 0){
			$total_balance_b = $balance_b[0]['value'];
		}
		
		if (isset($network[$id][1]) AND ! empty($network[$id][1])){
			$network_a = $this->getTreeArray($id);
			if (isset($network[$id][2]) AND ! empty($network[$id][2])) unset($network_a[$network[$id][2]]);
			$side_a = $this->getTotalPointsPerNetwork($network_a,$total_points);
		}else{
			$side_a = 0;
		}

		if (isset($network[$id][2]) AND ! empty($network[$id][2])){
			$network_b = $this->getTreeArray($id, $network);
            if(isset($network[$id][1]) AND ! empty($network[$id][1])) unset($network_b[$network[$id][1]]);
            $side_b = $this->getTotalPointsPerNetwork($network_b,$total_points);
		}else{
			$side_b = 0;
		}

		$balance_a = $total_balance_a;
		$balance_b = $total_balance_b;
		return array(
			'a' 						 => $side_a - $balance_a,
			'b' 						 => $side_b - $balance_b,
			'a_without_balance' 		 => $side_a,
			'b_without_balance' 		 => $side_b,
			'lower_side'				 => (($side_a - $balance_a) < ($side_b - $balance_b)) ? $side_a  - $balance_a : $side_b  - $balance_b,
			'lower_side_without_balance' => ($side_a < $side_b) ? $side_a : $side_b,
			'larger_side'				 => (($side_a - $balance_a) > ($side_b - $balance_b)) ? $side_a  - $balance_a : $side_b  - $balance_b,
			'larger_side_without_balance' => ($side_a > $side_b) ? $side_a : $side_b,
			'lower_side_name'			  => (($side_a - $balance_a) < ($side_b - $balance_b)) ? 'a' : 'b',
			'lower_side_balance'		 => ($balance_a < $balance_b) ? $balance_a : $balance_b,
		);
	}

	function getTotalPointsPerNetwork($network, $pontos)
	{
		if (is_array($network))
		    $network = $this->multi_array_keys($network);

		$total = 0;
		if (is_array($network))
		{
			foreach ($network as $id)
			{
				if (isset($pontos[$id]))
				{
					$total += $pontos[$id];
				}
			}
		}
		return floor($total);
	}

	public function getLessSide($id){
		$network = $this->network_array->list;
		$network_a = [];
		$network_b = [];
		if (isset($network[$id][1]) AND ! empty($network[$id][1])){
			$network_a = $this->getTreeArray($network[$id][1]);
		}
		if (isset($network[$id][2]) AND ! empty($network[$id][2])){
			$network_b = $this->getTreeArray($network[$id][2]);
		}
		if(sizeof($network_a, 1) < sizeof($network_b, 1)){
			return 1;
		}elseif(sizeof($network_b, 1) < sizeof($network_a, 1)){
			return 2;
		}
		return false;
	}

	public function isOfNetwork($id_network, $id){
		$network = $this->network_array->list;
		$tree_array = $this->getTreeArray($id_network, $network);
		if(is_array($tree_array)){
			$network_array = array_flip($this->multi_array_keys($tree_array));
			if(isset($network_array[$id]))
				return true;
		}
	}
}