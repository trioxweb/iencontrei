<?php

class MlmTitleHistoric extends \Eloquent {
	protected $fillable = ['id', 'user_id', 'title_id', 'created_at', 'updated_at'];
	protected $table = 'mlm_titles_historic';

	public function usermlm()
	{
		return $this->belongsTo('UserMlm', 'user_id', 'user_id');
	}
}