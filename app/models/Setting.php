<?php

class Setting extends \Eloquent {

    public $timestamps = false;
    
	/**
     * Get setting value
     * @param $key, $group
     * @return mixed
     */
    public static function getSetting($key, $group='config')
    {
        return Cache::remember('get_setting_'.$key.'_'.$group, 60, function() use ($key, $group){
        	if($result = Setting::where('key', '=', $key)->where('group', '=', $group)->first()){
        		if($result->serialized == 1){
        			$result = unserialize($result->value);
        		}else{
        			$result = $result->value;
        		}
        	}
            return $result;
        });
    }
}