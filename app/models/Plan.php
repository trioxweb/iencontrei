<?php

class Plan extends \Eloquent {
	protected $fillable = ['id', 'name', 'value', 'points', 'order'];
	public $timestamps = false;
	protected $orderby = 'order';

	public function __construct()
    {
        parent::__construct();
    }

    /**
	 * Get the calc of Upgrade Value.
	 *	
	 * @param Plan $plan_actual Plan Actual Class
	 * @param Plan $plan_upgrade Plan Upgrade Class
	 * @return Double
	 */
    public function getValuePlanUpgrade(Plan $plan_actual, Plan $plan_upgrade){
    	if($plan_actual->id == 1){
    		return $plan_upgrade->value;
    	}
    	return $plan_upgrade->value - $plan_actual->value;
    }
}