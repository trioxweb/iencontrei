<?php

class OrderActive extends \Eloquent {
	protected $fillable = ['id', 'order_id', 'name', 'total'];
	public $timestamps = false;
	protected $table = 'order_active';

	/**
	 * Get the order model.
	 *
	 * @return Order
	 */
	public function order()
	{
		return $this->belongsTo('Order', 'id', 'order_id');
	}
}