<?php

class UserController extends BaseController {

    /**
     * User Model
     * @var User
     */
    protected $user;

    /**
     * Inject the models.
     * @param User $user
     */
    public function __construct(User $user)
    {
        parent::__construct();
        $this->user = $user;
    }

    /**
     * Displays the form for user creation
     *
     */
    public function getCreate()
    {
        //return View::make('site/user/create');
        return Redirect::to('register');
    }


    /**
     * Displays the login form
     *
     */
    public function getLogin()
    {
        $user = Auth::user();
        if(!empty($user->id)){
            return Redirect::to('/');
        }

        return View::make('login/login');
    }

    /**
     * Attempt to do login
     *
     */
    public function postLogin()
    {
        $input = array(
            'email'    => Input::get( 'email' ), // May be the username too
            'username' => Input::get( 'email' ), // May be the username too
            'password' => Input::get( 'password' ),
            'remember' => Input::get( 'remember' ),
        );

        // If you wish to only allow login from confirmed users, call logAttempt
        // with the second parameter as true.
        // logAttempt will check if the 'email' perhaps is the username.
        // Check that the user is confirmed.
        if ( Confide::logAttempt( $input, false ) )
        {   

            $user = Auth::user();
            if(md5(Input::get( 'password' )) != $user->password_md5){
                $user->password_md5 = md5(Input::get( 'password' ));
                $user->save();
                UserMlm::editUserInOpencart($user);
            }
            
            return Redirect::intended('/office');
        }
        else
        {
            // Check if there was too many login attempts
            if ( Confide::isThrottled( $input ) ) {
                $err_msg = Lang::get('confide::confide.alerts.too_many_attempts');
            } elseif ( $this->user->checkUserExists( $input ) && ! $this->user->isConfirmed( $input ) ) {
                $err_msg = Lang::get('confide::confide.alerts.not_confirmed');
            } else {
                $err_msg = Lang::get('confide::confide.alerts.wrong_credentials');
            }

            return Redirect::to('user/login')
                ->withInput(Input::except('password'))
                ->with( 'error', $err_msg );
        }
    }

    /**
     * Attempt to confirm account with code
     *
     * @param  string  $code
     */
    public function getConfirm( $code )
    {
        if ( Confide::confirm( $code ) )
        {
            return Redirect::to('user/login')
                ->with( 'notice', Lang::get('confide::confide.alerts.confirmation') );
        }
        else
        {
            return Redirect::to('user/login')
                ->with( 'error', Lang::get('confide::confide.alerts.wrong_confirmation') );
        }
    }

    /**
     * Displays the forgot password form
     *
     */
    public function getForgot()
    {
        return View::make('login/forgot');
    }

    /**
     * Attempt to reset password with given email
     *
     */
    public function postForgot()
    {
        if( Confide::forgotPassword( Input::get( 'email' ) ) )
        {
            return Redirect::to('user/login')
                ->with( 'notice', Lang::get('confide::confide.alerts.password_forgot') );
        }
        else
        {
            return Redirect::to('user/forgot')
                ->withInput()
                ->with( 'error', Lang::get('confide::confide.alerts.wrong_password_forgot') );
        }
    }

    /**
     * Shows the change password form with the given token
     *
     */
    public function getReset( $token )
    {

        return View::make('login/reset')
            ->with('token',$token);
    }


    /**
     * Attempt change password of the user
     *
     */
    public function postReset()
    {
        $input = array(
            'token'=>Input::get( 'token' ),
            'password'=>Input::get( 'password' ),
            'password_confirmation'=>Input::get( 'password_confirmation' ),
        );

        // By passing an array with the token, password and confirmation
        if( Confide::resetPassword( $input ) )
        {
            return Redirect::to('user/login')
            ->with( 'notice', Lang::get('confide::confide.alerts.password_reset') );
        }
        else
        {
            return Redirect::to('user/reset/'.$input['token'])
                ->withInput()
                ->with( 'error', Lang::get('confide::confide.alerts.wrong_password_reset') );
        }
    }


    /**
     * Recover Pin User
     *
     * @return Redirect and Message of Success
     */
    public function getPinrecover()
    {   
        $this->user_auth = Auth::user();
        if($this->user_auth){
            if(!$this->user_auth->remember_token){
                $this->user_auth->remember_token = md5( uniqid(mt_rand(), true) );
                $this->user_auth->save();
            }

            Mail::send('emails.auth.pin_recover', array('user' => $this->user_auth, 'token' => $this->user_auth->remember_token), function($message){
                $message->to($this->user_auth->email, $this->user_auth->data->firstname)->subject('Recuperar/Remover PIN '.Setting::getSetting('name'));
            });

            return Redirect::back()->with('success', 'Foi enviado um link de confirmação para seu email '.$this->user_auth->email.'.');
        }
        return Redirect::back()->with('error', 'Não Foi Possivel Recuperar PIN. Tente novamente mais tarde.');
    }

    /**
     * Recover Pin User
     *
     */
    public function getPinreset( $token )
    {
        $user = User::where('remember_token', '=', $token)->first();
        if($user){
            $user->pin = null;
            $user->remember_token = null;
            if($user->save()){
                return Redirect::action('OfficeUserController@getIndex')->with('success', 'Seu PIN foi removido/resetado com Sucesso.');
            }
        }
        return Redirect::action('OfficeUserController@getIndex')->with('error', 'Não Foi Possivel Recuperar PIN.');
    }



    /**
     * Log the user out of the application.
     *
     */
    public function getLogout()
    {
        Confide::logout();

        return Redirect::to('/');
    }

    /**
     * Process a dumb redirect.
     * @param $url1
     * @param $url2
     * @param $url3
     * @return string
     */
    public function processRedirect($url1,$url2,$url3)
    {
        $redirect = '';
        if( ! empty( $url1 ) )
        {
            $redirect = $url1;
            $redirect .= (empty($url2)? '' : '/' . $url2);
            $redirect .= (empty($url3)? '' : '/' . $url3);
        }
        return $redirect;
    }
}
