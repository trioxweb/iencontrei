<?php

class FacebookController extends BaseController {

    /**
     * Initializer.
     *
     * @return BaseController
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function postLink()
    {
    	$rules = array(
            'link'    => 'required|url',
            'message' => 'required|min:5',
        );

        $validator = Validator::make(Input::all(), $rules);
    	if($validator->passes()){
    		if($this->post_link(Input::get('link'), Input::get('message'))){
    			return Redirect::action('OfficeTasksController@getIndex')->with('success', Lang::get('office/facebook/messages.postlink.success'));
    		}
			return Redirect::action('OfficeTasksController@getIndex')->with('error', Lang::get('office/facebook/messages.postlink.error'));
    	}
    	return Redirect::action('OfficeTasksController@getIndex')->withInput()->withErrors($validator);
    }

	private function post_link($link, $message){
		if($this->isLogged()) {
			try {
				$response = Facebook::object('me/feed')->with(array(
			    		'link' => $link,
			    		'message' => $message,
			    		'privacy' => "{'value':'EVERYONE'}"
			  		))->post();
				$response = $response->toArray();
				if(isset($response['id'])){
					$fbpost = new FacebookPost;
					$fbpost->user_id = Auth::user()->id;
					$fbpost->post_id = $response['id'];
					$fbpost->facebook_token = Auth::user()->access_token;
					if($fbpost->save()){
						return true;
					}
				}
				return false;
			} catch(Exception $e) {
				return false;
			}   
		}
	}

    public function getFblogin()
    {
    	return Redirect::to(Facebook::getLoginUrl());
    }

    // Endpoint that is redirected to after an authentication attempt
    public function getLogin()
    {
    	try{
	        if (!$token = Facebook::getTokenFromRedirect()){
	            return Redirect::to('office')->with('error', 'Unable to obtain access token.');
	        }
	    } catch (FacebookQueryBuilderException $e){
	        return Redirect::to('office')->with('error', $e->getPrevious()->getMessage());
	    }

	    if (!$token->isLongLived()){
	        try{
	            $token = $token->extend();
	        } catch (FacebookQueryBuilderException $e){
	            return Redirect::to('office')->with('error', $e->getPrevious()->getMessage());
	        }
	    }

	    Facebook::setAccessToken($token);

	    try{
	        $facebook_user = Facebook::object('me')->fields('id')->get();
	    } catch (FacebookQueryBuilderException $e){
	        return Redirect::to('office')->with('error', $e->getPrevious()->getMessage());
	    }

	    $user = User::createOrUpdateFacebookObject($facebook_user);   
	    Facebook::auth($user);

	    DB::table('users')->where('id', Auth::user()->id)->update(['access_token' => $token->access_token, 'facebook_user_id' => $facebook_user->toArray()['id']]);

	    return Redirect::action('OfficeTasksController@getIndex')->with('success', 'Successfully logged in with Facebook');
    }

    // Facebook Logout
    public function getLogout()
    {
    	DB::table('users')->where('id', Auth::user()->id)->update(['access_token' => null, 'facebook_user_id' => null]); 
	    return Redirect::action('OfficeTasksController@getIndex')->with('success', 'Successfully logged out.');
    }

    private function isLogged()
    {
    	if(Auth::user()->access_token){
			try{
				$token = Facebook::auth()->getUserAccessToken();
				try{
					Facebook::setAccessToken($token);
					return true;
				}catch(Exception $e){
					return false;
				}
			}catch(Exception $e){
				return false;
			}
		}
    }

}