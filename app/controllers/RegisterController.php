<?php

class RegisterController extends \BaseController {

	/**
     * User Model
     * @var User
     */
    protected $user;

    /**
     * Inject the models.
     * @param User $user
     */
    public function __construct(User $user)
    {
        parent::__construct();
        $this->user = $user;
    }

	/**
	 * Display a listing of the resource.
	 * GET /register
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		return Redirect::to('register/create');
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /register/create
	 *
	 * @return Response
	 */
	public function getCreate()
	{
		$plans = Plan::orderby('order')->get();
		$parent = (Session::get('refer_parent_id', false)) ? User::where('id', Session::get('refer_parent_id'))->first() : User::where('id', 2)->first();

		if(Auth::user()){
			$is_office = true;
			return View::make('register/register', compact('plans', 'parent', 'is_office'));
		}

		return View::make('register/register', compact('plans', 'parent'));
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /register
	 *
	 * @return Response
	 */
	public function postIndex()
	{
		if(!Input::get('cpf') && !Input::get('cnpj') && !Input::get('anydocument')){
			return Redirect::to('register/create')
	                    ->withInput(Input::except('password','password_confirmation'))
	                    ->with('error', Lang::get('admin/users/messages.document_not_be_blank'));
		// Verify if cpf has exists
		}elseif(Input::get('cpf')){
			if(UserData::whereRaw("document LIKE '%".Input::get('cpf')."%'")->first()){
				return Redirect::to('register/create')
	                    ->withInput(Input::except('password','password_confirmation'))
	                    ->with('error', Lang::get('admin/users/messages.document_already_register'));
			}
		}

	    $validator = Validator::make(Input::all(), $this->user->getRulesRegister());

	    if($validator->passes()){
			$this->user->username = Input::get( 'username' );
	        $this->user->email = Input::get( 'email' );

	        $password = Input::get( 'password' );
	        $password_md5 = md5(Input::get( 'password' ));
	        $passwordConfirmation = Input::get( 'password_confirmation' );

	        if(!empty($password)) {
	            if($password === $passwordConfirmation) {
	                $this->user->password = $password;
	                $this->user->password_confirmation = $passwordConfirmation;
	                $this->user->password_md5 = $password_md5;
	            } else {
	                // Redirect to the new user page
	                return Redirect::to('register/create')
	                    ->withInput(Input::except('password','password_confirmation'))
	                    ->with('error', Lang::get('admin/users/messages.password_does_not_match'));
	            }
	        } else {
	            unset($this->user->password);
	            unset($this->user->password_confirmation);
	        }

	   	    $this->user->confirmed = 1;
			$this->user->confirmation_code = md5(microtime().Config::get('app.key'));

	        $this->user->save();

	        if ($this->user->id){

	        	$user_data = new UserData;

	        	$user_data->firstname = Input::get('firstname');
	        	$user_data->lastname  = Input::get('lastname');

	        	$telephones = [];
	            foreach (Input::get('telephones', []) as $key => $telephone) {
	                if($telephone[0] != '' || $telephone[1] != ''){
	                    $telephones[] = $telephone;
	                }
	            }
	        	$user_data->telephone = serialize($telephones);

	        	$user_data->document_type  = 'all';
        		$user_data->document  = serialize(array(
        			'anydocument' => Input::get('anydocument'),
        			'cpf'  => Input::get('cpf'),
        			'cnpj' => Input::get('cnpj'),
        			'ins'  => Input::get('ins'),
        			'razaosocial' => Input::get('razaosocial'),
        		));
	        	

	        	$this->user->data()->save($user_data);

	        	$user_address = new UserAddress;
	        	$user_address->firstname = Input::get('firstname');
	        	$user_address->lastname  = Input::get('lastname');
	        	$user_address->address   = Input::get('address');
	        	$user_address->address_2 = Input::get('address_2');
	        	$user_address->city 	 = Input::get('city');
	        	$user_address->postcode  = Input::get('postcode');
	        	$user_address->country	 = Input::get('country');
	        	$user_address->zone	     = Input::get('zone');

				$this->user->address()->save($user_address);

				$user_mlm = new UserMlm;
				$parent   =  User::where('username', '=', Input::get('parent'))->first();
	        	$user_mlm->parent =  $parent->id;
	        	if(Auth::user()){
	        		if(Auth::user()->id == $user_mlm->parent || Auth::user()->hasRole('admin') || Auth::user()->hasRole('super_admin')){
	        			$user_mlm->side_network = (Input::get('side_network') > 0) ? Input::get('side_network') : null;
	        		}
	        	}

				$this->user->mlm()->save($user_mlm);

				$user_bank = new UserBank;

				$this->user->bank()->save($user_bank);

				try{
					// Send Mail to User
					Mail::send('emails.register', array('user' => $this->user), function($message){
					    $message->to($this->user->email, $this->user->data->firstname)->subject('Cadastro '.Setting::getSetting('name'));
					});

					// Send Mail to Users Admins
	                Mail::send('emails.status.registers.new_register_to_admin', array('user' => $this->user), function($message){
	                    $message->subject('Novo Cadastro Realizado');
	                    if($mails = User::getMailUsersCan('manage_users')){
	                        $message->to(key($mails[0]));
	                        unset($mails[0]);
	                    	foreach ($mails as $mail) {
	                            $message->cc(key($mail));
	                        }
	                    }
	                });
	            }catch(Exception $exception){
	                Session::flash('warning', $exception->getMessage());
	            }

	            return Redirect::to('register/success');
	        }else {
	            // Get validation errors (see Ardent package)
	            $error = $this->user->errors()->all();

	            return Redirect::to('register/create')
	                ->withInput(Input::except('password'))
	                ->with( 'error', $error );
	        }
	    }

	    // Form validation failed
        return Redirect::to('register/create')->withInput()->withErrors($validator);
	}

	public function getSuccess(){
		return View::make('register/success');
	}

	public function getLoadparent($username=''){
		if (!empty($username)){
			$user = User::where('username', '=', $username)->first();
			if($user){
				if($user->isActiveInNetwork()){
					echo $user->data->firstname.' '.$user->data->lastname;
				}
			}
		}
	}

	public function getLoadusername($username=''){
		if (!empty($username)){
			$user = User::where('username', $username);
			if($user->count() > 0){
				echo Lang::get('register/regiter.username_exist');
			}else{
				$setting = new Setting();
				echo 'http://'.$username.'.'.$setting->getSetting('url').'/'; 
			}
		}
	}

}