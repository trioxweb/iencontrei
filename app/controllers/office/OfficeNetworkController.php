<?php

class OfficeNetworkController extends OfficeController {

	/**
	 * Office Network
	 *
	 */
	public function getIndex()
	{
        return Redirect::to('/office/network/organization/');
	}

	public function getBinary($id=null)
	{
		$network = new Network(true);

		if(!$id){
			$id = Auth::user()->id;
		}elseif(!Auth::user()->hasRole('admin') && !Auth::user()->hasRole('super_admin')){
			if(!$network->isOfNetwork(Auth::user()->id, $id))
				return Redirect::action('OfficeNetworkController@getBinary');
		}

		$tree = $network->getBinaryTree($id, 4);

        $table[1][0] = User::find($id);
        $table[1][0]['points'] = $network->getPointsInNetwork($id);

		for ($a=1; $a<=4; $a++){
			if ($a==1){
				if (@isset($tree[1][$id])){
					
					$table[$a][0]['points'] = $network->getPointsInNetwork($id, $network->network_array->list);

					ksort($tree[1][$id]);
					foreach($tree[1][$id] AS $lado => $cad){
						if (is_array($cad)){
                            $cadastro = User::find($cad['id']);
                            $cadastro['points'] = $network->getPointsInNetwork($cad['id']);
                            $table[($a+1)][] = $cadastro;

							$next[($a+1)][] = $cad['id'];
						}
						else{
							$table[($a+1)][] = null;
							$next[($a+1)][] = '';
						}
					}
				}
			}else {
				if (isset($next[$a])) {
					foreach($next[$a] AS $uid) {
						$user = ($a==2 OR $a==3) ? '2' : '3';
						if (!empty($uid) AND isset($tree[$a][$uid])){
							foreach($tree[$a][$uid] AS $lado=>$cad){
								if (is_array($cad)){
									$cadastro = User::find($cad['id']);
                                    $cadastro['points'] = $network->getPointsInNetwork($cad['id']);
                                    $table[($a+1)][] = $cadastro;

									$next[($a+1)][] = $cad['id'];
								}else{
									$table[$a+1][] = null;
									$next[($a+1)][] = '';
								}

							}
						}else{
							$table[($a+1)][] = null;
							$table[($a+1)][] = null;
							$next[($a+1)][] = '';
							$next[($a+1)][] = '';
						}
					}
				}
			}
		}

    	$last_id = array('a' => $network->overflow($id, 1), 'b' => $network->overflow($id, 2));
    	return View::make('office/network/binary')
    	->with('network', $table)
    	->with('last_id', $last_id)
    	->with('next_level_id', Network::where('user_id', $id)->first()->parent_user_id);
	}

	public function getFind($username)
	{
		$user = User::where('username', $username)->first();
		if($user){
			if($user->mlm)
				if($user->mlm->network_in)
					return Redirect::to('/office/network/binary/'.$user->id);
		}
		return Redirect::to('/office/network/binary/');
	}

	/**
	 * Office Network Organization
	 * 
	 * @return Response View
	 */
	public function getOrganization($id=null)
	{
		$title = Lang::get('office/network/title.organization');

		$network = new Network(true);
		$mlm_array = UserMlm::getUserMlmArray(true);

		if(!$id){
			$id = Auth::user()->id;
		}elseif(!Auth::user()->hasRole('admin') && !Auth::user()->hasRole('super_admin')){
			if(!$this->isDescendingOfUser(Auth::user()->id, $id, $mlm_array->natural))
				return Redirect::action('OfficeNetworkController@getOrganization');
		}
		
		return View::make('office/network/organization', compact('title'))
		->with('html_tree', $this->htmlLevelDefaultTree($id, $mlm_array->natural, $mlm_array->natural_network, 10))
		->with('user', User::find($id));
	}

	public function getFindorganization($username)
	{
		$user = User::where('username', $username)->first();
		if($user){
			if($user->mlm->network_in)
				return Redirect::action('OfficeNetworkController@getOrganization', $user->id);
		}
		return Redirect::action('OfficeNetworkController@getOrganization');
	}

	private function isDescendingOfUser($parent_id, $find_id, $network){
		if (isset($network[$parent_id])){
			foreach ($network[$parent_id] AS $key=>$value)
			{
				if($value == $find_id){
					return true;
				}
			}
			foreach ($network[$parent_id] as $key => $value) {
				if(isset($network[$value])){
					return $this->isDescendingOfUser($value, $find_id, $network);
				}
			}
		}
	}

	private function htmlLevelDefaultTree($id=0, $network, $user, $limit=0,$level=1)
	{
		$html_tree = '<div class="panel-group joined" id="accordion-directs'.$level.'">';
		if (isset($network[$id]))
		{
			foreach ($network[$id] AS $key=>$value)
			{
				$title = ($user[$value]->mlm->title) ? ' <img src="/assets/ace/images/linear/t_'.$user[$value]->mlm->title->id.'.png" height="30px"> <button type="button" class="btn btn-orange btn-xs">'.$user[$value]->mlm->title->name.'</button>' : null;
				//$title .= $last_title = ($last_title = MlmTitle::find($user[$value]->mlm->getLastTitle())) ? ' <button type="button" class="btn btn-blue btn-xs">'.$last_title->name.'</button>' : null;
				$vg = '<button type="button" class="btn btn-blue btn-xs">'.$user[$value]->mlm->getGroupVolume().'</button>';
				$active = ($user[$value]->mlm->isActive()) ? '<button type="button" class="btn btn-green btn-xs">Ativo</button>' : '<button type="button" class="btn btn-red btn-xs">Inativo</button>';
				$html_tree .= '
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion-directs'.$level.'" href="#collapse'.$user[$value]->id.'">
								<button type="button" class="btn btn-gray btn-xs">'.$level.'º</button> ' .$user[$value]->username.' '.$title.' '.$vg.' '.$active.' 
							</a>
						</h4>
					</div>
					<div id="collapse'.$user[$value]->id.'" class="panel-collapse collapse out">
						<div class="panel-body" style="padding-left: 10px; padding-top: 10px; padding-right: 0;">';
				if (isset($network[$value]) AND $limit==0 OR
					isset($network[$value]) AND $limit>0 AND $level<$limit)
				{
					$html_tree .= $this->htmlLevelDefaultTree($value, $network, $user, $limit, $level+1);
				}elseif(isset($network[$value]) AND $limit>0 AND !$level < $limit){
					$html_tree .= '<a href="'.URL::action('OfficeNetworkController@getOrganization', $value).'" class="btn btn-blue btn-xs">Ver Organização Abaixo</a>';
				}

				$html_tree .= '	
				</div>
					</div>
				</div>';
			}
		}
		$html_tree .= '</div>';
		return $html_tree;
	}

	/**
	 * Office Network Directs
	 * 
	 * @return Response View
	 */
	public function getDirects()
	{
		// Title
        $title = Lang::get('office/network/title.my_directs');

        return View::make('office/network/directs', compact('title'));
	}

	/**
     * Show a list of all the users formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getDirectsdata()
    {
        $users = User::leftjoin('users_data', 'users_data.user_id', '=', 'users.id')
                    ->join('users_mlm', 'users_mlm.user_id', '=', 'users.id')
                    ->leftjoin('plans', 'plans.id', '=', 'users_mlm.network_plan_id')
                    ->leftjoin('mlm_titles', 'mlm_titles.id', '=', 'users_mlm.network_title_id')
                    ->where('users_mlm.parent', Auth::user()->id)
                    ->select(array('users.id', 'users.username', 'users_data.firstname', 'users.email', 'users.created_at', 'plans.name as plan', 'mlm_titles.name as title'));

        return Datatables::of($users)
        ->add_column('status', '@if(UserMlm::where("user_id", $id)->first()->isActive()) Ativo @else Inativo @endif')
        ->remove_column('id')
        ->make();
    }

}