<?php

class OfficeOrdersController extends OfficeController {

    /**
     * User Model
     * @var User
     */
    protected $order;

    /**
     * Inject the models.
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        parent::__construct();
        $this->order = $order;
    }

    /**
     * Show a list of all the orders of user.
     * 
     * @return View
     */
    public function getIndex()
    {
        // Desactive Alert of Order
        Session::flash('alert_order', '');

        // Title
        $title = Lang::get('office/orders/title.orders');

        // Grab all the orders
        $orders = Auth::user()->orders()->orderby('updated_at', 'DESC')->where('order_status_id', '!=', 0)->get();

        if($orders->count() > 0){
            // Desactive Alert of Order
            Session::flash('alert_order', '');
        }

        return View::make('office/orders/index', compact('orders','title'));
    }

    /**
     * Show a order
     * 
     * @return View
     */
    public function getShow($order)
    {

        // Desactive Alert of Order
        Session::flash('alert_order', '');

        // Title
        $title = Lang::get('office/orders/title.order');

        return View::make('office/orders/show', compact('order', 'title'));
    }

    /**
     * Show a view to create order.
     * 
     * @return View
     */
    public function getCreate($type)
    {
        // User
        $user  = Auth::user();

        // Desactive Alert of Order
        Session::flash('alert_order', '');

        // Title
        $title = Lang::get('office/orders/title.orders_create');

        // Plans
        $plans = Plan::orderby('order');

        if($type == 'plan_upgrade')
            $plans = $plans->where('order', '>', $user->mlm->plan->order);
        

        $plans = $plans->get();

        // Payment Methods
        $payment_methods = Setting::getSetting('payment_methods');

        return View::make('office/orders/create_edit', compact('title', 'type','user', 'plans', 'payment_methods'));
    }

    /**
     * Create order with Plan.
     * 
     * @return Response
     */
    public function postCreate($type)
    {

        // Declare the rules for the form validation
        $rules = array(
            'network_plan_id'   => 'required',
            'payment_method_code' => 'required'
        );

        if($type == 'active_monthly')  
            unset($rules['network_plan_id']);

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            $user = Auth::user();
            $order = $this->order->createOrder($user, Currency::getCurrencyCode(), Input::get('payment_method_code'));

            // Was the order created?
            if($order){

                if($type == 'active_monthly'){
                    $order = $order->createOrderActiveMonthly();
                    $desc = 'Ativo Mensal';
                }elseif($type == 'plan_upgrade'){
                    $plan = Plan::find(Input::get('network_plan_id'));
                    $order = $order->createOrderPlanUpgrade($plan);
                    $desc = 'Upgrade para '.$plan->name;
                }elseif($type == 'plan'){
                    $plan = Plan::find(Input::get('network_plan_id'));
                    $order = $order->createOrderPlan($plan);
                    $desc = 'Plano '.$plan->name;
                }else{
                    $order = false;
                }

                // Was the order with type as created ?
                if($order){

                    // Gera Total Informations and Shipping
                    $order->createOrderTotal();

                    //Gera Fatura no Meio de Pagamento
                    $payment = Payment::createPayment($order, $user, $desc);

                    if($payment->result == 0){
                        $order->delete();
                        return Redirect::action('OfficeOrdersController@getCreate', $type)->with('error', $payment->messages);
                    }

                    // Redirect to the order page show
                    return Redirect::action('OfficeOrdersController@getShow', $order->id)->with('success', Lang::get('office/orders/messages.create.success'));
                }
            }

            // Redirect to the order create page
            return Redirect::action('OfficeOrdersController@getCreate', $type)->with('error', Lang::get('office/orders/messages.create.error'));
        }

        // Form validation failed
        return Redirect::action('OfficeOrdersController@getCreate', $type)->withInput()->withErrors($validator);
    }

    /**
     * Cancel order with Plan.
     * 
     * @return Response
     */
    public function getCancel($order)
    {

        $user = Auth::user();

        if($order->user_id != $user->id || $order->order_status_id != 1){
             return Redirect::action('OfficeOrdersController@getShow', $order->id)->with('error', Lang::get('office/orders/messages.cancel.impossible'));
        }

        // Was the order created?
        if($order->updateHistoryStatus(9))
        {
            // Redirect to the order page show
            return Redirect::action('OfficeOrdersController@getIndex')->with('success', Lang::get('office/orders/messages.cancel.success'));
        }

        // Redirect to the order create page
        return Redirect::action('OfficeOrdersController@getShow', $order->id)->with('error', Lang::get('office/orders/messages.cancel.error'));
    }

    /**
     * Reactive order with Plan.
     * 
     * @return Response
     */
    public function getActive($order)
    {

        // Salva Plano Escolhido
        $user = Auth::user();

        if($order->user_id != $user->id){
             return Redirect::action('OfficeOrdersController@getActive')->with('error', Lang::get('office/orders/messages.active.impossible'));
        }

        // Was the order created?
        if($order->updateHistoryStatus(1))
        {
            // Redirect to the order page show
            return Redirect::action('OfficeOrdersController@getShow', $order->id)->with('success', Lang::get('office/orders/messages.active.success'));
        }

        // Redirect to the order create page
        return Redirect::action('OfficeOrdersController@getShow', $order->id)->with('error', Lang::get('office/orders/messages.active.error'));
    }

    /**
     * Pay Order with Balance.
     * 
     * @return Response
     */
    public function getPaywithbalance($order)
    {

        if($order->order_status_id == 1){
            if(Credit::getBalance() >= $order->total){
                $credit = new Credit([
                    'user_id'     => Auth::user()->id,
                    'value'       => '-'.$order->total, 
                    'description' => 'Pedido #'.$order->id, 
                    'created_at'  => date('Y-m-d H:i:s'),
                ]);
                if($credit->save()){
                    $order->updateHistoryStatus(4);

                    if($order->plan){
                        UserMlm::definePlanPositionApprovePayBonus(User::find($order->user_id), $order);
                    }elseif($order->upgrade){
                        if(UserMlm::upgrade(User::find($order->user_id), $order->upgrade->plan_upgrade_id)){
                            $order->updateHistoryStatus(6);
                        }
                    }elseif($order->active){
                        if(UserMlm::active($order->user_id)){
                            $order->updateHistoryStatus(6);
                        }
                    }

                    return Redirect::action('OfficeOrdersController@getShow', $order->id)->with('success', Lang::get('office/orders/messages.active.success'));
                }
            }else{
                return Redirect::action('OfficeOrdersController@getShow', $order->id)->with('error', Lang::get('office/orders/messages.balance.error'));
            }
        }
        return Redirect::action('OfficeOrdersController@getShow', $order->id)->with('error', Lang::get('office/orders/messages.update.error'));
    }

    /**
     * Finance with Balance.
     * 
     * @return Response
     */
    public function getFinancewithbalance($order)
    {
        if(Setting::getSetting('has_financings')){
            if($order->order_status_id == 1){
                if(Credit::getBalance() >= $order->total){
                    $credit = new Credit([
                        'user_id'     => Auth::user()->id,
                        'value'       => '-'.$order->total, 
                        'description' => 'Pedido Financiado #'.$order->id, 
                        'created_at'  => date('Y-m-d H:i:s'),
                    ]);
                    if($credit->save()){
                        $order->updateHistoryStatus(4);

                        $financing = new Financing([
                            'order_id' => $order->id,
                            'funder_user_id'  => Auth::user()->id,
                            'funded_user_id'  => $order->user_id,
                            'amount_financed' => $order->total,
                        ]);

                        $financing->save();

                        if($order->plan){
                            UserMlm::definePlanPositionApprovePayBonus(User::find($order->user_id), $order);
                        }elseif($order->upgrade){
                            if(UserMlm::upgrade(User::find($order->user_id), $order->upgrade->plan_upgrade_id)){
                                $order->updateHistoryStatus(6);
                            }
                        }elseif($order->active){
                            if(UserMlm::active($order->user_id)){
                                $order->updateHistoryStatus(6);
                            }
                        }

                        return Redirect::action('OfficeOrdersController@getShow', $order->id)->with('success', Lang::get('office/orders/messages.active.success'));
                    }
                }else{
                    return Redirect::action('OfficeOrdersController@getShow', $order->id)->with('error', Lang::get('office/orders/messages.balance.error'));
                }
            }
        }
        return Redirect::action('OfficeOrdersController@getShow', $order->id)->with('error', Lang::get('office/orders/messages.update.error'));
    }

    /**
     * Find Balance
     * 
     * @return Response
     */
    public function postFind()
    {
        // Declare the rules for the form validation
        $rules = array(
            'order_id'   => 'required|integer',
        );

        // Validate the inputs
        $validator = Validator::make(Input::all(), $rules);

        // Check if the form validates with success
        if ($validator->passes())
        {
            $order = Order::find(Input::get('order_id'));
            if($order){
                if($order->payment_code == 'pay_with_balance')
                    return Redirect::action('OfficeOrdersController@getShow', $order->id);
            }

            // Redirect to the order create page
            return Redirect::action('OfficeOrdersController@getIndex')->with('error', Lang::get('office/orders/messages.find.error'));
        }

        // Form validation failed
        return Redirect::action('OfficeOrdersController@getIndex')->withInput()->withErrors($validator);
    }
}