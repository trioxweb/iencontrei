<?php

class OfficeUserController extends OfficeController {

    /**
     * User Model
     * @var User
     */
    protected $user;

    /**
     * Inject the models.
     * @param User $user
     */
    public function __construct()
    {
        parent::__construct();
        $this->user = Auth::user();
    }

    /**
     * Users settings page
     *
     * @return View
     */
    public function getIndex()
    {
        return View::make('office/myaccount');
    }

    /**
     * Change User Access Info User
     *
     * @return Redirect and Message of Success
     */
    public function postUser()
    {
        $user = $this->user;

        // Validate the inputs
        $validator = Validator::make(Input::all(), $user->getUpdateRules());
        if ($validator->passes())
        {
            $oldUser = clone $user;
            $user->username = Input::get('username');
            $user->email = Input::get('email');
            $password = Input::get('password');
            $passwordConfirmation = Input::get('password_confirmation');
            if(!empty($password)) {
                if($password === $passwordConfirmation) {
                    $user->password = $password;
                    $user->password_confirmation = $passwordConfirmation;
                    $user->password_md5 = md5($password);
                } else {
                    // Redirect to the new user page
                    return Redirect::action('OfficeUserController@getIndex')->with('error', Lang::get('admin/users/messages.password_does_not_match'));
                }
            } else {
                unset($user->password);
                unset($user->password_confirmation);
            }
            $user->prepareRules($oldUser, $user);
            // Save if valid. Password field will be hashed before save
            $user->amend();
        }else{
            return Redirect::action('OfficeUserController@getIndex')->withInput()->withErrors($validator);
        }
        
        // Get validation errors (see Ardent package)
        $error = $user->errors()->all();
        if(empty($error)) {

            if(Setting::getSetting('has_shop')){
                UserMlm::editUserInOpencart(User::find($user->id));
            }

            return Redirect::action('OfficeUserController@getIndex')
                ->with( 'success', Lang::get('office/myaccount/messages.update.success'));
        } else {
            return Redirect::action('OfficeUserController@getIndex')
                ->withInput(Input::except('password','password_confirmation'))
                ->with( 'error', $error );
        }
    }

    /**
     * Change Pin User
     *
     * @return Redirect and Message of Success
     */
    public function postPin()
    {

        $rules = [
            'pin'   => 'required|min:3|confirmed',
        ];

        if($this->user->pin){
            Validator::extend('pin', function($attribute, $value, $parameters) {
                return Hash::check($value, $this->user->pin);
            });

            $rules = array_merge($rules, ['pin_actual' => 'required|pin']);
        }

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->passes()){

            $this->user->pin = Hash::make(Input::get('pin'));

            if($this->user->save()){
                return Redirect::action('OfficeUserController@getIndex')->with('success', Lang::get('office/myaccount/messages.update.success'));
            }
            return Redirect::action('OfficeUserController@getIndex')->with('error', Lang::get('office/myaccount/messages.update.error'));
        }

        return Redirect::action('OfficeUserController@getIndex')->withInput()->withErrors($validator);
    }

    /**
     * Change Bank Info User
     *
     * @return Redirect and Message of Success
     */
    public function postBank()
    {

        $validator = Validator::make(Input::all(), $this->user->getRulesBankUpdate());
        if ($validator->passes()){

            if(!$this->user->bank()->first()){
                $this->user->bank()->save(new UserBank);
            }

            $this->user->bank->bank_name      = Input::get('bank_name');
            $this->user->bank->agency         = Input::get('agency');
            $this->user->bank->account_number = Input::get('account_number');
            $this->user->bank->account_type   = Input::get('account_type');
            $this->user->bank->titular_name   = Input::get('titular_name');
            $this->user->bank->titular_cpf    = Input::get('titular_cpf');
            //$this->user->bank->nib            = Input::get('nib');
            //$this->user->bank->iban           = Input::get('iban');
            //$this->user->bank->swift          = Input::get('swift');
            //$this->user->bank->skrill         = Input::get('skrill');
            //$this->user->bank->paypal         = Input::get('paypal');
            $this->user->bank->pagseguro      = Input::get('pagseguro');

            if($this->user->bank->save()){
                try{
                    // Send Mail to User
                    Mail::send('emails.status.users.updated_account', array(), function($message){
                        $message->subject('Alteração nos Dados Bancarios Completa');
                        $message->to($this->user->email);
                    });
                }catch(Exception $exception){
                    Session::flash('warning', $exception->getMessage());
                }    
                return Redirect::action('OfficeUserController@getIndex')->with('success', Lang::get('office/myaccount/messages.update.success'));
            }
            return Redirect::action('OfficeUserController@getIndex')->with('error', Lang::get('office/myaccount/messages.update.error'));
        }

        return Redirect::action('OfficeUserController@getIndex')->withInput()->withErrors($validator);
    }


    /**
     * Change Address Info User
     *
     * @return Redirect and Message of Success
     */
    public function postAddress()
    {

        $validator = Validator::make(Input::all(), $this->user->getRulesAddressUpdate());
        if ($validator->passes()){

            if(!$this->user->address()->first()){
                $this->user->address()->save(new UserAddress);
            }

            $this->user->address->address   = Input::get('address');
            $this->user->address->address_2 = Input::get('address_2');
            $this->user->address->city      = Input::get('city');
            $this->user->address->postcode  = Input::get('postcode');
            $this->user->address->country   = Input::get('country');
            $this->user->address->zone      = Input::get('zone');

            if($this->user->address->save()){
                return Redirect::action('OfficeUserController@getIndex')->with('success', Lang::get('office/myaccount/messages.update.success'));
            }
            return Redirect::action('OfficeUserController@getIndex')->with('error', Lang::get('office/myaccount/messages.update.error'));
        }

        return Redirect::action('OfficeUserController@getIndex')->withInput()->withErrors($validator);
    }


    /**
     * Change Data Info User
     *
     * @return Redirect and Message of Success
     */
    public function postData()
    {

        if(!Input::get('cpf') && !Input::get('cnpj') && !Input::get('anydocument')){
            return Redirect::action('OfficeUserController@getIndex')
                        ->withInput(Input::except('password','password_confirmation'))
                        ->with('error', Lang::get('admin/users/messages.document_not_be_blank'));
        }
        
        $validator = Validator::make(Input::all(), $this->user->getRulesDataUpdate());
        if ($validator->passes()){

            if(!$this->user->data()->first()){
                $this->user->data()->save(new UserData);
            }



            $this->user->data->firstname = Input::get('firstname');
            $this->user->data->lastname  = Input::get('lastname');
            $this->user->data->document_type  = 'all';
            $this->user->data->document  = serialize(array(
                'anydocument' => Input::get('anydocument'),
                'cpf'  => Input::get('cpf'),
                'cnpj' => Input::get('cnpj'),
                'ins'  => Input::get('ins'),
                'razaosocial' => Input::get('razaosocial'),
            ));

            $telephones = [];
            foreach (Input::get('telephones', []) as $key => $telephone) {
                if($telephone[0] != '' || $telephone[1] != ''){
                    $telephones[] = $telephone;
                }
            }
            $this->user->data->telephone = serialize($telephones);

            if($this->user->data->save()){
                return Redirect::action('OfficeUserController@getIndex')->with('success', Lang::get('office/myaccount/messages.data_info_changed_with_success'));
            }
            return Redirect::action('OfficeUserController@getIndex')->with('error', Lang::get('office/myaccount/messages.update.error'));
        }

        return Redirect::action('OfficeUserController@getIndex')->withInput()->withErrors($validator);
    }


    /**
     * Change Side Pattern Network
     *
     * @return Redirect and Message of Success
     */
    public function postChangesidepatternnetwork()
    {
        $validator = Validator::make(Input::all(), ['side_pattern_network' => 'required|max:1']);
        if ($validator->passes()){
            
            $this->user->mlm->side_pattern_network = Input::get('side_pattern_network');
            if($this->user->mlm->save()){
                Session::flash('success', Lang::get('office/myaccount/messages.side_changed_with_success'));
            }
        }

        return Redirect::action('OfficeDashboardController@getIndex');
    }
}
