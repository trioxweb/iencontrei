<?php

class OfficeTasksController extends OfficeController {

	function __construct() {
		if(Auth::user()->access_token){
			try{
				$token = Facebook::auth()->getUserAccessToken();
				try{
					Facebook::setAccessToken($token);
				}catch(FacebookSDKException $e){
					return Redirect::route('facebook_logout');
				}
			}catch(FacebookQueryBuilderException $e){
				return Redirect::route('facebook_logout');
			}
		}
	}
	

	/**
	 * Task index
	 *
	 */
	public function getIndex()
	{
		$title = Lang::get('office/tasks/title.tasks');
		$user_profile = false;
		if(Auth::user()->access_token){
			try{
				$user_profile = Facebook::object('me')->fields('id', 'email', 'name', 'link')->get()->toArray();
			}catch(Exception $e){
				return Redirect::route('facebook_logout');
			}
		}

		$user = Auth::user();
		$facebook_link   = Setting::getSetting('facebook_link', 'mlm');
		$required_shares = Setting::getSetting('required_shares', 'mlm');
		
		$required_shares = (isset($required_shares[$user->mlm->network_plan_id])) ? $required_shares[$user->mlm->network_plan_id] : $required_shares[1];
		$required_shares = $required_shares - FacebookPost::getSharePostsCount($user->id);
		
		return Response::view('office/tasks/index', compact('title', 'user_profile', 'required_shares', 'facebook_link'));
	}
}