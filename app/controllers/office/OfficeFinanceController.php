<?php

class OfficeFinanceController extends OfficeController {

	/**
	 * Finance extract
	 *
	 */
	public function getIndex()
	{
        return View::make('office/finance/extract');
	}

	/**
	 * Finance WithDraw
	 *
	 */
	public function getWithdraw()
	{
        return View::make('office/finance/extract');
	}

	/**
	 * Finance WithDraw
	 *
	 */
	public function postWithdraw()
	{
        $validator = Validator::make(Input::all(), ['payment_method' => 'required', 'value' => 'required']);

        if ($validator->passes()){

            $withdraw_value = (double) str_replace( ',', '.', str_replace(".", "", Input::get('value')));
            $withdraw_value_less_tax = $withdraw_value - ($withdraw_value * ((double) Setting::getSetting('tax_withdrawal')));

        	if($withdraw_value < Setting::getSetting('minimum_withdrawal') 
        		|| $withdraw_value > Credit::getBalance()
        		|| UserWithdraw::isPending()){
        			return Redirect::action('OfficeFinanceController@getIndex')->with('error', Lang::get('office/finances/withdraws/messages.create.error'));
        	}

        	$credit = new Credit(array(
        		'user_id'		 => Auth::user()->id,
        		'description'	 => "Withdraw of ".Currency::format($withdraw_value)." per ".Input::get('payment_method')." Pending",
        		'value'      	 => '-'.$withdraw_value
        	));

        	if($credit->save()){
				$withdraw = new UserWithdraw(array(
					'credit_id'		 => $credit->id,
	        		'payment_method' => Input::get('payment_method'),
	        		'value'      	 => $withdraw_value_less_tax,
	        	));

	            if(Auth::user()->withdraw()->save($withdraw)){
                    try{
                        // Send Mail to Finances Admins
                        Mail::send('emails.status.withdraws.requested_withdrawal', array('user' => Auth::user(), 'withdraw' => $withdraw), function($message){
                            $message->subject('Novo Pedido de Saque Realizado');
                            if($mails = User::getMailUsersCan('manage_credits_withdraws')){
                                $message->to($mails[0]);
                                unset($mails[0]);
                                foreach ($mails as $mail) {
                                    $message->cc($mail);
                                }
                            }
                        });
                    }catch(Exception $exception){
                        Session::flash('warning', $exception->getMessage());
                    }

                    // Redirect to Finance with Success
	            	return Redirect::action('OfficeFinanceController@getIndex')->with('success', Lang::get('office/finances/withdraws/messages.create.success'));
                }else{
                    $credit->delete();
                }
			}

            return Redirect::action('OfficeFinanceController@getIndex')->with('error', Lang::get('office/finances/withdraws/messages.create.error'));
        }
        return Redirect::action('OfficeFinanceController@getIndex')->withInput()->withErrors($validator);
	}

	/**
	 * Finance Transfer to Open Cart
	 *
	 */
	public function postTransferoc()
	{
        $validator = Validator::make(Input::all(), ['value' => 'required']);

        if ($validator->passes()){

            $transfer_value = (double) str_replace( ',', '.', str_replace(".", "", Input::get('value')));
            $transfer_value_less_tax = $transfer_value - ($transfer_value * ((double) Setting::getSetting('tax_transfer_oc')));

        	if($transfer_value < Setting::getSetting('minimum_transfer_oc') 
        		|| $transfer_value > Credit::getBalance()){
        			return Redirect::action('OfficeFinanceController@getIndex')->with('error', Lang::get('office/finances/transfers_oc/messages.create.error'));
        	}

        	$credit = new Credit(array(
        		'user_id'		 => Auth::user()->id,
        		'description'	 => "Transfer of ".Currency::format($transfer_value)." to Store",
        		'value'      	 => '-'.$transfer_value
        	));

            $customer = DB::connection('shop')->table('customer')->where('email', '=', Auth::user()->email)->first();

        	if($customer){
                if($credit->save()){

                    $transaction = array(
                        'customer_id' => $customer->customer_id,
                        'order_id' => '0',
                        'date_added' => date('Y-m-d H:i:s'),
                        'description' => 'Transfer of Office',
                        'amount' => $transfer_value_less_tax,
                    );

                    DB::connection('shop')->table('customer_transaction')->insert($transaction);
                    try{
                        // Send Mail to User
                        Mail::send('emails.status.users.finance.transfer_to_store', array('transfer_value' => $transfer_value), function($message){
                            $message->to(Auth::user()->email, Auth::user()->data->firstname)->subject('Transferência para Loja Realizada');
                        });
                    }catch(Exception $exception){
                        Session::flash('warning', $exception->getMessage());
                    }

    				return Redirect::action('OfficeFinanceController@getIndex')->with('success', Lang::get('office/finances/transfers_oc/messages.create.success'));
    			}
            }
            return Redirect::action('OfficeFinanceController@getIndex')->with('error', Lang::get('office/finances/transfers_oc/messages.create.error'));
        }

        return Redirect::action('OfficeFinanceController@getIndex')->withInput()->withErrors($validator);
	}

	/**
	 * Finance Transfer to Other User
	 *
	 */
	public function postTransferuser()
	{
        $validator = Validator::make(Input::all(), ['value' => 'required', 'username' => 'required|exists:users', ]);

        if ($validator->passes()){

            $automatic_transfer = (Setting::getSetting('automatic_transfer') == 0) ? false : true;
            $transfer_value = (double) str_replace( ',', '.', str_replace(".", "", Input::get('value')));

        	if($transfer_value < Setting::getSetting('minimum_transfer_user') 
        		|| $transfer_value > Credit::getBalance()){
        			return Redirect::action('OfficeFinanceController@getIndex')->with('error', Lang::get('office/finances/transfers_user/messages.create.error'));
        	}

        	$toUser = User::where('username', '=', Input::get('username'))->first();

        	$negative_credit = new Credit(array(
        		'user_id'		 => Auth::user()->id,
        		'description'	 => 'Transfer to '.$toUser->username.' of '.Currency::format($transfer_value).''.(($automatic_transfer) ? '' : ' Pending'),
        		'value'      	 => '-'.$transfer_value
        	));

            $transfer_value_less_tax = $transfer_value - ($transfer_value * ((double) Setting::getSetting('tax_transfer_user')));
        	$credit = new Credit(array(
                'status'         => $automatic_transfer,
        		'user_id'		 => $toUser->id,
        		'description'	 => 'Transfer of '.Auth::user()->data->firstname.'('.Auth::user()->username.')',
        		'value'      	 => $transfer_value_less_tax
        	));

        	if($negative_credit->save() && $credit->save()){

                $transfer = new UserTransfer([
                    'status'             => $automatic_transfer,
                    'user_id'            => Auth::user()->id,
                    'negative_credit_id' => $negative_credit->id,
                    'credit_id'          => $credit->id,
                ]);
                $transfer->save();

                if($automatic_transfer){
                    try{
                        // Send Mail to FromUser
                        Mail::send('emails.status.users.finance.transfer_to_user', array('user' => $toUser,'transfer_value' => $transfer_value), function($message){
                            $message->to(Auth::user()->email, Auth::user()->data->firstname)->subject('Transferência de Usuário Realizada');
                        });

                        // Send Mail to ToUser
                        $this->toUser = $toUser;
                        Mail::send('emails.status.users.finance.transfer_recived', array('user' => Auth::user(),'transfer_value' => $transfer_value), function($message){
                            $message->to($this->toUser->email, $this->toUser->data->firstname)->subject('Transferência de Usuário Recebida');
                        });
                    }catch(Exception $exception){
                        Session::flash('warning', $exception->getMessage());
                    }
                }
				return Redirect::action('OfficeFinanceController@getIndex')->with('success', Lang::get('office/finances/transfers_user/messages.create.success'));
			}

            return Redirect::action('OfficeFinanceController@getIndex')->with('error', Lang::get('office/finances/transfers_user/messages.create.error'));
        }
        return Redirect::action('OfficeFinanceController@getIndex')->withInput()->withErrors($validator);
	}

	/**
     * Show a list of all the credits formatted for Datatables.
     *
     * @return Datatables JSON
     */
	public function getExtract()
	{
		$credits = Credit::select(array('id', 'created_at', 'value', 'description'))
		->where('user_id', '=', Auth::user()->id)
		->where('status', '=', '1')
		->orderby('created_at', 'DESC');

        return Datatables::of($credits)
        ->edit_column('value', '{{ Currency::format($value) }}')
        ->edit_column('created_at', '<?php 
            $date = DateTime::createFromFormat("Y-m-d H:i:s", $created_at);
            echo $date->format("d/m/Y H:i:s"); 
        ?>')
        ->make();	
	}

    /**
     * Show a list of all the credits additionals formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getDataadditionals()
    {
        $credits = CreditAdditional::select(array('id', 'created_at', 'value', 'description'))
        ->where('user_id', '=', Auth::user()->id)
        ->where('status', '=', '1')
        ->orderby('created_at', 'DESC');

        return Datatables::of($credits)
        ->edit_column('value', '{{ Currency::format($value) }}')
        ->edit_column('created_at', '{{ DateTime::createFromFormat("Y-m-d H:i:s", $created_at)->format("d/m/Y H:i:s")}}')
        ->make();   
    }


}