<?php

class OfficeDashboardController extends OfficeController {

	/**
	 * Office dashboard
	 *
	 */
	public function getIndex()
	{
		$posts = Post::orderBy('created_at', 'DESC')->limit(5)->get();
		
		$userMlm = Auth::user()->mlm;

        return View::make('office/home', compact('posts', 'userMlm'));
	}

	/**
	 * Formulario para Inserir PIN
	 *
	 */
	public function getPin()
	{
		if(isset($_COOKIE['pin'])){
			if ($_COOKIE['pin'] == Auth::user()->pin){
				return Redirect::action('OfficeFinanceController@getIndex');
			}
		}
		return View::make('office/pin');
	}

	/**
	 * Post Controller Action to PIN access
	 *
	 */
	public function postPin()
	{

        Validator::extend('pin', function($attribute, $value, $parameters) {
            return Hash::check($value, Auth::user()->pin);
        });

        $rules = ['pin' => 'required|pin'];

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->passes()){
        	$cookie = setcookie('pin', Auth::user()->pin, time() + (60*5), '/');
            Redirect::action('OfficeFinanceController@getIndex');
        }

        return Redirect::action('OfficeDashboardController@getPin')->withInput()->withErrors($validator);
        
	}

	/**
	 * Ajax Request to Charts
	 * 
	 * @return JSON
	 */
	public function getCharts($type='gains_of_last_5_month')
	{	
		$arr = [];
		if($type == 'gains_of_last_5_month'){
			$balance_all = Credit::where('user_id', '=', Auth::user()->id)
			->where('status', '=', '1')
			->where('value', 'NOT LIKE', '%-%')
			->groupBy(DB::raw('MONTH(created_at)'))
			->orderBy('created_at', 'ASC')
			->select(DB::raw('SUM(value) as balance, DATE_FORMAT(created_at, \'%m/%Y\') as date'))
			->take(5)
			->get();

			foreach ($balance_all as $balance) {
				// x => data y => gains
				$arr[] = ['x' => $balance->date, 'y' => (double)$balance->balance];
			}
		}
		return Response::json($arr);
	}
}