<?php

class AdminDashboardController extends AdminController {

	/**
	 * Admin dashboard
	 *
	 */
	public function getIndex()
	{
		$users_actives = UserMlm::usersActives();
		$users_inactives = UserMlm::usersInactives();
		$userMlm_principal = UserMlm::find(2);
		$total_volume = $userMlm_principal->getGroupVolume();
		$new_users = $userMlm_principal->getUsersInNetwotk(true);
        return View::make('admin/home', compact('users_actives', 'users_inactives', 'total_volume', 'new_users'));
	}

	/*
	 * Get Link of Facebook View
	 * @return View
     */
	public function getFacebooklink()
	{
		$title = 'Facebook Link';
		$facebook_link = Setting::getSetting('facebook_link', 'mlm');
        return View::make('admin/facebook_link', compact('title', 'facebook_link'));
	}

	/*
	 * Post Link of Facebook
	 * @return Response
     */
	public function postFacebooklink()
	{
        // Declare the rules for the form validation
        $rules = array(
            'facebook_link'   => 'required|url',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes())
        {
        	$facebook_link = Setting::where('key', 'facebook_link')->first();
        	
        	$facebook_link->value = Input::get('facebook_link');

        	if($facebook_link->save()){
        		return Redirect::action('AdminDashboardController@getFacebooklink')->with('success', Lang::get('admin/home/messages.facebook_link_update.success'));
        	}

            return Redirect::action('AdminDashboardController@getFacebooklink')->with('error', Lang::get('admin/home/messages.facebook_link_update.error'));
        }

        return Redirect::action('AdminDashboardController@getFacebooklink')->withInput()->withErrors($validator);
	}

	/**
	 * Ajax Request to Charts
	 * 
	 * @return JSON
	 */
	public function getCharts($type='title')
	{
		if($type == 'title'){
			$arr = [];
			foreach (MlmTitle::orderBy('order')->get() as $title) {
				$arr[] = ['x' => $title->name, 'y' => $this->getQtdUsersByTitle($title)];
			}
		}else{
			$arr = [];
			foreach (Plan::orderBy('order')->get() as $plan) {
				$arr[] = ['x' => $plan->name, 'y' => $this->getQtdUsersByPlan($plan)];
			}
		}
		return Response::json($arr);
	}

	function getQtdUsersByTitle(MlmTitle $title){
		return $qtd_users = User::leftjoin('users_mlm', 'users_mlm.user_id', '=', 'users.id')
        	->where('users_mlm.network_in', '=', 1)
        	->where('users_mlm.network_title_id', '=', $title->id)
        	->count();
	}

	function getQtdUsersByPlan(Plan $plan){
		return $qtd_users = User::leftjoin('users_mlm', 'users_mlm.user_id', '=', 'users.id')
        	->where('users_mlm.network_in', '=', 1)
        	->where('users_mlm.network_plan_id', '=', $plan->id)
        	->count();
	}
}