<?php

class AdminPlansController extends AdminController {


    /**
     * Plan Model
     * @var Plan
     */
    protected $plan;

    /**
     * Inject the models.
     * @param Plan $plan
     */
    public function __construct(Plan $plan)
    {
        parent::__construct();
        $this->plan = $plan;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        // Grab all the plans
        $plans = $this->plan;

        // Show the page
        return View::make('admin/plans/index', compact('plans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {
        $title = Lang::get('admin/plan.create_plan');

        $mode = 'create';

		// Show the page
		return View::make('admin/plans/create_edit', compact('title', 'mode'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate()
    {
        // Declare the rules for the form validation
        $rules = array(
            'name'   => 'required',
            'value'  => 'required|numeric',
            'points' => 'required|numeric',
            'order'  => 'required|numeric'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes())
        {
            $plan = new Plan;
            $plan->name = Input::get('name');
            $plan->value = Input::get('value');            
            $plan->points = Input::get('points');
            $plan->order = Input::get('order');

            if($plan->save()){
                 return Redirect::to('admin/plans/' . $plan->id . '/edit')->with('success', Lang::get('admin/plans/messages.create.success'));
            }

            return Redirect::to('admin/plans/create')->with('error', Lang::get('admin/plans/messages.create.error'));
        }

        return Redirect::to('admin/plans/create')->withInput()->withErrors($validator);
    }

    /**
     * Display the specified resource.
     *
     * @param $plan
     * @return Response
     */
    public function getShow($plan)
    {
        // redirect to the frontend
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $plan
     * @return Response
     */
    public function getEdit($plan)
    {
        if ($plan->id)
        {
            // Title
            $title = Lang::get('admin/plans/title.plan_update');
            // mode
            $mode = 'edit';

            return View::make('admin/plans/create_edit', compact('plan', 'mode', 'title'));
        }

        return Redirect::to('admin/plans')->with('error', Lang::get('admin/users/messages.does_not_exist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $plan
     * @return Response
     */
    public function postEdit($plan)
    {
        $rules = array(
            'name'   => 'required',
            'value'  => 'required|numeric',
            'points' => 'required|numeric',
            'order'  => 'required|numeric'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes())
        {
            $plan->name = Input::get('name');
            $plan->value = Input::get('value');            
            $plan->points = Input::get('points');
            $plan->order = Input::get('order');

            if($plan->save()){
                 return Redirect::to('admin/plans/' . $plan->id . '/edit')->with('success', Lang::get('admin/plans/messages.create.success'));
            }
            return Redirect::to('admin/plans/' . $plan->id . '/edit')->with('error', Lang::get('admin/plans/messages.create.error'));
        }

        return Redirect::to('admin/plans/' . $plan->id . '/edit')->withInput()->withErrors($validator);
    }

    /**
     * Remove the specified plan from storage.
     *
     * @param $plan
     * @return Response
     */
    public function getDelete($plan)
    {
        $id = $plan->id;
        $plan->delete();

        // Was the blog plan deleted?
        $plan = Plan::find($id);
        if(empty($plan))
        {
            // Redirect to the blog posts management page
            return Redirect::to('admin/plans')->with('success', Lang::get('admin/plans/messages.delete.success'));
        }
        // There was a problem deleting the blog plan
        return Redirect::to('admin/plans')->with('error', Lang::get('admin/plans/messages.delete.error'));
    }

    /**
     * Show a list of all the plans formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData()
    {
        $plans = Plan::select(array('id', 'name', 'value', 'points', 'order'))->orderBy('order');

        return Datatables::of($plans)
        ->edit_column('value', '{{ Currency::format($value) }}')
        ->add_column('actions', '
            <a style="margin-bottom:5px;" href="{{{ URL::to(\'admin/plans/\' . $id . \'/edit\' ) }}}" class="iframe btn btn-default btn-sm">{{{ Lang::get(\'button.edit\') }}}</a>
            <a href="#modal-dialog" data-toggle="modal" data-href="{{{ URL::to(\'admin/plans/\' . $id . \'/delete\' ) }}}" class="btn btn-danger btn-sm">{{{ Lang::get(\'button.delete\') }}}</a>
        ')
        ->remove_column('id')
        ->make();
    }
}
