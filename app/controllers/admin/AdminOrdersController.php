<?php

class AdminOrdersController extends AdminController {


    /**
     * Order Model
     * @var Order
     */
    protected $order;

    /**
     * Inject the models.
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        parent::__construct();
        $this->order = $order;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        // Grab all the orders
        $orders = $this->order;

        // Show the page
        return View::make('admin/orders/index', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {
        $title = Lang::get('admin/orders/title.create');

        $mode = 'create';

        // Plans
        $plans = Plan::orderby('order')->get();

        // Payment Methods
        foreach (Setting::getSetting('payment_methods') as $key => $payment_method) {
            $payment_methods[$key] = $payment_method['name'];
        }

        // Show the page
        return View::make('admin/orders/create_edit', compact('title', 'mode', 'plans', 'payment_methods'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate()
    {
        // Declare the rules for the form validation
        $rules = array(
            'username'   => 'required|exists:users',
            'value'      => 'required|numeric',
            'status'     => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes())
        {
            $user = User::where('username', '=', Input::get('username'))->first();

            $order = new Order;
            $order->value  = Input::get('value');
            $order->status = Input::get('status');
            if($user->order()->save($order)){
                 return Redirect::to('admin/orders/' . $order->id . '/edit')->with('success', Lang::get('admin/orders/messages.create.success'));
            }

            return Redirect::to('admin/orders/create')->with('error', Lang::get('admin/orders/messages.create.error'));
        }

        return Redirect::to('admin/orders/create')->withInput()->withErrors($validator);
    }

    /**
     * Display the specified resource.
     *
     * @param $order
     * @return Response
     */
    public function getShow($order)
    {
        // redirect to the frontend
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $order
     * @return Response
     */
    public function getEdit($order)
    {
        if ($order->id)
        {
            // Title
            $title = Lang::get('admin/orders/title.order_edit');
            // mode
            $mode = 'edit';

            // Plans
            $plans = Plan::orderby('order')->get();

            // Payment Methods
            foreach (Setting::getSetting('payment_methods') as $key => $payment_method) {
                $payment_methods[$key] = $payment_method['name'];
            }

            return View::make('admin/orders/create_edit', compact('order', 'mode', 'title', 'plans', 'payment_methods'));
        }

        return Redirect::to('admin/orders')->with('error', Lang::get('admin/users/messages.does_not_exist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $order
     * @return Response
     */
    public function postEdit($order)
    {
        // Declare the rules for the form validation
        $rules = array(
            'username'   => 'required|exists:users',
            'value'      => 'required|numeric'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes())
        {
            $user = User::where('username', '=', Input::get('username'))->first();

            $order->value = Input::get('value');
            $order->status = Input::get('status');

            if($user->order()->save($order)){
                 return Redirect::to('admin/orders/' . $order->id . '/edit')->with('success', Lang::get('admin/orders/messages.create.success'));
            }
            return Redirect::to('admin/orders/' . $order->id . '/edit')->with('error', Lang::get('admin/orders/messages.create.error'));
        }

        return Redirect::to('admin/orders/' . $order->id . '/edit')->withInput()->withErrors($validator);
    }

    /**
     * Remove the specified order from storage.
     *
     * @param $order
     * @return Response
     */
    public function getDelete($order)
    {

        $id = $order->id;
        $order->delete();

        // Was the blog order deleted?
        $order = Order::find($id);
        if(empty($order))
        {
            // Redirect to the blog posts management page
            return Redirect::to('admin/orders')->with('success', Lang::get('admin/orders/messages.delete.success'));
        }
        // There was a problem deleting the blog order
        return Redirect::to('admin/orders')->with('error', Lang::get('admin/orders/messages.delete.error'));
    }

    /**
     * Show a list of all the orders formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData()
    {

        $orders = Order::leftjoin('users', 'users.id', '=', 'order.user_id')
        ->leftjoin('users_mlm', 'users_mlm.user_id', '=', 'order.user_id')
        ->leftjoin('order_status', 'order_status.id', '=', 'order.order_status_id')
        ->leftjoin('order_plan', 'order_plan.order_id', '=', 'order.id')
        ->leftjoin('order_plan_upgrade', 'order_plan_upgrade.order_id', '=', 'order.id')
        ->leftjoin('order_active', 'order_active.order_id', '=', 'order.id')
        ->select(array('order.id as id', 'users.username as username', 
            'order_plan_upgrade.name as order_plan_or_upgrade_name',
            'order_plan_upgrade.plan_upgrade_id as order_plan_upgrade',
            'order_plan.name as order_plan',
            'order_active.name as order_active',
            'order.total', 'order.created_at', 'order.payment_method', 'order_status.name', 'order_status.id as status_id', 'order.user_id as user_id', 'users_mlm.network_in', 'users_mlm.bonus', 'users_mlm.network_plan_id as network_plan_id'))
        ->orderby('order.created_at', 'DESC');

        return Datatables::of($orders)
        ->add_column('actions', '
            @if(isset($order_plan))
                <?php $userMlm = UserMlm::where("user_id", $user_id)->first(); ?>
                @if((!$network_in || $userMlm->contractNeedPay($user_id) || !$bonus) && $status_id != 9 && $status_id != 6)
                    <a style="margin-bottom:5px;" href="#modal-dialog" data-toggle="modal" data-href="{{{ URL::to(\'admin/users/\' . $user_id . \'/allmlm/\'. $id ) }}}" class="btn btn-green btn-sm">{{{ Lang::get(\'button.position_approve_and_pay_bonus\') }}}</a><br> 
                @endif

                @if(!$network_in && $status_id != 9 && $status_id != 6)  
                    <a style="margin-bottom:5px;" href="#modal-dialog" data-toggle="modal" data-href="{{{ URL::to(\'admin/users/\' . $user_id . \'/position\' ) }}}" class="btn btn-green btn-sm">{{{ Lang::get(\'button.position_in_network\') }}}</a><br> 
                @elseif($userMlm->contractNeedPay($user_id) && $status_id != 9 && $status_id != 6)
                    <a style="margin-bottom:5px;" href="#modal-dialog" data-toggle="modal" data-href="{{{ URL::to(\'admin/users/\' . $user_id . \'/approve/\'. $id ) }}}" class="btn btn-green btn-sm">{{{ Lang::get(\'button.aprove\') }}}</a><br>
                @endif

                @if($network_in && !$bonus && $status_id != 9 && $status_id != 6 && !$userMlm->contractNeedPay($user_id))
                    <a style="margin-bottom:5px;" href="#modal-dialog" data-toggle="modal" data-href="{{{ URL::to(\'admin/users/\' . $user_id . \'/paybonus/\'. $id ) }}}" class="btn btn-blue btn-sm">{{{ Lang::get(\'button.pay_bonus\') }}}</a><br>
                @endif
            @elseif(isset($order_plan_upgrade) && $status_id != 9 && $status_id != 6)
                @if($order_plan_upgrade > $network_plan_id)
                    <a style="margin-bottom:5px;" href="#modal-dialog" data-toggle="modal" data-href="{{{ URL::to(\'admin/users/\' . $user_id . \'/upgrade/\' . $id ) }}}" class="btn btn-green btn-sm">Upgrade</a><br> 
                @endif
            @elseif(isset($order_active) && $status_id != 9 && $status_id != 6)
                @if($userMlm->activeMonthlyNeedPay($user_id) && !$userMlm->contractNeedPay($user_id))
                    <a style="margin-bottom:5px;" href="#modal-dialog" data-toggle="modal" data-href="{{{ URL::to(\'admin/users/\' . $user_id . \'/active/\'. $id ) }}}" class="btn btn-green btn-sm">{{{ Lang::get(\'button.aprove_active_monthly\') }}}</a><br>
                @endif
            @endif
            <a href="{{{ URL::to(\'admin/orders/\' . $id . \'/history\' ) }}}" class="iframe btn btn-blue btn-sm">{{{ Lang::get(\'admin/orders/button.button.history\') }}}</a>
            <a href="#modal-dialog" data-toggle="modal" data-href="{{{ URL::to(\'admin/orders/\' . $id . \'/delete\' ) }}}" class="btn btn-danger btn-sm">{{{ Lang::get(\'admin/orders/button.delete\') }}}</a>
        ')
        ->edit_column('order_plan_or_upgrade_name', function($row){
            if($row->order_plan)
                return $row->order_plan;
            elseif($row->order_plan_upgrade)
                return $row->order_plan_or_upgrade_name;
            elseif($row->order_active)
                return $row->order_active;
            else
                return '';
        })
        ->edit_column('username', '<a href="{{{ URL::to(\'admin/users/\' . $user_id . \'/edit\' ) }}}" class="iframe">{{$username}}</a>')
        ->remove_column('status_id')
        ->remove_column('user_id')
        ->remove_column('network_in')
        ->remove_column('bonus')
        ->remove_column('order_plan_upgrade')
        ->remove_column('order_plan')
        ->remove_column('network_plan_id')
        ->remove_column('order_active')
        ->make();  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $order
     * @return Response
     */
    public function getHistory($order)
    {
        if ($order->id)
        {
            // Title
            $title = Lang::get('admin/orders/title.order_history');

            // Status
            $order_status = OrderStatus::all();

            // Order Historys
            $order_historys = $order->order_history()->orderby('created_at', 'ASC')->get();

            return View::make('admin/orders/history', compact('order', 'title', 'order_status', 'order_historys'));
        }

        return Redirect::to('admin/orders')->with('error', Lang::get('admin/orders/messages.does_not_exist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $order
     * @return Response
     */
    public function postHistory($order)
    {
        // Declare the rules for the form validation
        $rules = array(
            'order_status_id'   => 'required|numeric|exists:order_status,id',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes())
        {
            $history = new OrderHistory(array(
                'order_status_id' => Input::get('order_status_id'),
                'notify'          => 0,
                'created_at'      => date("Y-m-d H:i:s")
            ));
            $order->order_history()->save($history);

            $order->order_status_id = Input::get('order_status_id');

            if($order->save()){
                 return Redirect::to('admin/orders/' . $order->id . '/history')->with('success', Lang::get('admin/orders/messages.history.success'));
            }
            return Redirect::to('admin/orders/' . $order->id . '/history')->with('error', Lang::get('admin/orders/messages.history.error'));
        }

        return Redirect::to('admin/orders/' . $order->id . '/history')->withInput()->withErrors($validator);
    }

    /**
     * Remove the specified history from storage.
     *
     * @param $history_id
     * @return Response
     */
    public function getHistoryDelete($history_id)
    {

        $order_history = OrderHistory::find($history_id);

        $order_id = $order_history->order_id;

        $order_history->delete();

        // Was the blog order deleted?
        $order_history = Order::find($history_id);
        if(empty($order_history))
        {
            // Redirect to the blog posts management page
            return Redirect::to('admin/orders/' . $order_id . '/history')->with('success', Lang::get('admin/orders/messages.order_history_delete.success'));
        }
        // There was a problem deleting the blog order
        return Redirect::to('admin/orders/' . $order_id . '/history')->with('error', Lang::get('admin/orders/messages.order_history_delete.error'));
    }
}
