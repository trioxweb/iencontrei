<?php

class AdminReportController extends AdminController {

    /**
     * @return Response
     */
    public function getIndex()
    {
        return Redirect::action('AdminReportController@getTitle');
    }

    /**
     * @return Response
     */
    public function getBinary()
    {
        return View::make('admin/report/binary');
    }

    /**
     * @return Response
     */
    public function getTitle()
    {
        return View::make('admin/report/title');
    }

    /**
     * Show a list of all the points formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getDatabinary()
    {
        $log_bonuses = LogBonus::leftjoin('users', 'users.id', '=', 'log_bonuses.user_id')
                    ->where('log_bonuses.bonus_name', 'Bonus Binário')
                    ->orderby('log_bonuses.created_at', 'DESC')
                    ->select(array('users.username', 'log_bonuses.bonus_name', 'log_bonuses.log', 'log_bonuses.created_at'));

        return Datatables::of($log_bonuses)
        ->edit_column('created_at', '<?php 
            $date = DateTime::createFromFormat("Y-m-d H:i:s", $created_at);
            echo $date->format("d/m/Y"); 
        ?>')
        ->make();
    }

    /**
     * Show a list of all the points formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getDatatitle()
    {
        $historics = MlmTitleHistoric::leftjoin('users', 'users.id', '=', 'mlm_titles_historic.user_id')
            ->leftjoin('mlm_titles', 'mlm_titles.id', '=', 'mlm_titles_historic.title_id')
            ->orderby('mlm_titles_historic.created_at', 'DESC')
            ->select(array('users.username', 'mlm_titles.name', 'mlm_titles_historic.created_at'));

        return Datatables::of($historics)
        ->edit_column('created_at', '<?php 
            $date = DateTime::createFromFormat("Y-m-d H:i:s", $created_at);
            echo $date->format("d/m/Y"); 
        ?>')
        ->make();   
    }
}
