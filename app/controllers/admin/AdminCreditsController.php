<?php

class AdminCreditsController extends AdminController {


    /**
     * Credit Model
     * @var Credit
     */
    protected $credit;

    /**
     * Inject the models.
     * @param Credit $credit
     */
    public function __construct(Credit $credit)
    {
        parent::__construct();
        $this->credit = $credit;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        // Grab all the credits
        $credits = $this->credit;

        // Show the page
        return View::make('admin/credits/index', compact('credits'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {
        $title = Lang::get('admin/credit.create_credit');

        $mode = 'create';

		// Show the page
		return View::make('admin/credits/create_edit', compact('title', 'mode'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate()
    {
        // Declare the rules for the form validation
        $rules = array(
            'username'   => 'required|exists:users',
            'value'      => 'required|numeric',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes())
        {
            $user = User::where('username', '=', Input::get('username'))->first();

            $credit = new Credit;
            $credit->value       = Input::get('value');
            $credit->description = Input::get('description');

            if($user->credit()->save($credit)){
                 return Redirect::to('admin/credits/' . $credit->id . '/edit')->with('success', Lang::get('admin/credits/messages.create.success'));
            }

            return Redirect::to('admin/credits/create')->with('error', Lang::get('admin/credits/messages.create.error'));
        }

        return Redirect::to('admin/credits/create')->withInput()->withErrors($validator);
    }

    /**
     * Display the specified resource.
     *
     * @param $credit
     * @return Response
     */
    public function getShow($credit)
    {
        // redirect to the frontend
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $credit
     * @return Response
     */
    public function getEdit($credit)
    {
        if ($credit->id)
        {
            // Title
            $title = Lang::get('admin/credits/title.credit_update');
            // mode
            $mode = 'edit';

            return View::make('admin/credits/create_edit', compact('credit', 'mode', 'title'));
        }

        return Redirect::to('admin/credits')->with('error', Lang::get('admin/users/messages.does_not_exist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $credit
     * @return Response
     */
    public function postEdit($credit)
    {
        $validator = Validator::make(Input::all(), array());

        if ($validator->passes())
        {
            $credit->description = Input::get('description');

            if($credit->save()){
                 return Redirect::to('admin/credits/' . $credit->id . '/edit')->with('success', Lang::get('admin/credits/messages.create.success'));
            }
            return Redirect::to('admin/credits/' . $credit->id . '/edit')->with('error', Lang::get('admin/credits/messages.create.error'));
        }

        return Redirect::to('admin/credits/' . $credit->id . '/edit')->withInput()->withErrors($validator);
    }

    /**
     * Remove the specified credit from storage.
     *
     * @param $credit
     * @return Response
     */
    public function getDelete($credit)
    {
        return Redirect::to('admin/credits')->with('error', Lang::get('admin/credits/messages.delete.error'));
        
        $id = $credit->id;
        $credit->delete();

        // Was the blog credit deleted?
        $credit = Credit::find($id);
        if(empty($credit))
        {
            // Redirect to the blog posts management page
            return Redirect::to('admin/credits')->with('success', Lang::get('admin/credits/messages.delete.success'));
        }
        // There was a problem deleting the blog credit
        return Redirect::to('admin/credits')->with('error', Lang::get('admin/credits/messages.delete.error'));
    }

    /**
     * Get View the to .
     *
     * @return Response
     */
    public function getDivideprofit()
    {
        // Title
        $title = Lang::get('admin/credits/title.divide_profit');

        return View::make('admin/credits/divide_profit', compact('title'));
    }

    /**
     * Divide Profit Post.
     *
     * @return Response
     */
    public function postDivideprofit()
    {
        // Verifica Alternação entre Plan ou Title
        $network_plan_id = Input::get('network_plan_id', false);
        $network_title_id = Input::get('network_title_id', false);
        if((!$network_plan_id && !$network_title_id) ||
            ($network_plan_id && $network_title_id)){
            return Redirect::action('AdminCreditsController@postDivideprofit')->withInput()->with('error', Lang::get('admin/credits/messages.select_one_plan_or_title.error'));
        }

        $rules = array(
            'value'       => 'required',
            'verify_post' => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes())
        {
            $value = str_replace( ',', '.', str_replace(".", "", Input::get('value')));
            if(Credit::divideProfit($value, $network_plan_id, $network_title_id, Input::get('verify_post')))
            {
                return Redirect::action('AdminCreditsController@postDivideprofit')->with('success', Lang::get('admin/credits/messages.divide_profit.success'));
            }
            return Redirect::action('AdminCreditsController@postDivideprofit')->withInput()->with('error', Lang::get('admin/credits/messages.divide_profit.error'));
        }
        return Redirect::action('AdminCreditsController@postDivideprofit')->withInput()->withErrors($validator);
    }



    /**
     * Show a list of all the credits formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData()
    {
        $credits = Credit::leftjoin('users', 'users.id', '=', 'credits.user_id')
        ->select(array('credits.id as id', 'users.username', 'credits.created_at', 'credits.value', 'credits.description'))
        ->where('status', '=', '1')
        ->orderby('credits.created_at', 'DESC');

        return Datatables::of($credits)
        ->edit_column('value', '{{ Currency::format($value) }}')
        ->edit_column('created_at', '<?php 
            $date = DateTime::createFromFormat("Y-m-d H:i:s", $created_at);
            echo $date->format("d/m/Y"); 
        ?>')
        ->add_column('actions', '
            <a href="{{{ URL::to(\'admin/credits/\' . $id . \'/edit\' ) }}}" class="iframe btn btn-default btn-sm">{{{ Lang::get(\'button.edit\') }}}</a>
        ')
        ->make();   
    }

    /**
     * Display a listing of the transfers pending.
     *
     * @return Response
     */
    public function getTransfers()
    {
        return View::make('admin/credits/transfers');
    }

    /**
     * Approve the specified transfer from storage.
     *
     * @param $user_transfer_id
     * @return Response
     */
    public function getTransferapprove($user_transfer_id)
    {
        if(!$transfer = UserTransfer::find($user_transfer_id))
            return Redirect::to('admin/credits/transfers')->with('error', Lang::get('admin/credits/messages.approve_transfer.error'));

        if(UserTransfer::approveTransfer($transfer)){

            $this->fromUser = $transfer->negativecredit->user;
            $this->toUser = $transfer->credit->user;
            try{
                Mail::send('emails.status.users.finance.transfer_to_user', array('user' => $this->toUser, 'transfer_value' => $transfer->credit->value), function($message){
                    $message->to($this->fromUser->email, $this->fromUser->data->firstname)->subject('Transferência para Usuário Realizada');
                });

                Mail::send('emails.status.users.finance.transfer_recived', array('user' => $this->fromUser, 'transfer_value' => $transfer->credit->value), function($message){
                    $message->to($this->toUser->email, $this->toUser->data->firstname)->subject('Transferência de Usuário Recebida');
                });
            }catch(Exception $exception){
                Session::flash('warning', $exception->getMessage());
            }

            return Redirect::to('admin/credits/transfers')->with('success', Lang::get('admin/credits/messages.approve_transfer.success'));
        }
        
        return Redirect::to('admin/credits/transfers')->with('error', Lang::get('admin/credits/messages.approve_transfer.error'));
    }

    /**
     * Unapprove the specified transfer from storage.
     *
     * @param $user_transfer_id
     * @return Response
     */
    public function getTransferunapprove($user_transfer_id)
    {
        if(!$transfer = UserTransfer::find($user_transfer_id))
            return Redirect::to('admin/credits/transfers')->with('error', Lang::get('admin/credits/messages.unapprove_transfer.error'));

        if(UserTransfer::unapproveTransfer($transfer)){

            return Redirect::to('admin/credits/transfers')->with('success', Lang::get('admin/credits/messages.unapprove_transfer.success'));
        }

        return Redirect::to('admin/credits/transfers')->with('error', Lang::get('admin/credits/messages.unapprove_transfer.error'));
    }

    /**
     * Show a list of all the transfers formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getDatatransfers()
    {
        $transfers = UserTransfer::leftjoin('users', 'users.id', '=', 'users_transfers.user_id')
        ->leftjoin('credits', 'credits.id', '=', 'users_transfers.negative_credit_id')
        ->where('users_transfers.status', '!=', '1')
        ->orderby('users_transfers.created_at', 'DESC')
        ->select(array('users_transfers.id as id', 'users.username', 'users_transfers.created_at', 'credits.value', 'credits.description'));

        return Datatables::of($transfers)
        ->edit_column('value', '{{ Currency::format($value) }}')
        ->edit_column('created_at', '<?php 
            $date = DateTime::createFromFormat("Y-m-d H:i:s", $created_at);
            echo $date->format("d/m/Y"); 
        ?>')
        ->add_column('actions', '
            <a style="margin-bottom:5px;" href="#modal-dialog" data-toggle="modal" data-href="{{{ URL::to(\'admin/credits/\' . $id . \'/transfer_approve\' ) }}}" class="btn btn-blue btn-sm">{{{ Lang::get(\'button.approve_transfer\') }}}</a><br>
            <a href="#modal-dialog" data-toggle="modal" data-href="{{{ URL::to(\'admin/credits/\' . $id . \'/transfer_unapprove\' ) }}}" class="btn btn-blue btn-sm">{{{ Lang::get(\'button.unapprove_transfer\') }}}</a>
        ')
        ->make();   
    }

    /**
     * Display a listing of the gains.
     *
     * @return Response
     */
    public function getGains()
    {
        return View::make('admin/credits/gains');
    }


     /**
     * Show a list of all the gains formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getDatagains()
    {
        $users = User::leftjoin('users_data', 'users_data.user_id', '=', 'users.id')
                ->join('users_mlm', 'users_mlm.user_id', '=', 'users.id')
                ->select(array('users.id', 'users.username'))
                ->where('users.id', '!=', (Auth::user()->hasRole('admin')) ? 1 : 0);

        return Datatables::of($users)
        ->add_column('value', '{{ round(Credit::getTotalGains($id), 2) }}')
        ->remove_column('id')
        ->make();   
    }

    /**
     * Display a listing of the credits additionals.
     *
     * @return Response
     */
    public function getAdditionals()
    {
        return View::make('admin/credits/additionals');
    }

    /**
     * Show a list of all the credits additionals formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getDataadditionals()
    {
        $credits_additional = CreditAdditional::leftjoin('users', 'users.id', '=', 'credits_additional.user_id')
        ->select(array('credits_additional.id as id', 'users.username', 'credits_additional.created_at', 'credits_additional.value', 'credits_additional.description'))
        ->where('status', '=', '1')
        ->orderby('credits_additional.created_at', 'DESC');

        return Datatables::of($credits_additional)
        ->edit_column('value', '{{ Currency::format($value) }}')
        ->edit_column('created_at', '{{DateTime::createFromFormat("Y-m-d H:i:s", $created_at)->format("d/m/Y")}}')
        ->make();   
    }
}
