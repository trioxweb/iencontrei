<?php

class AdminFinancingsController extends AdminController {


    /**
     * Financing Model
     * @var Financing
     */
    protected $financing;

    /**
     * Inject the models.
     * @param Financing $financing
     */
    public function __construct(Financing $financing)
    {
        parent::__construct();
        $this->financing = $financing;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        $title = Lang::get('admin/financings/title.financings');

        // Grab all the financings
        $financings = $this->financing;

        // Show the page
        return View::make('admin/financings/index', compact('title', 'financings'));
    }

    /**
     * Show a list of all the financings formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData()
    {
        $financings = User::join('financings', 'users.id', '=', 'financings.funder_user_id')
        ->leftjoin('users as txz_funded_user', 'funded_user.id', '=', 'financings.funded_user_id')
        ->select(array('users.username as funder_username', 'funded_user.username as funded_username', 'financings.amount_financed', 'financings.amount_paid', 'financings.created_at', 'financings.updated_at'))
        ->orderby('financings.updated_at', 'DESC');
        return Datatables::of($financings)
        ->edit_column('amount_financed', '{{ Currency::format($amount_financed) }}')
        ->edit_column('amount_paid', '{{ Currency::format($amount_paid) }}')
        ->edit_column('created_at', '<?php 
            $date = DateTime::createFromFormat("Y-m-d H:i:s", $created_at);
            echo $date->format("d/m/Y"); 
        ?>')
        ->edit_column('updated_at', '<?php 
            $date = DateTime::createFromFormat("Y-m-d H:i:s", $updated_at);
            echo $date->format("d/m/Y"); 
        ?>')
        ->make();   
    }
}
