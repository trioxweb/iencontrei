<?php

class AdminPointsController extends AdminController {


    /**
     * Point Model
     * @var Point
     */
    protected $point;

    /**
     * Inject the models.
     * @param Point $point
     */
    public function __construct(Point $point)
    {
        parent::__construct();
        $this->point = $point;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        // Grab all the points
        $points = $this->point;

        // Show the page
        return View::make('admin/points/index', compact('points'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {
        $title = Lang::get('admin/points.create');

        $mode = 'create';

		// Show the page
		return View::make('admin/points/create_edit', compact('title', 'mode'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate()
    {
        // Declare the rules for the form validation
        $rules = array(
            'username'   => 'required|exists:users',
            'value'      => 'required|numeric',
            'status'     => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes())
        {
            $user = User::where('username', '=', Input::get('username'))->first();

            $point = new Point;
            $point->value  = Input::get('value');
            $point->status = Input::get('status');
            if($user->point()->save($point)){
                 return Redirect::to('admin/points/' . $point->id . '/edit')->with('success', Lang::get('admin/points/messages.create.success'));
            }

            return Redirect::to('admin/points/create')->with('error', Lang::get('admin/points/messages.create.error'));
        }

        return Redirect::to('admin/points/create')->withInput()->withErrors($validator);
    }

    /**
     * Display the specified resource.
     *
     * @param $point
     * @return Response
     */
    public function getShow($point)
    {
        // redirect to the frontend
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $point
     * @return Response
     */
    public function getEdit($point)
    {
        if ($point->id)
        {
            // Title
            $title = Lang::get('admin/points.point_edit');
            // mode
            $mode = 'edit';

            return View::make('admin/points/create_edit', compact('point', 'mode', 'title'));
        }

        return Redirect::to('admin/points')->with('error', Lang::get('admin/users/messages.does_not_exist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $point
     * @return Response
     */
    public function postEdit($point)
    {
        // Declare the rules for the form validation
        $rules = array(
            'username'   => 'required|exists:users',
            'value'      => 'required|numeric'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes())
        {
            $user = User::where('username', '=', Input::get('username'))->first();

            $point->value = Input::get('value');
            $point->status = Input::get('status');

            if($user->point()->save($point)){
                 return Redirect::to('admin/points/' . $point->id . '/edit')->with('success', Lang::get('admin/points/messages.create.success'));
            }
            return Redirect::to('admin/points/' . $point->id . '/edit')->with('error', Lang::get('admin/points/messages.create.error'));
        }

        return Redirect::to('admin/points/' . $point->id . '/edit')->withInput()->withErrors($validator);
    }

    /**
     * Remove the specified point from storage.
     *
     * @param $point
     * @return Response
     */
    public function getDelete($point)
    {

        $id = $point->id;
        $point->delete();

        // Was the blog point deleted?
        $point = Point::find($id);
        if(empty($point))
        {
            // Redirect to the blog posts management page
            return Redirect::to('admin/points')->with('success', Lang::get('admin/points/messages.delete.success'));
        }
        // There was a problem deleting the blog point
        return Redirect::to('admin/points')->with('error', Lang::get('admin/points/messages.delete.error'));
    }

    /**
     * Show a list of all the points formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData()
    {
        $points = Point::leftjoin('users', 'users.id', '=', 'points.user_id')
        ->select(array('points.id as id', 'users.username','points.value', 'points.created_at', 'points.status'))
        ->orderby('points.created_at', 'DESC');

        return Datatables::of($points)
        ->edit_column('status', '@if($status) Ativo @else Desativado @endif')
        ->edit_column('created_at', '<?php 
            $date = DateTime::createFromFormat("Y-m-d H:i:s", $created_at);
            echo $date->format("d/m/Y"); 
        ?>')
        ->add_column('actions', '
            <a href="{{{ URL::to(\'admin/points/\' . $id . \'/edit\' ) }}}" class="iframe btn btn-default btn-sm">{{{ Lang::get(\'button.edit\') }}}</a>
            <a href="#modal-dialog" data-toggle="modal" data-href="{{{ URL::to(\'admin/points/\' . $id . \'/delete\' ) }}}" class="btn btn-danger btn-sm">{{{ Lang::get(\'button.delete\') }}}</a>
        ')
        ->make();   
    }
}
