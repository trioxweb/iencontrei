<?php

class AdminUsersController extends AdminController {


    /**
     * User Model
     * @var User
     */
    protected $user;

    /**
     * Role Model
     * @var Role
     */
    protected $role;

    /**
     * Permission Model
     * @var Permission
     */
    protected $permission;

    /**
     * Inject the models.
     * @param User $user
     * @param Role $role
     * @param Permission $permission
     */
    public function __construct(User $user, Role $role, Permission $permission)
    {
        parent::__construct();
        $this->user = $user;
        $this->role = $role;
        $this->permission = $permission;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        // Grab all the users
        $users = $this->user;

        // Show the page
        return View::make('admin/users/index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {
        // All roles
        $roles = $this->role->all();

        // Get all the available permissions
        $permissions = $this->permission->all();

        // Selected groups
        $selectedRoles = Input::old('roles', array());

        // Selected permissions
        $selectedPermissions = Input::old('permissions', array());

		// Title
		$title = Lang::get('admin/users/title.create_a_new_user');

		// Mode
		$mode = 'create';

		// Show the page
		return View::make('admin/users/create_edit', compact('roles', 'permissions', 'selectedRoles', 'selectedPermissions', 'title', 'mode'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate()
    {
        $this->user->username = Input::get( 'username' );
        $this->user->email = Input::get( 'email' );
        $this->user->password = Input::get( 'password' );

        // The password confirmation will be removed from model
        // before saving. This field will be used in Ardent's
        // auto validation.
        $this->user->password_confirmation = Input::get( 'password_confirmation' );
        $this->user->confirmed = Input::get( 'confirm' );

        // Permissions are currently tied to roles. Can't do this yet.
        //$user->permissions = $user->roles()->preparePermissionsForSave(Input::get( 'permissions' ));

        // Save if valid. Password field will be hashed before save
        $this->user->save();

        if ( $this->user->id )
        {
            // Save roles. Handles updating.
            $this->user->saveRoles(Input::get( 'roles' ));

            // Redirect to the new user page
            return Redirect::to('admin/users/' . $this->user->id . '/edit')->with('success', Lang::get('admin/users/messages.create.success'));
        }
        else
        {
            // Get validation errors (see Ardent package)
            $error = $this->user->errors()->all();

            return Redirect::to('admin/users/create')
                ->withInput(Input::except('password'))
                ->with( 'error', $error );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $user
     * @return Response
     */
    public function getShow($user)
    {
        // redirect to the frontend
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $user
     * @return Response
     */
    public function getEdit($user)
    {
        if ( $user->id )
        {
            $roles = $this->role->all();
            $permissions = $this->permission->all();

            // Title
        	$title = Lang::get('admin/register.edit_user');
        	// mode
        	$mode = 'edit';

        	return View::make('admin/users/create_edit', compact('user', 'roles', 'permissions', 'title', 'mode'));
        }
        else
        {
            return Redirect::to('admin/users')->with('error', Lang::get('admin/users/messages.does_not_exist'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $user
     * @return Response
     */
    public function postEdit($user)
    {
        if(!Input::get('cpf') && !Input::get('cnpj') && !Input::get('anydocument')){
            return Redirect::to('admin/users/' . $user->id . '/edit')
                        ->withInput(Input::except('password','password_confirmation'))
                        ->with('error', Lang::get('admin/users/messages.document_not_be_blank'));
        }
        
        // Validate the inputs
        $validator = Validator::make(Input::all(), $user->getRulesAllUpdate());

        if ($validator->passes())
        {
            $oldUser = clone $user;
            $user->username = Input::get( 'username' );
            $user->email = Input::get( 'email' );
            $user->confirmed = Input::get( 'confirm' );

            $password = Input::get( 'password' );
            $passwordConfirmation = Input::get( 'password_confirmation' );

            if(!empty($password)) {
                if($password === $passwordConfirmation) {
                    $user->password = $password;
                    // The password confirmation will be removed from model
                    // before saving. This field will be used in Ardent's
                    // auto validation.
                    $user->password_confirmation = $passwordConfirmation;
                    $user->password_md5 = md5($password);
                } else {
                    // Redirect to the new user page
                    return Redirect::to('admin/users/' . $user->id . '/edit')->with('error', Lang::get('admin/users/messages.password_does_not_match'));
                }
            } else {
                unset($user->password);
                unset($user->password_confirmation);
            }
            
            if($user->confirmed == null) {
                $user->confirmed = $oldUser->confirmed;
            }

            $user->prepareRules($oldUser, $user);

            // Save if valid. Password field will be hashed before save
            $user->amend();

            if(!$user->mlm->network_in){
                $user->mlm->parent = (User::where('username', '=', Input::get('parent'))->first()) ? User::where('username', '=', Input::get('parent'))->first()->id : null; 
                $user->mlm->side_network = (Input::get('side_network') > 0) ? Input::get('side_network') : null;
            }
            
            $user->mlm->network_plan_id = (Input::get('network_plan_id') > 0) ? Input::get('network_plan_id') : null;
            $user->mlm->network_title_id = (Input::get('network_title_id') > 0) ? Input::get('network_title_id') : null;

            //Save Other Informations
            $user->data->firstname = Input::get('firstname');
            $user->data->lastname  = Input::get('lastname');
            $user->data->document_type  = 'all';
            $user->data->document  = serialize(array(
                'anydocument' => Input::get('anydocument'),
                'cpf'  => Input::get('cpf'),
                'cnpj' => Input::get('cnpj'),
                'ins'  => Input::get('ins'),
                'razaosocial' => Input::get('razaosocial'),
            ));

            $telephones = [];
            foreach (Input::get('telephones', []) as $key => $telephone) {
                if($telephone[0] != '' || $telephone[1] != ''){
                    $telephones[] = $telephone;
                }
            }
            $user->data->telephone = serialize($telephones);

            if(!$user->address()->first()){
                $user->address()->save(new UserAddress);
            }

            $user->address->address   = Input::get('address');
            $user->address->address_2 = Input::get('address_2');
            $user->address->city      = Input::get('city');
            $user->address->postcode  = Input::get('postcode');
            $user->address->country   = Input::get('country');
            $user->address->zone      = Input::get('zone');

            if(!$user->bank()->first()){
                $user->bank()->save(new UserBank);
            }

            $user->bank->bank_name      = Input::get('bank_name');
            $user->bank->agency         = Input::get('agency');
            $user->bank->account_number = Input::get('account_number');
            $user->bank->account_type   = Input::get('account_type');
            $user->bank->titular_name   = Input::get('titular_name');
            $user->bank->titular_cpf    = Input::get('titular_cpf');
            $user->bank->nib            = Input::get('nib');
            $user->bank->iban           = Input::get('iban');
            $user->bank->swift          = Input::get('swift');
            $user->bank->skrill         = Input::get('skrill');
            $user->bank->paypal         = Input::get('paypal');
            $user->bank->pagseguro      = Input::get('pagseguro');

            if(!$user->mlm->save() || !$user->data->save() || !$user->address->save() || !$user->bank->save()){
                return Redirect::to('admin/users/' . $user->id . '/edit')->with('error', Lang::get('admin/users/messages.edit.error'));
            }
        } else {
            return Redirect::to('admin/users/' . $user->id . '/edit')->withInput()->withErrors($validator);
        }

        // Get validation errors (see Ardent package)
        $error = $user->errors()->all();

        if(empty($error)) {
            if(Setting::getSetting('has_shop')){
                UserMlm::editUserInOpencart($user);
            }
            
            // Redirect to the new user page
            return Redirect::to('admin/users/' . $user->id . '/edit')->with('success', Lang::get('admin/users/messages.edit.success'));
        } else {
            return Redirect::to('admin/users/' . $user->id . '/edit')->with('error', Lang::get('admin/users/messages.edit.error'));
        }
    }

    /**
     * Remove the specified user from storage.
     *
     * @param $user
     * @return Response
     */
    public function getDelete($user)
    {
        // Check if we are not trying to delete ourselves
        if ($user->id === Confide::user()->id || $user->mlm->network_in)
        {
            // Redirect to the user management page
            return Redirect::to('admin/users')->with('error', Lang::get('admin/users/messages.delete.impossible'));
        }

        AssignedRoles::where('user_id', $user->id)->delete();

        $id = $user->id;
        $user->delete();

        // Was the comment post deleted?
        $user = User::find($id);
        if ( empty($user) )
        {
            // TODO needs to delete all of that user's content
            return Redirect::to('admin/users')->with('success', Lang::get('admin/users/messages.delete.success'));
        }
        else
        {
            // There was a problem deleting the user
            return Redirect::to('admin/users')->with('error', Lang::get('admin/users/messages.delete.error'));
        }
    }

    /**
     * Access the specified user.
     *
     * @param $user
     * @return Response
     */
    public function getAccess($user)
    {
        if($user->hasRole('super_admin') && !Auth::user()->hasRole('super_admin')){
            return Redirect::to('admin/users')->with('error', Lang::get('admin/users/messages.access.error'));
        }

        Auth::login($user, true);
        return Redirect::action('OfficeDashboardController@getIndex');
    }

    /**
     * Position, Approve and PayBonus
     *
     * @param $user
     * @return Response
     */
    public function getAllmlm($user, $order = null)
    {
        UserMlm::definePlanPositionApprovePayBonus($user, $order);

        $user = User::find($user->id);

        if(!$user->mlm->network_in){
            return Redirect::action('AdminUsersController@getIndex')->with('error', Lang::get('admin/users/messages.position.impossible'));
        }elseif(!$user->mlm->isActiveContract($user->id)){
            return Redirect::action(($order) ? 'AdminOrdersController@getIndex' : 'AdminUsersController@getIndex')->with('error', Lang::get('admin/users/messages.approve.error'));
        }elseif(!$user->mlm->bonus){
            return Redirect::action(($order) ? 'AdminOrdersController@getIndex' : 'AdminUsersController@getIndex')->with('error', Lang::get('admin/users/messages.paybonus.error'));
        }

        return Redirect::action(($order) ? 'AdminOrdersController@getIndex' : 'AdminUsersController@getIndex')->with('success', Lang::get('admin/users/messages.allmlm.success'));
    }

    /**
     * Position the specified user in Network.
     *
     * @param $user
     * @return Response
     */
    public function getPosition($user)
    {
        if(Network::position($user->id, $user->mlm->parent, $user->mlm->getSide())){
            return Redirect::action('AdminUsersController@getIndex')->with('success', Lang::get('admin/users/messages.position.success'));
        }

        return Redirect::action('AdminUsersController@getIndex')->with('error', Lang::get('admin/users/messages.position.impossible'));
    }

    /**
     * Position the specified user in Network.
     *
     * @param $user
     * @param Order $order
     * @return Response
     */
    public function getPaybonus($user, $order = null)
    {
        if($order)
        {
            if($order_plan = $order->order_plan()->first()){
                $user->mlm->network_plan_id = $order_plan->plan_id;
                $user->mlm->save();
            }
        }
        if($user->mlm->network_plan_id != null){
            if(UserMlm::payBonus($user)){
                return Redirect::action(($order) ? 'AdminOrdersController@getIndex' : 'AdminUsersController@getIndex')->with('success', Lang::get('admin/users/messages.paybonus.success'));
            }
        }

        return Redirect::action(($order) ? 'AdminOrdersController@getIndex' : 'AdminUsersController@getIndex')->with('error', Lang::get('admin/users/messages.paybonus.error'));
    }

    /**
     * Position the specified user in Network.
     *
     * @param $user
     * @param Order $order
     * @return Response
     */
    public function getApprove($user, $order = null)
    {
        if($order)
        {
            if($order_plan = $order->order_plan()->first()){
                $user->mlm->network_plan_id = $order_plan->plan_id;
                $user->mlm->save();
            }
        }
        if($user->mlm->network_plan_id != null){
            if(UserMlm::approve($user->id)){
                return Redirect::action(($order) ? 'AdminOrdersController@getIndex' : 'AdminUsersController@getIndex')->with('success', Lang::get('admin/users/messages.approve.success'));
            }
        }
        

        return Redirect::action(($order) ? 'AdminOrdersController@getIndex' : 'AdminUsersController@getIndex')->with('error', Lang::get('admin/users/messages.approve.error'));
    }

    /**
     * Active the specified user in Network.
     *
     * @param $user
     * @param Order $order
     * @return Response
     */
    public function getActive($user, $order = null)
    {
        if(UserMlm::active($user->id)){
            return Redirect::action(($order) ? 'AdminOrdersController@getIndex' : 'AdminUsersController@getIndex')->with('success', Lang::get('admin/users/messages.active.success'));
        }

        return Redirect::action(($order) ? 'AdminOrdersController@getIndex' : 'AdminUsersController@getIndex')->with('error', Lang::get('admin/users/messages.active.error'));
    }

    /**
     * Upgrade the specified user in Network.
     *
     * @param $user
     * @param Order $order
     * @return Response
     */
    public function getUpgrade($user, $order = null)
    {
        if($order){
            if($order_plan = $order->order_plan_upgrade()->first()){
                if(UserMlm::upgrade($user, $order_plan->plan_upgrade_id)){
                    $order->updateHistoryStatus(6);
                    return Redirect::action(($order) ? 'AdminOrdersController@getIndex' : 'AdminUsersController@getIndex')->with('success', Lang::get('admin/users/messages.upgrade.success'));
                }
            }
        }

        return Redirect::action(($order) ? 'AdminOrdersController@getIndex' : 'AdminUsersController@getIndex')->with('error', Lang::get('admin/users/messages.upgrade.error'));
    }

    /**
     * Show a list of all the users formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData()
    {
        $users = User::leftjoin('users_data', 'users_data.user_id', '=', 'users.id')
                    ->join('users_mlm', 'users_mlm.user_id', '=', 'users.id')
                    ->leftjoin('plans', 'plans.id', '=', 'users_mlm.network_plan_id')
                    ->select(array('users.id', 'users.username', 'users_data.firstname', 'users.email', 'users.created_at', 'users_mlm.parent as parent','plans.name as plan', 'users_mlm.network_in', 'users_mlm.bonus', 'users_mlm.side_network as side'))
                    ->where('users.id', '!=', (Auth::user()->hasRole('admin')) ? 1 : 0);

        return Datatables::of($users)
        ->edit_column('side', '@if($side == 1) Esquerdo @elseif($side == 2) Direito @else Definido pelo Patrocinador @endif')
        ->add_column('status', '@if($network_in) Posicionado @else Pendente @endif')
        ->add_column('actions', '
            <?php $userMlm = UserMlm::where("user_id", $id)->first(); ?>

            @if((!$network_in && $userMlm->contractNeedPay($id) && !$bonus) && isset($plan))
                <a style="margin-bottom:5px;" href="#modal-dialog" data-toggle="modal" data-href="{{{ URL::to(\'admin/users/\' . $id . \'/allmlm\' ) }}}" class="btn btn-green btn-sm">{{{ Lang::get(\'button.position_approve_and_pay_bonus\') }}}</a><br> 
            @endif

            @if(!$network_in)  
                <a style="margin-bottom:5px;" href="#modal-dialog" data-toggle="modal" data-href="{{{ URL::to(\'admin/users/\' . $id . \'/position\' ) }}}" class="btn btn-green btn-sm">{{{ Lang::get(\'button.position_in_network\') }}}</a><br> 
            @elseif($userMlm->contractNeedPay($id) && isset($plan))
                <a style="margin-bottom:5px;" href="#modal-dialog" data-toggle="modal" data-href="{{{ URL::to(\'admin/users/\' . $id . \'/approve\' ) }}}" class="btn btn-green btn-sm">{{{ Lang::get(\'button.aprove\') }}}</a><br>
            @elseif($userMlm->activeMonthlyNeedPay() && !$userMlm->contractNeedPay())
                <a style="margin-bottom:5px;" href="#modal-dialog" data-toggle="modal" data-href="{{{ URL::to(\'admin/users/\' . $id . \'/active/\' ) }}}" class="btn btn-green btn-sm">{{{ Lang::get(\'button.aprove_active_monthly\') }}}</a><br>
            @endif

            @if($network_in && !$bonus && !$userMlm->contractNeedPay($id))
                <a style="margin-bottom:5px;" href="#modal-dialog" data-toggle="modal" data-href="{{{ URL::to(\'admin/users/\' . $id . \'/paybonus\' ) }}}" class="btn btn-blue btn-sm">{{{ Lang::get(\'button.pay_bonus\') }}}</a><br>
            @endif

            <a style="margin-bottom:5px;" href="{{{ URL::to(\'admin/users/\' . $id . \'/edit\' ) }}}" class="btn btn-default btn-sm iframe">{{{ Lang::get(\'button.edit\') }}}</a> 
            
            @if(!$network_in)
                <a style="margin-bottom:5px;" href="#modal-dialog" data-toggle="modal" data-href="{{{ URL::to(\'admin/users/\' . $id . \'/delete\' ) }}}" class="btn btn-danger btn-sm">{{{ Lang::get(\'button.delete\') }}}</a>
            @endif

            <a style="margin-bottom:5px;" href="{{{ URL::to(\'admin/users/\' . $id . \'/access\' ) }}}" class="btn btn-info btn-sm">Acessar</a>
        ')
        ->edit_column('plan', '@if(isset($plan)) {{$plan}} @endif')
        ->edit_column('parent', '@if($parent != 0) {{ User::find($parent)->username }} @endif')
        ->remove_column('id')
        ->remove_column('network_in')
        ->remove_column('bonus')
        ->make();
    }
}
