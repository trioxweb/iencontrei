<?php

class AdminSettingsController extends AdminController{


	/**
     * Setting Model
     * @var Setting
     */
    protected $setting;

    /**
     * Inject the models.
     * @param Setting $setting
     */
    public function __construct(Setting $setting)
    {
        parent::__construct();
        $this->setting = $setting;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {

    	$minimum_withdrawal       = Setting::getSetting('minimum_withdrawal');
    	$tax_withdrawal 	      = ((float) Setting::getSetting('tax_withdrawal')) * 100;
    	$minimum_transfer_oc      = Setting::getSetting('minimum_transfer_oc');
    	$tax_transfer_oc 	      = ((float) Setting::getSetting('tax_transfer_oc')) * 100;
    	$minimum_transfer_user    = Setting::getSetting('minimum_transfer_user');
    	$tax_transfer_user        = ((float) Setting::getSetting('tax_transfer_user')) * 100;
        $tax_payment_with_balance = ((float) Setting::getSetting('tax_payment_with_balance')) * 100;

        $payment_methods = Setting::getSetting('payment_methods');

        // Show the page
        return View::make('admin/settings/index', compact('minimum_withdrawal', 'tax_withdrawal', 'minimum_transfer_oc',
        	'tax_transfer_oc', 'minimum_transfer_user', 'tax_transfer_user', 'tax_payment_with_balance', 'payment_methods'));
    }

    /**
     * Edit All Settings
     *
     * @return Redirect and Message of Success
     */
    public function postEdit()
    {
    	$rules = array(
            'minimum_withdrawal'    => 'required',
            'tax_withdrawal'		=> 'required',
            'minimum_transfer_oc'   => 'required',
            'tax_transfer_oc'		=> 'required',
            'minimum_transfer_user' => 'required',
            'tax_transfer_user'		=> 'required',
            'payment_methods'       => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->passes()){

        	$minimum_withdrawal        = Setting::where('key', 'minimum_withdrawal')->first();
        	$minimum_withdrawal->value = Input::get('minimum_withdrawal', 100);
        	$minimum_withdrawal->save();

        	$tax_withdrawal 	   = Setting::where('key', 'tax_withdrawal')->first();
        	$tax_withdrawal->value = ((double) Input::get('tax_withdrawal', 0)) / 100;
        	$tax_withdrawal->save();

        	$minimum_transfer_oc        = Setting::where('key', 'minimum_transfer_oc')->first();
        	$minimum_transfer_oc->value = Input::get('minimum_transfer_oc', '300.00');
        	$minimum_transfer_oc->save();

        	$tax_transfer_oc 	    = Setting::where('key', 'tax_transfer_oc')->first();
        	$tax_transfer_oc->value = ((double) Input::get('tax_transfer_oc', 0)) / 100;
        	$tax_transfer_oc->save();

        	$minimum_transfer_user        = Setting::where('key', 'minimum_transfer_user')->first();
        	$minimum_transfer_user->value = Input::get('minimum_transfer_user', '300.00');
        	$minimum_transfer_user->save();

        	$tax_transfer_user        = Setting::where('key', 'tax_transfer_user')->first();  
        	$tax_transfer_user->value = ((double) Input::get('tax_transfer_user', 0)) / 100;
        	$tax_transfer_user->save();      	

            $tax_payment_with_balance        = Setting::where('key', 'tax_payment_with_balance')->first();  
            $tax_payment_with_balance->value = ((double) Input::get('tax_payment_with_balance', 0)) / 100;
            $tax_payment_with_balance->save();     

            $payment_methods = Setting::where('key', 'payment_methods')->first();
            $payment_methods_arr = unserialize($payment_methods->value);
            foreach (Setting::getSetting('payment_methods') as $key => $payment_method) {
                $exist = false;
                foreach (Input::get('payment_methods') as $payment) {
                    if($payment == $key){
                        $exist = true;
                        $payment_methods_arr[$key]['status'] = 1;
                    }
                }
                if(!$exist){
                    $payment_methods_arr[$key]['status'] = 0;
                }
            }
            $payment_methods->value = serialize($payment_methods_arr);
            $payment_methods->save();

        	return Redirect::action('AdminSettingsController@getIndex')->with('success', Lang::get('admin/settings/messages.update.success'));
		}

        return Redirect::action('AdminSettingsController@getIndex')->withInput()->withErrors($validator);
    }

}