<?php

class AdminWithdrawsController extends AdminController {


    /**
     * Withdraw Model
     * @var Withdraw
     */
    protected $withdraw;

    /**
     * Inject the models.
     * @param Withdraw $withdraw
     */
    public function __construct(UserWithdraw $withdraw)
    {
        parent::__construct();
        $this->withdraw = $withdraw;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        // Grab all the withdraws
        $withdraws = $this->withdraw;

        // Show the page
        return View::make('admin/withdraws/index', compact('withdraws'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {
        $title = Lang::get('admin/withdrawal.create_withdraw');

        $mode = 'create';

		// Show the page
		return View::make('admin/withdraws/create_edit', compact('title', 'mode'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate()
    {
        // Declare the rules for the form validation
        $rules = array(
            'username'   => 'required|exists:users',
            'value'      => 'required|numeric',
            'payment_method' => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes())
        {
            $user = User::where('username', '=', Input::get('username'))->first();

            $credit = new Credit(array(
                'user_id'        => $user->id,
                'description'    => "Withdraw per ".Input::get('payment_method')." Pending",
                'value'          => '-'.Input::get('value')
            ));

            if($credit->save()){

                $withdraw = new UserWithdraw(array(
                    'credit_id'      => $credit->id,
                    'payment_method' => Input::get('payment_method'),
                    'value'          => Input::get('value')
                ));

                if($user->withdraw()->save($withdraw)){
                     return Redirect::to('admin/withdraws/' . $withdraw->id . '/edit')->with('success', Lang::get('admin/withdraws/messages.create.success'));
                }else{
                    $credit->delete();
                }
            }

            return Redirect::to('admin/withdraws/create')->with('error', Lang::get('admin/withdraws/messages.create.error'));
        }

        return Redirect::to('admin/withdraws/create')->withInput()->withErrors($validator);
    }

    /**
     * Display the specified resource.
     *
     * @param $withdraw
     * @return Response
     */
    public function getShow($withdraw)
    {
        // redirect to the frontend
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $withdraw
     * @return Response
     */
    public function getEdit($withdraw)
    {
        if ($withdraw->id)
        {
            // Title
            $title = Lang::get('admin/withdraws/title.withdraw_update');
            // mode
            $mode = 'edit';

            return View::make('admin/withdraws/create_edit', compact('withdraw', 'mode', 'title'));
        }

        return Redirect::to('admin/withdraws')->with('error', Lang::get('admin/users/messages.does_not_exist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $withdraw
     * @return Response
     */
    public function postEdit($withdraw)
    {
        $rules = array(
            'username'       => 'required|exists:users',
            'value'          => 'required|numeric',
            'payment_method' => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes())
        {
            $user = User::where('username', '=', Input::get('username'))->first();

            $withdraw->value = Input::get('value');
            $withdraw->payment_method = Input::get('payment_method');

            if($user->withdraw()->save($withdraw)){
                 return Redirect::to('admin/withdraws/' . $withdraw->id . '/edit')->with('success', Lang::get('admin/withdraws/messages.create.success'));
            }
            return Redirect::to('admin/withdraws'.$withdraw->id.'/edit')->with('error', Lang::get('admin/withdraws/messages.create.error'));
        }
        return Redirect::to('admin/withdraws/'.$withdraw->id.'/edit')->withInput()->withErrors($validator);
    }

    /**
     * Remove the specified withdraw from storage.
     *
     * @param $withdraw
     * @return Response
     */
    public function getDelete($withdraw)
    {
        return Redirect::to('admin/withdraws')->with('error', Lang::get('admin/withdraws/messages.delete.error'));
        
        $id = $withdraw->id;
        $withdraw->delete();

        // Was the blog withdraw deleted?
        $withdraw = Withdraw::find($id);
        if(empty($withdraw))
        {
            // Redirect to the blog posts management page
            return Redirect::to('admin/withdraws')->with('success', Lang::get('admin/withdraws/messages.delete.success'));
        }
        // There was a problem deleting the blog withdraw
        return Redirect::to('admin/withdraws')->with('error', Lang::get('admin/withdraws/messages.delete.error'));
    }

    /**
     * Position the specified withdraw in Network.
     *
     * @param $withdraw
     * @return Response
     */
    public function getMarkpay($withdraw)
    {
        
        if(UserWithdraw::markAsPaid($withdraw)){
            $this->withdraw = $withdraw;
            try{
                // Send Mail to User
                Mail::send('emails.status.withdraws.withdrawal_finish', ['withdraw' => $withdraw], function($message){
                    $message->subject('Pedido de Saque Completo');
                    $message->to($this->withdraw->user->email);
                });   
            }catch(Exception $exception){
                Session::flash('warning', $exception->getMessage());
            }                 
            return Redirect::to('admin/withdraws')->with('success', Lang::get('admin/withdraws/messages.mark_as_paid.success'));
        }

        return Redirect::to('admin/withdraws')->with('error', Lang::get('admin/withdraws/messages.mark_as_paid.error'));
    }

    /**
     * Show a list of all the withdraws formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function getData()
    {
        $withdraws = UserWithdraw::leftjoin('users', 'users.id', '=', 'users_withdraws.user_id')
        ->select(array('users_withdraws.id as id', 'users.username', 'users_withdraws.created_at', 'users_withdraws.value', 'users_withdraws.payment_method', 'users_withdraws.status as status'))
        ->orderby('users_withdraws.created_at', 'DESC');

        return Datatables::of($withdraws)
        ->edit_column('value', '{{ Currency::format($value) }}')
        ->edit_column('created_at', '<?php 
            $date = DateTime::createFromFormat("Y-m-d H:i:s", $created_at);
            echo $date->format("d/m/Y"); 
        ?>')
        ->edit_column('status', '@if(!$status) {{{ Lang::get(\'to_pay\') }}} @else {{{ Lang::get(\'paid\') }}} @endif')
        ->add_column('actions', '
            @if(!$status)  
                <a style="margin-bottom:5px;" href="#modal-dialog" data-toggle="modal" data-href="{{{ URL::to(\'admin/withdraws/\' . $id . \'/markpay\' ) }}}" class="btn btn-green btn-sm">{{{ Lang::get(\'button.mark_as_paid\') }}}</a>
                <a style="margin-bottom:5px;" href="{{{ URL::to(\'admin/withdraws/\' . $id . \'/edit\' ) }}}" class="iframe btn btn-default btn-sm">{{{ Lang::get(\'button.edit\') }}}</a>
            @else
                <div class="label label-info">{{{ Lang::get(\'paid\') }}}</div>
            @endif
        ')
        ->make();   
    }
}
