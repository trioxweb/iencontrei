<?php

class CronController extends \BaseController {

	private $senha = "115684";

	/*
	 * Executar todo dia 01 as 00:00
	 */
	public function getBonuses($senha=null)
	{
		if($senha != $this->senha){ echo "Senha Incorreta"; return; }

    	$bonus = new Bonus;
    	$bonus->ExecuteBonuses();
	}

	/*
	 * Executar todos os dias as 00:00
	 */	
	public function getBonusbinary($senha=null)
	{
		if($senha != $this->senha){ echo "Senha Incorreta"; return; }

		$bonus = new Bonus;
    	$bonus->BonusBinary(true);
	}

	/*
	 * Executar todo os dias as 00:00
	 */
	public function getBonusfixo($senha=null)
	{
		if($senha != $this->senha){ echo "Senha Incorreta"; return; }

		$bonus = new Bonus;
    	$bonus->BonusFixed(true);
	}

	/*
	 * Executar todo primeiro dia do mês as 00:00
	 */
	public function getBonusdivideprofit($senha=null)
	{
		if($senha != $this->senha){ echo "Senha Incorreta"; return; }

		$bonus = new Bonus;
    	$bonus->BonusDivideProfit(true);
	}

	/*
	 * Executar a cada 5 min
	 */
	public function getCarrerplan()
	{
		$bonus = new Bonus;
		$bonus->BonusCarrerPlan(true);
	}

	/*
	 * Executar a cada 5 min
	 */
	public function getImportpoints()
	{
		$this->automaticAddPointsToUsers();

		$rewards = DB::connection('shop')->table('customer_reward')->get();
		foreach ($rewards as $reward) {
			if(Point::where('reward_shop_id', $reward->customer_reward_id)->count() == 0){

				$shop_user = DB::connection('shop')->table('customer')
								->where('customer_id', $reward->customer_id)->first();
				$order = DB::connection('shop')->table('order')
								->where('order_id', $reward->order_id)->first();

				if($shop_user && $order){
					$db[] = array(
						'user_id'		      => $shop_user->user_id,
						'status'		      => 1,
						'reward_shop_id'	  => $reward->customer_reward_id,
						'value'		 		  => $reward->points,
						'created_at' 		  => $order->date_added,
						'updated_at' 		  => $order->date_added,
					);
					echo "{$shop_user->user_id} -> {$reward->customer_reward_id} | {$reward->points} <br>";
				}
			}
		}

		if(isset($db)){
			DB::table('points')->insert($db);
		}
	}

	private function automaticAddPointsToUsers(){
		$orders_actives = DB::connection('shop')->table('order')
							->where('order_status_id', 5)
							->orWhere('order_status_id', 3)
							->orWhere('order_status_id', 17)
							->select('order_id', 'customer_id')
							->get();

		foreach ($orders_actives as $order) {
			$reward_total = DB::connection('shop')->table('customer_reward')->where('order_id', $order->order_id)->count();
			if(!$reward_total){

				$reward = DB::connection('shop')->table('order_product')->where('order_id', $order->order_id)
									->select(DB::raw('sum(`reward`) as `value`'))->first();	
				$points = ($reward) ? $reward->value : 0;

				if($points > 0){
					$db[] = [
						'customer_id' => $order->customer_id,
						'order_id' 	  => $order->order_id,
						'points'	  => (int)$points,
						'description' => '',
						'date_added'  => date('Y-m-d H:i:s'),
					];
				}
			}
		}

		if(isset($db)){
			DB::connection('shop')->table('customer_reward')->insert($db);
		}
	}

	/*
	 * Executar a cada 5 min
	 */
	public function getAutomatictransferadditionalcredittocredit($save=true){

		$balances_contempled = CreditAdditional::select(DB::raw('SUM(value) as balance'), 'user_id')
			->where('status', '=', '1')
			->groupBy('user_id')
			->get();

		foreach ($balances_contempled as $balance_contempled) {
			if($balance_contempled->balance >= 500){
				$db_credit[] = array(
					'user_id' 	  => $balance_contempled->user_id, 
					'value'   	  => $balance_contempled->balance, 
					'description' => 'Transferência do Crédito Adicional',
					'created_at'  => date('Y-m-d H:i:s'), 
				);
				$db_credit_additional[] = array(
					'user_id' 	  => $balance_contempled->user_id, 
					'value'   	  => '-'.$balance_contempled->balance, 
					'description' => 'Tranferência para Créditos', 
					'created_at'  => date('Y-m-d H:i:s'),
				);
				echo "Tranferência do user {$balance_contempled->user_id} de {$balance_contempled->balance}<br>";
			}
		}

		if(isset($db_credit_additional) && $save) 
			DB::table('credits_additional')->insert($db_credit_additional);
		if(isset($db_credit) && $save) 
			DB::table('credits')->insert($db_credit);
	}

}