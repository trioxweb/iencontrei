<?php

return array(

	'plan'   => 'Plan',
	'value' => 'Cantidad',

	'plan1'  => 'Partner',
	'plan2'  => 'Adcentral',
	'plan3'	 => 'Family',
	
	'description1' => 'Adquisición de un equipo de empresario y obtener derecho a una tienda virtual para el período del contrato para la reventa de productos NEONN.',
	'description2' => 'La compra de un kit que consta de un sitio con el comercio electrónico personalizado.',
	'description3' => 'Adquisición del kit se compone de: Un sitio con el comercio electrónico personalizados y aplicaciones móviles.',
	
);
