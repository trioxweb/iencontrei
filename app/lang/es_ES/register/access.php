<?php

return array(

	'sponsor'   => 'Patrocinador',
	'side' => 'Seleccione el Side',
	'automatic' => 'Automático',
	'left' => 'Izquierda',
	'right' => 'Derecho',
	'email' => 'email',
	'confirm_email' => 'Confirmar Contraseña',
	'username' => 'Usuário',
	'password' => 'Contraseña',
	'confirm_password' => 'Confirmar Senha',
	'text_atention' => 'Confirmo que he leído el contrato',
	'done' => 'Hecho',
	'go_to_login' => 'Ir a la página de inicio de sesión',
);
