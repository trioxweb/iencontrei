<?php

return array(

	'firstname'   => 'Nombre',
	'lastname' => 'Apellido',
	'document' => 'Documento',
	'telephone' => 'Teléfono',
	'postcode' => 'CEP',
	'address' => 'Dirección',
	'address_2' => '',
	'city' => 'Ciudad',
	'zone' => 'Estado',
	'country' => 'País',

	'passport' => 'PASSPORT',
	'cpf' => 'CPF',
	'cnpj' => 'CNPJ',
	'ins' => 'Inscrição estadual',
	'razaosocial' => 'Razão Social',

	'anydocument' => 'Otro Documento (Tax Id, Passport, Etc)',

);
