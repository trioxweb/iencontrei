<?php

return array(

	'info' => 'Información',
	'alerts' => 'Alertas',
	'errors' => 'Errores',
	'success' => 'Éxito',
	'error' => 'Error',
	'please_check_the_form_below_for_errors' => 'Por favor, compruebe los errores del siguiente formulario.',

);
