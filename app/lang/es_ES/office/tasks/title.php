<?php

return array(

	'tasks'    => 'Tareas',
	'logged_in' => 'Iniciar sesión',
	'logout' => 'Desconectar',
	'shares_required' => 'Acciones necesarias',
	'shares_completed' => 'Acciones completadas',
	'message' => 'Mensaje',
	'publish' => 'Publicar',
	'login_with_facebook' => 'Ingresar con Facebook',

);