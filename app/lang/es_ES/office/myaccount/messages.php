<?php

return array(

	'side_changed_with_success' => 'Lado preferencial cambiado correctamente',
	'leave_blank_for_no_change' => 'Dejar en blanco si no quiere cambiar.',
	'recovery_pin' => 'Haga clic aquí para Eliminar/Recuperar Pin.',

	'password_does_not_match' => 'La contraseña no coinciden.',

	'data_info_changed_with_success' => 'Cambiado correctamente la información.',

	'side_changed_with_success' => 'Cambiado correctamente lado.',

	'update' => array(
		'success'   => 'Actualizado con éxito.',
		'error'     => 'Se ha producido un error de actualización.',
	),

);
