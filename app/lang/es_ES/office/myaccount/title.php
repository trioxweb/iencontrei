<?php

return array(

	'personal_data' => 'Datos de Carácter Personal',
	'access' => 'Acesso',
	'new_passwords' => 'Nuevas Contraseñas',
	'address' => 'Dirección',
	'bank_details' => 'Datos bancarios',
	'pin' => 'PIN',

);
