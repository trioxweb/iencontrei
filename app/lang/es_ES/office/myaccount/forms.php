<?php

return array(

	'firstname' => 'Primero nombre',
	'lastname' => 'Apellido',
	'document' => 'Documento',
	'telephone' => 'Teléfono',

	'username' => 'Usuario',
	'email' => 'Email',

	'new_password' => 'Nueva contraseña',
	'confirm_new_password' => 'Confirmar nueva contraseña',

	'pin' => 'PIN',
	'pin_actually' => 'PIN corriente',
	'new_pin' => 'Nuevo PIN',
	'confirm_new_pin' => 'confirmar nuevo PIN',

	'address' => 'Dirección',
	'city' => 'Ciudad',
	'zone' => 'Estado',
	'country' => 'Pais',
	'postcode' => 'Código Postal',

	'bank' => 'Banco',
	'agency' => 'Agencia',
	'account' => 'Cuenta',
	'account_type' => 'Tipo de cuenta',
	'account_titular' => 'Titular de la Cuenta',
	'titular_cpf' => 'CPF de titular',
	'nib' => 'NIB',
	'iban' => 'IBAN',
	'swift' => 'SWIFT',
	'skrill' => 'SKRILL (Email)',
	'paypal' => 'Paypal (Email)',

);
