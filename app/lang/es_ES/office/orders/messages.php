<?php

return array(

	'does_not_exist' => 'Post no existe.',

	'payment_package' => 'Paquete de membresía de pago',

	'create' => array(
		'error'   => 'Post no fue creado, por favor intente de nuevo.',
		'success' => 'Post creado correctamente.'
	),

	'update' => array(
		'error'   => 'Post no se ha actualizado, por favor intente de nuevo',
		'success' => 'Post actualizado correctamente.'
	),

	'delete' => array(
		'error'   => 'Había un problema de eliminar la entrada del blog. Por favor, vuelva a intentarlo.',
		'success' => 'La entrada en el blog se ha eliminado correctamente.'
	),

	'payment_activation' => 'Realizar pago para la activación.',
	'not_yet_pay_activation' => 'Usted no ha realizado la activación de Pago.',

	'cancel' => array(
		'success' => 'Solicitud cancelado con éxito',
		'impossible' => 'Imposible de cancelar el pedido.',
	),

	'active' => array(
		'success' => 'Solicitud ativado con éxito',
	),

	'balance' => array(
		'error' => 'Saldo insuficiente para pagar Orden.',
	),

	'find' => array(
		'error' => 'Aplicación no encontrada.',
	),	

	'pay_with_balance_required' => 'Solicitar Forma de pago debe ser "pagar con la balanza".',

);
