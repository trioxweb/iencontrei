<?php

return array(

	'chackout_active'    => 'Pedido de Activación',
	'chackout_upgrade'      => 'Pedido de actualización',
	'payment_activation_monthly' => 'Pago activa Mensual',
	'pay_active_monthly' => 'Preste Mensual Activo',	
	'payment_activation_monthly' => 'Activación Mensual Pedido',
	'payment_order_of_user' => 'El pago de solicitud de usuario',
	'finance_order_of_user' => 'Finanzas solicitud de usuario',
);
