<?php

return array(

	'title'      => 'Título del blog',
	'comments'   => '# de Comentarios',
	'created_at' => 'Creado el',
	'post_id' => 'ID del anuncio',
	'product' => 'Producto',
	'quality' => 'Calidad',
	'price' => 'Precio',
	'plan' => 'Plano',
	'no_orders_found' => 'No hay pedidos encontrados',
	'active_monthly' => 'Activos mensuales',
	'plan_upgrade' => 'El Plan de actualizaciones',

);
