<?php

return array(

	'orders'    => 'Pedido',
	'pedido_update'        => 'Editar Pedido',
	'pedido_delete'        => 'Borrar Pedido',
	'create_a_new_order'   => 'Crear un nuevo Pedido',
	'order' => 'Pedido',
	'orders_create' => 'Crear Pedido',
	'edit_order' => 'Editar Pedido',
	'print_order' => 'Imprimir Pedido',
	'payment_details' => 'Detalles de pago',
	'payment_address' => 'Pago Dirección',
	'shipping_address' => 'Dirección de entrega',
	'cancel_order' => 'Cerrar Pedido',
	'active_order_again' => 'Habilitar solicitud de nuevo',
	'back_to_all_orders' => 'Volver a todas las peticiones',

);