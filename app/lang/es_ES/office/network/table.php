<?php

return array(

	'username'    => 'Nombre de usuari',
	'name' => 'Nombre',
	'date_register' => 'Fecha de Registro',
	'plan' => 'Plan',
	'title' => 'Titulo',
	'status' => 'Estado',
	'active_to_binary' => 'Activo en binario',
	'total_of_users_directs' => 'Usuarios directos',
	'side_left' => 'Izquierda',
	'side_right' => 'Derecho',
	'title_max' => 'Título máximo logrado',
	'back_to_my_organization' => 'Volver a mi organización',
	'side' => 'Red Side',
	'manager_qualify' => 'Gerente Calificado',

);