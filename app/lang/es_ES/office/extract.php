<?php

return array(

	'date'   => 'Fecha',
	'withdrawal_loading' => 'Botín en curso',
	'withdrawal' => 'RETIRAR',
	'transfer_to_user' => 'TRANSFERIDO AL USUARIO',
	'transfer_to_shop' => 'TRANSFERIDO A TIENDA',
	'print' => 'IMPRIMIR',
	'extract' => 'Extracto',
	'your_balance' => 'Your balance is',
	'minimum_transfer' => 'Mínimo para transferir',
	'total_earnings' => 'Total de ganhos',
	
);
