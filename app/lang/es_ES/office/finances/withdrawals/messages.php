<?php

return array(

	'create' => array(
		'success'   => 'Dibujo solicitud Celebrada con éxito.',
		'error'     => 'Se produjo un error al realizar la solicitud de retiro.',
	),

);
