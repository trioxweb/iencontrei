<?php

return array(

	'create' => array(
		'success'   => 'Transferencia a compras realizadas con éxito.',
		'error'     => 'Se ha producido un error al descargar a la tienda.',
	),

);
