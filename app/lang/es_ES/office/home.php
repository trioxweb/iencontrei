<?php

return array(

	'user'   => 'Usuário',
	'ecommerce' => 'Tienda Virtual',
	'title' => 'Título',
	'active' => 'Activa',
	'days_reactive' => 'Días para reactivar',
	'total_left' => 'Total Izquierda',
	'total_right' => 'Total derecho',
	'total_receive' => 'Total para Recibir',
	'side_preference' => 'Lado preferêncial',
	'plan' => 'Plan',
	'active_true' => 'SI',
	'active_false' => 'NO',
	'accumulated_right' => 'Acumulado derecho',
	'accumulated_left' => 'Acumulada izquierda ',
	'register_link' => 'Mi registro Enlace',

	'personal_volume' => 'Volumen Personal',
	'group_volume' => 'Volumen de Grupo',
	'active_teams' => 'Equipos activos',
	'qualified_teams' => 'Los equipos clasificados',
	'growth_vg6' => 'Crecimiento por VG6',
	'volume_group_to_its_sixth_generation_in_depth' => 'Grupo de volúmenes a su sexta generación en profundidad',

	'consultants_in_your_network' => 'Consultores en su red',
	'consultants_active' => 'Consultores Activos',
	'new_consultants_this_month' => 'Nuevos consultores este mes',

	'title_max' => 'Título máxima', 
	'this_month' => 'Este mes',
	'last_month' => 'El mes pasado',
	'teams_in_your_network' => 'Equipos en su red',

	'qualified_teams_bronze' => 'Equipos Bronce',
	'qualified_teams_silver' => 'Equipos plata',
	'qualified_teams_gold' => 'Equipos Oro',
	'qualified_teams_diamond' => 'Equipos Diamond',
	
	'gains_in' => 'Las ganancias en',
	'gains_of_last_5_month' => 'Las ganancias de los últimos 5 meses',
);
