<?php

return array(

	'username' => 'Usuario',
	'firstname' => 'Nombre de pila',
	'lastname' => 'Apellido',
	'telephone' => 'Teléfono',
	'company_name' => 'Nombre de empresa',
	'company_segment' => 'Segmento Empresa',
	'company_description' => 'Descripción de la empresa',
	'company_logo' => 'Logotipo de la empresa',
	'send' => 'Enviar',	

);
