<?php

return array(

	'up_one_level'   => 'SUBIR A NIVEL',
	'back_to_top' => 'VOLVER AL PRINCIPIO',
	'back_to_end' => 'IR A LA FINAL',
	'search' => 'Búsqueda',
	'search_user' => 'Buscar Nombre de usuario',
	'binary_network' => 'Red binaria',
	'directs_network' => 'Red directa',
	
);
