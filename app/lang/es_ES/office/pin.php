<?php

return array(

	'enter_your_pin' => 'Escribir su PIN',
	'your_pin' => 'Su PIN',
	'access' => 'Acceso',
	'forget_your_pin' => '¿Olvidó su PIN? Haga clic aquí para recuperarse.',
);
