<?php

return array(

	'myaccount'   => 'Mi Cuenta',
	'myaccount_personaldata' => 'Datos Personales',
	'myaccount_bankdetails' => 'Los datos del Banco',
	'myaccount_changepassword' => 'Cambiar contraseña',
	'requests' => 'Orden',
	'myrequests' => 'Mis Pedidos',
	'myrequests_historic' => 'Historia',
	'mynetwork' => 'Mi Red',
	'mynetwork_binary' => 'Binario',
	'mynetwork_organization' => 'Organización',
	'mynetwork_direct' => 'Mis Derechos',
	'finance' => 'Financiero',
	'finance_myextract' => 'Mi extracto',
	'finance_withdrawal' => 'Retirar',
	'finance_transfer_office' => 'Subir a la Oficina',
	'finance_transfer_store' => 'Subir a la tienda',
	'dailytasks' => 'Tareas diarias',
	'register' => 'Nuevo Mensaje',
	'text_welcome' => 'Bienvenido',
	'balance' => 'Balance',
	'balance_withdrawal' => 'Retirar',
	'logout' => 'Salir',
	'support' => 'Soporte',
	'contact_us'   => 'Contáctenos',
	'warning' => 'Aviso importante!',
	'warning_binary' => 'El gobierno aun no ha dado a conocer un avance de la red binaria. Espere un momento!',
	'price_of_the_dollar' => 'Tasa de cambio del dólar',
	'tasks' => 'Tareas',

);
