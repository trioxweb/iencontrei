<?php

return array(

	'does_not_exist' => 'Post no existe.',

	'create' => array(
		'error'   => 'Post no fue creado, por favor intente de nuevo.',
		'success' => 'Post creado correctamente.'
	),

	'update' => array(
		'error'   => 'Post no se ha actualizado, por favor intente de nuevo',
		'success' => 'Post actualizado correctamente.'
	),

	'delete' => array(
		'error'   => 'Había un problema de eliminar la entrada del blog. Por favor, vuelva a intentarlo.',
		'success' => 'La entrada en el blog se ha eliminado correctamente.'
	)

);
