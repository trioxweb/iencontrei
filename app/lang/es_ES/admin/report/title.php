<?php

return array(

	'report'            => 'Informes',
	'binary'            => 'Binario',
	'title'             => 'Titulo',
	'title_historic'    => 'Título Histórico',
	'binary_report'     => 'Informe de binário',

);