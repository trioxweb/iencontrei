<?php

return array(

	'finance_resume' => 'Resumen financiero',
	'current_balance'   => 'Saldo actual',
	'receivable_balance' => 'Saldo por cobrar',
	'balance_payable' => 'Saldo a pagar',
	'approved_associated' => 'Total Aprobado Asociados',
	'total_registers_titles' => 'Entradas totales por título',
	'users_active' => 'Usuarios activos',
	'points_of_volume_in_the_month' => 'Puntos de volumen en el mes',
	'new_consultants_in_month' => 'Nuevas Consultoras en meses',
	'users_pending' => 'Usuarios pendientes',
	'total_pay' => 'Total pagado',
	'total_registers_plans' => 'Entradas totales por planos',

	
);
