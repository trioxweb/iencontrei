<?php

return array(

	'name' => 'Nombre',
	'list' => 'Registros',
	'date_register' => 'Fecha',
	'status' => 'Status',
	'actions' => 'Acciones',
	'cancel' => 'Cancelar',
	'general' => 'General',
	'account' => 'Cuenta',
	'Address' => 'Dirección',
	'bank' => 'Banco',
	'back' => 'Volver',
	'are_you_sure_about_this' => 'Está usted seguro de esto',
	'edit_user' => 'Editar Usuário'
	
);