<?php

return array(

	'minimum_withdrawal'       => 'Sacar mínimo',
	'tax_withdrawal'           => 'Tasa de Despido (%)',
	'minimum_transfer_oc'      => 'Mínimo para la transferencia a la tienda',
	'tax_transfer_oc'          => 'Tasa de transferencia para almacenar (%)',
	'minimum_transfer_user'    => 'Mínimo para el usuario para transferir',
	'tax_transfer_user'        => 'Tasa de transferencia para el usuario (%)',
	'tax_payment_with_balance' => 'Tarifa de pago con el equilibrio (%)',
	'save'                     => 'Guardar',
	'payment_methods'          => 'Formas de pago',

);
