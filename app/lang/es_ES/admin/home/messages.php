<?php

return array(

	'facebook_link_update' => array(
		'error'   => 'Erro ao Atualizar Link de Compartilhamento do Facebook!',
		'success' => 'Link de Compartilhamento do Facebook Alterado com Sucesso!'
	),

);
