<?php

return array(

	'direct_selling' => 'Premio venta directa',
	'subsidized_week' => 'Premio semana bonificada',
	'direct_indication' => 'Premio indicación directa',
	'binary_award' => 'Premio Binário',
	'residual_award' => 'Premio Residual',
	'super_residual_award' => 'Premio super residual',
	'profit_division' => 'Premio divición de lucro',
	'carrier_plan' => 'Plan de carrera',
	
);