<?php

return array(

	/* 1 */


'direct_selling' => 'Ganancias de las ventas, del 30% al 100% sobre la reventa en la tienda), siendo el pagamento de este bono después de 1 semana de la venta.',

	/* 2 */

	'subsidized_week_1' => '(Recibe un premio semanal, como ayuda de inicio rápido, por 8 semanas, después de estas 8 semanas, deja de ganar este Bono. Los 
     Bonos no ganados continúan acumulándose  en el escritorio virtual, para el momento que el hacer (dos nuevas ventas del mismo valor de su paquete)
     desbloquear el saldo, liberando hasta completar las 24 semanas, 6 meses) El contrato es anual, en otras palabras, en 6 meses, ocurre la reentrada ´pagando nuevamente y adquiriendo mãs un paquete, y recibiendo nuevamente. Después de 12 meses, adquiere el paquete la red.',



    'subsidized_week_2' => '- PREMIO SEMANA PREMIADA PAQUETE PARTNER = NO TIENE',
	'subsidized_week_3' => '- PREMIO SEMANA PREMIADA PAQUETE ADCENTRAL = U$ 20.00',
	'subsidized_week_4' => '- PREMIO SEMANA PREMIADA PAQUETE FAMILIA = U$ 100.00',
	/* 3 */

	
'direct_indication_1' => '- Ganancias de U$20.00 en el momento de la entrada de un <b>PAQUETE ADCENTRAL</b> directo.',
'direct_indication_2' => '- Ganancias de U$ 100.00 en el momento de la entrada de un <b>PACOTE FAMILY</b> direto.',
'direct_indication_3' => '(Pago hecho en Dolár en el  backoffice)',
	/* 4 */
	
	'binary_award_1' => '(Par equiparadi, Solamente par de valores y paquetes idénticos)',
	'binary_award_2' => 'EL PRIMER PAR ES CALIFICADOR, EL SISTEMA NO PAGA, Y NO ACUMULA .',
	'binary_award_3' => '- PAR BINÁRIO DEL PAQUETE ADENTRAL = U$ 20.00',
	'binary_award_4' => '- PAR BINÁRIO DEL PAQUETE FAMILY = U$ 100.00',
	

	/* 5 */

	'residual_award' => 'Es generado 2%  por nivel  hasta 5 niveles sobre el valor de ventas de productos concretizados y pagos en la tienda NEONN, considerando la red UNILEVEL.',
	/* 6 */

	'super_residual_award' => 'Es generado 5% por niveles hasta 6 niveles sobre el valor de las mensalidades (ativaciones) pagas en la red.',

	/* 7 */

	'profit_division_1' => 'El 3% de lo generado en el unilevel es destinado a los calificados:',
	'profit_division_2' => 'Título',
	'profit_division_3' => 'Porcentaje',
	'profit_division_4' => 'Rubi',
	'profit_division_5' => '32%',
	'profit_division_6' => 'Esmeralda',
	'profit_division_7' => '24%',
	'profit_division_8' => 'Diamond',
	'profit_division_9' => '15%',
	'profit_division_10' => 'Blue Diamond',
	'profit_division_11' => '10%',
	'profit_division_12' => 'Black Diamond',
	'profit_division_13' => '19%',
	'profit_division_14' => 'Neonn Presidencial',
	'profit_division_15' => '19%',
	'profit_division_16' => 'Neonn Imperial',
	'profit_division_17' => '19%',

	/* 8 */

	'carrier_plan_1' => 'Se considera cantidad de paquetes en la pierna menor del binario.',
	'carrier_plan_2' => 'Título',
	'carrier_plan_3' => 'cantidad de paquetes',
	'carrier_plan_4' => 'Premio',
	'carrier_plan_5' => 'Rubi',
	'carrier_plan_6' => '50 PAQUETES ADCENTRAL',
	'carrier_plan_7' => 'IPAD MINI',
	'carrier_plan_8' => 'Esmeralda',
	'carrier_plan_9' => '50 PACOTES FAMILY',
	'carrier_plan_10' => 'VIAJE',
	'carrier_plan_11' => 'Diamond',
	'carrier_plan_12' => '100 PACOTES FAMILY',
	'carrier_plan_13' => 'CELTA',
	'carrier_plan_14' => 'Blue Diamond',
	'carrier_plan_15' => '200 PACOTES FAMILY',
	'carrier_plan_16' => 'FIESTA',
	'carrier_plan_17' => 'Black Diamond',
	'carrier_plan_18' => '400 PACOTES FAMILY',
	'carrier_plan_19' => 'COROLLA',
	'carrier_plan_20' => 'Neonn Presidencial',
	'carrier_plan_21' => '1.000 PACOTES FAMILY',
	'carrier_plan_22' => 'FUSION',
	'carrier_plan_23' => 'Neonn Imperial',
	'carrier_plan_24' => '2.000 PACOTES FAMILY',
	'carrier_plan_25' => 'EVOQUE',


);