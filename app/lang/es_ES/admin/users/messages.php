<?php

return array(

	'already_exists'    => 'El usuario ya existe!',
	'does_not_exist'    => 'El usuario no existe.',
	'login_required'    => 'Se requiere que el campo de entrada.',
	'password_required' => 'Se requiere la contraseña.',
	'password_does_not_match' => 'Las contraseñas proporcionadas no coinciden.',

    'edit_success' => 'El usuario ha sido editado con éxito.',
    'cpf_or_cnpj_not_be_blank' => 'CPF o CNPJ no pueden estar en blanco.',
    'document_not_be_blank' => 'Documento não podem estar em branco.',

	'create' => array(
		'error'   => 'El usuario no se ha creado, por favor intente de nuevo.',
		'success' => 'El usuario ha creado correctamente.'
	),

    'edit' => array(
        'impossible' => 'No se puede editar a ti mismo.',
        'error'      => 'No era una cuestión de editar el usuario. Por favor, inténtalo de nuevo',
        'success'    => 'El usuario ha sido editado con éxito.'
    ),

    'delete' => array(
        'impossible' => 'No se puede eliminar a ti mismo.',
        'error'      => 'No era una cuestión de eliminar el usuario. Por favor, inténtalo de nuevo.',
        'success'    => 'El usuario se ha eliminado correctamente.'
    ),

     'position' => array(
        'impossible' => 'No se puede aprobar este usuario.',
        'error'      => 'Hubo un problema al aprobar este usuario.',
        'success'    => 'El usuario ha sido aprobado con éxito.'
    ),

    'approve' => array(
        'impossible' => 'No se puede aprobar este usuario.',
        'error'      => 'Hubo un problema al aprobar este usuario.',
        'success'    => 'El usuario se aprobó con éxito.'
    ),

    'paybonus' => array(
        'impossible' => 'Você não pode Pagar Bônus deste usuário.',
        'error'      => 'No se puede pagar bonificaciones este usuario.',
        'success'    => 'El usuario se pagó con éxito.'
    ),

    'active' => array(
        'success' => 'Con éxito habilitado usuario',
    ),

    'allmlm' => array(
        'impossible' => 'Puede no Position, aprobar o el pago de bonificación sin definir el plano de usuario.',
        'success'    => 'El usuario ha sido posicionado, y el patrocinador fue aprobado subvencionado con éxito.'
    ),

);
