<?php

return array(

	'first_name' => 'Primer nombre',
	'last_name'  => 'Apellido',
	'user_id'  => 'ID (identificación) de usuario',
	'username'  => 'Nombre de usuario',
	'email'      => 'Email',
	'groups'     => 'Grupos',
	'roles'     => 'Funciones',
	'activated'  => 'Activado',
	'created_at' => 'Creado el',

);
