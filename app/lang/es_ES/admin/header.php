<?php

return array(

	'signups'   => 'Registros',
	'signups_search' => 'Buscar',
	'signups_pending' => 'En espera',
	'signups_approved' => 'Aprobado',
	'signups_upgrade' => 'Upgrade',
	'signups_exclude' => 'Eliminados',
	'signups_add_on' => 'Añadir nueva',
	'finance' => 'Financiero',
	'finance_credits' => 'Créditos',
	'finance_withdrawal' => 'Saqueo',
	'finance_add_on' => 'Añadir crédito',
	'finance_transfers' => 'Transferencias',
	'fixed_bonus' => 'Bono Fijo',
	'points' => 'Puntos',
	'points_historic' => 'Historia',
	'points_binary' => 'Binário',
	'comunication' => 'Comunicación',
	'comunication_notices' => 'Advertencias',
	'text_welcome' => 'Bienvenido',
	'support' => 'Soporte',
	'logout' => 'Salir',
	'backoffice' => 'Oficina Virtual',
	'financings' => 'Financiamiento',
	'finance_gains' => 'Ganancias',
	'finance_credits_additionals' => 'Créditos de Adicionales',
	
);
