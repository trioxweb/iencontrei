<?php

return array(

	'funder' => 'Financiero',
	'funded' => 'Financiado',
	'amount_financed' => 'Monto financiado',
	'amount_paid' => 'Cantidad pagada',
	'created_at' => 'Creado',
	'updated_at' => 'Actualizado en',
	
);