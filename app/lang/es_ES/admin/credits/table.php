<?php

return array(

	'username' => 'Nombre de usuario',
	'create_at' => 'Creado em',
	'value' => 'Valor',
	'description' => 'Descripción',
	'actions' => 'Acciones',
	'total_gains' => 'Ganancias Totales',
);