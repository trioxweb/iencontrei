<?php

return array(

	'approve_transfer' => array(
		'error'   => 'No se pudo aprobar la transferencia, por favor vuelva a intentarlo.',
		'success' => 'Con éxito aprobado transferencia.'
	),

	'unapprove_transfer' => array(
		'error'   => 'Incapaz de rechazar la transferencia, por favor vuelva a intentarlo.',
		'success' => 'Traslado frunció el ceño con éxito sobre.'
	),

);