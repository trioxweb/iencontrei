<?php

return array(

	'credits' => 'Créditos',
	'description' => 'Descripción',
	'create_credit' => 'Crear Crédito',
	'create' => 'Crear',
	'divide_profit' => 'División de lucro',
	'value' => 'Valor',
	'verify_post' => 'Compruebe Publicar',
	'credit_update' => 'Actualización de Crédito',
	
);