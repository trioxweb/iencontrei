<?php

return array(

	'username' => 'Usuario',
	'plan' => 'Plan',
	'payment_method' => 'Método de pago',
	'total' => 'Total',
	'create' => 'Crear',
	'order_edit' => 'Editar Orden',
	'add_to_history' => 'Añadir Historia',
	'order_history' => 'Historial de pedidos',
	'orders' => 'Orden',
	
);