<?php

return array(

	'name' => 'Nombre',
	'created_at' => 'Creado',
	'actions' => 'Acciones',
	'username' => 'Usuario',
	'total' => 'Total',
	'payment_method' => 'Método de pago',
	'status' => 'Status',
	'no_history_found' => 'No hay pedidos encontrados',
	
);