<?php

return array(

	'does_not_exist' => 'Solicitud no existe',

	'create' => array(
		'error'   => 'La solicitud no se hizo, por favor intente de nuevo.',
		'success' => 'La solicitud creado con éxito.'
	),

	'update' => array(
		'error'   => 'El orden no se ha actualizado, por favor intente de nuevo',
		'success' => 'El actualizado correctamente solicitar.'
	),

	'delete' => array(
		'error'   => 'Hubo un problema al eliminar la solicitud. Por favor, vuelva a intentarlo.',
		'success' => 'La solicitud se ha eliminado correctamente.'
	),

	'cancel' => array(
		'success' => 'Solicitud cancelado con éxito',
	),

	'history' => array(
		'success' => 'Solicitud adicional con éxito',
	),


);