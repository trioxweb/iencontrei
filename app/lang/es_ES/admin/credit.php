<?php

return array(

	'credits' => 'Créditos',
	'description' => 'Descripción',
	'create_credit' => 'Crear Crédito',
	'create' => 'Crear',
	'divide_profit' => 'División de lucro',
	'transfers'		=> 'Transferencias',
	'extract_gains' => 'Extracto de ganancias',
	'credits_additionals' => 'Créditos adicionales',

);