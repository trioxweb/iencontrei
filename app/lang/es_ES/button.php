<?php

return array(

	'edit'   => 'Editar',
	'delete' => 'Borrar',
	'aprove' => 'Aprobar',
	'pay_bonus' => 'Preste Bonos',
	'position_in_network' => 'Posición en la Red',
	'aprove_active_monthly' => 'Aprobar Mensual Activo',
	'mark_as_paid' => 'Marca como pagado',
	'pay_active_monthly' => 'Preste Mensual Activo',
	'position_approve_and_pay_bonus' => 'Posición, Aprobar y Pagar Bono',
	'approve_transfer' => 'Aprobar Transferencia',
	'unapprove_transfer' => 'No Aprobar Transferencia',

);
