<?php

return array(

	'plan'   => 'Plano',
	'value' => 'Valor',

	'plan1'  => 'Partner',
	'plan2'  => 'Adcentral',
	'plan3'	 => 'Family',
	
	'description1' => 'Aquisição de um kit empreendedor e recebe direito a uma loja virtual pelo período do contrato para revenda dos produtos NEONN.',
	'description2' => 'Aquisição de um kit composto por um site com e-commerce personalizado.',
	'description3' => 'Aquisição do kit composto por: Um site com e-commerce personalizado e um aplicativo mobile.',

);
