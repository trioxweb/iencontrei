<?php

return array(

	'firstname'   => 'Primeiro Nome',
	'lastname' => 'Sobrenome',
	'document' => 'Documento',
	'telephone' => 'Telefone',
	'postcode' => 'CEP',
	'address' => 'Endereço',
	'address_2' => '',
	'city' => 'Cidade',
	'zone' => 'Estado',
	'country' => 'País',

	'passport' => 'PASSPORT',
	'cpf' => 'CPF',
	'cnpj' => 'CNPJ',
	'ins' => 'Inscrição estadual',
	'razaosocial' => 'Razão Social',

	'anydocument' => 'Outro Documento (Tax Id, Passport, Etc)',

);
