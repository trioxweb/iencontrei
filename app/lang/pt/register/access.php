<?php

return array(

	'sponsor'   => 'Patrocinador',
	'side' => 'Escolha o Lado',
	'automatic' => 'Automático',
	'left' => 'Esquerda',
	'right' => 'Direita',
	'email' => 'email',
	'confirm_email' => 'Confirmar Email',
	'username' => 'Usuário',
	'password' => 'Senha',
	'confirm_password' => 'Confirmar Senha',
	'text_atention' => 'Confirmo que li o contrato',
	'done' => 'Concluído',
	'go_to_login' => 'Ir para a página de login'
);
