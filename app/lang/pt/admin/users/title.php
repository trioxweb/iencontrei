<?php

return array(

	'user_management'    => 'Gerenciar usuário',
	'user_update'        => 'Atualizar um usuário',
	'user_delete'        => 'Deletar um usuário',
	'create_a_new_user'  => 'Criar um novo usuário',

);
