<?php

return array(

	'already_exists'    => 'Usuário já existe!',
	'does_not_exist'    => 'Usuário não existe.',
	'login_required'    => 'O campo de login é necessário.',
	'password_required' => 'A necessária a senha.',
	'password_does_not_match' => 'As senhas fornecidas não são iguais.',

    'edit_success' => 'Ousuário foi editado com sucesso.',
    'cpf_or_cnpj_not_be_blank' => 'CPF ou CNPJ não podem estar em branco.',
    'document_not_be_blank' => 'Algum documento deve estar preenchido.',

	'create' => array(
		'error'   => 'Usuário não foi criado, por favor, tente novamente.',
		'success' => 'Usuário criado com sucesso.'
	),

    'edit' => array(
        'impossible' => 'Você não pode editar você mesmo.',
        'error'      => 'Não foi uma questão de editar o usuário. Por favor, tente novamente',
        'success'    => 'O usuário foi editado com sucesso.'
    ),

    'delete' => array(
        'impossible' => 'Você não pode excluir-se.',
        'error'      => 'Houve um problema ao excluir o usuário. Por favor, tente novamente.',
        'success'    => 'O usuário foi excluído com sucesso.'
    ),

     'position' => array(
        'impossible' => 'Você não pode posicionar este usuário.',
        'error'      => 'Houve um problema de posicionamento deste usuário.',
        'success'    => 'O usuário foi posicionado com sucesso.'
    ),

    'approve' => array(
        'impossible' => 'Você não pode aprovar este usuário.',
        'error'      => 'Houve um problema aprovar este usuário.',
        'success'    => 'O usuário foi aprovado com sucesso.'
    ),

    'paybonus' => array(
        'impossible' => 'Você não pode pagar bônus deste usuário.',
        'error'      => 'Houve um problema pagando bônus deste usuário.',
        'success'    => 'O usuário foi pago com sucesso.'
    ),

    'active' => array(
        'success' => 'Usuário ativado com sucesso',
    ),

    'allmlm' => array(
        'impossible' => 'Você não pode Posicionar, Aprovar ou Pagar Bonus sem definir o plano do usuário.',
        'success'    => 'O usuário foi Posicionado, Aprovado e o Patrocinador foi Bonificado com sucesso.'
    ),

);
