<?php

return array(

	'first_name' => 'Primeiro Nome',
	'last_name'  => 'Sobrenome',
	'user_id'  => 'ID do usuário',
	'username'  => 'Usuário',
	'email'      => 'Email',
	'groups'     => 'Grupos',
	'roles'     => 'Funções',
	'activated'  => 'Ativado',
	'created_at' => 'Criado em',

);
