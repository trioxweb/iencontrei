<?php

return array(

	'approve_transfer' => array(
		'error'   => 'Não foi possivel aprovar a transferencia, por favor, tente novamente.',
		'success' => 'Transferencia aprovada com sucesso.'
	),

	'unapprove_transfer' => array(
		'error'   => 'Não foi possivel desaprovar a transferencia, por favor, tente novamente.',
		'success' => 'Transferencia desaprovada com sucesso.'
	),

);