<?php

return array(

	'username' => 'Usuário',
	'create_at' => 'Criado em',
	'value' => 'Valor',
	'description' => 'Descrição',
	'actions' => 'Ações',
	'total_gains' => 'Total de Ganhos',
);