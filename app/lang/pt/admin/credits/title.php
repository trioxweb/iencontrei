<?php

return array(

	'credits' => 'Créditos',
	'description' => 'Descrição',
	'create_credit' => 'Criar Crédito',
	'create' => 'Criar',
	'divide_profit' => 'Divisão de lucro',
	'value' => 'Valor',
	'verify_post' => 'Verificar Post',
	'credit_update' => 'Atualização de crédito',
	
);