<?php

return array(

	'minimum_withdrawal'       => 'Saque mínimo',
	'tax_withdrawal'           => 'Taxa de Saque (%)',
	'minimum_transfer_oc'      => 'Mínimo para Transferência para loja',
	'tax_transfer_oc'          => 'Taxa de Transferência para loja (%)',
	'minimum_transfer_user'    => 'Mínimo para Transferência para usuário',
	'tax_transfer_user'        => 'Taxa de Transferência para usuário (%)',
	'tax_payment_with_balance' => 'Taxa de pagar com saldo (%)',
	'save'                     => 'Salvar',
	'payment_methods'          => 'Meios de Pagamento',

);
