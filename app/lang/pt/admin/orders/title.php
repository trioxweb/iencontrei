<?php

return array(

	'username' => 'Usuário',
	'plan' => 'Plano',
	'payment_method' => 'Método de Pagamento',
	'total' => 'Total',
	'create' => 'Criar',
	'order_edit' => 'Editar Pedido',
	'add_to_history' => 'Adicionar Histórico',
	'order_history' => 'Histórico de Pedidos',
	'orders' => 'Pedidos',
	
);