<?php

return array(

	'name' => 'Nome',
	'created_at' => 'Criado em',
	'actions' => 'Ações',
	'username' => 'Usuário',
	'total' => 'Total',
	'payment_method' => 'Método de Pagamento',
	'status' => 'Status',
	'no_history_found' => 'Nenhum pedido encontrado',
	
);