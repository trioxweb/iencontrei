<?php

return array(

	'does_not_exist' => 'pedido não existe',

	'create' => array(
		'error'   => 'O pedido não foi criado, por favor, tente novamente.',
		'success' => 'O pedido criado com sucesso.',
	),

	'update' => array(
		'error'   => 'O pedido não foi atualizado, por favor, tente novamente',
		'success' => 'O pedido atualizado com sucesso.',
	),

	'delete' => array(
		'error'   => 'Houve um problema a exclusão do pedido. Por favor, tente novamente.',
		'success' => 'O pedido foi apagado com sucesso.',
	),

	'cancel' => array(
		'success' => 'Cancelado com sucesso',
	),

	'history' => array(
		'success' => 'Histórico adicionado com sucesso',
	),

);