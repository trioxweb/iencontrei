<?php

return array(

	'does_not_exist' => 'Post não existe',

	'create' => array(
		'error'   => 'O post não foi criado, por favor, tente novamente.',
		'success' => 'O post criado com sucesso.'
	),

	'update' => array(
		'error'   => 'O post não foi atualizado, por favor, tente novamente',
		'success' => 'O post atualizado com sucesso.'
	),

	'delete' => array(
		'error'   => 'Houve um problema a exclusão do post. Por favor, tente novamente.',
		'success' => 'O post foi apagado com sucesso.'
	)

);
