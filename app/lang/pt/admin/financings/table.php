<?php

return array(

	'funder' => 'Financiador',
	'funded' => 'Financiado',
	'amount_financed' => 'Valor financiado',
	'amount_paid' => 'Valor pago',
	'created_at' => 'Criado em',
	'updated_at' => 'Atualizado em',
	
);