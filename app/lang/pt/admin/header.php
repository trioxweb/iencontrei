<?php

return array(

	'signups'   => 'Cadastros',
	'signups_search' => 'Pesquisar',
	'signups_pending' => 'Pendentes',
	'signups_approved' => 'Aprovados',
	'signups_upgrade' => 'Upgrade',
	'signups_exclude' => 'Excluídos',
	'signups_add_on' => 'Adicionar novo',
	'finance' => 'Financeiro',
	'finance_credits' => 'Creditos',
	'finance_withdrawal' => 'Saques',
	'finance_add_on' => 'Adicionar Crédito',
	'finance_transfers' => 'Transferência entre Usuários',
	'fixed_bonus' => 'Bônus Fixo',
	'points' => 'Pontos',
	'points_historic' => 'Histórico',
	'points_binary' => 'Binário',
	'comunication' => 'Comunicação',
	'comunication_notices' => 'Avisos',
	'text_welcome' => 'Seja bem vindo',
	'support' => 'Suporte',
	'logout' => 'Sair',
	'backoffice' => 'Escritório Virtual',
	'financings' => 'Financiamentos',
	'finance_gains' => 'Ganhos',
	'finance_credits_additionals' => 'Créditos Adicionais',
	
);
