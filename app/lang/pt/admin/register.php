<?php

return array(

	'name' => 'Nome',
	'list' => 'Cadastros',
	'date_register' => 'Data',
	'status' => 'Status',
	'actions' => 'Ações',
	'cancel' => 'Cancelar',
	'are_you_sure' => 'Você tem certeza',
	'general' => 'Geral',
	'account' => 'Conta',
	'Address' => 'Endereço',
	'bank' => 'Banco',
	'back' => 'Voltar',
	'are_you_sure_about_this' => 'Você tem certeza disso',
	'edit_user' => 'Editar Usuário',
	
);