<?php

return array(

	'credits' 		=> 'Créditos',
	'description' 	=> 'Descrição',
	'create_credit' => 'Criar Crédito',
	'create' 		=> 'Criar',
	'divide_profit' => 'Divisão de lucro',
	'transfers'		=> 'Transferências',
	'extract_gains' => 'Extrato de ganhos',
	'credits_additionals' => 'Créditos adicionais',
);