<?php

return array(

	'report'            => 'Relatórios',
	'binary'            => 'Binario',
	'title'             => 'Título',
	'title_historic'    => 'Histórico de Titulos',
	'binary_report'     => 'Relatório do binário',

);