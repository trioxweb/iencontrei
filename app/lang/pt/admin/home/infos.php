<?php

return array(

	/* 1 */

	'direct_selling' => 'Ganho das Vendas, de 30% a 100% sobre revenda na loja), sendo o pagamento desse bônus após 1 semana da venda.',
	
	/* 2 */

	'subsidized_week_1' => '(Recebe um prêmio semanal, como ajuda de início rápido, por 8 semanas, após estas 8 semanas, deixa de ganhar estes Bonus. Os Bônus não ganhos continuam acumulando no escritório virtual, para o momento que ele fizer (duas novas vendas do mesmo valor do seu pacote) desbloquear o saldo, liberando até completar 24 semanas, 6 meses) O Contrato é Anual, ou seja, em 6 meses,ocorre a reentrada (pagando novamente e adquirindo mais um PACOTE, e recebendo novamente.Após 12 meses, adquire o pacote e reinicia a rede.',	
	'subsidized_week_2' => '- PRÊMIO SEMANA PREMIADA PACOTE PARTNER = NÃO TEM',
	'subsidized_week_3' => '- PRÊMIO SEMANA PREMIADA PACOTE ADCENTRAL = U$ 20.00',
	'subsidized_week_4' => '- PRÊMIO SEMANA PREMIADA PACOTE FAMILY = U$ 100.00',

	/* 3 */

	'direct_indication_1' => '- Ganho de U$ 20.00 no ato de uma entrada de um <b>PACOTE ADCENTRAL</b> direto.',
	'direct_indication_2' => '- Ganho de U$ 100.00 no ato de uma entrada de um <b>PACOTE FAMILY</b> direto.',
	'direct_indication_3' => '(Pagamento feito em Dolár no backoffice)',

	/* 4 */
	
	'binary_award_1' => '(Par equiparado, Somente par de valores e pacotes idênticos)',
	'binary_award_2' => 'O PRIMEIRO PAR É QUALIFICADOR, o sistema não paga, e não acumula.',
	'binary_award_3' => '- PAR BINÁRIO DE PACOTE ADENTRAL = U$ 20.00',
	'binary_award_4' => '- PAR BINÁRIO DE PACOTE FAMILY = U$ 100.00',

	/* 5 */

	'residual_award' => 'É gerado 2% por nível até 5 níveis sobre o valor de vendas de produtos concretizados e pagos na loja NEONN, considerando a rede UNILEVEL.',

	/* 6 */

	'super_residual_award' => 'É gerado 5% por nível até 6 níveis sobre o valor das mensalidades (ativações) pagas na rede.',

	/* 7 */

	'profit_division_1' => 'O 3% do gerado no unilevel é destinado aos qualificados:',
	'profit_division_2' => 'Título',
	'profit_division_3' => 'Porcentagem',
	'profit_division_4' => 'Rubi',
	'profit_division_5' => '32%',
	'profit_division_6' => 'Esmeralda',
	'profit_division_7' => '24%',
	'profit_division_8' => 'Diamond',
	'profit_division_9' => '15%',
	'profit_division_10' => 'Blue Diamond',
	'profit_division_11' => '10%',
	'profit_division_12' => 'Black Diamond',
	'profit_division_13' => '19%',
	'profit_division_14' => 'Neonn Presidencial',
	'profit_division_15' => '19%',
	'profit_division_16' => 'Neonn Imperial',
	'profit_division_17' => '19%',

	/* 8 */

	'carrier_plan_1' => 'Considera-se quantidade de pacotes na perna menor do binário.',
	'carrier_plan_2' => 'Título',
	'carrier_plan_3' => 'Quantidade de pacotes',
	'carrier_plan_4' => 'Prêmio',
	'carrier_plan_5' => 'Rubi',
	'carrier_plan_6' => '50 PACOTES ADCENTRAL',
	'carrier_plan_7' => 'IPAD MINI',
	'carrier_plan_8' => 'Esmeralda',
	'carrier_plan_9' => '50 PACOTES FAMILY',
	'carrier_plan_10' => 'VIAGEM',
	'carrier_plan_11' => 'Diamond',
	'carrier_plan_12' => '100 PACOTES FAMILY',
	'carrier_plan_13' => 'CELTA',
	'carrier_plan_14' => 'Blue Diamond',
	'carrier_plan_15' => '200 PACOTES FAMILY',
	'carrier_plan_16' => 'FIESTA',
	'carrier_plan_17' => 'Black Diamond',
	'carrier_plan_18' => '400 PACOTES FAMILY',
	'carrier_plan_19' => 'COROLLA',
	'carrier_plan_20' => 'Neonn Presidencial',
	'carrier_plan_21' => '1.000 PACOTES FAMILY',
	'carrier_plan_22' => 'FUSION',
	'carrier_plan_23' => 'Neonn Imperial',
	'carrier_plan_24' => '2.000 PACOTES FAMILY',
	'carrier_plan_25' => 'EVOQUE',


);