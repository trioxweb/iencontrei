<?php

return array(

	'direct_selling' => 'Prêmio venda direta',
	'subsidized_week' => 'Prêmio semana bonificada',
	'direct_indication' => 'Prêmio indicação direta',
	'binary_award' => 'Prêmio Binário',
	'residual_award' => 'Prêmio Residual',
	'super_residual_award' => 'Prêmio super residual',
	'profit_division' => 'Prêmio divisão de lucro',
	'carrier_plan' => 'Plano de carreira',
	
);