<?php

return array(

	'finance_resume' => 'Resumo Financeiro',
	'current_balance'   => 'Saldo Atual',
	'receivable_balance' => 'Saldo a receber',
	'balance_payable' => 'Saldo a pagar',
	'approved_associated' => 'Total de Associados Aprovados',
	'total_registers_titles' => 'Total de Cadastros por título',
	'users_active' => 'Usuários Ativos',
	'points_of_volume_in_the_month' => 'Volume de Pontos no Mês',
	'new_consultants_in_month' => 'Novos Consultores no Mês',
	'users_pending' => 'Usuários Pendentes',
	'total_pay' => 'Total pago',
	'total_registers_plans' => 'Total de Cadastros por planos',

	
);
