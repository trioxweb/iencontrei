<?php

return array(

	'info' => 'Informações',
	'alerts' => 'Alertas',
	'errors' => 'Erros',
	'success' => 'Sucesso',
	'error' => 'Erro',
	'please_check_the_form_below_for_errors' => 'Por favor, verifique os erros do formulário abaixo.',

);
