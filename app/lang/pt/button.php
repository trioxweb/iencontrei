<?php

return array(

	'edit'   => 'Editar',
	'delete' => 'Apagar',
	'aprove' => 'Aprovar',
	'pay_bonus' => 'Pagar Bônus',
	'position_in_network' => 'Posicionar na Rede',
	'aprove_active_monthly' => 'Aprovar Ativo Mensal',
	'mark_as_paid' => 'Marcar como pago',
	'pay_active_monthly' => 'Pagar Ativo Mensal',
	'position_approve_and_pay_bonus' => 'Posicionar, Aprovar e Pagar Bonus',
	'approve_transfer' => 'Aprovar Transferência',
	'unapprove_transfer' => 'Não Aprovar Transferência',
);
