<?php

return array(

	'username'    => 'Usuário',
	'name' => 'Nome',
	'date_register' => 'Data de Cadastro',
	'plan' => 'Plano',
	'title' => 'Título',
	'status' => 'Status',
	'active_to_binary' => 'Ativo no binario',
	'total_of_users_directs' => 'Usuários diretos',
	'side_left' => 'Lado esquerdo',
	'side_right' => 'Lado direito',
	'title_max' => 'Título máximo alcançado',
	'back_to_my_organization' => 'Voltar para a minha organização',
	'side' => 'Lado na Rede',
	'manager_qualify' => 'Gerente Qualificado',

);