<?php

return array(

	'tasks'    => 'Tarefas',
	'logged_in' => 'Fazer login',
	'logout' => 'Sair',
	'shares_required' => 'Compartilhamentos obrigatórios',
	'shares_completed' => 'Compartilhamentos concluídos',
	'message' => 'Mensagem',
	'publish' => 'Publicar',
	'login_with_facebook' => 'Login com Facebook',

);