<?php

return array(

	'side_changed_with_success' => 'Lado preferêncial alterado com sucesso',
	'leave_blank_for_no_change' => 'Deixe em branco para não alterar.',
	'recovery_pin' => 'Clique aqui para Remover/Recuperar Pin.',
	'password_does_not_match' => 'As senha não correspondem.',
	'data_info_changed_with_success' => 'Informações alteradas com sucesso.',
	'side_changed_with_success' => 'Lado alterado com sucesso.',

	'update' => array(
		'success'   => 'Atualizado com sucesso.',
		'error'     => 'Houve um erro ao atualizar.',
	),

);
