<?php

return array(

	'personal_data' => 'Dados Pessoais',
	'access' => 'Acesso',
	'new_passwords' => 'Novas Senhas',
	'address' => 'Endereço',
	'bank_details' => 'Detalhes Bancários',
	'pin' => 'PIN',

);
