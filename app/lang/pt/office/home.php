<?php

return array(

	'user'   => 'Usuário',
	'ecommerce' => 'Loja Virtual',
	'title' => 'Título',
	'active' => 'Ativo',
	'days_reactive' => 'Dias para reativar',
	'total_left' => 'Total esquerda',
	'total_right' => 'Total direita',
	'total_receive' => 'Total à receber',
	'side_preference' => 'Lado preferencial',
	'plan' => 'Plano',
	'active_true' => 'SIM',
	'active_false' => 'NÃO',
	'accumulated_right' => 'Acumulado na Direita',
	'accumulated_left' => 'Acumulado na Esquerda',
	'register_link' => 'Link de cadastro',

	'personal_volume' => 'Volume Pessoal',
	'group_volume' => 'Volume de Grupos',
	'active_teams' => 'Equipes Ativas',
	'qualified_teams' => 'Equipes Qualificadas',
	'growth_vg6' => 'Crescimento até VG6',
	'volume_group_to_its_sixth_generation_in_depth' => 'Volume de Grupo até sua sexta geração em profundidade',

	'consultants_in_your_network' => 'Consultores em sua rede',
	'consultants_active' => 'Consultores Ativos',
	'new_consultants_this_month' => 'Novos consultores neste mês',

	'title_max' => 'Titulo máximo', 
	'this_month' => 'Neste mês',
	'last_month' => 'Mês passado',
	'teams_in_your_network' => 'Equipes em sua rede',

	'qualified_teams_bronze' => 'Equipes Bronze',
	'qualified_teams_silver' => 'Equipes Prata',
	'qualified_teams_gold' => 'Equipes Ouro',
	'qualified_teams_diamond' => 'Equipes Diamante',

	'gains_in' => 'Ganhos em',
	'gains_of_last_5_month' => 'Ganhos dos ultimos 5 Meses',

);
