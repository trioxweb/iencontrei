<?php

return array(

	'chackout_active'    => 'Realizar Pedido de Ativação',
	'chackout_upgrade'      => 'Realizar Pedido de Upgrade',
	'payment_activation_monthly' => 'Pagar ativo mensal',
	'pay_active_monthly' => 'Pagar Ativo Mensal',
	'payment_activation_monthly' => 'Realizar Pedido de Ativação Mensal',
	'payment_order_of_user' => 'Pagar Pedido de Usuário',
	'finance_order_of_user' => 'Financiar Pedido de Usuário',
);
