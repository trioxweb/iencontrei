<?php

return array(

	'orders'    => 'Pedidos',
	'pedido_update'        => 'Editar Pedido',
	'pedido_delete'        => 'Deletar Pedido',
	'create_a_new_order'   => 'Criar um novo Pedido',
	'order' => 'Pedido',
	'orders_create' => 'Criar Pedido',
	'edit_order' => 'Editar Pedido',
	'print_order' => 'Imprimir Pedido',
	'payment_details' => 'Detalhes de Pagamento',
	'payment_address' => 'Endereço de Pagamento',
	'shipping_address' => 'Endereço de entrega',
	'cancel_order' => 'Cancelar Pedido',
	'active_order_again' => 'Ativar pedido de novo',
	'back_to_all_orders' => 'Voltar para todos os pedidos',

);