<?php

return array(

	'does_not_exist' => 'Pedido não existe',

	'payment_package' => 'Pagar pacote de adesão',


	'create' => array(
		'error'   => 'O pedido não foi criado, por favor, tente novamente.',
		'success' => 'O pedido criado com sucesso.'
	),

	'update' => array(
		'error'   => 'O pedido não foi atualizado, por favor, tente novamente',
		'success' => 'O pedido atualizado com sucesso.'
	),

	'delete' => array(
		'error'   => 'Houve um problema a exclusão do pedido. Por favor, tente novamente.',
		'success' => 'O pedido foi apagado com sucesso.'
	),

	'payment_activation' => 'Realizar Pagamento para Ativação.',
	'not_yet_pay_activation' => 'Você Ainda não Realizou o Pagamento da Ativação.',

	'cancel' => array(
		'success' => 'Pedido cancelado com sucesso',
		'impossible' => 'Impossivel cancelar pedido'
	),

	'active' => array(
		'success' => 'Pedido ativado com sucesso',
	),

	'balance' => array(
		'error' => 'Saldo insuficiente para Pagar Pedido.',
	),

	'find' => array(
		'error' => 'Pedido não Encontrado.',
	),

	'pay_with_balance_required' => 'Meio de Pagamento do Pedido deve ser "Pagar com Saldo".',

);
