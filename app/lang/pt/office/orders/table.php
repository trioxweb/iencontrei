<?php

return array(

	'order_n'    => 'Pedido Nº',
	'items'      => 'Itens',
	'total' 	 => 'Total',
	'created_at' => 'Adicionado em',
	'status'	 => 'Situação',
	'show_order'		   => 'Ver Pedido',
	'product' => 'Produto',
	'quality' => 'Qualidade',
	'price' => 'Preço',
	'plan' => 'Plano',
	'no_orders_found' => 'Nenhum pedido encontrado',
	'active_monthly' => 'Ativo mensal',
	'plan_upgrade' => 'Upgrade de Plano',

);
