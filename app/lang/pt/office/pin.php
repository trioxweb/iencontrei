<?php

return array(

	'enter_your_pin' => 'Digite seu Pin Abaixo',
	'your_pin' => 'Seu PIN',
	'access' => 'Acessar',
	'forget_your_pin' => 'Esqueceu seu PIN? Clique Aqui Para Recuperar.',
);
