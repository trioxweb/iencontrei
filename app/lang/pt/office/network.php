<?php

return array(

	'up_one_level'   => 'SUBIR UM NÍVEL',
	'back_to_top' => 'VOLTAR AO TOPO',
	'back_to_end' => 'IR PARA O FIM',
	'search' => 'Pesquisar',
	'search_user' => 'Pesquisar usuário',
	'binary_network' => 'Rede Binária',
	'directs_network' => 'Rede de Diretos',
	
);
