<?php

return array(

	'myaccount'   => 'Minha Conta',
	'myaccount_personaldata' => 'Dados Pessoais',
	'myaccount_bankdetails' => 'Dados Bancários',
	'myaccount_changepassword' => 'Alterar Senhas',
	'requests' => 'Pedidos',
	'myrequests' => 'Meus Pedidos',
	'myrequests_historic' => 'Histórico',
	'mynetwork' => 'Minha Rede',
	'mynetwork_binary' => 'Binário',
	'mynetwork_organization' => 'Organização',
	'mynetwork_direct' => 'Meus Diretos',
	'finance' => 'Financeiro',
	'finance_myextract' => 'Meu extrato',
	'finance_withdrawal' => 'Saque',
	'finance_transfer_office' => 'Tranferência para escritório',
	'finance_transfer_store' => 'Transferir para loja',
	'dailytasks' => 'Tarefas Diárias',
	'register' => 'Cadastrar novo',
	'text_welcome' => 'Seja bem-vindo',
	'balance' => 'Saldo',
	'balance_withdrawal' => 'Sacar',
	'logout' => 'Sair',
	'support' => 'Suporte',
	'contact_us'   => 'Entre em contato conosco',
	'warning' => 'Aviso importante!',
	'warning_binary' => 'A administração ainda não liberou a visualização da rede binária. Aguarde!',
	'price_of_the_dollar' => 'Cotação do dólar',
	'tasks' => 'Tarefas',
	
);
