<?php

return array(

	'create' => array(
		'success'   => 'Transferencia para usuário realizado com Sucesso.',
		'error'     => 'Houve um erro ao transferir para usuário.',
	),

);
