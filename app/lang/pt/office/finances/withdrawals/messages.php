<?php

return array(

	'create' => array(
		'success'   => 'Pedido de Saque Realizado com Sucesso.',
		'error'     => 'Houve um erro ao realizar pedido de saque.',
	),

);
