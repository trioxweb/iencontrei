<?php

return array(

	'create' => array(
		'success'   => 'Transferencia para loja realizada com Sucesso.',
		'error'     => 'Houve um erro ao transferir para a loja.',
	),

);
