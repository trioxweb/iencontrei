<?php

return array(

	'send' => array(
		'error'   => 'A solicitação não foi enviada, por favor, tente novamente.',
		'success' => 'A solicitação foi enviada com sucesso.'
	),
	
);
