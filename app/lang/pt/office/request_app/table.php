<?php

return array(

	'username' => 'Usuário',
	'firstname' => 'Primeiro Nome',
	'lastname' => 'Sobrenome',
	'telephone' => 'Telefone',
	'company_name' => 'Nome da empresa',
	'company_segment' => 'Segmento da empresa',
	'company_description' => 'Descrição da empresa',
	'company_logo' => 'Logomarca da empresa',
	'send' => 'Enviar',	

);
