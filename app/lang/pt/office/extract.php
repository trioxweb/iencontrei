<?php

return array(

	'date'   => 'Data',
	'withdrawal_loading' => 'Saque em andamento',
	'withdrawal' => 'SAQUE',
	'transfer_to_user' => 'TRANSFERIR PARA USUARIO',
	'transfer_to_shop' => 'TRANSFERIR PARA LOJA',
	'print' => 'IMPRIMIR',
	'extract' => 'Extrato',
	'your_balance' => 'Seu saldo atual é de',
	'minimum_transfer' => 'Mínimo para transferir',
	'total_earnings' => 'Total de ganhos',
	
);
