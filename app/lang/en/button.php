<?php

return array(

	'edit'   => 'Edit',
	'delete' => 'Delete',
	'aprove' => 'Aprove',
	'pay_bonus' => 'Pay Bonus',
	'position_in_network' => 'Position In Network',
	'aprove_active_monthly' => 'Approve Active Monthly',
	'mark_as_paid' => 'Mark as paid',
	'pay_active_monthly' => 'Pay active monthly',
	'position_approve_and_pay_bonus' => 'Position, Approve and Pay Bonus',
	'approve_transfer' => 'Approve Transfer',
	'unapprove_transfer' => 'Unapprove Transfer',
);
