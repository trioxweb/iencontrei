<?php

return array(

	'firstname' => 'First Name',
	'lastname' => 'Last Name',
	'document' => 'Document',
	'telephone' => 'Phone',
	'postcode' => 'CEP',
	'address' => 'Address',
	'address_2' => '',
	'city' => 'City',
	'zone' => 'Zone',
	'country' => 'Country',
	
	'passport' => 'PASSPORT',
	'cpf' => 'CPF',
	'cnpj' => 'CNPJ',
	'ins' => 'Inscrição estadual',
	'razaosocial' => 'Razão Social',

	'anydocument' => 'Any Document (Tax Id, Passport, Etc)',

);
