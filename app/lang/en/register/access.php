<?php

return array(

	'sponsor' => 'Sponsor',
	'side' => 'Select the Side',
	'automatic' => 'Automatic',
	'left' => 'left',
	'right' => 'Right',
	'email' => 'email',
	'confirm_email' => 'Confirm Email',
	'username' => 'Username',
	'password' => 'Password',
	'confirm_password' => 'Confirm Password',
	'text_atention' => 'I confirm that I have read the contract',
	'done' => 'Done',
	'go_to_login' => 'Go to login page',
	
);
