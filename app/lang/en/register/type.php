<?php

return array(

	'plan' => 'Plan',
	'value' => 'Value',

	'plan1'  => 'Partner',
	'plan2'  => 'Adcentral',
	'plan3'	 => 'Family',
	
	'description1' => 'Acquisition of an entrepreneur kit and get right to a virtual store for the period of the contract for resale of NEONN products.',
	'description2' => 'Purchasing a kit consisting of a site with custom e-commerce.',
	'description3' => 'Acquisition of the kit consists of: A site with custom e-commerce and mobile application.',

);
