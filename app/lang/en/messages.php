<?php

return array(

	'info' => 'Info',
	'alerts' => 'Alerts',
	'errors' => 'Errors',
	'success' => 'Success',
	'error' => 'Error',
	'please_check_the_form_below_for_errors' => 'Please, check the form below for errors.',

);
