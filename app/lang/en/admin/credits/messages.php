<?php

return array(

	'approve_transfer' => array(
		'error'   => 'Could not approve the transfer, please try again.',
		'success' => 'Successfully approved transfer.'
	),

	'unapprove_transfer' => array(
		'error'   => 'Unable to disapprove the transfer, please try again.',
		'success' => 'Transfer successfully frowned upon.'
	),

);