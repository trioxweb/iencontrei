<?php

return array(

	'credits' => 'Credit',
	'description' => 'Description',
	'create_credit' => 'Create Credit',
	'create' => 'Create',
	'divide_profit' => 'Division of profit',
	'value' => 'Value',
	'verify_post' => 'Verify Post',
	'credit_update' => 'Credit Update',
	
);