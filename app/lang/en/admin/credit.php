<?php

return array(

	'credits' => 'Credit',
	'description' => 'Description',
	'create_credit' => 'Create Credit',
	'create' => 'Create',
	'divide_profit' => 'Division of profit',
	'transfers'		=> 'Transfers',
	'extract_gains' => 'Extract Gains',
	'credits_additionals' => 'Credits Additionals',

);