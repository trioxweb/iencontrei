<?php

return array(

	'minimum_withdrawal'       => 'Minimum Withdrawal',
	'tax_withdrawal'           => 'Tax Withdrawal (%)',
	'minimum_transfer_oc'      => 'Minimum Transfer to Shop',
	'tax_transfer_oc'          => 'Tax Transfer to Shop (%)',
	'minimum_transfer_user'    => 'Minimum Transfer to user',
	'tax_transfer_user'        => 'Tax Transfer to user (%)',
	'tax_payment_with_balance' => 'Tax payment with balance (%)',
	'save'                     => 'Save',
	'payment_methods'          => 'Payment Methods',

);
