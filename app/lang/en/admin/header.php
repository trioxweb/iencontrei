<?php

return array(

	'signups' => 'Registers',
	'signups_search' => 'Search',
	'signups_pending' => 'Pending',
	'signups_approved' => 'Approved',
	'signups_upgrade' => 'Upgrade',
	'signups_exclude' => 'Deleted',
	'signups_add_on' => 'Add new',
	'finance' => 'Financial',
	'finance_credits' => 'Credits',
	'finance_withdrawal' => 'Withdrawal',
	'finance_add_on' => 'Add Credit',
	'finance_transfers' => 'Users Transfers',
	'fixed_bonus' => 'Fixed Bonus',
	'points' => 'Points',
	'points_historic' => 'History',
	'points_binary' => 'Binary',
	'comunication' => 'Communication',
	'comunication_notices' => 'Warnings',
	'text_welcome' => 'Welcome',
	'support' => 'Support',
	'logout' => 'Exit',
	'backoffice' => 'Virtual Office',
	'financings' => 'Financings',
	'finance_gains' => 'Gains',
	'finance_credits_additionals' => 'Credits Additionals',
	
);
