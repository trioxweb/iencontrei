<?php

return array(

	'finance_resume' => 'Financial Summary',
	'current_balance' => 'Current Balance',
	'receivable_balance' => 'Balance receivable',
	'balance_payable' => 'Balance payable',
	'approved_associated' => 'Total Approved Associates',
	'total_registers_titles' => 'Total Registers by title',
	'users_active' => 'Active Members',
	'points_of_volume_in_the_month' => 'Points of volume in the month',
	'new_consultants_in_month' => 'New Consultants in month',
	'users_pending' => 'Pending users',
	'total_pay' => 'Total paid',
	'total_registers_plans' => 'Total Registers by Plans',

	
);
