<?php

return array(

	'username' => 'Username',
	'plan' => 'Plan',
	'payment_method' => 'Payment Method',
	'total' => 'Total',
	'create' => 'Create',
	'order_edit' => 'Edit Order',
	'add_to_history' => 'Add to history',
	'order_history' => 'Order History',
	'orders' => 'Orders',
	
);