<?php

return array(

	'name' => 'Name',
	'created_at' => 'Create At',
	'actions' => 'Actions',
	'username' => 'Username',
	'total' => 'Total',
	'payment_method' => 'Payment Method',
	'status' => 'Status',
	'no_history_found' => 'No history Found',
	
);