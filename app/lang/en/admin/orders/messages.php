<?php

return array(

	'does_not_exist' => 'request does not exist',

	'create' => array(
		'error'   => 'The request was not made, please try again.',
		'success' => 'The successfully created request.'
	),

	'update' => array(
		'error'   => 'The order was not updated, please try again',
		'success' => 'The updated successfully request.'
	),

	'delete' => array(
		'error'   => 'There was a problem deleting the request. Please try again.',
		'success' => 'The request was successfully deleted.'
	),

	'cancel' => array(
		'success' => 'Order canceled successfully',
	),

	'history' => array(
		'success' => 'History add successfully',
	),

);