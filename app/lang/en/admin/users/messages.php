<?php

return array(

	'already_exists'    => 'User already exists!',
	'does_not_exist'    => 'User does not exist.',
	'login_required'    => 'The login field is required',
	'password_required' => 'The password is required.',
	'password_does_not_match' => 'The passwords provided do not match.',

    'edit_success' => 'The user was edited successfully.',
    'cpf_or_cnpj_not_be_blank' => 'CPF or CNPJ not be blank.',
    'document_not_be_blank' => 'Documento não podem estar em branco.',

	'create' => array(
		'error'   => 'User was not created, please try again.',
		'success' => 'User created successfully.'
	),

    'edit' => array(
        'impossible' => 'You cannot edit yourself.',
        'error'      => 'There was an issue editing the user. Please try again.',
        'success'    => 'The user was edited successfully.'
    ),

    'delete' => array(
        'impossible' => 'You cannot delete yourself.',
        'error'      => 'There was an issue deleting the user. Please try again.',
        'success'    => 'The user was deleted successfully.'
    ),

    'position' => array(
        'impossible' => 'You cannot positioning this user.',
        'error'      => 'There was an issue positioning this user.',
        'success'    => 'The user was positioned successfully.'
    ),

    'approve' => array(
        'impossible' => 'You cannot approve this user.',
        'error'      => 'There was an issue approving this user.',
        'success'    => 'The user was approved successfully.'
    ),

    'paybonus' => array(
        'impossible' => 'You cannot pay bonus this user.',
        'error'      => 'There was an issue paying bonus this user.',
        'success'    => 'The user was paid successfully.'
    ),

    'active' => array(
        'success' => 'Successfully active User',
    ),

    'allmlm' => array(
        'impossible' => 'You can not Position, approve or Pay Bonus without defining the user plan.',
        'success'    => 'The user has been positioned, and the sponsor was approved Subsidised successfully.'
    ),

);
