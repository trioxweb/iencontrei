<?php

return array(

	'direct_selling' => 'Award direct sales',
	'subsidized_week' => 'Award subsidized week',
	'direct_indication' => 'Award direct indication',
	'binary_award' => 'Award Binary',
	'residual_award' => 'Residual award',
	'super_residual_award' => 'Super residual Award',
	'profit_division' => 'Award profit sharing',
	'carrier_plan' => 'Career Path',
	
);