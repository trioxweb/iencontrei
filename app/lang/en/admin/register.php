<?php

return array(

	'name' => 'Name',
	'list' => 'Registers',
	'date_register' => 'Date',
	'status' => 'Status',
	'actions' => 'Actions',
	'cancel' => 'Cancel',
	'are_you_sure' => 'Are You Sure',
	'general' => 'General',
	'account' => 'Account',
	'Address' => 'Address',
	'bank' => 'Bank',
	'back' => 'Back',
	'are_you_sure_about_this' => 'Are you sure about this',
	'edit_user' => 'Edit User',

);
