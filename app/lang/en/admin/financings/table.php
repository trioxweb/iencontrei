<?php

return array(

	'funder' => 'Funder',
	'funded' => 'Funded',
	'amount_financed' => 'Amount Financed',
	'amount_paid' => 'Amount Paid',
	'created_at' => 'Created at',
	'updated_at' => 'Updated at',
	
);