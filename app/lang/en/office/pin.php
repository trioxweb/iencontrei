<?php

return array(

	'enter_your_pin' => 'Enter your PIN',
	'your_pin' => 'Your PIN',
	'access' => 'Access',
	'forget_your_pin' => 'Forgot your PIN? Click Here to recover.',
);
