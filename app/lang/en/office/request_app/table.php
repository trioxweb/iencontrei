<?php

return array(

	'username' => 'Username',
	'firstname' => 'First name',
	'lastname' => 'Last name',
	'telephone' => 'Telephone',
	'company_name' => 'Company name',
	'company_segment' => 'Company segment',
	'company_description' => 'Company description',
	'company_logo' => 'Company logo',
	'send' => 'Send',	

);
