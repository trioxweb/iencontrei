<?php

return array(

	'send' => array(
		'error'   => 'The request was not sent, please try again.',
		'success' => 'The request was successfully sent.'
	),
	
);
