<?php

return array(

	'date'   => 'Date',
	'withdrawal_loading' => 'Withdrawal in progress',
	'withdrawal' => 'WITHDRAWAL',
	'transfer_to_user' => 'TRANSFER TO USER',
	'transfer_to_shop' => 'TRANSFER TO SHOP',
	'print' => 'PRINT',
	'extract' => 'Extract',
	'your_balance' => 'Your balance is',
	'minimum_transfer' => 'Minimum to transfer',
	'total_earnings' => 'Total earnings',
	
);
