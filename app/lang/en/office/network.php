<?php

return array(

	'up_one_level'   => 'UP ONE LEVEL',
	'back_to_top' => 'BACK TO TOP',
	'back_to_end' => 'GO TO END',
	'search' => 'Search',
	'search_user' => 'Search Username',
	'binary_network' => 'Binary Network',
	'directs_network' => 'Directs Network',
	
);
