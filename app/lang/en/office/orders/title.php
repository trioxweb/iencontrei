<?php

return array(

	'orders'    => 'Orders',
	'pedido_update'        => 'Update Order',
	'pedido_delete'        => 'Delete Order',
	'create_a_new_order'   => 'Create a new Order',
	'order' => 'Order',
	'orders_create' => 'Create Order',
	'edit_order' => 'Edit Order',
	'print_order' => 'Print Order',
	'payment_details' => 'Payment Details',
	'payment_address' => 'Payment Address',
	'shipping_address' => 'Shipping Address',
	'cancel_order' => 'Cancel Order',
	'active_order_again' => 'Active order again',
	'back_to_all_orders' => 'Back to all orders',

);