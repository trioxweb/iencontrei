<?php

return array(

	'chackout_active'    => 'Checkout Activation',
	'chackout_upgrade'      => 'Checkout upgrade',
	'payment_activation_monthly' => 'Payment activation monthly',
	'pay_active_monthly' => 'Pay active monthly',
	'payment_activation_monthly' => 'Checkout Monthly Activation',
	'payment_order_of_user' => 'Paying User Request',
	'finance_order_of_user' => 'Finance User Request',

);
