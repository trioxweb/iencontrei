<?php

return array(

	'does_not_exist' => 'Blog post does not exist.',

	'payment_package' => 'Pay membership package',

	'create' => array(
		'error'   => 'Blog post was not created, please try again.',
		'success' => 'Blog post created successfully.'
	),

	'update' => array(
		'error'   => 'Blog post was not updated, please try again',
		'success' => 'Blog post updated successfully.'
	),

	'delete' => array(
		'error'   => 'There was an issue deleting the blog post. Please try again.',
		'success' => 'The blog post was deleted successfully.'
	),

	'payment_activation' => 'Make Payment for activation.',
	'not_yet_pay_activation' => 'You have not performed the Payment Activation.',

	'cancel' => array(
		'success' => 'Order canceled successfully',
		'impossible' => 'Impossible to cancel order.'
	),

	'active' => array(
		'success' => 'Order active successfully',
	),

	'balance' => array(
		'error' => 'Balance insufficient to Pay Order',
	),

	'find' => array(
		'error' => 'Order not found.',
	),

	'pay_with_balance_required' => 'Order Payment Method should be "Paying with Balance".',

);
