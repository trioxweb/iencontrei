<?php

return array(

	'title'      => 'Blog Title',
	'comments'   => '# of Comments',
	'created_at' => 'Created at',
	'post_id' => 'Post Id',
	'product' => 'Product',
	'quality' => 'Quality',
	'price' => 'Price',
	'plan' => 'Plan',
	'no_orders_found' => 'No orders found',
	'active_monthly' => 'Active monthly',
	'plan_upgrade' => 'Plan Upgrade',

);
