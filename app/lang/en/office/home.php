<?php

return array(

	'user' => 'User',
	'ecommerce' => 'Virtual Store',
	'title' => 'Title',
	'active' => 'Active',
	'days_reactive' => 'Days to reactivate',
	'total_left' => 'Total Left',
	'total_right' => 'Total Right',
	'total_receive' => 'Total to receive',
	'side_preference' => 'Preferential Side',
	'plan' => 'Plan',
	'active_true' => 'YES',
	'active_false' => 'NO',
	'accumulated_right' => 'Accumulated Right',
	'accumulated_left' => 'Accumulated Left',
	'register_link' => 'My Register Link',
	
	'personal_volume' => 'Personal Volume',
	'group_volume' => 'Group Volume',
	'active_teams' => 'Active Teams',
	'qualified_teams' => 'EQualified Teams',
	'growth_vg6' => 'Growth VG6',
	'volume_group_to_its_sixth_generation_in_depth' => 'Volume group to its sixth generation in depth',

	'consultants_in_your_network' => 'Consultants in your network',
	'consultants_active' => 'Consultants active',
	'new_consultants_this_month' => 'New consultants this month',

	'title_max' => 'Title maximum', 
	'this_month' => 'This Month',
	'last_month' => 'Last Month',
	'teams_in_your_network' => 'Teams in your network',

	'qualified_teams_bronze' => 'Bronze Teams',
	'qualified_teams_silver' => 'Silver Teams',
	'qualified_teams_gold' => 'Gold Teams',
	'qualified_teams_diamond' => 'Diamond Teams',

	'gains_in' => 'Gains in',
	'gains_of_last_5_month' => 'Gains of last 5 month',

);
