<?php

return array(

	'username'    => 'Username',
	'name' => 'Name',
	'date_register' => 'Date of Register',
	'plan' => 'Plan',
	'title' => 'Title',
	'status' => 'Status',
	'active_to_binary' => 'Active in binary',
	'total_of_users_directs' => 'Direct users',
	'side_left' => 'Side left',
	'side_right' => 'Side right',
	'title_max' => 'Maximum title achieved',
	'back_to_my_organization' => 'Back to my organization',
	'side' => 'Side Network',
	'manager_qualify' => 'Manager Qualify',

);