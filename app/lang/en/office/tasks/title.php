<?php

return array(

	'tasks'    => 'Tasks',
	'logged_in' => 'Login in',
	'logout' => 'Logout',
	'shares_required' => 'Shares required',
	'shares_completed' => 'Shares completed',
	'message' => 'Message',
	'publish' => 'Publish',
	'login_with_facebook' => 'Login with Facebook',

);