<?php

return array(

	'personal_data' => 'Personal Data',
	'access' => 'Access',
	'new_passwords' => 'New Password',
	'address' => 'Address',
	'bank_details' => 'Bank Details',
	'pin' => 'PIN'

);