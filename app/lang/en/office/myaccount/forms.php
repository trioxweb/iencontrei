<?php

return array(

	'firstname' => 'Firtsname',
	'lastname' => 'Lastname',
	'document' => 'Document',
	'telephone' => 'Telephone',

	'username' => 'Username',
	'email' => 'Email',

	'new_password' => 'New Password',
	'confirm_new_password' => 'Confirm new password',

	'pin' => 'PIN',
	'pin_actually' => 'PIN actually',
	'new_pin' => 'New PIN',
	'confirm_new_pin' => 'Confirm new PIN',

	'address' => 'Address',
	'city' => 'City',
	'zone' => 'Zone',
	'country' => 'Country',
	'postcode' => 'Post Code',

	'bank' => 'Bank',
	'agency' => 'Agency',
	'account' => 'Account',
	'account_type' => 'Account type',
	'account_titular' => 'Account titular',
	'titular_cpf' => 'CPF titular',
	'nib' => 'NIB',
	'iban' => 'IBAN',
	'swift' => 'SWIFT',
	'skrill' => 'SKRILL (Email)',
	'paypal' => 'Paypal (Email)',

);
