<?php

return array(

	'side_changed_with_success' => 'Preferential side successfully changed',
	'leave_blank_for_no_change' => 'Leave blank for no change.',
	'recovery_pin' => 'Click here for Remove/Recovery PIN.',

	'password_does_not_match' => 'Password does not match.',

	'data_info_changed_with_success' => 'Data info changed with success.',

	'side_changed_with_success' => 'Side changed with success.',

	'update' => array(
		'success'   => 'Updtade success.',
		'error'     => 'Update error.',
	),

);
