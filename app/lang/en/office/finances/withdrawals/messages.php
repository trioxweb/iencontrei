<?php

return array(

	'create' => array(
		'success'   => 'Drawing request Held Successfully.',
		'error'     => 'There was an error when making application for withdrawal.',
	),

);
