<?php

return array(

	'myaccount' => 'My Account',
	'myaccount_personaldata' => 'Personal Data',
	'myaccount_bankdetails' => 'Data Bank',
	'myaccount_changepassword' => 'Change Password',
	'requests' => 'Order',
	'myrequests' => 'My Orders',
	'myrequests_historic' => 'History',
	'mynetwork' => 'My Network',
	'mynetwork_binary' => 'Binary',
	'mynetwork_organization' => 'Organization',
	'mynetwork_direct' => 'My Directs',
	'finance' => 'Financial',
	'finance_myextract' => 'My statement',
	'finance_withdrawal' => 'Booty',
	'finance_transfer_office' => 'Transfer Office',
	'finance_transfer_store' => 'Transfer to shop',
	'dailytasks' => 'Daily Tasks',
	'register' => 'Register new',
	'text_welcome' => 'Welcome',
	'balance' => 'Balance',
	'balance_withdrawal' => 'Withdraw',
	'logout' => 'Exit',
	'support' => 'Support',
	'contact_us'   => 'Contact Us',
	'warning' => 'Warning important!',
	'warning_binary' => 'The administration has not yet released a preview of the binary network. Waiting!',
	'price_of_the_dollar' => 'Price of the dollar',
	'tasks' => 'Tasks',
	
);
