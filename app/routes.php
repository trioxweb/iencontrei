<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/** ------------------------------------------
 *  Route model binding
 *  ------------------------------------------
 */
Route::model('user', 'User');
Route::model('comment', 'Comment');
Route::model('post', 'Post');
Route::model('role', 'Role');
Route::model('point', 'Point');
Route::model('credit', 'Credit');
Route::model('withdraw', 'UserWithdraw');
Route::model('plan', 'Plan');
Route::model('order', 'Order');
Route::model('financing', 'Financing');

/** ------------------------------------------
 *  Route constraint patterns
 *  ------------------------------------------
 */
Route::pattern('comment', '[0-9]+');
Route::pattern('post', '[0-9]+');
Route::pattern('user', '[0-9]+');
Route::pattern('role', '[0-9]+');
Route::pattern('token', '[0-9a-z]+');
Route::pattern('point', '[0-9]+');
Route::pattern('credit', '[0-9]+');
Route::pattern('withdraw', '[0-9]+');
Route::pattern('plan', '[0-9]+');
Route::pattern('order', '[0-9]+');
Route::pattern('type', '[a-z]+');


/** ------------------------------------------
 *  Admin Routes
 *  ------------------------------------------
 */
Route::group(array('prefix' => 'admin', 'before' => 'auth'), function()
{

    /* # Comment Management
    Route::get('comments/{comment}/edit', 'AdminCommentsController@getEdit');
    Route::post('comments/{comment}/edit', 'AdminCommentsController@postEdit');
    Route::get('comments/{comment}/delete', 'AdminCommentsController@getDelete');
    Route::post('comments/{comment}/delete', 'AdminCommentsController@postDelete');
    Route::controller('comments', 'AdminCommentsController'); */

    # Manual
    Route::get('manual', function(){
        $title = Lang::get('admin/manual/title.manual');
        return View::make('admin/manual/index', compact('title'));
    });

    # Financings Management
    Route::controller('financings', 'AdminFinancingsController');

    # Blog Management
    Route::get('blogs/{post}/show', 'AdminBlogsController@getShow');
    Route::get('blogs/{post}/edit', 'AdminBlogsController@getEdit');
    Route::post('blogs/{post}/edit', 'AdminBlogsController@postEdit');
    Route::get('blogs/{post}/delete', 'AdminBlogsController@getDelete');
    Route::controller('blogs', 'AdminBlogsController');

    # Withdraw Management
    Route::get( 'plans/{plan}/show',     'AdminPlansController@getShow');
    Route::get( 'plans/{plan}/edit',   'AdminPlansController@getEdit');
    Route::post('plans/{plan}/edit',   'AdminPlansController@postEdit');
    Route::get( 'plans/{plan}/delete', 'AdminPlansController@getDelete');
    Route::controller('plans', 'AdminPlansController');

    # Withdraw Management
    Route::get('withdraws/{withdraw}/show',     'AdminWithdrawsController@getShow');
    Route::get( 'withdraws/{withdraw}/edit',   'AdminWithdrawsController@getEdit');
    Route::post('withdraws/{withdraw}/edit',   'AdminWithdrawsController@postEdit');
    Route::get( 'withdraws/{withdraw}/delete', 'AdminWithdrawsController@getDelete');
    Route::get( 'withdraws/{withdraw}/markpay', 'AdminWithdrawsController@getMarkpay');
    Route::controller('withdraws', 'AdminWithdrawsController');

    # Credit Management
    Route::get('users/{credit}/show',     'AdminCreditsController@getShow');
    Route::get( 'credits/{credit}/edit',   'AdminCreditsController@getEdit');
    Route::post('credits/{credit}/edit',   'AdminCreditsController@postEdit');
    Route::get( 'credits/{credit}/delete', 'AdminCreditsController@getDelete');
    Route::get('credits/{user_transfer_id}/transfer_approve',   'AdminCreditsController@getTransferapprove');
    Route::get( 'credits/{user_transfer_id}/transfer_unapprove', 'AdminCreditsController@getTransferunapprove');
    Route::controller('credits', 'AdminCreditsController');

    # Point Management
    Route::get('points/{point}/show',     'AdminPointsController@getShow');
    Route::get( 'points/{point}/edit',   'AdminPointsController@getEdit');
    Route::post('points/{point}/edit',   'AdminPointsController@postEdit');
    Route::get( 'points/{point}/delete', 'AdminPointsController@getDelete');
    Route::controller('points', 'AdminPointsController');

    # Report Management
    Route::controller('reports', 'AdminReportController');

    # Orders Management
    Route::get('orders/{order}/show',   'AdminOrdersController@getShow');
    Route::get('orders/{order}/edit',   'AdminOrdersController@getEdit');
    Route::post('orders/{order}/edit',  'AdminOrdersController@postEdit');
    Route::get('orders/{order}/delete', 'AdminOrdersController@getDelete');
    Route::get('orders/{order}/cancel', 'AdminOrdersController@getCancel');
    Route::get('orders/{order}/active', 'AdminOrdersController@getActive');
    Route::get('orders/{order}/history', 'AdminOrdersController@getHistory');
    Route::post('orders/{order}/history', 'AdminOrdersController@postHistory');
    Route::get('orders/{history_id}/history_delete', 'AdminOrdersController@getHistoryDelete');
    Route::controller('orders', 'AdminOrdersController');

    # User Management
    Route::get('users/{user}/show', 'AdminUsersController@getShow');
    Route::get('users/{user}/edit', 'AdminUsersController@getEdit');
    Route::post('users/{user}/edit', 'AdminUsersController@postEdit');
    Route::get('users/{user}/delete', 'AdminUsersController@getDelete');
    Route::get('users/{user}/position', 'AdminUsersController@getPosition');
    Route::get('users/{user}/paybonus/{order?}', 'AdminUsersController@getPaybonus');
    Route::get('users/{user}/approve/{order?}', 'AdminUsersController@getApprove');
    Route::get('users/{user}/active/{order?}', 'AdminUsersController@getActive');
    Route::get('users/{user}/upgrade/{order?}', 'AdminUsersController@getUpgrade');
    Route::get('users/{user}/allmlm/{order?}', 'AdminUsersController@getAllmlm');
    Route::get('users/{user}/access', 'AdminUsersController@getAccess');
    Route::controller('users', 'AdminUsersController');

    # User Role Management
    Route::get('roles/{role}/show', 'AdminRolesController@getShow');
    Route::get('roles/{role}/edit', 'AdminRolesController@getEdit');
    Route::post('roles/{role}/edit', 'AdminRolesController@postEdit');
    Route::get('roles/{role}/delete', 'AdminRolesController@getDelete');
    Route::post('roles/{role}/delete', 'AdminRolesController@postDelete');
    Route::controller('roles', 'AdminRolesController');

    # Configure Facebook Link Share
    Route::get('facebook_link', 'AdminDashboardController@getFacebooklink');
    Route::post('facebook_link', 'AdminDashboardController@postFacebooklink');

    # Settings
    Route::post('settings/edit', 'AdminSettingsController@postEdit');
    Route::controller('settings', 'AdminSettingsController');

    # Admin Dashboard
    Route::controller('/', 'AdminDashboardController');
    Route::get('charts', 'AdminDashboardController@getCharts');
});

/** ------------------------------------------
 *  Office Routes
 *  ------------------------------------------
 */
Route::group(array('prefix' => 'office', 'before' => 'auth'), function()
{
    # Tasks Office
    Route::controller('tasks', 'OfficeTasksController');


    # Orders Office Management
    Route::get('orders/{order}/show',   'OfficeOrdersController@getShow');
    Route::get('orders/{order}/edit',   'OfficeOrdersController@getEdit');
    Route::post('orders/{order}/edit',  'OfficeOrdersController@postEdit');
    Route::get('orders/{order}/delete', 'OfficeOrdersController@getDelete');
    Route::get('orders/create/{type}',  'OfficeOrdersController@getCreate');
    Route::post('orders/create/{type}', 'OfficeOrdersController@postCreate');
    Route::get('orders/{order}/cancel',  'OfficeOrdersController@getCancel');
    Route::get('orders/{order}/active',  'OfficeOrdersController@getActive');
    Route::get('orders/{order}/paywithbalance', array('as' => 'pay_with_balance', 'uses' => 'OfficeOrdersController@getPaywithbalance'));
    Route::get('orders/{order}/financewithbalance', 'OfficeOrdersController@getFinancewithbalance');
    Route::controller('orders', 'OfficeOrdersController');

    # Network Manage
    Route::controller('network', 'OfficeNetworkController');

    Route::get('network/find/{user}', function($user){
        if($user)
            Redirect::to('/network/binary/'.$user->id);
        Redirect::to('/network/binary');
    });

    # User Management
    Route::controller('account', 'OfficeUserController');

    #PIN Block Filter
    Route::group(array('before' => 'pin'), function()
    {
        # Finance Management
        Route::controller('finance', 'OfficeFinanceController');
    });

    Route::get('blog', 'BlogController@getIndex');

    # Posts - Second to last set, match slug
    Route::get('blog/{postSlug}', 'BlogController@getView');
    Route::post('blog/{postSlug}', 'BlogController@postView');

    # Office Dashboard
    Route::controller('/', 'OfficeDashboardController');
});




/** ------------------------------------------
 *  Frontend Routes
 *  ------------------------------------------
 */



//:: User Account Routes ::

# User login route
Route::post('user/login', 'UserController@postLogin');

# User reset routes
Route::get('user/reset/{token}', 'UserController@getReset');

# User password reset
Route::post('user/reset/{token}', 'UserController@postReset');

# User RESTful Routes (Login, Logout, Register, etc)
Route::controller('user', 'UserController');

//:: Application Routes ::

Route::get('/', function(){
    return Redirect::action('OfficeDashboardController@getIndex');
});

# Set Language Route
Route::get('setlang/{lang}', function($lang){
    $rules = [ 'language' => 'in:en,it,pt,en_ES' ]; //list of supported languages of your application.

    $language = Input::get('lang'); //lang is name of form select field.
    $validator = Validator::make(compact($language),$rules);

    if($validator->passes()) {
        Session::put('language',$lang);
        App::setLocale($language);
    }
    return Redirect::back();
});

# Register Route Controller
Route::controller('register', 'RegisterController');

# Cron Route Controller
Route::controller('cron', 'CronController');

# Facebook Routes
Route::post('facebook/postlink', ['as' => 'facebook_post_link', 'uses' => 'FacebookController@postLink']);
Route::get('facebook/fblogin',   ['as' => 'facebook_login',     'uses' => 'FacebookController@getFblogin']);
Route::get('facebook/login', 'FacebookController@getLogin');
Route::get('facebook/logout',    ['as' => 'facebook_logout',    'uses' => 'FacebookController@getLogout']);