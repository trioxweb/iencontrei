<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('credits_types', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
		});

		Schema::create('credits', function(Blueprint $table)
		{
			$table->increments('id');
			$table->boolean('status')->default(true);
			$table->integer('user_id')->unsigned();
			$table->integer('type_id')->unsigned()->nullable();
			$table->decimal('value',15,4);
			$table->string('description');
			$table->timestamps();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->foreign('type_id')->references('id')->on('credits_types')->onDelete('set null');
		});

		Schema::create('credits_additional', function(Blueprint $table)
		{
			$table->increments('id');
			$table->boolean('status')->default(true);
			$table->integer('user_id')->unsigned();
			$table->integer('type_id')->unsigned()->nullable();
			$table->decimal('value',15,4);
			$table->string('description');
			$table->timestamps();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->foreign('type_id')->references('id')->on('credits_types')->onDelete('set null');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('credits_additional');
		Schema::drop('credits');
		Schema::drop('credits_types');
	}

}
