<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersMlmTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_mlm', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');

			$table->integer('user_id')->unsigned();
			$table->integer('parent')->unsigned();

			$table->boolean('network_in')->default(false);

			$table->integer('network_plan_id')->nullable();
			$table->integer('network_title_id')->nullable();

			$table->dateTime('approved_at')->nullable();
			//$table->dateTime('first_buy_at')->nullable();
			$table->dateTime('contracted_at')->nullable();
			$table->dateTime('canceled_at')->nullable();

			$table->integer('overflow_network')->nullable();
			$table->enum('side_network', array(1, 2))->nullable();
			$table->enum('side_pattern_network', array(0, 1, 2))->default(0);
			
			$table->enum('active_binary', array(0, 1, 2, 3))->default(0);
			$table->boolean('manager_qualify')->default(false);
			$table->boolean('bonus')->default(false);

			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_mlm');
	}

}
