<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFinancingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('financings', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('order_id')->nullable();
			$table->integer('funder_user_id')->unsigned();
			$table->integer('funded_user_id')->unsigned();
			$table->decimal('amount_financed',15,4)->default(0);
			$table->decimal('amount_paid',15,4)->default(0);
			$table->timestamps();
			$table->foreign('funder_user_id')->references('id')->on('users')->onDelete('cascade');
			$table->foreign('funded_user_id')->references('id')->on('users')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('financings');
	}

}
