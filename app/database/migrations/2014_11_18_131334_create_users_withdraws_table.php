<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersWithdrawsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_withdraws', function(Blueprint $table)
		{
			$table->increments('id');
			$table->boolean('status')->default(false); // true for pay and false for unpay
			$table->integer('user_id')->unsigned();
			$table->integer('credit_id')->unsigned();
			$table->decimal('value',15,4);
			$table->string('payment_method', 128);
			$table->string('payment_code', 128);
			$table->timestamps();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->foreign('credit_id')->references('id')->on('credits')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_withdraws');
	}

}
