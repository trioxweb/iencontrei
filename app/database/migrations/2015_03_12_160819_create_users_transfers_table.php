<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTransfersTable extends Migration {


	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_transfers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->boolean('status')->default(false); // true for approve and false for pending
			$table->integer('user_id')->unsigned();
			$table->integer('negative_credit_id')->unsigned();
			$table->integer('credit_id')->unsigned();
			$table->timestamps();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->foreign('negative_credit_id')->references('id')->on('credits')->onDelete('cascade');
			$table->foreign('credit_id')->references('id')->on('credits')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_transfers');
	}


}
