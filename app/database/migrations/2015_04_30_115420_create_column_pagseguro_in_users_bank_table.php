<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColumnPagseguroInUsersBankTable extends Migration {

	 /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_bank', function(Blueprint $table)
        {
            $table->string('pagseguro', 128);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_bank', function(Blueprint $table)
        {
            $table->dropColumn('pagseguro');
        });
    }

}
