<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersBankTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_bank', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('user_id')->unsigned();

			$table->string('bank_name', 128);

			$table->string('agency', 32);
			$table->string('account_number', 128);
			$table->string('account_type', 32);
			$table->string('titular_name', 128);
			$table->string('titular_cpf', 32);

			$table->string('nib', 128);
			$table->string('iban', 128);
			$table->string('swift', 128);

			$table->string('skrill', 128);
			$table->string('paypal', 128);

			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_bank');
	}

}
