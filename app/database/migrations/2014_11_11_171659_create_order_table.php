<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('user_id');
			$table->string('firstname', 32);
			$table->string('lastname', 32);
			$table->string('email', 96);
			$table->string('telephone', 32);
			$table->string('payment_firstname', 32);
			$table->string('payment_lastname', 32);
			$table->string('payment_address_1', 32);
			$table->string('payment_address_2', 128);
			$table->string('payment_city', 128);
			$table->string('payment_postcode', 128);
			$table->string('payment_country', 10);
			$table->string('payment_zone', 128);
			$table->string('payment_method', 128);
			$table->string('payment_code', 128);
			$table->string('shipping_firstname', 32);
			$table->string('shipping_lastname', 32);
			$table->string('shipping_address_1', 32);
			$table->string('shipping_address_2', 128);
			$table->string('shipping_city', 128);
			$table->string('shipping_postcode', 128);
			$table->string('shipping_country', 10);
			$table->string('shipping_zone', 128);
			$table->string('shipping_method', 128);
			$table->string('shipping_code', 128);
			$table->text('comment');
			$table->decimal('total', 15, 4);
			$table->integer('order_status_id');
			$table->string('currency_code', 3);
			$table->timestamps();
		});

		Schema::create('order_total', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('order_id')->unsigned()->index();
			$table->string('code', 32);
			$table->string('title', 225);
			$table->string('text', 225);
			$table->decimal('value', 15, 4);
			$table->integer('sort_order');
			$table->foreign('order_id')->references('id')->on('order')->onDelete('cascade');
		});

		Schema::create('order_status', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('language_id');
			$table->string('name', 32);
		});

		Schema::create('order_history', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('order_id')->unsigned()->index();
			$table->integer('order_status_id');
			$table->tinyInteger('notify');
			$table->text('comment');
			$table->dateTime('created_at');
			$table->foreign('order_id')->references('id')->on('order')->onDelete('cascade');
		});

		Schema::create('order_plan', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('order_id')->unsigned()->index();
			$table->integer('plan_id');
			$table->string('name', 255);
			$table->decimal('total', 15, 4);
			$table->foreign('order_id')->references('id')->on('order')->onDelete('cascade');
		});

		Schema::create('order_plan_upgrade', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('order_id')->unsigned()->index();
			$table->integer('plan_upgrade_id');
			$table->string('name', 255);
			$table->decimal('total', 15, 4);
			$table->foreign('order_id')->references('id')->on('order')->onDelete('cascade');
		});

		Schema::create('order_active', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('order_id')->unsigned()->index();
			$table->string('name', 255);
			$table->decimal('total', 15, 4);
			$table->foreign('order_id')->references('id')->on('order')->onDelete('cascade');
		});
		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_active');
		Schema::drop('order_plan_upgrade');
		Schema::drop('order_plan');
		Schema::drop('order_history');
		Schema::drop('order_status');
		Schema::drop('order_total');
		Schema::drop('order');
	}

}
