<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatMessagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('chat_messages', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('sender_user_id')->unsigned();
			$table->integer('receiver_user_id')->unsigned();
			$table->text('message');
			$table->dateTime('read_at')->nullable();
			$table->timestamps();
			$table->foreign('sender_user_id')->references('id')->on('users')->onDelete('cascade');
			$table->foreign('receiver_user_id')->references('id')->on('users')->onDelete('cascade');
		});
		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('chat_messages');
	}

}
