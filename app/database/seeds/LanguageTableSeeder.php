<?php

class LanguageTableSeeder extends Seeder {

	public function run()
	{
		DB::table('language')->delete();

        $languages = array(
            array(
                'id'      => 1,
                'name' => 'Português',
                'code'   => 'pt',
                'locale'   => 'pt_BR.UTF-8, pt_BR, UTF-8',
                'sort_order'   => 1,
                'status'   => 1,
            ),
            array(
                'id'      => 2,
                'name' => 'English',
                'code'   => 'en',
                'locale'   => 'en_US.UTF-8,en_US,en-gb,english',
                'sort_order'   => 2,
                'status'   => 1,
            ),
            array(
                'id'      => 3,
                'name' => 'Español',
                'code'   => 'es',
                'locale'   => 'es_ES.UTF-8,es_ES,es-es,es_es,español',
                'sort_order'   => 3,
                'status'   => 1,
            ),
            array(
                'id'      => 4,
                'name' => 'Italiano',
                'code'   => 'it',
                'locale'   => 'it_IT.UTF-8,it_IT,italian',
                'sort_order'   => 4,
                'status'   => 0,
            ),
        );

        DB::table('language')->insert( $languages );
	}

}