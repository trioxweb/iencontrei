<?php

class OrderStatusTableSeeder extends Seeder {

    public function run()
    {
        DB::table('order_status')->delete();


        $order_status = array(
            array(
                'id'     => 1,
                'language_id'   => 1,
                'name'  => 'Aguardando Pagamento',
            ),
            array(
                'id'     => 2,
                'language_id'   => 1,
                'name'  => 'Cancelamento Revertido',
            ),
            array(
                'id'     => 3,
                'language_id'   => 1,
                'name'  => 'Processando Pagamento',
            ),
            array(
                'id'     => 4,
                'language_id'   => 1,
                'name'  => 'Pagamento Confirmado',
            ),
            array(
                'id'     => 5,
                'language_id'   => 1,
                'name'  => 'Despachado',
            ),
            array(
                'id'     => 6,
                'language_id'   => 1,
                'name'  => 'Completo',
            ),
            array(
                'id'     => 7,
                'language_id'   => 1,
                'name'  => 'Não Aprovado',
            ),
            array(
                'id'     => 8,
                'language_id'   => 1,
                'name'  => 'Anulado',
            ),
            array(
                'id'     => 9,
                'language_id'   => 1,
                'name'  => 'Cancelado',
            ),
            array(
                'id'     => 10,
                'language_id'   => 1,
                'name'  => 'Cancelado pela Operadora',
            ),
            array(
                'id'     => 11,
                'language_id'   => 1,
                'name'  => 'Expirado',
            ),
            array(
                'id'     => 12,
                'language_id'   => 1,
                'name'  => 'Estornado',
            ),
            array(
                'id'     => 13,
                'language_id'   => 1,
                'name'  => 'Reembolsado',
            ),
        );

        DB::table('order_status')->insert( $order_status );
    }

}
