<?php

class PlansTableSeeder extends Seeder {

    public function run()
    {
        DB::table('plans')->delete();

        $plans = array(
            array(
                'id'     => 1,
                'name'   => 'Divulgador',
                'value'  => 60,
                'points' => 20,
                'order'  => 1,
            ),
            array(
                'id'     => 2,
                'name'   => 'Promotor',
                'value'  => 200,
                'points' => 65,
                'order'  => 2,
            ),
            array(
                'id'     => 3,
                'name'   => 'Supervisor',
                'value'  => 600,
                'points' => 200,
                'order'  => 3,
            ),
            array(
                'id'     => 4,
                'name'   => 'Supervisor Premium',
                'value'  => 1800,
                'points' => 600,
                'order'  => 4,
            ),
            array(
                'id'     => 5,
                'name'   => 'Gerente Premium',
                'value'  => 3600,
                'points' => 1200,
                'order'  => 5,
            ),
        );

        DB::table('plans')->insert( $plans );
    }

}
