<?php

class DatabaseSeeder extends Seeder {

    public function run()
    {
        Eloquent::unguard();

        // Add calls to Seeders here
        $this->call('SettingsTableSeeder');
        $this->call('LanguageTableSeeder');
        $this->call('OrderStatusTableSeeder');
        $this->call('MlmTitlesTableSeeder');
        $this->call('UsersTableSeeder');
        $this->call('UsersDataTableSeeder');
        $this->call('UsersAddressTableSeeder');
        $this->call('UsersMlmTableSeeder');
        $this->call('RolesTableSeeder');
        $this->call('PermissionsTableSeeder');
        $this->call('PlansTableSeeder');
    }

}