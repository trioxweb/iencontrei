<?php

class MlmTitlesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('mlm_titles')->delete();


        $mlm_titles = array(
            array(
                'id'     => 1,
                'name'   => 'Diretor',
                'order'  => 1,
            ),
            array(
                'id'     => 2,
                'name'   => 'Presidente',
                'order'  => 2,
            ),
            array(
                'id'     => 3,
                'name'   => 'Presidente Nacional',
                'order'  => 3,
            ),
            array(
                'id'     => 4,
                'name'   => 'Presidente Internacional',
                'order'  => 4,
            ),
        );

        DB::table('mlm_titles')->insert( $mlm_titles );
    }

}
