<?php

class UsersMlmTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users_mlm')->delete();

        $users_mlm = array(
            array(
                'id'    => 1,
                'user_id'    => 1,
                'network_in' => 0,
            ),
            array(
                'id'    => 2,
                'user_id'    => 2,
                'network_in' => 0,
            ),
            array(
                'id'    => 3,
                'user_id'    => 3,
                'network_in' => 0,
            ),
        );

        DB::table('users_mlm')->insert( $users_mlm );
    }

}
