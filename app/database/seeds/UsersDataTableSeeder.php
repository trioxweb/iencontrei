<?php

class UsersDataTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users_data')->delete();

        $users_data = array(
            array(
                'id'    => 1,
                'user_id'    => 1,
                'firstname'      => 'superAdmin',
            ),
            array(
                'id'    => 2,
                'user_id'    => 2,
                'firstname'      => 'Admin',
            ),
            array(
                'id'    => 3,
                'user_id'    => 3,
                'firstname'      => 'Admin Finance',
            ),
        );

        DB::table('users_data')->insert( $users_data );
    }

}
