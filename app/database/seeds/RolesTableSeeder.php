<?php

class RolesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('roles')->delete();

        $superAdminRole = new Role;
        $superAdminRole->id   = 1;
        $superAdminRole->name = 'super_admin';
        $superAdminRole->save();

        $adminRole = new Role;
        $adminRole->id   = 2;
        $adminRole->name = 'admin';
        $adminRole->save();

        $adminFinanceRole = new Role;
        $adminFinanceRole->id   = 3;
        $adminFinanceRole->name = 'admin_finance';
        $adminFinanceRole->save();

        $user = User::where('username','=','super_admin')->first();
        $user->attachRole( $superAdminRole );

        $user = User::where('username','=','admin')->first();
        $user->attachRole( $adminRole );

        $user = User::where('username','=','admin_finance')->first();
        $user->attachRole( $adminFinanceRole );
        
    }

}
