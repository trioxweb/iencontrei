<?php

class SettingsTableSeeder extends Seeder {

	public function run()
	{
		DB::table('settings')->delete();


        $settings = array(
            array(
                'group'    => 'config',
                'key'      => 'email',
                'value'      => 'contato@iencontrei.com.br',
                'serialized'   => 0,
            ),
            array(
                'group'    => 'config',
                'key'      => 'name',
                'value'      => 'Iencontrei',
                'serialized'   => 0,
            ),
            array(
                'group'    => 'config',
                'key'      => 'url',
                'value'      => 'iencontrei.com.br',
                'serialized'   => 0,
            ),
            array(
                'group'    => 'config',
                'key'      => 'telephone',
                'value'      => '+55 (99) 9999-9999',
                'serialized'   => 0,
            ),
            array(
                'group'    => 'config',
                'key'      => 'has_shop',
                'value'      => 1, // True / False
                'serialized'   => 0,
            ),
            array(
                'group'    => 'config',
                'key'      => 'has_financings',
                'value'      => 1, // True / False
                'serialized'   => 0,
            ),
            array(
                'group'    => 'config',
                'key'      => 'automatic_transfer',
                'value'      => 1, // True / False
                'serialized'   => 0,
            ),
            array(
                'group'    => 'config',
                'key'      => 'payment_methods',
                'value'    => serialize([
                    "bank_transfer" => ['status' => 1, 'name' => 'Transferência Bancária', 'sort_order' => 1],
                    "pay_with_balance" => array('status' => 1, 'name' => 'Pagar com Saldo', 'sort_order' => 2),
                ]),
                'serialized'   => 1,
            ),
            array(
                'group' => 'page',
                'key'   => 'support',
                'value' => '<p><i class="fa fa-phone"></i> +55 (99) 9999-9999</p> 
                            <p><i class="fa fa-home"></i> Rua...</p>',
                'serialized' => 0,
            ),
            array(
                'group'    => 'config',
                'key'      => 'minimum_withdrawal',
                'value'      => 500.00,
                'serialized'   => 0,
            ),
            array(
                'group'    => 'config',
                'key'      => 'tax_withdrawal',
                'value'      => 0.0,
                'serialized'   => 0,
            ),
            array(
                'group'    => 'config',
                'key'      => 'minimum_transfer_oc',
                'value'      => 500.00,
                'serialized'   => 0,
            ),
            array(
                'group'    => 'config',
                'key'      => 'tax_transfer_oc',
                'value'      => 0.0,
                'serialized'   => 0,
            ),
            array(
                'group'    => 'config',
                'key'      => 'minimum_transfer_user',
                'value'      => 500.00,
                'serialized'   => 0,
            ),
            array(
                'group'    => 'config',
                'key'      => 'tax_transfer_user',
                'value'      => 0.0,
                'serialized'   => 0,
            ),
            array(
                'group'    => 'config',
                'key'      => 'tax_payment_with_balance',
                'value'      => 0.0,
                'serialized'   => 0,
            ),
            array(
                'group'    => 'mlm',
                'key'      => 'contract_time',
                'value'      => 365,
                'serialized'   => 0,
            ),
            array(
                'group'    => 'mlm',
                'key'      => 'bonus_indication',
                'value'      => serialize([
                    1 => [1 => 0,      2 => 0,     3 => 0],    // Plan => [1ºnivel, 2ºnivel, 3ºnivel]
                    2 => [1 => "0.15", 2 => 0,     3 => 0],    // Plan => [1ºnivel, 2ºnivel, 3ºnivel]
                    3 => [1 => "0.15", 2 => "0.3", 3 => 0],    // Plan => [1ºnivel, 2ºnivel, 3ºnivel]
                    4 => [1 => "0.15", 2 => "0.3", 3 => "0.15"], // Plan => [1ºnivel, 2ºnivel, 3ºnivel]
                    5 => [1 => "0.15", 2 => "0.3", 3 => "0.15"], // Plan => [1ºnivel, 2ºnivel, 3ºnivel]
                ]),
                'serialized'   => 1,
            ),
            array(
                'group'    => 'mlm',
                'key'      => 'cap',
                'value'    => serialize([
                    1 => 0,    // Plan => Limite Diário
                    2 => 300,  // Plan => Limite Diário
                    3 => 1000, // Plan => Limite Diário
                    4 => 2000, // Plan => Limite Diário
                    5 => 3500, // Plan => Limite Diário
                ]),
                'serialized'   => 1,
            ),
            array(
                'group'    => 'mlm',
                'key'      => 'plan_porc',
                'value'    => serialize([
                    1 => 0,      // Plan => Procentagem
                    2 => "0.1",  // Plan => Procentagem
                    3 => "0.25", // Plan => Procentagem
                    4 => "0.35", // Plan => Procentagem
                    5 => "0.5",    // Plan => Procentagem
                ]),
                'serialized'   => 1,
            ),
            array(
                'group'    => 'mlm',
                'key'      => 'carrer_plans',
                'value'    => serialize(array (
                    1 => 25000,   // Titulo => (pontos minimos)
                    2 => 60000,   // Titulo => (pontos minimos)
                    3 => 960000,  // Titulo => (pontos minimos)
                    4 => 5500000, // Titulo => (pontos minimos)
                )),
                'serialized'   => 1,
            ),
            array(
                'group'    => 'mlm',
                'key'      => 'bonus_fixed',
                'value'    => serialize(array (
                    1 => 0,   // Plan => (ganhos por semana)
                    2 => 0,   // Plan => (ganhos por semana)
                    3 => 40,  // Plan => (ganhos por semana)
                    4 => 120, // Plan => (ganhos por semana)
                    5 => 240, // Plan => (ganhos por semana)
                )),
                'serialized'   => 1,
            ),
            array(
                'group'    => 'mlm',
                'key'      => 'required_shares',
                'value'    => 1,
                'serialized'   => 0,
            ),
            array(
                'group'    => 'mlm',
                'key'      => 'active_monthly',
                'value'      => serialize(array (
                    1 => 0, // Pacote => ativo
                    2 => 0, // Pacote => ativo
                    3 => 0, // Pacote/Ativo => ativo
                )),
                'serialized'   => 1,
            ),
        );

        DB::table('settings')->insert( $settings );
	}

}