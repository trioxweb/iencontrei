<?php

class UsersTableSeeder extends Seeder {

    public function run()
    {
        DB::table('network')->delete();
        DB::table('users')->delete();

        $users = array(
            array(
                'id'    => 1,
                'username'      => 'super_admin',
                'email'      => 'super_admin@trioxweb.com.br',
                'password'   => Hash::make('super_admin@trioxweb'),
                'password_md5'   => md5('super_admin@trioxweb'),
                'confirmed'   => 1,
                'confirmation_code' => md5(microtime().Config::get('app.key')),
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'id'    => 2,
                'username'      => 'admin',
                'email'      => 'admin@example.org',
                'password'   => Hash::make('admin'),
                'password_md5'   => md5('admin'),
                'confirmed'   => 1,
                'confirmation_code' => md5(microtime().Config::get('app.key')),
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'id'    => 3,
                'username'      => 'admin_finance',
                'email'      => 'admin_finance@example.org',
                'password'   => Hash::make('admin_finance'),
                'password_md5'   => md5('admin_finance'),
                'confirmed'   => 1,
                'confirmation_code' => md5(microtime().Config::get('app.key')),
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
        );

        DB::table('users')->insert( $users );
    }

}
