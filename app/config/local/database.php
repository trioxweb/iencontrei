<?php

return array(

	'connections' => array(
		'mysql' => array(
			'driver'    => 'mysql',
			'host'      => '127.0.0.1',
			'database'  => 'iencontrei_trioxmatriz',
			'username'  => 'root',
			'password'  => '',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => 'txz_',
		),

		'shop' => array(
			'driver'    => 'mysql',
			'host'      => '127.0.0.1',
			'database'  => 'iencontrei_shop',
			'username'  => 'root',
			'password'  => '',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => 'txz_',
		),
	),

);
