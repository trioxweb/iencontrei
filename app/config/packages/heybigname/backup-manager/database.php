<?php

return [
    'development' => [
        'type' => 'mysql',
        'host' => 'localhost',
        'port' => '3306',
        'user' => 'root',
        'pass' => '',
        'database' => 'trioxmatriz',
    ],
    'production' => [
        'type' => 'mysql',
        'port' => '3306',
        'host'      => getenv('DB_HOST'),
        'database'  => getenv('DB_NAME'),
        'user'  => getenv('DB_USERNAME'),
        'pass'  => getenv('DB_PASSWORD'),
    ],
];
