@extends('login.layouts.default')

@section('title')
Password ::
@parent
@stop

@section('content')

		<form method="post" role="form" id="form_forgot_password">
				<input type="hidden" name="token" value="{{{ $token }}}">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				
				@if ( Session::get('notice') )
		        	<div class="alert" style="color:white;">{{ Session::get('notice') }}</div>
		        @endif
				<div class="form-login-error" style="display: block;">
					@if ( Session::get('error') )
			        	<p>{{ Session::get('error') }}</p>
			        @endif
				</div>
				
				<div class="form-steps">
					
					<div class="step current" id="step-1">

						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">
									<i class="entypo-key"></i>
								</div>
								<input class="form-control" placeholder="{{{ Lang::get('confide::confide.password') }}}" type="password" name="password" id="password" autocomplete="off">
							</div>
						</div>

						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">
									<i class="entypo-key"></i>
								</div>
								<input class="form-control" placeholder="{{{ Lang::get('confide::confide.password_confirmation') }}}" type="password" name="password_confirmation" id="password_confirmation" autocomplete="off">
							</div>
						</div>
						
						<div class="form-group">
							<button type="submit" class="btn btn-info btn-block btn-login">
								{{{ Lang::get('confide::confide.forgot.submit') }}}
								<i class="entypo-right-open-mini"></i>
							</button>
						</div>
					
					</div>
					
				</div>
				
			</form>
			
			
			<div class="login-bottom-links">
				
				<a href="{{{ URL::to('office/login') }}}" class="link">
					<i class="entypo-lock"></i>
					{{{ Lang::get('login/password.return_login_page') }}}
				</a>
				<br />
				<a href="https://trioxweb.com" target="_blank"><img src="{{ asset('assets/img/trioxweb.png') }}"></a>

@stop