@extends('login.layouts.default')

@section('title')
Login ::
@parent
@stop

@section('content')
			
			<form method="post" role="form" id="form_login" action="{{ URL::to('user/login') }}" accept-charset="UTF-8">
				@if ( Session::get('notice') )
		        <div class="alert" style="color:white;">{{ Session::get('notice') }}</div>
		        @endif

				<div class="form-login-error" style="display: block;">
					@if ( Session::get('error') )
			        	<p>{{ Session::get('error') }}</p>
			        @endif
				</div>

				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="form-group">
					
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-user"></i>
						</div>
						
						<input style="color:#000" type="text" class="form-control" name="email" id="email" placeholder="{{ Lang::get('confide::confide.username_e_mail') }}" autocomplete="off" value="{{ Input::old('email') }}" />
					</div>
					
				</div>

				<div class="form-group">
					
					<div class="input-group">
						<div class="input-group-addon">
							<i class="entypo-key"></i>
						</div>

						<input style="color:#000" class="form-control" tabindex="2" placeholder="{{ Lang::get('confide::confide.password') }}" type="password" name="password" id="password" autocomplete="off">
					</div>
				
				</div>

				<div class="form-group">
		            <div class="col-md-8">
		                <div class="checkbox">
		                    <label for="remember">{{ Lang::get('confide::confide.login.remember') }}
		                        <input type="hidden" name="remember" value="0">
		                        <input tabindex="4" type="checkbox" name="remember" id="remember" value="1">
		                    </label>
		                </div>
		            </div>
		        </div>
				
				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-block btn-login">
						<i class="entypo-login"></i>
						{{ Lang::get('confide::confide.login.submit') }}
					</button>
				</div>
				
					<div class="form-group">

							<div class="login-bottom-links" align="center">
				
								<a href="{{{ URL::to('user/forgot') }}}" class="link">{{ Lang::get('confide::confide.login.forgot_password') }}</a>
								<br />
								<a href="https://trioxweb.com" target="_blank"><img src="{{ asset('assets/img/trioxweb.png') }}"></a>

							</div>

					</div>
					
				</div>
				
								
			</form>

@stop