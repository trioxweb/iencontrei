@extends('login.layouts.default')

@section('title')
Password ::
@parent
@stop

@section('content')

		<form method="post" role="form" id="form_forgot_password">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<div class="form-login-error" style="display: block;">
					@if ( Session::get('error') )
			        	<p class="error" style="color:white !important;">{{ Session::get('error') }}</p>
			        @endif
				</div>
				
				<div class="form-steps">
					
					<div class="step current" id="step-1">

						<div class="form-group">
							<div class="input-group">
								<div class="input-group-addon">
									<i class="entypo-mail"></i>
								</div>
								
								<input class="form-control" tabindex="1" placeholder="{{ Lang::get('confide::confide.e_mail') }}" type="text" name="email" id="email" value="{{ Input::old('email') }}">
							</div>
						</div>
						
						<div class="form-group">
							<button type="submit" class="btn btn-info btn-block btn-login">
								Recover Passoword
								<i class="entypo-right-open-mini"></i>
							</button>
						</div>
					
					</div>
					
				</div>
				
			</form>
			
			
			<div class="login-bottom-links">
				
				<a href="{{{ URL::to('office/login') }}}" class="link">
					<i class="entypo-lock"></i>
					{{{ Lang::get('login/password.return_login_page') }}}
				</a>
				<br />
				<a href="https://trioxweb.com" target="_blank"><img src="{{ asset('assets/img/trioxweb.png') }}"></a>

@stop