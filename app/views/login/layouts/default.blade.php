<!DOCTYPE html>

<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="keywords" content="@yield('keywords')" />
	<meta name="author" content="@yield('author')" />
	<!-- Google will often use this as its description of your page/site. Make it good. -->
	<meta name="description" content="@yield('description')" />

	<!-- Speaking of Google, don't forget to set your site up: http://google.com/webmasters -->
	<meta name="google-site-verification" content="">

	<!-- Dublin Core Metadata : http://dublincore.org/ -->
	<meta name="DC.title" content="TrioX-Matriz 3.0">
	<meta name="DC.subject" content="@yield('description')">
	<meta name="DC.creator" content="@yield('author')">
	
	<title>
		@section('title')
		@show
	</title>
	
	<link rel="shortcut icon" href="{{{ asset('assets/img/favicon.png') }}}">

	<!-- CSS -->
    <link rel="stylesheet" href="{{asset('assets/admin/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/font-icons/entypo/css/entypo.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/bootstrap-min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/neon-core-min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/neon-theme-min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/neon-forms-min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/custom-min.css')}}">
    <!--<link rel="stylesheet" href="{{asset('assets/admin/css/skins/white.css')}}">-->
    <link rel="stylesheet" href="{{asset('assets/css/escritorio/style.css')}}">
    <!-- ALTERA COR SKIN -->
	<link rel="stylesheet" href="{{asset('assets/admin/css/skins/white.css')}}">

	<script src="{{asset('assets/admin/js/jquery-1.11.0.min.js')}}"></script>
<script>$.noConflict();</script>

	<!--[if lt IE 9]><script src="http://demo.neontheme.com/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	
	
	<!-- TS1406030955: Neon - Responsive Admin Template created by Laborator -->
</head>
<body class="page-body login-page login-form-fall skin-default">

<script type="text/javascript">
var baseurl = '{{{ URL::to('/') }}}';
</script>

<div class="login-container">
	
	<div class="login-header login-caret">
		
		<div class="login-content">
		
				<img src="/assets/img/logo/logo_login.png" class="logo">
			
		</div>
		
	</div>
	
	<div class="login-progressbar">
		<div></div>
	</div>
	
	<div class="login-form">
		
		<div class="login-content">
			
			<!-- Content -->
			@yield('content')
			<!-- ./ content -->

		</div>

	</div>

</div>

	<script src="{{asset('assets/admin/js/gsap/main-gsap.js')}}"></script>
	<script src="{{asset('assets/admin/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js')}}"></script>
	<script src="{{asset('assets/admin/js/bootstrap.js')}}"></script>
	<script src="{{asset('assets/admin/js/joinable.js')}}"></script>
	<script src="{{asset('assets/admin/js/resizeable.js')}}"></script>
	<script src="{{asset('assets/admin/js/neon-api.js')}}"></script>
	<script src="{{asset('assets/admin/js/cookies.min.js') }}"></script>
	<script src="{{asset('assets/admin/js/jquery.validate.min.js') }}"></script>
	<script src="{{asset('assets/admin/js/neon-login.js') }}"></script>
	<script src="{{asset('assets/admin/js/neon-custom.js') }}"></script>	
	<script src="{{asset('assets/admin/js/neon-demo.js') }}"></script>	
	<script src="{{asset('assets/admin/js/neon-skins.js') }}"></script>
	
</body>
</html>