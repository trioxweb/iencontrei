	
	<div class="modal fade" id="modal-support">
		<div class="modal-dialog">
			<div class="modal-content">
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">{{{ Lang::get('office/header.support') }}}</h4>
				</div>
				
				<div class="modal-body" style="align-text:center">
					<h4>{{{ Lang::get('office/header.contact_us') }}}</h4>

					{{ Setting::getSetting('support', 'page') }}
 
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">{{{ Lang::get('admin/register.cancel') }}}</button>
				</div>
			</div>
		</div>
	</div>

	<style>
	.modal-backdrop.fade.in{
		display: none !important;
	}
	</style>