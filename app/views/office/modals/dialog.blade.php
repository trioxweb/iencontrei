<!-- Modal Dialog -->
<div id="modal-dialog" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">{{ Lang::get('admin/register.are_you_sure') }}</h4>
      </div>
      <div class="modal-body">
        <p>{{ Lang::get('admin/register.are_you_sure_about_this') }}?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{{ Lang::get('admin/register.cancel') }}}</button>
        <a href="#" class="btn btn-danger" id="confirm">{{{ Lang::get('office/home.active_true') }}}</a>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function() {
    $('#modal-dialog').on('show.bs.modal', function (e) {
          href = $(e.relatedTarget).attr('data-href');
          $(this).find('.modal-footer #confirm').attr('href', href);
      });
  });
</script>