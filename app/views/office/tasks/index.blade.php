@extends('office.layouts.default')

@section('title')
{{$title}} ::
@parent
@stop

@section('content')

<div class="row">

	<div class="col-md-12">

		<h3>{{$title}}</h3>
		<br>

		 @if($user_profile)
            <div class="row">
                <div class="col-lg-12">
                    <p>
                    	<img class="img-thumbnail" data-src="holder.js/40x40" alt="40x40" src="https://graph.facebook.com/{{$user_profile['id']}}/picture?type=large" style="width: 40px; height: 40px;">
		            	{{Lang::get('office/tasks/title.logged_in')}} <a href="{{$user_profile['link']}}" target="_blank">{{$user_profile['name']}}</a> | 
		            	<a href="{{URL::route('facebook_logout')}}">{{Lang::get('office/tasks/title.logout')}}</a></p>
                	
                </div>
            </div>

            <div class="row">
            	<div class="col-lg-12">
            		@if($required_shares > 0)
            			<b><?php echo $required_shares; ?></b> {{Lang::get('office/tasks/title.shares_required')}}
            		@else
						<b>{{Lang::get('office/tasks/title.shares_completed')}}</b>
            		@endif
            		<hr>
            	</div>
            </div>

			<div class="row">
                <div class="col-md-6">
                    <div class="panel panel-primary">
        				<div class="panel-body">
        					<form action="{{URL::route('facebook_post_link')}}" method="POST">
        						{{Form::token()}}

                                <div class="form-group {{{ $errors->has('link') ? 'has-error' : '' }}}">
                                    <label for="link" class="control-label">Link</label>
                                    <input type="text" class="form-control" id="link" name="link" value="{{{ Input::old('link', $facebook_link) }}}" readonly="true">
                                    {{ $errors->first('link', '<span class="help-inline">:message</span>') }}
                                </div>

                                <div class="form-group {{{ $errors->has('message') ? 'has-error' : '' }}}">
                                    <label for="message" class="control-label">{{Lang::get('office/tasks/title.message')}}</label>
                                    <textarea class="form-control" id="message" name="message">{{{ Input::old('message') }}}</textarea>
                                    {{ $errors->first('message', '<span class="help-inline">:message</span>') }}
                                </div>

        						<button type="submit" class="btn btn-primary btn-block">{{Lang::get('office/tasks/title.publish')}}</button>
        					</form>
        				</div>
                    </div>
                </div>
			</div>
        @else
            <h2 class="form-signin-heading">{{Lang::get('office/tasks/title.login_with_facebook')}}</h2>
            <a href="{{URL::route('facebook_login')}}" class="btn btn-lg btn-primary btn-block btn-icon icon-left facebook-button" role="button">
            	<i class="entypo-facebook"></i>	Login
            </a>
        @endif
	</div>
</div>

@stop

{{-- Scripts --}}
@section('scripts')

@stop