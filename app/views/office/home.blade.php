@extends('office.layouts.default')

@section('title')
Home ::
@parent
@stop

@section('content')

<div class="row" style="padding-bottom:20px">
	<div class="col-md-12">

		<div class="row">

		<div class="col-sm-3">
			@if(Auth::user()->mlm->network_in) 
			<a href="//{{{ Auth::user()->username }}}.{{{ Setting::getSetting('url') }}}/register" target="_blank"> 
			<div class="tile-title tile-plum"> 
				<div class="icon"> 
					<i class="glyphicon glyphicon-plus-sign"></i> 
				</div> 

				<div class="title"> 
					<h3>{{{ Lang::get('office/home.register_link') }}}</h3> 
					<p>{{{ Auth::user()->username }}}.{{{ Setting::getSetting('url') }}}/register</p> 
				</div> 
			</div>
			</a> 
			@endif
		</div>

		<div class="col-sm-3">
			@if(Auth::user()->mlm->network_in) 
			<a href="//{{{ Auth::user()->username }}}.{{{ Setting::getSetting('url') }}}/shop" target="_blank">
			<div class="tile-title tile-plum"> 
				<div class="icon"> 
					<i class="glyphicon glyphicon-shopping-cart"></i> 
				</div> 
				
				<div class="title"> 
					<h3>{{{ Lang::get('office/home.ecommerce') }}}</h3> 
					<p style="text-transform: lowercase;">{{{ Auth::user()->username }}}.{{{ Setting::getSetting('url') }}}/shop</p>
				</div> 
			</div>
			</a>
			@endif 
		</div>

		<div class="col-sm-6">
			<div class="tile-title tile-gray"> 
				<div class="icon" style="background: #cbcbcb;"> 
					<h1>{{{ Lang::get('office/home.side_preference') }}}</h1> 
				</div> 
				<div class="title" style="height:90px;background: #bebebe;"> 
					<h3 style="padding-top: 30px; padding-left: 30px; padding-right: 30px;">
					<form class="form-horizontal form-groups-bordered" style="color:black!important;text-align:center" action="{{{ URL::action('OfficeUserController@postChangesidepatternnetwork') }}}" method="POST" id="side_pattern_network_form">
						{{ Form::token() }}
						<?php 
							$arr = array('id' => 'side_pattern_network', 'class' => 'form-control'); 
							if(!Auth::user()->mlm->isActive()){
								$arr['disabled'] = 'disabled';
							}
						?>
						{{ Form::select('side_pattern_network', 
							array('0' => Lang::get('register/access.automatic'), 
								'1' => Lang::get('register/access.left'), 
								'2'	=> Lang::get('register/access.right')),
							Auth::user()->mlm->side_pattern_network, $arr) }}
					</form>
					</h3>
				</div> 
			</div>
		</div>

		</div>

	</div>
</div>

<div class="row" style="padding-bottom:20px">
	<div class="col-sm-12">

			<div class="row">

				<div class="col-sm-3"> 
					<div class="tile-stats tile-gray" style="background:white"> 
						<div class="icon">
							<i class="entypo-suitcase"></i>
						</div> 
						<div class="num" style="color: @if(Auth::user()->mlm->isActive()) green @else red @endif">{{{ (Auth::user()->mlm->isActive()) ? Lang::get('office/home.active_true') : Lang::get('office/home.active_false') }}}</div> 
						<h3>{{{ Lang::get('office/home.active') }}}</h3> 
					</div> 
				</div>

				@if(Auth::user()->mlm->network_plan_id)	
				<div class="col-sm-3"> 
					<div class="tile-stats tile-gray"  style="background:white"> 
						<div class="icon">
							<i class="entypo-user-add"></i>
						</div> 
						<div class="num" style="color:#1a2843;font-size: 30px;height: 54px;">{{{ Auth::user()->mlm->plan->name }}}</div> 
						<h3>{{{ Lang::get('office/home.plan') }}}</h3>  
					</div> 
				</div>
				@endif

				<div class="col-sm-3"> 
					<div class="tile-stats tile-gray" style="background:white"> 
						<div class="icon">
							<i class="entypo-trophy"></i>
						</div> 
						<div class="num" style="color:#1a2843">@currency(Credit::getBalance())</div> 
						<h3>{{{ Lang::get('office/home.total_receive') }}}</h3> 
					</div> 
				</div>

				<div class="col-sm-3"> 
					<div class="tile-stats tile-gray" style="background:white"> 
						<div class="icon">
							<i class="entypo-database"></i>
						</div> 
						<div class="num" style="color:#1a2843">@currency(Credit::getTotalGains())</div> 
						<h3>{{{ Lang::get('office/extract.total_earnings') }}}</h3> 
					</div> 
				</div>
			</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<div class="row">
			@if($posts->count() > 0)
			<div class="col-md-12">
					
				<div class="panel panel-default panel-shadow" data-collapsed="0"><!-- setting the attribute data-collapsed="1" will collapse the panel -->
					
					<!-- panel head -->
					<div class="panel-heading" style="color:#fff;background-color:#1a2843">
						<div class="panel-title">{{{ Lang::get('office/blog.title') }}}</div>
						<div class="panel-options">
						</div>
					</div>
					
					<!-- panel body -->
					<div class="panel-body">
						
						<div class="scrollable" data-height="125" data-scroll-position="right" data-rail-color="#333" data-rail-opacity="0.6" data-rail-width="8" data-rail-radius="10" data-autohide="0">
							@foreach ($posts as $post)
							<div class="row">
								<div class="col-md-12">
									<!-- Post Title -->
									<div class="row">
										<div class="col-md-8">
											<h2 style="margin-top: 0;"><strong><a href="{{{ $post->url() }}}">{{ String::title($post->title) }}</a></strong></h2>
											<p style="margin-bottom: 0;"><span class="glyphicon glyphicon-calendar"></span> {{{ $post->date() }}}</p>
										</div>
									</div>
									<!-- ./ post title -->
									<hr>	
								</div>
							</div>
							@endforeach		
						</div>		
					</div>
					
				</div>
				
			</div>
			@endif

		</div>
	</div>

	<div class="col-sm-6">
		<div class="panel panel-primary">
			<table class="table table-bordered">
				<tbody>
					<tr>
						<td width="100%">
							<strong>{{{ Lang::get('office/home.gains_of_last_5_month') }}}</strong>
							<br />
							<div id="gains_of_last_5_month"></div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>	

</div>

<div class="row">

	
</div>

@stop

@section('scripts')
	<link rel="stylesheet" href="{{asset('assets/admin/js/rickshaw/rickshaw.min.css')}}">
	<script src="{{asset('assets/admin/js/rickshaw/vendor/d3.v3.js')}}" id="script-resource-8"></script>
	<script src="{{asset('assets/admin/js/rickshaw/rickshaw.min.js')}}" id="script-resource-9"></script>
	<script src="{{asset('assets/admin/js/raphael-min.js')}}" id="script-resource-10"></script>
	<script src="{{asset('assets/admin/js/morris.min.js')}}" id="script-resource-11"></script>
	<script src="{{asset('assets/admin/js/jquery.peity.min.js')}}" id="script-resource-12"></script>
	<script src="{{asset('assets/admin/js/neon-charts.js')}}" id="script-resource-13"></script>
	<script src="{{asset('assets/admin/js/jquery.sparkline.min.js')}}" id="script-resource-14"></script>
	
	<script>
		$.getJSON( "{{{ URL::action('OfficeDashboardController@getCharts')}}}", function( data ) {
			Morris.Bar({
	            element: 'gains_of_last_5_month',
	            data: data,
	            xkey: 'x',
	            ykeys: 'y',
	            labels: ['{{{ Lang::get('office/home.gains_in') }}} {{trim(Currency::getCurrencySymbol())}}'],
	            stacked: true,
	        });
		});
	</script>
	<script>
		$(document).ready(function(){
			$("#side_pattern_network").change(function(){
				$('#side_pattern_network_form').submit();
			});
		});
	</script>
@stop