<!DOCTYPE html>

<html lang="en">

<head>

	@include('layouts/head', array('title'=>'Office'))

</head>

<body class="page-body page-fade skin-default gray" data-url="{{{ URL::to('/') }}}">

	<div class="page-container">	

		<div class="sidebar-menu">
					
			<header class="logo-env">
				
				<!-- logo -->
				<div class="logo">
					<a href="{{{ URL::action('OfficeDashboardController@getIndex') }}}">
						<img src="{{ asset('assets/img/logo/logo_escritorio.png') }}" width="130"/>
					</a>
				</div>
				
							<!-- logo collapse icon -->
							
				<div class="sidebar-collapse">
					<a href="#" class="sidebar-collapse-icon with-animation"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
						<i class="entypo-menu" style="color:#701c1c"></i>
					</a>
				</div>
				
										
				
				<!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
				<div class="sidebar-mobile-menu visible-xs">
					<a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
						<i class="entypo-menu"></i>
					</a>
				</div>
				
			</header>
			
			<ul id="main-menu" class="">
											
					
			<li @if(Request::url() == URL::action('OfficeDashboardController@getIndex'))class="active opened"@endif>
			<a href="{{{ URL::action('OfficeDashboardController@getIndex') }}}"><i class="entypo-home" style="color:#701c1c"></i><span>Home</span></a>
			</li>

			<li><a href="{{{ URL::action('OfficeOrdersController@getIndex') }}}"><i class="entypo-basket" style="color:#701c1c"></i><span>{{ Lang::get('office/orders/title.orders') }}</span></a>

			<li @if(Request::url() == URL::action('OfficeUserController@getIndex'))class="active opened"@endif><a href="{{{ URL::action('OfficeUserController@getIndex') }}}"><i class="entypo-vcard" style="color:#701c1c"></i><span>{{{ Lang::get('office/header.myaccount') }}}</span></a>

			</li>

			@if(Auth::user()->mlm->network_in)
			<li @if(Request::is('office/network*'))class="opened"@endif>
			<a href="{{{ URL::to('office/network') }}}"><i class="entypo-flow-tree" style="color:#701c1c"></i><span>{{{ Lang::get('office/header.mynetwork') }}}</span></a>
			
			<ul>
				<li>
				<a href="{{{ URL::to('office/network/binary') }}}"><span>{{{ Lang::get('office/header.mynetwork_binary') }}}</span></a>
				<?php /*<a href="{{{ URL::to('office/network/binary') }}}"><span>{{{ Lang::get('office/header.mynetwork_binary') }}}</span></a>*/ ?>
				</li>
		
				<li>
				<!--<a href="javascript:;" onclick="jQuery('#modal-binary').modal('show');"><span>{{{ Lang::get('office/header.mynetwork_natural') }}}</span></a>-->
				<a href="{{{ URL::to('office/network/directs') }}}"><span>{{{ Lang::get('office/network/title.my_directs') }}}</span></a>
				</li>

				<li>
				<!--<a href="javascript:;" onclick="jQuery('#modal-binary').modal('show');"><span>{{{ Lang::get('office/header.mynetwork_natural') }}}</span></a>-->
				<a href="{{{ URL::to('office/network/organization') }}}"><span>{{{ Lang::get('office/network/title.organization') }}}</span></a>
				</li>
			</ul>
			</li>
			@endif

			<li @if(Request::url() == URL::action('OfficeFinanceController@getIndex'))class="active opened"@endif>
			<a href="{{{ URL::action('OfficeFinanceController@getIndex') }}}"><i class="entypo-credit-card" style="color:#701c1c"></i><span>{{{ Lang::get('office/header.finance') }}}</span></a>
			</li>	

			<!--<li>
			<a href="#"><i class="entypo-down" style="color:#701c1c"></i><span>Downloads</span><span class="badge badge-success">1</span></a>
			</li>-->

			@if(Auth::user()->mlm->network_in)
			<li @if(Request::url() == URL::action('RegisterController@getIndex'))class="active opened"@endif>
				<a href="//{{{ Auth::user()->username }}}.{{{ Setting::getSetting('url') }}}/register"><i class="entypo-user-add" style="color:#701c1c"></i><span>{{{ Lang::get('office/header.register') }}}</span></a>
			</li>
			@endif
			
			</ul>
				
		
				
		</div>	
	
		<div class="main-content">
		
			<div class="row">
			
				<!-- Profile Info and Notifications -->
				<div class="col-md-6 col-sm-8 clearfix">
					
					<ul class="user-info pull-left pull-none-xsm">
					
									<!-- Profile Info -->
						<li class="profile-info dropdown"><!-- add class "pull-right" if you want to place this from right --> 
							 <div style="margin-top: 15px"></div>
							 {{{ Lang::get('office/header.text_welcome') }}}, <b>{{{ Auth::user()->data->firstname }}}</b>		
						</li>

						@if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('super_admin'))
						<li class="profile-info dropdown"> 
							 <div style="margin-top: 15px"></div>
							 <a href="{{{ URL::to('admin') }}}" target="_blank" class="btn btn-blue btn-icon icon-left btn-xs">
							 	<span style="color:#fff">Admin<i class="entypo-tools"></i></span>
							 </a>		
						</li>
						@endif

						<li class="profile-info dropdown"> 
							 <div style="margin-top: 15px"></div>
							 <a href="javascript:;" onclick="jQuery('#modal-support').modal('show');" class="btn btn-blue btn-icon icon-left btn-xs"><span style="color:#fff">{{{ Lang::get('office/header.support') }}}<i class="entypo-phone"></i></span></a>		
						</li>		
					
					</ul>
				</div>
				
				<div class="col-md-6 col-sm-4 clearfix hidden-xs">	
						
					<ul class="list-inline links-list pull-right">
					
						<li class="profile-info dropdown">		
									
						</li>
						
						<li class="sep"></li>							
			        		
			        	<li class="dropdown language-selector">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-close-others="true">
								<img src="/assets/img/idioma/{{{ App::getLocale() }}}.png" />
							</a>
							
							<ul class="dropdown-menu pull-right">
								<li @if(App::getLocale() == 'en') class="active" @endif>
									<a href="/setlang/en">
										<img src="/assets/img/idioma/en.png" />
										<span>English</span>
									</a>
								</li>
								<li @if(App::getLocale() == 'pt') class="active" @endif>
									<a href="/setlang/pt">
										<img src="/assets/img/idioma/pt.png" />
										<span>Portugues</span>
									</a>
								</li>
								<li @if(App::getLocale() == 'es_ES') class="active" @endif>
									<a href="/setlang/es_ES">
										<img src="/assets/img/idioma/es_ES.png" />
										<span>Espanish</span>
									</a>
								</li>
							</ul>
							
						</li>
						
						<li>
						{{{ Lang::get('office/header.balance') }}}: <strong><span class="badge badge-success">@currency(Credit::getBalance())</span></strong>			
						</li>
						
						<!--<li class="sep"></li>

						<li>
							<a href="#" data-toggle="chat" data-animate="1">
								<i class="entypo-chat"></i>
								Chat
								
								<span class="badge badge-success chat-notifications-badge is-hidden">0</span>
							</a>
						</li>-->

						<li class="sep"></li>							
						
						<li>
							<a href="{{{ URL::to('user/logout') }}}">
								{{{ Lang::get('office/header.logout') }}} <i class="entypo-logout right" style="color:#701c1c"></i>
							</a>
						</li>
					</ul>
				</div>
			</div>
		
			<div class="row">

			</div>

			<!-- ./ navbar -->

			<!-- Notifications -->
			@include('notifications')
			<!-- ./ notifications -->

			<!-- Content -->
			@yield('content')
			<!-- ./ content -->

			<!-- Footer -->
			<footer class="main">
		
			<div class="pull-right">
			<a href="https://trioxweb.com" target="_blank"><img src="{{ asset('assets/img/trioxweb.png') }}"></a>
			</div>
			
			&copy; 2015 <strong>{{ Setting::getSetting('name')}}</strong> - Todos os direitos reservados.
		
			</footer>

		@include('office/chat')

		<!-- ./ Footer -->

	</div>
	<!-- ./ container -->
	
	@include('office/modals/support')
	@include('office/modals/binary')

	@yield('scripts_default')

	@yield('scripts')

	<script>
		/* Chat */
		$(function(){
			<?php /*
			$.ajax();

			@foreach (ChatMessage::getAllHistory(Auth::user()->id) as $message) 
				neonChat.showChat(false);
				neonChat.pushMessage('ui-id-1', 'This test message ' + (new Date().getTime()), 'Susan J. Best', new Date(), true, true);
				neonChat.renderMessages('ui-id-1', false);

				neonChat.pushMessage(
					'ui-id-'{{$message->getChatId(Auth::user()->id)}}, 
					'{{$message->message}}', 
					{{$message->getFromName(Auth::user()->id)}}, 
					{{$message->created_at}}, 
					{{}}, 
					{{($message->isRead()) ? 'false' : 'true'}}
				);
			@endforeach
			*/ ?>
		});	
	</script>
	

</body>

</html>
