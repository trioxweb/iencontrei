<!DOCTYPE html>

<html lang="en">

<head>

	@include('layouts/head', array('title'=>'Office'))

</head>

<body>
	<!-- Container -->
	<div class="container">

		<!-- Notifications -->
		@include('notifications')
		<!-- ./ notifications -->

		<div class="page-header">
			<h3>
				{{ $title }}
				<div class="pull-right">
					<button class="btn btn-default btn-small btn-inverse close_popup" onclick="parent.oTable.fnReloadAjax(); parent.$.colorbox.close();"><span class="glyphicon glyphicon-circle-arrow-left"></span> {{{ Lang::get('admin/register.back') }}}</button>
				</div>
			</h3>
		</div>

		<!-- Content -->
		@yield('content')
		<!-- ./ content -->

		<!-- Footer -->
		<footer class="clearfix">
			@yield('footer')
		</footer>
		<!-- ./ Footer -->

	</div>
	<!-- ./ container -->

 	<script type="text/javascript">
		$(document).ready(function(){
			$('.formAjax').submit(function(event) {
				var form = $(this);
				$.ajax({
					type: form.attr('method'),
					url: form.attr('action'),
					data: form.serialize()
					}).done(function() {
						parent.$.colorbox.close();
						parent.oTable.fnReloadAjax();
					}).fail(function() {
				});
				event.preventDefault();
			});
		});
	</script>

    @yield('scripts')

</body>

</html>
