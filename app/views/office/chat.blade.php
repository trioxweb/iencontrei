<div id="chat" class="fixed">
	
	<div class="chat-inner">
	
		<h2 class="chat-header">
			<a href="javascript:neonChat.toggleChat(true, false);" class="close"><i class="entypo-cancel"></i></a>
			<i class="entypo-users"></i>
			Chat
		</h2>
		
		@if(Auth::user()->mlm->parentuser)
		<div class="chat-group" id="group-1">
			<strong>Patrocinador</strong>
			<a href="#" id="ui-id-{{Auth::user()->mlm->parentuser->id}}">
				<em>{{Auth::user()->mlm->parentuser->data->name}}</em>
			</a>
		</div>
		@endif

		<div class="chat-group" id="group-2">
			<strong>Diretos</strong>
			@foreach (UserMlm::where('parent', Auth::user()->id)->get() as $direto)
				<a href="#" id="ui-id-{{$direto->user->id}}" data-conversation-history="#history-{{$direto->user->id}}-{{$direto->user->id}}">
					<em>{{$direto->user->data->name}}</em>
				</a>
				
				<div class="chat-history" id="history-{{$direto->user->id}}-{{$direto->user->id}}">	
					@foreach (ChatMessage::getHistory($direto->user->id, Auth::user()->id) as $message) 
						@if($message->sender_user_id == Auth::user()->id)	
							<li class="opponent {{($message->isRead()) ? '' : 'unread'}}">
								<span class="user">{{Auth::user()->data->name}}</span>
						@elseif($message->sender_user_id == $direto->user->id)
							<li>
								<span class="user">{{$direto->user->data->name}}</span>
						@endif
								<p>{{$message->message}}</p>
								<span class="time">{{$message->created_at}}</span>
							</li>
						
					@endforeach
				</div>
				
			@endforeach
		</div>
	
	</div>
	
	<!-- conversation template -->
	<div class="chat-conversation">
		
		<div class="conversation-header">
			<a href="#" class="conversation-close"><i class="entypo-cancel"></i></a>
			
			<span class="user-status"></span>
			<span class="display-name"></span> 
			<small></small>
		</div>
		
		<ul class="conversation-body">	
		</ul>
		
		<div class="chat-textarea">
			<textarea class="form-control autogrow" placeholder="Escreva sua mensagem"></textarea>
		</div>
		
	</div>
	
</div>
