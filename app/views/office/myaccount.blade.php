@extends('office.layouts.default')

@section('title')
My Account ::
@parent
@stop

@section('content')

	<div class="row">
	<div class="col-md-6">
		<div class="col-md-12">
			<form role="form" method="post" class="form-horizontal form-groups-bordered validate" action="{{{ URL::action('OfficeUserController@postData') }}}">
			{{ Form::token() }}
			
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title">
							{{{ Lang::get('office/myaccount/title.personal_data') }}}
						</div>
						
					</div>
					
					<div class="panel-body">
		
						<div class="form-group {{{ $errors->has('firstname') ? 'has-error' : '' }}}">
							<label for="firstname" class="col-sm-3 control-label">{{{ Lang::get('office/myaccount/forms.firstname') }}}</label>
							
							<div class="col-sm-5">
								<input type="text" class="form-control" id="firstname" name="firstname" value="{{{ Input::old('firstname', isset(Auth::user()->data->firstname) ? Auth::user()->data->firstname : null) }}}">
							</div>
						</div>

						<div class="form-group {{{ $errors->has('lastname') ? 'has-error' : '' }}}">
							<label for="lastname" class="col-sm-3 control-label">{{{ Lang::get('office/myaccount/forms.lastname') }}}</label>
							
							<div class="col-sm-5">
								<input type="text" class="form-control" id="lastname" name="lastname" value="{{{ Input::old('lastname', isset(Auth::user()->data->lastname) ? Auth::user()->data->lastname : null) }}}">
							</div>
						</div>

						<div class="cpf">
							<div class="form-group {{{ $errors->has('cpf') ? 'has-error' : '' }}}">
								<label for="cpf" class="col-sm-3 control-label">{{{ Lang::get('register/personal_data.cpf') }}}</label>
								
								<div class="col-sm-5">
									<input readonly="readonly" type="text" class="form-control" id="cpf" name="cpf" value="{{ Input::old('cpf', isset(Auth::user()->data->cpf) ? Auth::user()->data->cpf : null) }}">
								</div>
								{{ $errors->first('cpf', '<span class="help-inline">:message</span>') }}
							</div>
						</div>

						<div class="form-group {{{ $errors->has('telephone') ? 'has-error' : '' }}}">
							<label for="telephone" class="col-sm-3 control-label">{{{ Lang::get('office/myaccount/forms.telephone') }}}</label>
							
							<div class="col-sm-4">
								<input type="text" class="form-control" id="telephone" name="telephones[0][0]" data-mask="(99) 999999999" value="{{{ Input::old('telephones.0.0', isset(Auth::user()->data->telephones[0][0]) ? Auth::user()->data->telephones[0][0] : null) }}}">
							</div>
							<div class="col-sm-3">
								<input type="text" class="form-control" id="provider" name="telephones[0][1]" placeholder="Operadora" value="{{{ Input::old('telephones.0.1', isset(Auth::user()->data->telephones[0][1]) ? Auth::user()->data->telephones[0][1] : null) }}}">
							</div>
						</div>
						@foreach (Auth::user()->data->telephones as $key => $telephone)
							<?php if($key == 0){ continue; }?>
							<div class="form-group {{{ $errors->has('telephone') ? 'has-error' : '' }}}">
								<label for="telephone" class="col-sm-3 control-label"></label>
								<div class="col-sm-4">
									<input type="text" class="form-control" id="telephone" name="telephones[{{$key}}][0]" data-mask="(99) 999999999" value="{{{ Input::old('telephones.'.$key.'.0', isset($telephone[0]) ? $telephone[0] : null) }}}">
								</div>
								<div class="col-sm-3">
									<input type="text" class="form-control" id="provider" name="telephones[{{$key}}][1]" placeholder="Operadora" value="{{{ Input::old('telephones.'.$key.'.1', isset($telephone[1]) ? $telephone[1] : null) }}}">
								</div>
								<div class="col-sm-1">
									<a class="btn btn-default rmTelephone" onclick="rmTelephone(this);"><i class="entypo-minus"></i></a>
								</div>
							</div>
							<?php $last_key = $key; ?>
						@endforeach
						@foreach (Input::old('telephones', []) as $key => $telephone)
						<?php if($key > $last_key){ ?>
							<div class="form-group {{{ $errors->has('telephone') ? 'has-error' : '' }}}">
								<label for="telephone" class="col-sm-3 control-label"></label>
								<div class="col-sm-4">
									<input type="text" class="form-control" id="telephone" name="telephones[{{$key}}][0]" data-mask="(99) 999999999" value="{{{ Input::old('telephones.'.$key.'.0', isset($telephone[0]) ? $telephone[0] : null) }}}">
								</div>
								<div class="col-sm-3">
									<input type="text" class="form-control" id="provider" name="telephones[{{$key}}][1]" placeholder="Operadora" value="{{{ Input::old('telephones.'.$key.'.1', isset($telephone[1]) ? $telephone[1] : null) }}}">
								</div>
								<div class="col-sm-1">
									<a class="btn btn-default rmTelephone" onclick="rmTelephone(this);"><i class="entypo-minus"></i></a>
								</div>
							</div>
						<?php }?>
						@endforeach
						@if(isset($key))
							<script>window.idTelephone = {{$key}}</script>
						@endif
						<div id="newtelephone" class="form-group">
							<div class="col-sm-offset-10 col-sm-1">
								<a href="javascript:addNewTelephone();" class="btn btn-default" style=""><i class="entypo-plus"></i></a>
							</div>
						</div>
						
						<div class="form-group">
						
							<label for="submit_data" class="col-sm-3 control-label"></label>
							<div class="col-sm-5">
								<button type="submit" id="submit_data" class="btn btn-blue">{{{ Lang::get('office/myaccount/buttons.save') }}}</button>	
							</div>
						</div>
						
					</div>
				
				</div>

			</form>
		
		</div>
	
		<div class="col-md-12">
			<form role="form" method="post" class="form-horizontal form-groups-bordered validate" action="{{{ URL::action('OfficeUserController@postUser') }}}">
			{{ Form::token() }}
			
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title">
							{{{ Lang::get('office/myaccount/title.access') }}}
						</div>
						
					</div>
					
					<div class="panel-body">
			
						<div class="form-group {{{ $errors->has('username') ? 'has-error' : '' }}}">
							<label for="username" class="col-sm-3 control-label">{{{ Lang::get('office/myaccount/forms.username') }}}</label>
							
							<div class="col-sm-5">
								<input type="text" class="form-control" id="username" name="username" value="{{{ Auth::user()->username }}}">
							</div>
						</div>
		
						<div class="form-group {{{ $errors->has('email') ? 'has-error' : '' }}}">
							<label for="email" class="col-sm-3 control-label">{{{ Lang::get('office/myaccount/forms.email') }}}</label>
							
							<div class="col-sm-5">
								<input readonly="readonly" type="text" class="form-control" id="email" name="email" value="{{{ Auth::user()->email }}}">
							</div>
						</div>

						<div class="form-group">
						
							<label for="submit_data" class="col-sm-3 control-label"></label>
							<div class="col-sm-5">
								<button type="submit" id="submit_data" class="btn btn-blue">{{{ Lang::get('office/myaccount/buttons.save') }}}</button>	
							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-primary" data-collapsed="0">
					<div class="panel-heading">
						<div class="panel-title">
							{{{ Lang::get('office/myaccount/title.new_passwords') }}}
							<p>{{{ Lang::get('office/myaccount/messages.leave_blank_for_no_change') }}}</p>
						</div>
						
					</div>
					<div class="panel-body">
						<div class="panel-body {{{ $errors->has('password') ? 'has-error' : '' }}}">

							<div class="form-group">
								<label for="password" class="col-sm-3 control-label">{{{ Lang::get('office/myaccount/forms.new_password') }}}</label>
								
								<div class="col-sm-5">
									<input type="password" class="form-control" name="password" id="password">
								</div>
							</div>
							
						</div>

						<div class="panel-body {{{ $errors->has('password_confirmation') ? 'has-error' : '' }}}">

							<div class="form-group">
								<label for="password" class="col-sm-3 control-label">{{{ Lang::get('office/myaccount/forms.confirm_new_password') }}}</label>
								
								<div class="col-sm-5">
									<input type="password" class="form-control" name="password_confirmation" id="password_confirm">
								</div>
							</div>
							
						</div>
	
						<div class="form-group">
						
							<label for="submit_data" class="col-sm-3 control-label"></label>
							<div class="col-sm-5">
								<button type="submit" id="submit_data" class="btn btn-blue">{{{ Lang::get('office/myaccount/buttons.save') }}}</button>	
							</div>
						</div>						
					</div>
				</div>
			</form>

			<form role="form" method="post" class="form-horizontal form-groups-bordered validate" action="{{{ URL::action('OfficeUserController@postPin') }}}">
			{{ Form::token() }}
			
				<div class="panel panel-primary" data-collapsed="0">
					<div class="panel-heading">
						<div class="panel-title">
							{{{ Lang::get('office/myaccount/title.pin') }}}
							<p><a href="{{{ URL::action('UserController@getPinrecover') }}}">{{{ Lang::get('office/myaccount/messages.recovery_pin') }}}</a></p>
						</div>
						
					</div>
					<div class="panel-body">
						@if(Auth::user()->pin)
						<div class="panel-body {{{ $errors->has('pin_actual') ? 'has-error' : '' }}}">

							<div class="form-group">
								<label for="pin_actual" class="col-sm-3 control-label">{{{ Lang::get('office/myaccount/forms.actually_pin') }}}</label>
								
								<div class="col-sm-5">
									<input type="password" class="form-control" name="pin_actual" id="pin_actual">
								</div>
							</div>
							
						</div>
						@endif
						<div class="panel-body {{{ $errors->has('pin') ? 'has-error' : '' }}}">

							<div class="form-group">
								<label for="pin" class="col-sm-3 control-label">{{{ Lang::get('office/myaccount/forms.new_pin') }}}</label>
								
								<div class="col-sm-5">
									<input type="password" class="form-control" name="pin" id="pin">
								</div>
							</div>
							
						</div>

						<div class="panel-body {{{ $errors->has('pin_confirmation') ? 'has-error' : '' }}}">

							<div class="form-group">
								<label for="pin_confirmation" class="col-sm-3 control-label">{{{ Lang::get('office/myaccount/forms.confirm_new_pin') }}}</label>
								
								<div class="col-sm-5">
									<input type="password" class="form-control" name="pin_confirmation" id="pin_confirmation">
								</div>
							</div>
							
						</div>
	
						<div class="form-group">
						
							<label for="submit_data" class="col-sm-3 control-label"></label>
							<div class="col-sm-5">
								<button type="submit" id="submit_data" class="btn btn-blue">{{{ Lang::get('office/myaccount/buttons.save') }}}</button>	
							</div>
						</div>						
					</div>
				</div>
			</form>
		
		</div>


	</div>
	<div class="col-md-6">
		<div class="col-md-12">

			<form role="form" method="post" class="form-horizontal form-groups-bordered validate" action="{{{ URL::action('OfficeUserController@postAddress') }}}">
				{{ Form::token() }}
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title">
							{{{ Lang::get('office/myaccount/title.address') }}}
						</div>
						
					</div>
					
					<div class="panel-body">
			
						<div class="form-group {{{ $errors->has('address') ? 'has-error' : '' }}}">
							<label for="address" class="col-sm-3 control-label">{{{ Lang::get('office/myaccount/forms.address') }}}</label>
							
							<div class="col-sm-5">
								<input type="text" class="form-control" id="address" name="address" value="{{{ Input::old('address', isset(Auth::user()->address->address) ? Auth::user()->address->address : null) }}}">
							</div>
						</div>
						<div class="form-group {{{ $errors->has('address_2') ? 'has-error' : '' }}}">
							<label for="address_2" class="col-sm-3 control-label"></label>
							
							<div class="col-sm-5">
								<input type="text" class="form-control" id="address_2" name="address_2" value="{{{ Input::old('address_2', isset(Auth::user()->address->address_2) ? Auth::user()->address->address_2 : null) }}}">
							</div>
						</div>
		
						<div class="form-group {{{ $errors->has('city') ? 'has-error' : '' }}}">
							<label for="city" class="col-sm-3 control-label">{{{ Lang::get('office/myaccount/forms.city') }}}</label>
							
							<div class="col-sm-5">
								<input type="text" class="form-control" id="city" name="city" value="{{{ Input::old('city', isset(Auth::user()->address->city) ? Auth::user()->address->city : null) }}}">
							</div>
						</div>
		
						<div class="form-group {{{ $errors->has('zone') ? 'has-error' : '' }}}">
							<label for="zone" class="col-sm-3 control-label">{{{ Lang::get('office/myaccount/forms.zone') }}}</label>
							
							<div class="col-sm-5">
								<input type="text" class="form-control" id="zone" name="zone" value="{{{ Input::old('zone', isset(Auth::user()->address->zone) ? Auth::user()->address->zone : null) }}}">
							</div>
						</div>
		
						<div class="form-group {{{ $errors->has('country') ? 'has-error' : '' }}}">
							<label for="country" class="col-sm-3 control-label">{{{ Lang::get('office/myaccount/forms.country') }}}</label>
							
							<div class="col-sm-5">
								<input type="text" class="form-control" id="country" name="country" value="{{{ Input::old('country', isset(Auth::user()->address->country) ? Auth::user()->address->country : null) }}}">
							</div>
						</div>

						<div class="form-group {{{ $errors->has('postcode') ? 'has-error' : '' }}}">
							<label for="postcode" class="col-sm-3 control-label">{{{ Lang::get('office/myaccount/forms.postcode') }}}</label>
							
							<div class="col-sm-5">
								<input type="text" class="form-control" id="postcode" name="postcode" value="{{{ Input::old('postcode', isset(Auth::user()->address->postcode) ? Auth::user()->address->postcode : null) }}}">
							</div>
						</div>

						<div class="form-group">
						
							<label for="submit_address" class="col-sm-3 control-label"></label>
							<div class="col-sm-5">
								<button type="submit" id="submit_address" class="btn btn-blue">{{{ Lang::get('office/myaccount/buttons.save') }}}</button>	
							</div>
						</div>
						
					</div>
				
				</div>

			</form>
		
		</div>

		<div class="col-md-12">

			<form role="form" method="post" class="form-horizontal form-groups-bordered validate" action="{{{ URL::action('OfficeUserController@postBank') }}}">
				{{ Form::token() }}
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title">
							{{{ Lang::get('office/myaccount/title.bank_details') }}}
						</div>
						
					</div>
					
					<div class="panel-body">
						
		
						<div class="form-group {{{ $errors->has('bank_name') ? 'has-error' : '' }}}">
							<label for="bank_name" class="col-sm-3 control-label">{{{ Lang::get('office/myaccount/forms.bank') }}}</label>
							
							<div class="col-sm-5">
								<input type="text" class="form-control" id="bank_name" name="bank_name" value="{{{ Input::old('bank_name', isset(Auth::user()->bank->bank_name) ? Auth::user()->bank->bank_name : null) }}}">
							</div>
						</div>
		
						<div class="form-group {{{ $errors->has('agency') ? 'has-error' : '' }}}">
							<label for="agency" class="col-sm-3 control-label">{{{ Lang::get('office/myaccount/forms.agency') }}}</label>
							
							<div class="col-sm-5">
								<input type="text" class="form-control" id="agency" name="agency" value="{{{ Input::old('agency', isset(Auth::user()->bank->agency) ? Auth::user()->bank->agency : null) }}}">
							</div>
						</div>

						<div class="form-group {{{ $errors->has('account_number') ? 'has-error' : '' }}}">
							<label for="account_number" class="col-sm-3 control-label">{{{ Lang::get('office/myaccount/forms.account') }}}</label>
							
							<div class="col-sm-5">
								<input type="text" class="form-control" id="account_number" name="account_number" value="{{{ Input::old('account_number', isset(Auth::user()->bank->account_number) ? Auth::user()->bank->account_number : null) }}}">
							</div>
						</div>

						<div class="form-group {{{ $errors->has('account_type') ? 'has-error' : '' }}}">
							<label for="account_type" class="col-sm-3 control-label">{{{ Lang::get('office/myaccount/forms.account_type') }}}</label>
							
							<div class="col-sm-5">
								<input type="text" class="form-control" id="account_type" name="account_type" value="{{{ Input::old('account_type', isset(Auth::user()->bank->account_type) ? Auth::user()->bank->account_type : null) }}}">
							</div>
						</div>

						<div class="form-group {{{ $errors->has('titular_name') ? 'has-error' : '' }}}">
							<label for="titular_name" class="col-sm-3 control-label">{{{ Lang::get('office/myaccount/forms.account_titular') }}}</label>
							
							<div class="col-sm-5">
								<input type="text" class="form-control" id="titular_name" name="titular_name" value="{{{ Input::old('titular_name', isset(Auth::user()->bank->titular_name) ? Auth::user()->bank->titular_name : null) }}}">
							</div>
						</div>

						<div class="form-group {{{ $errors->has('titular_cpf') ? 'has-error' : '' }}}">
							<label for="titular_cpf" class="col-sm-3 control-label">{{{ Lang::get('office/myaccount/forms.titular_cpf') }}}</label>
							
							<div class="col-sm-5">
								<input type="text" class="form-control" id="titular_cpf" name="titular_cpf" value="{{{ Input::old('titular_cpf', isset(Auth::user()->bank->titular_cpf) ? Auth::user()->bank->titular_cpf : null) }}}">
							</div>
						</div>

						<!--<div class="form-group {{{ $errors->has('nib') ? 'has-error' : '' }}}">
							<label for="nib" class="col-sm-3 control-label">NIB</label>
							
							<div class="col-sm-5">
								<input type="text" class="form-control" id="nib" name="nib" value="{{{ Input::old('nib', isset(Auth::user()->bank->nib) ? Auth::user()->bank->nib : null) }}}">
							</div>
						</div>

						<div class="form-group {{{ $errors->has('iban') ? 'has-error' : '' }}}">
							<label for="iban" class="col-sm-3 control-label">IBAN</label>
							
							<div class="col-sm-5">
								<input type="text" class="form-control" id="iban" name="iban" value="{{{ Input::old('iban', isset(Auth::user()->bank->iban) ? Auth::user()->bank->iban : null) }}}">
							</div>
						</div>

						<div class="form-group {{{ $errors->has('swift') ? 'has-error' : '' }}}">
							<label for="swift" class="col-sm-3 control-label">SWIFT</label>
							
							<div class="col-sm-5">
								<input type="text" class="form-control" id="swift" name="swift" value="{{{ Input::old('swift', isset(Auth::user()->bank->swift) ? Auth::user()->bank->swift : null) }}}">
							</div>
						</div>

						<div class="form-group {{{ $errors->has('skrill') ? 'has-error' : '' }}}">
							<label for="skrill" class="col-sm-3 control-label">SKRILL (Email)</label>
							
							<div class="col-sm-5">
								<input type="text" class="form-control" id="skrill" name="skrill" value="{{{ Input::old('skrill', isset(Auth::user()->bank->skrill) ? Auth::user()->bank->skrill : null) }}}">
							</div>
						</div>-->

						<!--<div class="form-group {{{ $errors->has('pagseguro') ? 'has-error' : '' }}}">
							<label for="pagseguro" class="col-sm-3 control-label">Pagseguro (Email)</label>
							
							<div class="col-sm-5">
								<input type="text" class="form-control" id="pagseguro" name="pagseguro" value="{{{ Input::old('pagseguro', isset(Auth::user()->bank->pagseguro) ? Auth::user()->bank->pagseguro : null) }}}">
							</div>
						</div>-->

						<div class="form-group">
						
							<label for="submit_bank" class="col-sm-3 control-label"></label>
							<div class="col-sm-5">
								<button type="submit" id="submit_bank" class="btn btn-blue">{{{ Lang::get('office/myaccount/buttons.save') }}}</button>	
							</div>
						</div>
					
					</div>
					
				</div>

			</form>
		
		</div>
	</div>
		

	</div>
										
				
</form>

@stop