@extends('office.layouts.default')

@section('title')
PIN ::
@parent
@stop

@section('content')
	
		

<div class="row">
	<div class="col-md-6">

		<div class="panel panel-primary" data-collapsed="0">
			<div class="panel-body">

				<h2>{{{ Lang::get('office/pin.enter_your_pin') }}}</h2>

				<form method="post" class="form-horizontal validate" action="{{ URL::action('OfficeDashboardController@postPin') }}">
					{{ Form::token() }}

					<div class="form-group {{{ $errors->has('pin') ? 'has-error' : '' }}}">
						<label for="pin" class="col-sm-2 control-label">{{{ Lang::get('office/pin.your_pin') }}}</label> 
								
						<div class="col-sm-4">
							<input type="password" class="form-control" name="pin" id="pin">
						</div>
					</div>

					<div class="form-group">
						
						<label for="submit_pin" class="col-sm-2 control-label"></label>
						<div class="col-sm-4">
							<button type="submit" id="submit_pin" class="btn btn-blue">{{{ Lang::get('office/pin.access') }}}</button>	
						</div>
					</div>	

					<br />

					<p><a href="{{{ URL::action('UserController@getPinrecover') }}}">{{{ Lang::get('office/pin.forget_your_pin') }}}</a></p>

				</form>

			</div>
		</div>

	</div>
</div>

@stop