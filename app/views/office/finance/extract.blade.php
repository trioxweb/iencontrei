@extends('office.layouts.default')

@section('title')
Extract ::
@parent
@stop

@section('content')
	<div class="row">
		<div class="col-md-12">

			<h3>Extract</h3>
			
			<br />

			<table class="table table-bordered datatable" id="extract">
				<thead>
					<tr>
						<th class="col-md-1">#</th>
						<th class="col-md-2">{{{ Lang::get('office/extract.date') }}}</th>
						<th class="col-md-4">{{{ Lang::get('admin/points.value') }}}</th>
						<th class="col-md-6">{{{ Lang::get('admin/credit.description') }}}</th>
					</tr>
				</thead>
			</table>
			<div class="clear: both;"></div>
			@if(UserWithdraw::isPending())
			<a href="#" class="btn btn-green">{{{ Lang::get('office/extract.withdrawal_loading') }}}...</a>
			@elseif((int)date("d") >= 5 && (int)date("d") <= 10)
				<a href="javascript:;" onclick="jQuery('#modal-1').modal('show');" class="btn btn-blue btn-icon icon-left">{{{ Lang::get('office/extract.withdrawal') }}}<i class="entypo-download"></i></a>
			@endif
			<a href="javascript:;" onclick="jQuery('#modal-transfer-user').modal('show');" class="btn btn-green btn-icon icon-left">{{{ Lang::get('office/extract.transfer_to_user') }}}<i class="entypo-user"></i></a>
			<a href="javascript:;" onclick="jQuery('#modal-transfer-oc').modal('show');" class="btn btn-info btn-icon icon-left">{{{ Lang::get('office/extract.transfer_to_shop') }}}<i class="entypo-basket"></i></a>
			
			<button type="button" class="btn btn-gold btn-icon icon-left">{{{ Lang::get('office/extract.total_earnings') }}}: @currency(Credit::getTotalGains())<i class="entypo-chart-line"></i></button>

			<div style="float:right;">
				<a href="javascript:window.print();" class="btn btn-primary btn-icon icon-left hidden-print">{{{ Lang::get('office/extract.print') }}}<i class="entypo-doc-text"></i>
				</a>
			</div>	

		
		</div>
	</div>

	@include('office/finance/withdraw')

	@include('office/finance/transfer_oc')

	@include('office/finance/transfer_user')
	
	<br>
	<div class="row">
		<div class="col-md-12">

			<h3>Creditos Adicionais</h3>
			
			<br />

			<table class="table table-bordered datatable" id="credits_additionals">
				<thead>
					<tr>
						<th class="col-md-1">#</th>
						<th class="col-md-2">{{{ Lang::get('office/extract.date') }}}</th>
						<th class="col-md-4">{{{ Lang::get('admin/points.value') }}}</th>
						<th class="col-md-6">{{{ Lang::get('admin/credit.description') }}}</th>
					</tr>
				</thead>
			</table>

			<button type="button" class="btn btn-green btn-icon icon-left">Saldo de Credito Adicional: @currency(CreditAdditional::getBalance())<i class="entypo-chart-line"></i></button>
		
		</div>
	</div>

@stop

{{-- Scripts --}}
@section('scripts')
	<script type="text/javascript">
		var oTable;
		$(document).ready(function() {
			oTable = $('#extract').dataTable( {
				"sPaginationType": "bootstrap",
				"sDom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'<'export-data'T>f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
				"aaSorting": [[ 1, "desc"]],
		        "sAjaxSource": "{{ URL::action('OfficeFinanceController@getExtract') }}",
			});

			@if(isset($openModalWithdraw))
				$('#modal-1').modal({ show: true })
			@endif
		});
	</script>

	<script type="text/javascript">
		var oTable;
		$(document).ready(function() {
			oTable = $('#credits_additionals').dataTable( {
				"sPaginationType": "bootstrap",
				"sDom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'<'export-data'T>f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
				"aaSorting": [[ 1, "desc"]],
		        "sAjaxSource": "{{ URL::action('OfficeFinanceController@getDataadditionals') }}",
			});

			@if(isset($openModalWithdraw))
				$('#modal-1').modal({ show: true })
			@endif
		});
	</script>
@stop