	
	<div class="modal fade" id="modal-transfer-user">
		<div class="modal-dialog">
			<div class="modal-content">
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">{{{ Lang::get('office/extract.transfer_to_user') }}}</h4>
				</div>
				
				<div class="modal-body" style="align-text:center">
					<h4>{{{ Lang::get('office/extract.your_balance') }}} @currency(Credit::getBalance())</h4>

					<form method="post" action="{{ URL::action('OfficeFinanceController@postTransferuser') }}">
						{{ Form::token() }}

						<div class="form-group {{{ $errors->has('username') ? 'has-error' : '' }}}">
							<p>
								<label for="username"><span>{{{ Lang::get('register/access.username') }}}:</span> <input id="username" type="text" name="username" value="" /></label>
							</p>
						</div>
						<div class="form-group {{{ $errors->has('value') ? 'has-error' : '' }}}">
							<p>
								<label for="value"><span>{{{ Lang::get('admin/points.value') }}}:</span> <input id="value" class="reais" type="text" name="value" value="" /></label>
							</p>
						</div>
						<p><input type="submit" class="btn btn-red" value="{{{ Lang::get('office/extract.transfer_to_user') }}}" /></p>

						<br />

						<p>{{{ Lang::get('office/extract.minimum_transfer') }}}: @currency(Setting::getSetting('minimum_transfer_user')) </p>

					</form>

				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">{{{ Lang::get('admin/register.cancel') }}}</button>
				</div>
			</div>
		</div>
	</div>

	<style>
	.modal-backdrop.fade.in{
		display: none !important;
	}
	</style>