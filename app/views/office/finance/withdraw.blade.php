	
	<div class="modal fade" id="modal-1">
		<div class="modal-dialog">
			<div class="modal-content">
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">{{{ Lang::get('office/extract.withdrawal') }}}</h4>
				</div>
				
				<div class="modal-body" style="align-text:center">
					<h4>Seu saldo atual é de @currency(Credit::getBalance())</h4>

					<form method="post" action="{{ URL::action('OfficeFinanceController@postWithdraw') }}">
						{{ Form::token() }}
						<div class="form-group {{{ $errors->has('payment_method') ? 'has-error' : '' }}}">
							<p><strong>{{{ Lang::get('admin/withdrawal.payment_method') }}}:</strong>
								<select name="payment_method">
									<!--<option value="Pagseguro">Pagseguro</option>-->
									<option value="Deposito Bancario" selected="selected">Deposito Bancario</option>
								</select>
							</p>
						</div>
						<div class="form-group {{{ $errors->has('value') ? 'has-error' : '' }}}">
							<p>
								<label for="value"><span>{{{ Lang::get('admin/points.value') }}}:</span> <input id="value" class="reais" type="text" name="value" value="" /></label>
							</p>
						</div>
						<p><input type="submit" class="btn btn-red" value="{{{ Lang::get('office/extract.withdrawal') }}}" /></p>

						<br />

						<p>Minimum withdrawal: @currency(Setting::getSetting('minimum_withdrawal')) </p>

					</form>

				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">{{{ Lang::get('admin/register.cancel') }}}</button>
				</div>
			</div>
		</div>
	</div>

	<style>
	.modal-backdrop.fade.in{
		display: none !important;
	}
	</style>