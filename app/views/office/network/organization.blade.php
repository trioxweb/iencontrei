@extends('office.layouts.default')

@section('title')
{{$title}} ::
@parent
@stop

@section('content')

<div class="row">
		
	<div class="col-md-12">
	
		<h2>{{$title}}</h2>
		
		<div class="row">
			<div class="col-sm-6" style="margin-top:15px; margin-bottom: 15px;">
				<div class="input-group">
			    	<input type="text" class="form-control search-query" placeholder="{{{ Lang::get('office/network.search_user') }}}" value=""
			                   id="procurar_usuario" name="procurar_usuario" required>
		                <span class="input-group-btn">
		                    <button type="button"
		                            class="btn btn-purple btn-sm btn_procurar">
		                        {{{ Lang::get('office/network.search') }}}
		                        <i class="icon-search icon-on-right bigger-110"></i>
		                    </button>
		                </span>
			    </div>
			</div>
		</div>
		@if($user->id != Auth::user()->id)
		<div class="row">
			<div class="col-sm-6" style="margin-bottom: 15px;">
				<a href="{{{ URL::action('OfficeNetworkController@getOrganization') }}}"><button type="button" class="btn btn-blue">{{{ Lang::get('office/network/table.back_to_my_organization') }}}</button></a>
			</div>
		</div>
		@endif
		<div class="row">
			<div class="col-md-12">
				<div class="panel-group joined" id="accordion-directs">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion-directs" href="#collapse{{$user->id}}">
									{{$user->data->firstname}} <small>{{$user->username}}</small> 
									@if($user->mlm->title)<img src="/assets/ace/images/linear/t_{{$user->mlm->title->id}}.png" height="30px"> <button type="button" class="btn btn-orange btn-xs">{{$user->mlm->title->name}}</button> @endif
									<button type="button" class="btn btn-blue btn-xs">{{$user->mlm->getGroupVolume()}}</button>
									@if($user->mlm->isActive()) <button type="button" class="btn btn-green btn-xs">Ativo</button> @else <button type="button" class="btn btn-red btn-xs">Inativo</button> @endif
								</a>
							</h4>
						</div>
						<div id="collapse{{$user->id}}" class="panel-collapse collapse in">
							<div class="panel-body">
							{{$html_tree}}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@stop

@section('scripts')
<script src="/assets/ace/js/binario.js"></script>

<script type="text/javascript">
    $(window).ready(function () {
        $('.btn_procurar').click(function () {
            if ($('#procurar_usuario').val().replace(' ','') != '' && $('#procurar_usuario').val().replace(' ','') != ' ')
            window.location = '/office/network/findorganization/'+$('#procurar_usuario').val().replace(' ','%20');
        })
    });
</script>
@stop