@extends('office.layouts.default')

@section('title')
Binary Network ::
@parent
@stop

@section('content')

<div class="row">		
	<div class="col-md-12">

		<h2>{{{ Lang::get('office/network.binary_network') }}}</h2>

		<div class="row">

			<p>

			<img src="/assets/ace/images/binario/<?php 
                	if($network[1][0]->mlm->network_title_id){
                		echo 't_'.$network[1][0]->mlm->network_title_id.'_l';
                	}elseif($network[1][0]->mlm->network_plan_id){
                		echo 'p_'.$network[1][0]->mlm->network_plan_id.'_l';
                	}else{
                		echo 1;
                	}
                ?>.png" class="col-md-2">

			<div class="col-md-8">
				<h1 style="margin-left:10px; border-bottom:1px solid #a6a6a6;">{{$network[1][0]->data->firstname}} {{$network[1][0]->data->lastname}}</h1>

				<div class="col-sm-3" style="border-right:1px solid #a6a6a6;">
					<h4><b>Data de cadastro</b></h4>
					<p>{{$network[1][0]->created_at}}</p>
				</div>

				@if($network[1][0]->mlm->parent_user()->first())
				<div class="col-sm-3" style="border-right:1px solid #a6a6a6;">
					<h4><b>{{{ Lang::get('register/access.sponsor') }}}</b></h4>
					<p><a href="/office/network/binary/{{$network[1][0]->mlm->parent_user()->first()->id}}">{{$network[1][0]->mlm->parent_user()->first()->data->firstname}}</a></p>
				</div>
				@endif

				@if($network[1][0]->mlm->network_plan_id)
				<div class="col-sm-3" style="border-right:1px solid #a6a6a6;">
					<h4><b>{{{ Lang::get('register/type.plan') }}}</b></h4>
					<p>{{$network[1][0]->mlm->plan->name}}</p>
				</div>
				@endif

				@if($network[1][0]->mlm->network_title_id)
				<div class="col-sm-3" style="border-right:1px solid #a6a6a6;">
					<h4><b>{{{ Lang::get('office/home.title') }}}</b></h4>
					<p>{{$network[1][0]->mlm->title->name}}</p>
				</div>
				@endif

				<div class="col-sm-3" style="border-right:1px solid #a6a6a6;">
					<h4><b>{{{ Lang::get('office/home.active') }}}</b></h4>
					<p>{{{ ($network[1][0]->mlm->isActive()) ? Lang::get('office/home.active_true') : Lang::get('office/home.active_false') }}}</p>
				</div>

				<div class="col-sm-3" style="border-right:1px solid #a6a6a6;">
					<h4><b>{{{ Lang::get('office/network/table.active_to_binary') }}}</b></h4>
					<p>{{{ ($network[1][0]->mlm->isActiveToBinary()) ? Lang::get('office/home.active_true') : Lang::get('office/home.active_false') }}}</p>
				</div>

				<div class="col-sm-3" style="border-right:1px solid #a6a6a6;">
					<h4><b>{{{ Lang::get('office/network/table.manager_qualify') }}}</b></h4>
					<p>{{{ ($network[1][0]->mlm->isManagerQualify()) ? Lang::get('office/home.active_true') : Lang::get('office/home.active_false') }}}</p>
				</div>

				<div class="col-sm-3" style="border-right:1px solid #a6a6a6;">
					<h4><b>Pontos</b></h4>
					<p><b>{{{ Lang::get('office/network/table.side_left') }}}: {{ (isset($network[1][0]['points']['a'])) ? $network[1][0]['points']['a'] : 0 }}</b></p>
					<p><b>{{{ Lang::get('office/network/table.side_right') }}}: {{ (isset($network[1][0]['points']['b'])) ? $network[1][0]['points']['b'] : 0 }}</b></p>
				</div>
			</div>

			</p>
			

		</div>

	</div>
</div>

<div class="row">
	<div class="col-sm-8">
		<div class="row">

			<div class="col-sm-3">
				<h1>Minha Rede</h1>
			</div>

			<div class="col-sm-6" style="margin-top:15px">
				<div class="input-group">
			    	<input type="text" class="form-control search-query" placeholder="{{{ Lang::get('office/network.search_user') }}}" value=""
			                   id="procurar_usuario" name="procurar_usuario" required>
			                <span class="input-group-btn">
			                    <button type="button"
			                            class="btn btn-purple btn-sm btn_procurar">
			                        {{{ Lang::get('office/network.search') }}}
			                        <i class="icon-search icon-on-right bigger-110"></i>
			                    </button>
			                </span>
			    </div>
			</div>

		</div>
	</div>
</div>

<br />

<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered" style="font-size: small;width: 975px;">
			<tr>
			    <td style="width: 100px;">
			        <a href="/office/network/binary/{{$next_level_id}}" id="id-btn-dialog2" class="btn btn-info btn-sm">{{{ Lang::get('office/network.up_one_level') }}}</a>
			    </td>
			    <td class="text-left">
			        <a href="/office/network/binary/" id="id-btn-dialog2" class="btn btn-info btn-sm">{{{ Lang::get('office/network.back_to_top') }}}</a>
			    </td>
			</tr>
			<tr>
			    <td colspan="4" style="height: 315px;">

			            <div id="tree_wrap">
			                    @foreach($network as $k => $v)
			                        @foreach($v as $vk => $user)
			                            @if($user != null)

			                            <div class="thumbnail-item_0{{$k}}_0{{$vk+1}}" style="z-index: 1;" rel="popover" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="
										<b>{{{ Lang::get('register/access.username') }}}</b>: {{$user->username}}<br>
									@if($user->mlm->network_plan_id)
										<b>{{{ Lang::get('register/type.plan') }}}</b>: {{$user->mlm->plan->name}}<br>
									@endif
									@if($user->mlm->network_title_id)
										<b>Titulo</b>: {{$user->mlm->title->name}}<br>
									@endif
									@if($parent = $user->mlm->parent_user()->first())
										<b>{{{ Lang::get('register/access.sponsor') }}}</b>: {{$parent->data->firstname}} ({{$parent->username}})<br>
									@endif
									<b>{{{ Lang::get('office/network/table.side_left') }}}:</b> {{$user['points']['a']}} pts.<br>
									<b>{{{ Lang::get('office/network/table.side_right') }}}:</b> {{$user['points']['b']}} pts.
			                            " data-original-title="{{$user->data->firstname}}">
			                                <a href="/office/network/binary/{{$user->id}}">
			                                    <img src="/assets/ace/images/binario/<?php 
			                                    	if($user->mlm->network_title_id){
			                                    		echo 't_'.$user->mlm->network_title_id;
			                                    	}elseif($user->mlm->network_plan_id){
			                                    		echo 'p_'.$user->mlm->network_plan_id;
			                                    	}else{
			                                    		echo 1;
			                                    	}
			                                    ?>.png" class="thumbnail_01">
			                                </a>
			                            </div>
			                            @else
			                            <div class="thumbnail-item_0{{$k}}_0{{$vk+1}}" style="z-index: 1;">
			                            	<?php
										
												$pos = '0'.$k.'_0'.($vk+1);
												switch($pos) {
														case '02_01':
															$transbordo = $network[1][1-1];
															$lado = 1;
															break;
														case '02_02':
															$transbordo = $network[1][1-1];
															$lado = 2;
															break;
														case '03_01':
															$transbordo = $network[2][1-1];
															$lado = 1;
															break;
														case '03_02':
															$transbordo = $network[2][1-1];
															$lado = 2;
															break;
														case '03_03':
															$transbordo = $network[2][2-1];
															$lado = 1;
															break;
														case '03_04':
															$transbordo = $network[2][2-1];
															$lado = 2;
															break;
														case '04_01':
															$transbordo = $network[3][1-1];
															$lado = 1;
															break;
														case '04_02':
															$transbordo = $network[3][1-1];
															$lado = 2;
															break;
														case '04_03':
															$transbordo = $network[3][2-1];
															$lado = 1;
															break;
														case '04_04':
															$transbordo = $network[3][2-1];
															$lado = 2;
															break;
														case '04_05':
															$transbordo = $network[3][3-1];
															$lado = 1;
															break;
														case '04_06':
															$transbordo = $network[3][3-1];
															$lado = 2;
															break;
														case '04_07':
															$transbordo = $network[3][4-1];
															$lado = 1;
															break;
														case '04_08':
															$transbordo = $network[3][4-1];
															$lado = 2;
															break;
														case '05_01':
															$transbordo = $network[4][1-1];
															$lado = 1;
															break;
														case '05_02':
															$transbordo = $network[4][1-1];
															$lado = 2;
															break;
														case '05_03':
															$transbordo = $network[4][2-1];
															$lado = 1;
															break;
														case '05_04':
															$transbordo = $network[4][2-1];
															$lado = 2;
															break;
														case '05_05':
															$transbordo = $network[4][3-1];
															$lado = 1;
															break;
														case '05_06':
															$transbordo = $network[4][3-1];
															$lado = 2;
															break;
														case '05_07':
															$transbordo = $network[4][4-1];
															$lado = 1;
															break;
														case '05_08':
															$transbordo = $network[4][4-1];
															$lado = 2;
															break;
														case '05_09':
															$transbordo = $network[4][5-1];
															$lado = 1;
															break;
														case '05_010':
															$transbordo = $network[4][5-1];
															$lado = 2;
															break;
														case '05_011':
															$transbordo = $network[4][6-1];
															$lado = 1;
															break;
														case '05_012':
															$transbordo = $network[4][6-1];
															$lado = 2;
															break;
														case '05_013':
															$transbordo = $network[4][7-1];
															$lado = 1;
															break;
														case '05_014':
															$transbordo = $network[4][7-1];
															$lado = 2;
															break;
														case '05_015':
															$transbordo = $network[4][8-1];
															$lado = 1;
															break;
														case '05_016':
															$transbordo = $network[4][8-1];
															$lado = 2;
															break;
														default:
															break;
													}
												?>
												<?php 

												if($transbordo != null && false){ ?>
													<a href="/office/userastrar/aberta/<?php echo $transbordo['id'].'/'.$lado; ?>" class="userastra" id="0<?php echo $k; ?>_0<?php echo $vk+1; ?>"><img src="/assets/ace/images/binario/userastrar.png" class="thumbnail_01"></a>
												<?php }?>

												<style>
													.userastra{opacity: 0.0;}
													.userastra:hover{opacity: 0.7}
												</style>
			                            </div>
			                            @endif
			                            
			                        @endforeach
			                    @endforeach   
			        </div>

			    </td>
			</tr>
			<tr>
			    <td colspan="2" class="text-left">

			        <button onclick="document.location='/office/network/binary/{{$last_id['a']}}'" type="button" class="btn btn-sm btn-success">
			            <b class="icon-download-alt"></b> {{{ Lang::get('office/network.back_to_end') }}}
			        </button>

			    </td>
			    <td colspan="2" class="text-right">
			        <button onclick="document.location='/office/network/binary/{{$last_id['b']}}'" type="button" class="btn btn-success btn-sm">
			            {{{ Lang::get('office/network.back_to_end') }}}
			        </button>

			    </td>
			</tr>
			</table>
		</div>
	
		<img src="{{ asset('assets/ace/images/binario/flags_legenda.png') }}">

	</div>
</div>

@stop
@section('scripts')
<script src="/assets/ace/js/binario.js"></script>

<script type="text/javascript">
	$('[rel=popover]').popover({ 
        html : true, 
        content: function() {
          return $('#popover_content_wrapper').html();
        }
    });

    $(window).ready(function () {
        $('.btn_procurar').click(function () {
            if ($('#procurar_usuario').val().replace(' ','') != '' && $('#procurar_usuario').val().replace(' ','') != ' ')
            window.location = '/office/network/find/'+$('#procurar_usuario').val().replace(' ','%20');
        })
    });
</script>
@stop