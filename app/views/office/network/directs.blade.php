@extends('office.layouts.default')

@section('title')
{{$title}} ::
@parent
@stop

@section('content')

<div class="row">
		
	<div class="col-md-12">
	
		<h3>{{$title}}</h3>
		<br>
		<table class="table table-bordered datatable" id="users">
			<thead>
				<tr class="replace-inputs">
					<th>{{{ Lang::get('office/network/table.username') }}}</th>
					<th>{{{ Lang::get('office/network/table.name') }}}</th>
					<th>Email</th>
					<th>{{{ Lang::get('office/network/table.date_register') }}}</th>
					<th>{{{ Lang::get('office/network/table.plan') }}}</th>
					<th>{{{ Lang::get('office/network/table.title') }}}</th>
					<th>{{{ Lang::get('office/network/table.status') }}}</th>
				</tr>
				<tr>
					<th>{{{ Lang::get('office/network/table.username') }}}</th>
					<th>{{{ Lang::get('office/network/table.name') }}}</th>
					<th>Email</th>
					<th>{{{ Lang::get('office/network/table.date_register') }}}</th>
					<th>{{{ Lang::get('office/network/table.plan') }}}</th>
					<th>{{{ Lang::get('office/network/table.title') }}}</th>
					<th>{{{ Lang::get('office/network/table.status') }}}</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th>{{{ Lang::get('office/network/table.username') }}}</th>
					<th>{{{ Lang::get('office/network/table.name') }}}</th>
					<th>Email</th>
					<th>{{{ Lang::get('office/network/table.date_register') }}}</th>
					<th>{{{ Lang::get('office/network/table.plan') }}}</th>
					<th>{{{ Lang::get('office/network/table.title') }}}</th>
					<th>{{{ Lang::get('office/network/table.status') }}}</th>
				</tr>
			</tfoot>
		</table>
	</div>
</div>

@stop

{{-- Scripts --}}
@section('scripts')
	<script type="text/javascript">
		var oTable;
		$(document).ready(function() {
			oTable = $('#users').dataTable( {
				"sPaginationType": "bootstrap",
				"sDom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'<'export-data'T>f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
				"aaSorting": [[ 3, "desc"]],
				//"bServerSide": true,
				//"bProcessing": true,
		        "sAjaxSource": "{{ URL::action('OfficeNetworkController@getDirectsdata') }}",
		        "fnDrawCallback": function ( oSettings ) {
	           		$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
	     		}
			});

			oTable.columnFilter({
				"sPlaceHolder" : "head:after",
				aoColumns: [ "sPlaceHolder", "sPlaceHolder", "sPlaceHolder", "sPlaceHolder", "sPlaceHolder", "sPlaceHolder", "sPlaceHolder", { type: "select" }]
			});

			$('#modal-dialog').on('show.bs.modal', function (e) {
			      href = $(e.relatedTarget).attr('data-href');
			      $(this).find('.modal-footer #confirm').attr('href', href);
		    });
		});
	</script>
@stop