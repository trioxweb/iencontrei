@extends('office.layouts.default')

{{-- Content --}}
@section('content')

<h1>{{{ Lang::get('office/blog.title') }}}</h1>

@foreach ($posts as $post)

<div class="row">
	<div class="col-md-8">
		<div class="panel panel-primary" data-collapsed="0">
			<div class="panel-body">
				<!-- Post Title -->
				<div class="row">
					<div class="col-md-8">
							<h2><strong><a href="{{{ $post->url() }}}">{{ String::title($post->title) }}</a></strong></h2>
							<p>
								<span class="glyphicon glyphicon-calendar"></span> <!--Sept 16th, 2012-->{{{ $post->date() }}}
							</p>
					</div>
				</div>
				<!-- ./ post title -->

				<!-- Post Content -->
				<div class="row">
					<div class="col-md-6">
						<p>
							{{ String::tidy(Str::limit($post->content, 200)) }}
						</p>
						<p><a class="btn btn-mini btn-blue" href="{{{ $post->url() }}}">{{{ Lang::get('office/blog.read_more') }}}</a></p>
					</div>
				</div>
				<!-- ./ post content -->

				<!-- Post Footer -->
				<div class="row">
					<div class="col-md-8">
						<p></p>
						
					</div>
				</div>
				<!-- ./ post footer -->
			</div>
		</div>
	</div>
</div>

<hr />
@endforeach

@stop
