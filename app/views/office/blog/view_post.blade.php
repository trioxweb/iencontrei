@extends('office.layouts.default')

{{-- Web site Title --}}
@section('title')
{{{ String::title($post->title) }}} ::
@parent
@stop

{{-- Content --}}
@section('content')

<p>	<a href="{{{ URL::previous() }}}">{{{ Lang::get('office/blog.back') }}}</a></p>

<div class="panel panel-primary" data-collapsed="0">
	<div class="panel-body">
		<h1>{{ $post->title }}</h1>
		<div><span class="badge badge-info">{{{ Lang::get('office/blog.posted') }}} {{{ $post->date() }}}</span></div>
		<hr />
			<div class="row">	
				<div class="col-md-8">	
					<p>{{ $post->content() }}</p>
				</div>
			</div>
	</div>
</div>

@stop
