	
	<div class="modal fade" id="modal-find-order">
		<div class="modal-dialog">
			<div class="modal-content">
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">{{{ Lang::get('office/orders/buttons.payment_order_of_user') }}}</h4>
				</div>
				
				<div class="modal-body" style="align-text:center">

					<form class="form-horizontal" method="post" action="{{ URL::action('OfficeOrdersController@postFind') }}">
						{{ Form::token() }}

						<div class="form-group {{{ $errors->has('order_id') ? 'has-error' : '' }}}">
							<label class="col-md-3 control-label" for="order_id">{{ Lang::get('office/orders/table.order_n') }}:</label>
							<div class="col-md-4">
								<input class="form-control" type="text" id="order_id" type="text" name="order_id" value="" required="required" />
							</div>
							<div class="col-md-2">
								<button type="submit" class="btn btn-success">{{ Lang::get('office/orders/table.show_order') }}</button>
							</div>
						</div>
						<p style="color:red;">{{ Lang::get('office/orders/messages.pay_with_balance_required') }}</p>
					</form>

				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">{{{ Lang::get('admin/register.cancel') }}}</button>
				</div>
			</div>
		</div>
	</div>

	<style>
	.modal-backdrop.fade.in{
		display: none !important;
	}
	</style>