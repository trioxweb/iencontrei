@extends('office.layouts.default')

@section('title')
{{$title}} ::
@parent
@stop

@section('content')
	<div class="row">
		<div class="col-md-12">
			<h3>{{$title}}</h3>

			<div class="panel panel-primary" data-collapsed="0">
				<div class="panel-body">
					<form id="rootwizard-2" method="post" action="{{ ($type == 'plan_upgrade') ? URL::action('OfficeOrdersController@postCreate', 'plan_upgrade') : '' ; }}" class="form-wizard validate">
						<!-- CSRF Token -->
						<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
						<!-- ./ csrf token -->
						
						<div class="row" style="text-align:center">
							<style>
								.label_franquia{
									cursor: pointer;
								}
							</style>
						@if($type == 'plan_upgrade')
							<h3 class="{{{ $errors->has('network_plan_id') ? 'has-error' : '' }}}">Selecione o Plano para Upgrade:</h3>
							@foreach ($plans as $key => $plan)
								<input id="franquia_{{ $plan->id }}" class="input_franquia" type="radio" name="network_plan_id" value="{{ $plan->id }}" 
								@if($plans->count() == $key+1) checked="true" @endif />
									<label for="fraquia_{{ $plan->id }}" class="label_franquia" onclick="$('#franquia_{{ $plan->id }}').prop('checked', true);">
			
										<div class="tile-title tile-plum" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="{{{ Lang::get('register.plan'.$plan->id) }}}" data-original-title="{{{ Lang::get('register.description'.$plan->id) }}}">
					
										<div class="icon">
											<h3>{{{ Lang::get('register/type.plan') }}} {{ $plan->name }}</h3>
										</div>
										
										<div class="title">
											<h3>{{{ Lang::get('register/type.value') }}}: @currency($plan->getValuePlanUpgrade($user->mlm->plan, $plan)) </h3>
										</div>
									</div>
								
								</label>
							@endforeach
						@elseif($type == 'plan')
							<h3 class="{{{ $errors->has('network_plan_id') ? 'has-error' : '' }}}">Selecione o Plano:</h3>
							@foreach ($plans as $plan)

								<input id="franquia_{{ $plan->id }}" class="input_franquia" type="radio" name="network_plan_id" value="{{ $plan->id }}" 
								@if($plan->id == $user->mlm->network_plan_id)

								checked="true" 

								@endif />
									<label for="fraquia_{{ $plan->id }}" class="label_franquia" onclick="$('#franquia_{{ $plan->id }}').prop('checked', true);">
			
										<div class="tile-title tile-plum" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="{{{ Lang::get('register.plan'.$plan->id) }}}" data-original-title="{{{ Lang::get('register.description'.$plan->id) }}}">
					
										<div class="icon">
											<h3>{{{ Lang::get('register/type.plan') }}} {{ $plan->name }}</h3>
										</div>
										
										<div class="title">
											<h3>{{{ Lang::get('register/type.value') }}}: @currency($plan->value) </h3>
											@if($plan->id > 1)<p>Adesão já Inclusa</p>@endif
										</div>
									</div>
								
								</label>
							@endforeach
						@endif
						</div>

						<div class="row" style="text-align:center">
							<h3 class="{{{ $errors->has('payment_method_code') ? 'has-error' : '' }}}">Selecione o Meio de Pagamento:</h3>
							<style>
								.label_payment_method{
									cursor: pointer;
								}
							</style>
							@foreach ($payment_methods as $code => $payment_method)
								@if($code == 'pay_with_balance' && $type == 'plan')
									<?php echo "";//break; ?>
 								@endif
								@if($payment_method['status'] == 0)
									<?php continue; ?>
								@endif
								<input id="payment_method_{{ $code }}" class="input_payment_method" type="radio" name="payment_method_code" value="{{ $code }}" />

								<label for="payment_method_{{ $code }}" class="label_payment_method" onclick="$('#payment_method_{{ $code }}').prop('checked', true);">
		
									<div class="tile-title tile-gray">
										<div class="icon">
												<img src="{{asset('assets/img/payments/'.$code.'.png')}}" alt="" width="100">
											<h3 style="color:#333;">{{ $payment_method['name'] }}</h3>
										</div>
									</div>
								
								</label>
							@endforeach		
						</div>

						<button type="submit" class="btn btn-blue btn-lg">{{{ Lang::get('register/access.done') }}}</button>	
					</form>
				</div>
			</div>
		</div>
	</div>

@stop