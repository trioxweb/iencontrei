@extends('office.layouts.default')

@section('title')
{{$title}} ::
@parent
@stop

@section('content')

<a href="{{ URL::action('OfficeOrdersController@getIndex') }}"><i class="entypo-reply"></i>{{{ Lang::get('office/orders/title.back_to_all_orders') }}}</a>

<hr>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" style="padding: 10px 10px 10px 10px;">
	
			<div class="invoice">

				<div class="row">
				
					<div class="col-sm-6 invoice-left">

					
						<a href="#">
							<img src="{{ asset('assets/img/logo/logo_extrato.png') }}" height="52" />
						</a>
						
					</div>
					
					<div class="col-sm-6 invoice-right">
					
							<h3>ORDER NO. #{{$order->id}}</h3>
							<span>{{$order->date()}}</span>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6 invoice-left" style="margin-top:15px">
						<div class="label @if($order->order_status_id == 9) label-danger @endif label-info" style="font-size:15px; font-weight: 600"> {{ Lang::get('office/orders/title.order').' '.$order->order_status()->first()->name}}</div>
					</div>
				</div>
				
				<hr class="margin" />
				
				<div class="row">
				
					<div class="col-sm-3 invoice-left">
						 
						<h4>{{{ Lang::get('office/orders/title.shipping_address') }}}&nbsp;</h4>
						@if($order->shipping_firstname != $order->firstname)
						{{$order->shipping_firstname}} {{$order->shipping_lastname}}
						<br />
						@endif
						{{$order->shipping_address_1}}
						@if($order->shipping_address_2)
						<br />
						{{$order->shipping_address_2}}
						@endif
						<br />
						{{$order->shipping_city}}, {{$order->shipping_country}} - {{$order->shipping_zone}}
						<br />
						{{$order->shipping_postcode}}
					</div>

					<div class="col-sm-3 invoice-left">
						 
						<h4>{{{ Lang::get('office/orders/title.payment_address') }}}&nbsp;</h4>
						@if($order->payment_firstname != $order->firstname)
						{{$order->payment_firstname}} {{$order->payment_lastname}}
						<br />
						@endif
						{{$order->payment_address_1}}
						@if($order->payment_address_2)
						<br />
						{{$order->payment_address_2}}
						@endif
						<br />
						{{$order->payment_city}}, {{$order->payment_country}} - {{$order->payment_zone}}
						<br />
						{{$order->payment_postcode}}
					</div>
					
					<div class="col-md-6 invoice-right">
					
						<h4>{{{ Lang::get('office/orders/title.payment_details') }}}:</h4>
						{{Payment::getPayment($order)}}
					</div>
				</div>
				
				<div class="margin"></div>

				<table class="table table-bordered">
					@if(false)
						<thead>
							<tr>
								<th class="text-center">#</th>
								<th width="60%">{{{ Lang::get('office/orders/table.product') }}}</th>
								<th>{{{ Lang::get('office/orders/table.quality') }}}</th>
								<th>{{{ Lang::get('office/orders/table.price') }}}</th>
							</tr>
						</thead>
						
						<tbody>
							<tr>
								<td class="text-center"></td>
								<td></td>
								<td></td>
								<td class="text-right"></td>
							</tr>
						</tbody>
					@endif
					@if($order_plan = $order->order_plan()->first())
						<thead>
							<tr>
								<th width="60%">{{{ Lang::get('office/orders/table.plan') }}}</th>
								<th colspan="2">{{{ Lang::get('office/orders/table.price') }}}</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>{{$order_plan->name}}</td>
								<td colspan="2" class="text-right">{{Currency::format($order_plan->total, $order->currency_code)}}</td>
							</tr>
						</tbody>
					@endif
					@if($order_plan = $order->order_plan_upgrade()->first())
						<thead>
							<tr>
								<th width="60%">{{{ Lang::get('office/orders/table.plan_upgrade') }}}</th>
								<th colspan="2">{{{ Lang::get('office/orders/table.price') }}}</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>{{$order_plan->name}}</td>
								<td colspan="2" class="text-right">{{Currency::format($order_plan->total, $order->currency_code)}}</td>
							</tr>
						</tbody>
					@endif
					@if($order_active = $order->order_active()->first())
						<thead>
							<tr>
								<th width="60%">{{{ Lang::get('office/orders/table.active_monthly') }}}</th>
								<th colspan="2">{{{ Lang::get('office/orders/table.price') }}}</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>{{$order_active->name}}</td>
								<td colspan="2" class="text-right">{{Currency::format($order_active->total, $order_active->currency_code)}}</td>
							</tr>
						</tbody>
					@endif
				</table>
				
				<div class="margin"></div>

				<div class="row">
				
					<div class="col-sm-6">
					
						<div class="invoice-left">
							{{$order->firstname}} {{$order->lastname}}
							<br />
							{{$order->telephone}}
							<br />
							{{$order->email}}
						</div>
					</div>
					
					<div class="col-sm-6">
						
						<div class="invoice-right">
							
							<ul class="list-unstyled">
							@foreach($order->order_total()->orderBy('sort_order')->get() as $order_total)
								<li>
									{{ ($order_total->value > 0) ? $order_total->title.':' : $order_total->title}}
									@if($order_total->value > 0)
									<strong>{{$order_total->text}}</strong>
									@endif
								</li>
							@endforeach
							</ul>
							
						</div>
						
					</div>
				</div>

				<div class="row" style="margin-top: 30px;">
					<div class="col-sm-6">

						@if($order->order_status_id == 1)
							<a href="{{URL::action('OfficeOrdersController@getCancel', $order->id)}}" class="btn btn-danger btn-icon icon-left">
								{{{ Lang::get('office/orders/title.cancel_order') }}}
								<i class="entypo-cancel"></i>
							</a>
						@elseif($order->order_status_id == 9)
							<a href="{{URL::action('OfficeOrdersController@getActive', $order->id)}}" class="btn btn-green btn-icon icon-left">
								{{{ Lang::get('office/orders/title.active_order_again') }}}
								<i class="entypo-play"></i>
							</a>
						@endif

					</div>
					<div class="col-sm-6">
						<div class="invoice-right">
							<!--<a href="javascript:window.print();" class="btn btn-primary btn-icon icon-left hidden-print" style="text-align: left;">
								{{{ Lang::get('office/orders/title.print_order') }}}
								<i class="entypo-doc-text"></i>
							</a>-->

						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

@stop