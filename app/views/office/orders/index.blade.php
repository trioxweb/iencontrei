@extends('office.layouts.default')

@section('title')
{{$title}} ::
@parent
@stop

@section('content')

<div class="row">

	<div class="col-md-12">

		<h3>{{$title}}</h3>
		<br>

		@if(Auth::user()->mlm->ContractNeedPay())

		<div class="col-sm-2">
	
			<a href="{{URL::action('OfficeOrdersController@getCreate', 'plan')}}">
				<div class="tile-title tile-red">
					
					<div class="icon">
						<i class="glyphicon glyphicon-ok"></i>
					</div>
					
					<div class="title">
						<h3>{{{ Lang::get('office/orders/buttons.chackout_active') }}}</h3>
					</div>
				</div>
			</a>
		
		</div>

		<div class="col-sm-2 button-not-allow">
	
			<div class="tile-title tile-green">
					
				<div class="icon">
					<i class="glyphicon glyphicon-arrow-up"></i>
				</div>
				
				<div class="title">
					<h3>{{{ Lang::get('office/orders/buttons.chackout_upgrade') }}}</h3>
				</div>
			</div>
		
		</div>

		@elseif(Auth::user()->mlm->network_plan_id != Plan::orderby('order', 'DSC')->first()->id)

		<div class="col-sm-2 button-not-allow">
			<div class="tile-title tile-green">
					
				<div class="icon">
					<i class="glyphicon glyphicon-ok"></i>
				</div>
				
				<div class="title">
					<h3>{{{ Lang::get('office/orders/buttons.chackout_active') }}}</h3>
				</div>
			</div>
		</div>

		<div class="col-sm-2">
	
			<a href="{{URL::action('OfficeOrdersController@getCreate', 'plan_upgrade')}}">
				<div class="tile-title tile-red">
					
					<div class="icon">
						<i class="glyphicon glyphicon-arrow-up"></i>
					</div>
					
					<div class="title">
						<h3>{{{ Lang::get('office/orders/buttons.chackout_upgrade') }}}</h3>
					</div>
				</div>
			</a>
		
		</div>

		@endif
		
		@if(Auth::user()->mlm->ActiveMonthlyNeedPay())
			
		<div class="col-sm-2">
	
			<a href="{{URL::action('OfficeOrdersController@getCreate', 'active_monthly')}}">
				<div class="tile-title tile-red">
					
					<div class="icon">
						<i class="glyphicon glyphicon-off"></i>
					</div>
					
					<div class="title">
						<h3>{{{ Lang::get('office/orders/buttons.payment_activation_monthly') }}}</h3>
					</div>
				</div>
			</a>
		
		</div>

		@else

		<?php /*<div class="col-sm-2 button-not-allow">
	
			<div class="tile-title tile-green">
					
				<div class="icon">
					<i class="glyphicon glyphicon-off"></i>
				</div>
				
				<div class="title">
					<h3>{{{ Lang::get('office/orders/buttons.payment_activation_monthly') }}}</h3>
				</div>
			</div>
		
		</div> */?>

		@endif

		<div class="col-sm-2">
			
			<a href="javascript:;" onclick="jQuery('#modal-find-order').modal('show');">
				<div class="tile-title tile-aqua">
						
					<div class="icon">
						<i class="glyphicon glyphicon-shopping-cart"></i>
					</div>
					
					<div class="title">
						<h3>{{{ Lang::get('office/orders/buttons.payment_order_of_user') }}}</h3>
					</div>
				</div>
			</a>
		</div>

		<div class="col-sm-2">
			
			<a href="javascript:;" onclick="jQuery('#modal-find-order').modal('show');">
				<div class="tile-title tile-aqua">
						
					<div class="icon">
						<i class="glyphicon glyphicon-new-window"></i>
					</div>
					
					<div class="title">
						<h3>{{{ Lang::get('office/orders/buttons.finance_order_of_user') }}}</h3>
					</div>
				</div>
			</a>
		</div>

		@include('office/orders/modals/find_order')

	</div>

</div>

<hr />

<div class="row">

	<div class="col-md-12">

		<table id="orders" class="table table-striped table-hover">
			<thead>
				<tr>
					<th class="col-md-2">{{{ Lang::get('office/orders/table.order_n') }}}</th>
					<th class="col-md-2">{{{ Lang::get('office/orders/table.items') }}}</th>
					<th class="col-md-2">{{{ Lang::get('office/orders/table.total') }}}</th>
					<th class="col-md-2">{{{ Lang::get('office/orders/table.created_at') }}}</th>
					<th class="col-md-2">{{{ Lang::get('office/orders/table.status') }}}</th>
					<th class="col-md-2">{{{ Lang::get('table.actions') }}}</th>
				</tr>
			</thead>
			<tbody>
				@foreach($orders as $order)
				<tr>			
					<th class="col-md-2">#{{$order->id}}</th>
					<th class="col-md-2">{{$order->countItens()}}</th>
					<th class="col-md-2">@currency($order->total)</th>
					<th class="col-md-2">{{$order->date()}}</th>
					<th class="col-md-2" style="text-align: center; padding-top:15px;"><span class="label @if($order->order_status_id == 6) label-success @else label-info @endif" style="font-size:15px; padding-top: 10x;">{{$order->order_status()->first()->name}}</span></th>
					<th class="col-md-2">
						<a href="{{{ URL::to('office/orders/'.$order->id.'/show') }}}" class="btn btn-small btn-success iframe"><span class="glyphicon glyphicon-eye-open"></span> {{{ Lang::get('office/orders/table.show_order') }}}</a> 
					</th>	
				</tr>
				@endforeach
				@if($orders->count() == 0)
				<tr>			
					<th colspan="6">{{ Lang::get('office/orders/table.no_orders_found') }}</th>
				</tr>
				@endif
			</tbody>
		</table>
	</div>
</div>

@include('office/modals/dialog')

@stop

{{-- Scripts --}}
@section('scripts')
	<script type="text/javascript">

	</script>
@stop