<!DOCTYPE html>

<html lang="en">

<head>

	@include('register/skins/skin1/layouts/head', array('title'=>'Register'))

</head>

<body class="external-page sb-l-c sb-r-c">

<script>
    var boxtest = localStorage.getItem('boxed');
    if (boxtest === 'true') {
        document.body.className += ' boxed-layout';
    }
</script>

<div id="main" class="animated fadeIn">
 
<section id="content_wrapper">
 
	<div id="canvas-wrapper">
		<canvas id="demo-canvas"></canvas>
	</div>
 
	<section id="content" class="">
		<div class="admin-form theme-info mw700" style="margin-top: 3%;" id="login1">
			<div class="row mb15 table-layout">
				<div class="col-xs-6 va-m pln">
					<a href="{{{ URL::action('OfficeDashboardController@getIndex') }}}" title="Return to Dashboard">
						<img src="{{ asset('assets/img/logo/logo_empresa.png') }}" title="AdminDesigns Logo" class="img-responsive w150">
					</a>
				</div>
				
				<div class="col-xs-6 text-right va-b pr5">

						<li class="dropdown language-selector">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-close-others="true">
								<img src="/assets/img/idioma/{{{ App::getLocale() }}}.png" />
							</a>
							
							<ul class="dropdown-menu pull-right">
								<li @if(App::getLocale() == 'en') class="active" @endif>
									<a href="/setlang/en">
										<img src="/assets/img/idioma/en.png" />
										<span>English</span>
									</a>
								</li>
								<li @if(App::getLocale() == 'pt') class="active" @endif>
									<a href="/setlang/pt">
										<img src="/assets/img/idioma/pt.png" />
										<span>Portugues</span>
									</a>
								</li>
								<li @if(App::getLocale() == 'es_ES') class="active" @endif>
									<a href="/setlang/es_ES">
										<img src="/assets/img/idioma/es_ES.png" />
										<span>Espanish</span>
									</a>
								</li>
							</ul>

						</li>											
				
				</div>
			</div>

			<div class="panel panel-info mt10 br-n">

				<div class="panel-heading heading-border bg-white">
					<div class="section row mn">
						<div class="col-sm-4">
							@if(isset($_COOKIE['tracking_firstname']) && isset($_COOKIE['tracking_email']) && isset($_COOKIE['tracking']))
	  						<a href="#" style="color:#daac37; text-transform:uppercase;font-weight: bold;">Você chegou aqui graças ao {{$_COOKIE['tracking_firstname']}} ({{$_COOKIE['tracking_email']}})</a>
  							@endif	
						</div>
					</div>
				</div>

				<!-- Notifications -->
				@include('notifications')
				<!-- ./ notifications -->

				<!-- Content -->
				@yield('content')
				<!-- ./ content -->

			</div>
		</div>
	</section>
 
</section>
 
</div>

		<!-- Footer -->
		<footer class="clearfix">
			@yield('footer')
		</footer>
		<!-- ./ Footer -->

	</div>
	<!-- ./ container -->

	@yield('scripts_default')

	@yield('scripts')

</body>

</html>
