<meta charset="utf-8">

<title>
	@section('title')
		{{{$title}}}
	@show
</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<meta name="keywords" content="@yield('keywords')" />
<meta name="author" content="@yield('author')" />
<!-- Google will often use this as its description of your page/site. Make it good. -->
<meta name="description" content="@yield('description')" />
<!-- Speaking of Google, don't forget to set your site up: http://google.com/webmasters -->
<meta name="google-site-verification" content="">

<!-- Dublin Core Metadata : http://dublincore.org/ -->
<meta name="DC.title" content="TrioX-Matriz 3.0">
<meta name="DC.subject" content="@yield('description')">
<meta name="DC.creator" content="@yield('author')">

<link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800'>
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300">
 
<link rel="stylesheet" type="text/css" href="{{{ asset('assets/register/theme_1/css/ie_split1.css') }}}">
<link rel="stylesheet" type="text/css" href="{{{ asset('assets/register/theme_1/css/ie_split2.css') }}}">
<link rel="stylesheet" type="text/css" href="{{{ asset('assets/register/theme_1/css/ie_split3.css') }}}">
 
<link rel="stylesheet" type="text/css" href="{{{ asset('assets/register/theme_1/css/admin-forms.css') }}}">
 
<link rel="shortcut icon" href="{{{ asset('assets/img/favicon.png') }}}">

<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
 
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

@section('scripts_default')
        @parent

        <script type="text/javascript" src="{{{ asset('assets/register/theme_1/vendor/jquery/jquery-1.11.1.min.js') }}}"></script>
        <script type="text/javascript" src="{{{ asset('assets/register/theme_1/vendor/jquery/jquery_ui/jquery-ui.min.js') }}}"></script>
         
        <script type="text/javascript" src="{{{ asset('assets/register/theme_1/js/bootstrap/bootstrap.min.js') }}}"></script>
         
        <script type="text/javascript" src="{{{ asset('assets/register/theme_1/js/pages/login/EasePack.min.js') }}}"></script>
        <script type="text/javascript" src="{{{ asset('assets/register/theme_1/js/pages/login/rAF.js') }}}"></script>
        <script type="text/javascript" src="{{{ asset('assets/register/theme_1/js/pages/login/TweenLite.min.js') }}}"></script>
        <script type="text/javascript" src="{{{ asset('assets/register/theme_1/js/pages/login/login.js') }}}"></script>
         
        <script type="text/javascript" src="{{{ asset('assets/register/theme_1/js/utility/utility.js') }}}"></script>
        <script type="text/javascript" src="{{{ asset('assets/register/theme_1/js/main.js') }}}"></script>
        <script type="text/javascript" src="{{{ asset('assets/register/theme_1/js/demo.js') }}}"></script>
         
        <script type="text/javascript">
            jQuery(document).ready(function() {
                "use strict";
                // Init Theme Core      
                Core.init();
                
                // Init Demo JS
                Demo.init();

                // Init CanvasBG and pass target starting location
                CanvasBG.init({
                    Loc: {
                        x: window.innerWidth / 2.1,
                        y: window.innerHeight / 4.2
                    },
                });
            });
        </script>

@stop