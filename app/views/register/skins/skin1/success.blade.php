@extends('register.skins.skin1.layouts.default')

@section('title')
Success ::
@parent
@stop

@section('content')

<div class="panel-body p25 bg-light">

	<div class="section mb15" align="center">
			<i class="fa fa-check-circle fa-5x"></i>
			<h3>{{{ Lang::get('register/information.text_1') }}}</h3>
			<span>{{{ Lang::get('register/information.text_2') }}}</span>
			<br /><br />
			<a href="/register/pdf_register" target="_blank" class="button btn-primary">{{{ Lang::get('register/information.contract_download') }}}</a></p>
			<a href="/office" target="_blank" class="btn btn-warning"><i class="fa fa-home"></i> Backoffice</a>
	</div>

</div>

@stop