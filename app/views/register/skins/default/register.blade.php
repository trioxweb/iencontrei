@extends('register.default.layouts.default')

@section('title')
Register ::
@parent
@stop

@section('content')

<form method="post" action="/register" class="form-wizard validate" id="admin-form">
<!-- CSRF Token -->
<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
<!-- ./ csrf token -->
	<div class="panel-body p25 bg-light">
		<div class="section-divider mt10 mb40">
			<span>{{{ Lang::get('register/header.personal_data') }}}</span>
		</div>
 
		<div class="section row">
			<div class="col-md-6 {{{ $errors->has('firstname') ? 'has-error' : '' }}}">
				<label for="firstname" class="field prepend-icon">
					<input type="text" name="firstname" id="firstname" class="gui-input" placeholder="{{{ Lang::get('register/personal_data.firstname') }}}..." value="{{{ Input::old('firstname') }}}">
						<label for="firstname" class="field-icon"><i class="fa fa-user"></i>
						</label>
				</label>
			</div>
 
			<div class="col-md-6 {{{ $errors->has('lastname') ? 'has-error' : '' }}}">
				<label for="lastname" class="field prepend-icon">
					<input type="text" name="lastname" id="lastname" class="gui-input" placeholder="{{{ Lang::get('register/personal_data.lastname') }}}..." value="{{{ Input::old('lastname') }}}">
						<label for="lastname" class="field-icon"><i class="fa fa-user"></i>
						</label>
				</label>
			</div>
		</div>
		<div class="section row">
			<div class="col-md-6 {{{ $errors->has('cpf') ? 'has-error' : '' }}}">
				<label for="cpf" class="field prepend-icon">
					<input type="text" name="cpf" id="cpf" class="gui-input" placeholder="CPF" value="{{{ Input::old('cpf') }}}">
						<label for="cpf" class="field-icon"><i class="fa fa-newspaper-o"></i>
						</label>
				</label>
			</div>
 
			<div class="col-md-6 {{{ $errors->has('telephone') ? 'has-error' : '' }}}">
				<label for="telephone" class="field prepend-icon">
					<input type="text" name="telephones[0][0]" id="telephone" class="gui-input" placeholder="{{{ Lang::get('register/personal_data.telephone') }}}" value="{{{ Input::old('telephones.0.0') }}}">
						<label for="lastname" class="field-icon"><i class="fa fa-phone"></i>
						</label>
				</label>
			</div>
 
		</div>
		<div class="section row">
			<div class="col-md-4 {{{ $errors->has('postcode') ? 'has-error' : '' }}}">
				<label for="postcode" class="field prepend-icon">
					<input type="text" name="postcode" id="postcode" class="gui-input" placeholder="{{{ Lang::get('register/personal_data.postcode') }}}" value="{{{ Input::old('postcode') }}}">
						<label for="postcode" class="field-icon"><i class="fa fa-lemon-o"></i>
						</label>
				</label>
			</div>
 
			<div class="col-md-8 {{{ $errors->has('address') ? 'has-error' : '' }}}">
				<label for="address" class="field prepend-icon">
					<input type="text" name="address" id="address" class="gui-input" placeholder="{{{ Lang::get('register/personal_data.address') }}}" value="{{{ Input::old('address') }}}">
						<label for="address" class="field-icon"><i class="fa fa-home"></i>
						</label>
				</label>
			</div>
 
		</div>
		<div class="section row">
			<div class="col-md-4 {{{ $errors->has('city') ? 'has-error' : '' }}}">
				<label for="city" class="field prepend-icon">
					<input type="text" name="city" id="city" class="gui-input" placeholder="{{{ Lang::get('register/personal_data.city') }}}" value="{{{ Input::old('city') }}}">
						<label for="city" class="field-icon"><i class="fa fa-university"></i>
						</label>
				</label>
			</div>
 
			<div class="col-md-4 {{{ $errors->has('zone') ? 'has-error' : '' }}}">
				<label for="zone" class="field prepend-icon">
					<input type="text" name="zone" id="zone" class="gui-input" placeholder="{{{ Lang::get('register/personal_data.zone') }}}" value="{{{ Input::old('zone') }}}">
						<label for="zone" class="field-icon"><i class="fa fa-leaf"></i>
						</label>
				</label>
			</div>

			<div class="col-md-4 {{{ $errors->has('country') ? 'has-error' : '' }}}">
				<label for="country" class="field prepend-icon">
					<input type="text" name="zone" id="zone" class="gui-input" placeholder="{{{ Lang::get('register/personal_data.country') }}}" value="{{{ Input::old('country') }}}">
						<label for="country" class="field-icon"><i class="fa fa-globe"></i>
						</label>
				</label>
			</div>
 
		</div>		

		<div class="section-divider mt10 mb40">
			<span>{{{ Lang::get('register/header.access') }}}</span>
		</div>
 
		<div class="section {{{ $errors->has('email') ? 'has-error' : '' }}}">
			<label for="email" class="field prepend-icon">
				<input type="email" name="email" id="email" class="gui-input" placeholder="{{{ Lang::get('register/access.email') }}}">
					<label for="email" class="field-icon"><i class="fa fa-envelope"></i>
					</label>
				</label>
		</div>
 
		<div class="section {{{ $errors->has('username') ? 'has-error' : '' }}}">
			<div class="smart-widget sm-right smr-150">
				<label for="username" class="field prepend-icon">
					<input type="text" name="username" id="username" class="gui-input" placeholder="{{{ Lang::get('register/access.username') }}}">
						<label for="username" class="field-icon"><i class="fa fa-user"></i>
						</label>
				</label>
				<label for="username" class="button">.trioxmatriz-3.com</label>
			</div>
 
		</div>
 
 		<div class="section row">
			<div class="col-md-6 section {{{ $errors->has('password') ? 'has-error' : '' }}}">
				<label for="password" class="field prepend-icon">
					<input type="password" name="password" id="password" class="gui-input" placeholder="{{{ Lang::get('register/access.password') }}}">
						<label for="password" class="field-icon"><i class="fa fa-unlock-alt"></i>
						</label>
				</label>
			</div>
	 
			<div class="col-md-6 section {{{ $errors->has('password_confirmation') ? 'has-error' : '' }}}">
				<label for="confirmPassword" class="field prepend-icon">
					<input type="password" name="confirmPassword" id="confirmPassword" class="gui-input" placeholder="{{{ Lang::get('register/access.confirm_password') }}}">
						<label for="confirmPassword" class="field-icon"><i class="fa fa-lock"></i>
						</label>
				</label>
			</div>
		</div>
 
		<div class="section-divider mv40">
			<span>Review the Terms</span>
		</div>
 
		<div class="section mb15 {{{ $errors->has('chk_rules') ? 'has-error' : '' }}}">
			<label class="option block">
				<input type="checkbox" name="trial">
				<span class="checkbox"></span>{{{ Lang::get('register/access.text_atention') }}}
				<a href="{{ asset('downloads/CONTRATO DIVULGADOR.pdf') }}" class="smart-link"> Divulgador.</a>
			</label>
		</div>
 
	</div>
 
	<div class="panel-footer clearfix">
		<button type="submit" class="button btn-primary pull-right">{{{ Lang::get('register/access.done') }}}</button>
	</div>
 
</form>

@stop