@extends('register.layouts.default')

@section('title')
Success ::
@parent
@stop

@section('content')

<div class="row">
		<div class="col-md-12">
		
			<div class="panel panel-primary" data-collapsed="0">
				<div class="panel-body">

					<div class="cadastrar finalizar">

						<h2>

							{{{ Lang::get('register/information.text_1') }}}<br />

							{{{ Lang::get('register/information.text_2') }}}

						</h2>

						<!--<p><a href="/register/pdf_register" target="_blank" class="btn btn-blue">{{{ Lang::get('register/information.contract_download') }}}</a></p>-->

					</div>

				</div>
			</div>
		</div>
	</div>

@stop