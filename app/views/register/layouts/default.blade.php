<!DOCTYPE html>

<html lang="en">

<head>

	@include('layouts/head', array('title'=>'Register'))

</head>

<body class="page-body skin-black page-fade-only gray boxed-layout" data-url="{{{ URL::to('/') }}}">

	<div class="page-container no-sidebar">
	
	<div class="main-content">
	
	<div class="row">
	
	<!-- Profile Info and Notifications -->
	<div class="col-md-6 col-sm-8 clearfix">
		
		<ul class="user-info pull-left pull-none-xsm">
		
						<!-- Profile Info -->
			<li class="profile-info dropdown"><!-- add class "pull-right" if you want to place this from right -->
				 
				 <div style="margin-top: 15px"></div>
				<img src="{{ asset('assets/img/logo/logo_extrato.png') }}" width="130"/>
				
				
			</li>	
		
		</ul>
		</div>
		
		<div class="col-md-6 col-sm-4 clearfix hidden-xs">	
				
			<ul class="list-inline links-list pull-right">
				
				
				@if(isset($_COOKIE['tracking_firstname']) && isset($_COOKIE['tracking_email']) && isset($_COOKIE['tracking']))
					<li>
	  				<a href="#" style="color:#daac37; text-transform:uppercase;font-weight: bold;">Você chegou aqui graças ao {{$_COOKIE['tracking_firstname']}} ({{$_COOKIE['tracking_email']}})</a>
  					</li>
  				@endif	
				

				<li class="profile-info dropdown">		
						
				</li>

				<li class="dropdown language-selector">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-close-others="true">
								<img src="/assets/img/idioma/{{{ App::getLocale() }}}.png" />
							</a>
							
							<ul class="dropdown-menu pull-right">
								<li @if(App::getLocale() == 'en') class="active" @endif>
									<a href="/setlang/en">
										<img src="/assets/img/idioma/en.png" />
										<span>English</span>
									</a>
								</li>
								<li @if(App::getLocale() == 'pt') class="active" @endif>
									<a href="/setlang/pt">
										<img src="/assets/img/idioma/pt.png" />
										<span>Portugues</span>
									</a>
								</li>
								<li @if(App::getLocale() == 'es_ES') class="active" @endif>
									<a href="/setlang/es_ES">
										<img src="/assets/img/idioma/es_ES.png" />
										<span>Espanish</span>
									</a>
								</li>
							</ul>

				</li>										
				
				<li>
				<a href="/office"><i class="entypo-suitcase"></i> Backoffice</a>
				</li>
			
			</div>
		</div>
	
	<hr>
	
	<div class="row">

	</div>

		<!-- ./ navbar -->

		<!-- Notifications -->
		@include('notifications')
		<!-- ./ notifications -->

		<!-- Content -->
		@yield('content')
		<!-- ./ content -->

		<!-- Footer -->
		<footer class="clearfix">
			@yield('footer')
		</footer>
		<!-- ./ Footer -->

	</div>
	<!-- ./ container -->

	@yield('scripts_default')

	@yield('scripts')

</body>

</html>
