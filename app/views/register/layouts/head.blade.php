    <meta charset="UTF-8">

	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title>
		@section('title')
			{{{$title}}}
		@show
	</title>

	<meta name="keywords" content="@yield('keywords')" />
	<meta name="author" content="@yield('author')" />
	<!-- Google will often use this as its description of your page/site. Make it good. -->
	<meta name="description" content="@yield('description')" />

	<!-- Speaking of Google, don't forget to set your site up: http://google.com/webmasters -->
	<meta name="google-site-verification" content="">

	<!-- Dublin Core Metadata : http://dublincore.org/ -->
	<meta name="DC.title" content="TrioX-Matriz 3.0">
	<meta name="DC.subject" content="@yield('description')">
	<meta name="DC.creator" content="@yield('author')">

	<!--  Mobile Viewport Fix -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

	<!-- This is the traditional favicon -->
	<link rel="shortcut icon" href="{{{ asset('assets/img/favicon.png') }}}">
	
    <!-- CSS -->
    <link rel="stylesheet" href="{{asset('assets/admin/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/font-icons/entypo/css/entypo.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/bootstrap-min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/neon-core-min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/neon-theme-min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/neon-forms-min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/custom-min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/skins/default.css')}}">

    <link rel="stylesheet" href="{{asset('assets/admin/css/colorbox.css')}}">

	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic"  id="style-resource-3">

	<link rel="stylesheet" href="{{asset('assets/ace/css/binario.css')}}" type="text/css"/>

    <link rel="stylesheet" href="{{asset('assets/admin/js/datatables/responsive/css/datatables.responsive.css')}}">
	<link rel="stylesheet" href="{{asset('assets/admin/js/select2/select2-bootstrap.css')}}">
	<link rel="stylesheet" href="{{asset('assets/admin/js/select2/select2.css')}}">

	<link rel="stylesheet" href="{{asset('assets/admin/js/jquery-ui-1.11.4/jquery-ui.min.css')}}">

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	
	
	
	@yield('styles')

	<style type="text/css">
		.dataTables_processing{
			display: none;
		}
		.modal-backdrop.in{
			display: none !important;
		}
	</style>


	@section('scripts_default')
		@parent
		
		<!-- Javascripts ================================================= -->

		<script src="{{asset('assets/admin/js/jquery-1.11.0.min.js')}}"></script>

		<script src="{{asset('assets/admin/js/gsap/main-gsap.js')}}"></script>
		<script src="{{asset('assets/admin/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js')}}"></script>
		<script src="{{asset('assets/admin/js/bootstrap.js')}}"></script>
		<script src="{{asset('assets/admin/js/joinable.js')}}"></script>

		<script src="{{asset('assets/admin/js/resizeable.js')}}"></script>
		<script src="{{asset('assets/admin/js/neon-api.js')}}"></script>
		<script src="{{asset('assets/admin/js/cookies.min.js') }}"></script>

		<script src="{{asset('assets/admin/js/neon-chat.js')}}"></script>
		<script src="{{asset('assets/admin/js/neon-custom.js') }}"></script>
		<script src="{{asset('assets/admin/js/neon-demo.js') }}"></script>	
		<script src="{{asset('assets/admin/js/neon-skins.js') }}"></script>	

		<script src="{{asset('assets/admin/js/jquery.bootstrap.wizard.min.js')}}"></script>
		<script src="{{asset('assets/admin/js/jquery.validate.min.js')}}"></script>
		<!--<script src="{{asset('assets/admin/js/selectboxit/jquery.selectBoxIt.min.js')}}"></script>-->
		<script src="{{asset('assets/admin/js/jquery.inputmask.bundle.min.js')}}"></script>
		<!--<script src="{{asset('assets/admin/js/selectboxit/jquery.selectBoxIt.css')}}"></script>-->

		<script src="{{asset('assets/admin/js/jquery.masked.js')}}"></script>
	    <script src="{{asset('assets/admin/js/jquery.price_format.1.3.js')}}"></script>
	    <script src="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
	    <script src="{{asset('assets/admin/js/jquery.monthpicker.js')}}"></script>
	    <script src="{{asset('assets/js/trioxmatriz.js')}}"></script>
	    <script src="{{asset('assets/admin/js/jquery.colorbox-min.js')}}"></script>

	    <script src="{{asset('assets/admin/js/jquery-ui-1.11.4/jquery-ui.min.js')}}"></script>
			
		<script src="{{asset('assets/admin/js/jquery.dataTables.min.js')}}"></script>
		<script src="{{asset('assets/admin/js/datatables/TableTools.min.js')}}"></script>
		<script src="{{asset('assets/admin/js/dataTables.bootstrap.js')}}"></script>
		<script src="{{asset('assets/admin/js/datatables/jquery.dataTables.columnFilter.js')}}"></script>
		<script src="{{asset('assets/admin/js/datatables/lodash.min.js')}}"></script>
		<script src="{{asset('assets/admin/js/datatables/responsive/js/datatables.responsive.js')}}"></script>
		<script src="{{asset('assets/admin/js/datatables/fnReloadAjax.js')}}"></script>

	@stop