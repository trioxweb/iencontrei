@extends('register.layouts.default')

@section('title')
Register ::
@parent
@stop

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary" data-collapsed="0">
				<div class="panel-body">
					<form id="rootwizard-2" method="post" action="/register" class="form-wizard validate">
						<!-- CSRF Token -->
						<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
						<!-- ./ csrf token -->
						
						<div class="steps-progress">
							<div class="progress-indicator"></div>
						</div>
						
						<ul>
							<li class="active">
								<a href="#tab2-2" data-toggle="tab"><span>1</span>{{{ Lang::get('register/header.personal_data') }}}</a>
							</li>
							<li>
								<a href="#tab2-3" data-toggle="tab"><span>2</span>{{{ Lang::get('register/header.access') }}}</a>
							</li>
						</ul>
						
						<div class="tab-content">

							<div class="tab-pane active" id="tab2-2">
								
								<div class="row">

									<div class="col-md-6 {{{ $errors->has('firstname') ? 'has-error' : '' }}}">
										<div class="form-group">
											<label class="control-label" for="firstname">{{{ Lang::get('register/personal_data.firstname') }}}</label>
											<input class="form-control" name="firstname" id="firstname" data-validate="required" value="{{{ Input::old('firstname') }}}" placeholder="{{{ Lang::get('register/personal_data.firstname') }}}" />
										</div>
									</div>
									
									<div class="col-md-6 {{{ $errors->has('lastname') ? 'has-error' : '' }}}">
										<div class="form-group">
											<label class="control-label" for="lastname">{{{ Lang::get('register/personal_data.lastname') }}}</label>
											<input class="form-control" name="lastname" id="lastname" data-validate="required" value="{{{ Input::old('lastname') }}}" placeholder="{{{ Lang::get('register/personal_data.lastname') }}}" />
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-3 {{{ $errors->has('cpf') ? 'has-error' : '' }}}">
										<div class="form-group">
											<label class="control-label" for="cpf">CPF</label>
											<input class="form-control" name="cpf" id="cpf" value="{{{ Input::old('cpf') }}}" placeholder="CPF" />
										</div>
									</div>

									<div class="col-md-3 {{{ $errors->has('telephone') ? 'has-error' : '' }}}">
										<div class="form-group">
											<label class="control-label" for="telephone">{{{ Lang::get('register/personal_data.telephone') }}}</label>
											<input type="text" class="form-control" name="telephones[0][0]" id="telephone" value="{{{ Input::old('telephones.0.0') }}}" placeholder="{{{ Lang::get('register/personal_data.telephone') }}}" />
										</div>
									</div>

									<div class="col-md-3">
										<div class="form-group">
											<label class="control-label" for="telephone">Operadora(Claro,Tim,etc)</label>
											<input type="text" class="form-control" name="telephones[0][1]" id="provider"  value="{{{ Input::old('telephones.0.1')}}}" placeholder="Operadora" >
										</div>
									</div>

									<div class="col-md-3 {{{ $errors->has('postcode') ? 'has-error' : '' }}}">
										<div class="form-group">
											<label class="control-label" for="postcode">{{{ Lang::get('register/personal_data.postcode') }}}</label>
											<input class="form-control" name="postcode" id="postcode" value="{{{ Input::old('postcode') }}}" placeholder="{{{ Lang::get('register/personal_data.postcode') }}}" />
										</div>
									</div>
								</div>
								
								<div class="row">
									<div class="col-md-12 {{{ $errors->has('address') ? 'has-error' : '' }}}">
										<div class="form-group" >
											<label class="control-label" for="address">{{{ Lang::get('register/personal_data.address') }}}</label>
											<input class="form-control" name="address" id="address" value="{{{ Input::old('address') }}}" placeholder="{{{ Lang::get('register/personal_data.address') }}}" />
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-12 {{{ $errors->has('address_2') ? 'has-error' : '' }}}">
										<div class="form-group" >
											<input class="form-control" name="address_2" id="address_2" value="{{{ Input::old('address_2') }}}" placeholder="{{{ Lang::get('register/personal_data.address_2') }}}" />
										</div>
									</div>
								</div>
								
								<div class="row">
									
									<div class="col-md-4 {{{ $errors->has('city') ? 'has-error' : '' }}}">
										<div class="form-group">
											<label class="control-label" for="city">{{{ Lang::get('register/personal_data.city') }}}</label>
											<input class="form-control" name="city" id="city" value="{{{ Input::old('city') }}}" placeholder="{{{ Lang::get('register/personal_data.city') }}}" />
										</div>
									</div>

									<div class="col-md-4 {{{ $errors->has('zone') ? 'has-error' : '' }}}">
										<div class="form-group">
											<label class="control-label" for="zone">{{{ Lang::get('register/personal_data.zone') }}}</label>
											<input class="form-control" name="zone" id="zone" value="{{{ Input::old('zone') }}}" placeholder="{{{ Lang::get('register/personal_data.zone') }}}" />
										</div>
									</div>
									
									<div class="col-md-4 {{{ $errors->has('country') ? 'has-error' : '' }}}">
										<div class="form-group">
											<label class="control-label" for="country">{{{ Lang::get('register/personal_data.country') }}}</label>
											<input class="form-control" name="country" id="country" value="{{{ Input::old('country') }}}" placeholder="{{{ Lang::get('register/personal_data.country') }}}" />
										</div>
									</div>
									
								</div>
								
							</div>

							<div class="tab-pane" id="tab2-3">
								<div class="row">
									
									<div class="col-md-3 {{{ $errors->has('parent') ? 'has-error' : '' }}}">
										<div class="form-group">
											<label class="control-label">{{{ Lang::get('register/access.sponsor') }}}</label>
											
											<div class="input-group">
												<div class="input-group-addon">
													<i class="entypo-star"></i>
												</div>
												
												<input id="parent" type="text" class="form-control" name="parent" value="{{{ Input::old('parent', $parent->username) }}}">
											</div>
											<em class="loadPatrocinador"></em>
										</div>
									</div>
									<?php /*
									@if(isset($is_office) || isset($is_admin))
									<div class="col-md-5 {{{ $errors->has('side_network') ? 'has-error' : '' }}}">
										<div class="form-group">
											<label for="side_network" class="control-label">{{{ Lang::get('register/access.side') }}}</label>
											
											<div class="input-group">
					{{ Form::select(
						'side_network', ['' => 'Definido Pela Chave', 1 => 'Lado Esquerdo', 2 => 'Lado Direito'],
						Input::old('side_network', isset($parent->mlm->side_pattern_network) ? $parent->mlm->side_pattern_network : null), 
						array('id' => 'side_network', 'class' => 'form-control')
					) }}
											</div>
										</div>
									</div>
									@endif
									*/?>
								</div>

								<div class="row">
									
									<div class="col-md-6 {{{ $errors->has('email') ? 'has-error' : '' }}}">
										<div class="form-group">
											<label class="control-label">{{{ Lang::get('register/access.email') }}}</label>
											
											<div class="input-group">
												<div class="input-group-addon">
													<i class="entypo-key"></i>
												</div>
												
												<input type="text" class="form-control" name="email" id="email" data-validate="required" value="{{{ Input::old('email') }}}" placeholder="{{{ Lang::get('register/access.email') }}}" />
											</div>
										</div>
									</div>
								</div>
						
								<div class="row">
									<div class="col-md-12 {{{ $errors->has('username') ? 'has-error' : '' }}}">
										<div class="form-group">
											<label class="control-label">{{{ Lang::get('register/access.username') }}}</label>
											
											<div class="input-group">
												<div class="input-group-addon">
													<i class="entypo-user"></i>
												</div>
												
												<input type="text" class="form-control" name="username" id="username" data-validate="required,minlength[5]" data-mask="[a-z]+$" data-is-regex="true" value="{{{ Input::old('username') }}}" data-message-minlength="Username must have minimum of 5 chars." placeholder="{{{ Lang::get('register/access.username') }}}" />
											</div>
											<em class="loadUsuario"></em>
										</div>
									</div>
								</div>
						
								<div class="row">
									
									<div class="col-md-6 {{{ $errors->has('password') ? 'has-error' : '' }}}">
										<div class="form-group">
											<label class="control-label">{{{ Lang::get('register/access.password') }}}</label>
											
											<div class="input-group">
												<div class="input-group-addon">
													<i class="entypo-key"></i>
												</div>
												
												<input type="password" class="form-control" name="password" id="password" data-validate="required" placeholder="{{{ Lang::get('register/access.password') }}}" />
											</div>
										</div>
									</div>
									
									<div class="col-md-6 {{{ $errors->has('password_confirmation') ? 'has-error' : '' }}}">						
										<div class="form-group">
											<label class="control-label">{{{ Lang::get('register/access.confirm_password') }}}</label>
											
											<div class="input-group">
												<div class="input-group-addon">
													<i class="entypo-key"></i>
												</div>
												
												<input type="password" class="form-control" name="password_confirmation" id="password_confirmation" data-validate="required" placeholder="{{{ Lang::get('register/access.confirm_password') }}}" />
											</div>
										</div>
									</div>

								</div>

								<div class="row">
									<div class="col-md-12 {{{ $errors->has('chk_rules') ? 'has-error' : '' }}}">	
										<div class="form-group">
											<div class="checkbox checkbox-replace">
												<input type="checkbox" name="chk_rules" id="chk_rules" data-validate="required" data-message-message="You must accept rules in order to complete this registration.">
												<label for="chk_rules">{{{ Lang::get('register/access.text_atention') }}} <a href="{{ asset('downloads/CONTRATO DIVULGADOR.pdf') }}" target="_blank"></a>.</label>
											</div>
										</div>
									</div>
								</div>

								<button type="submit" class="btn btn-blue">{{{ Lang::get('register/access.done') }}}</button>
											
							</div>

							<ul class="pager wizard">
								<li class="previous">
									<a href="#"><i class="entypo-left-open"></i> {{{ Lang::get('register/footer.previus') }}}</a>
								</li>
										
								<li class="next">
									<a href="#">{{{ Lang::get('register/footer.next') }}} <i class="entypo-right-open"></i></a>
								</li>
							</ul>

						</div>

					</form>
				</div>
			</div>
		</div>
	</div>

@stop