@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')

	{{-- Create User Form --}}
	<form class="form-horizontal" method="post" action="@if (isset($withdraw)){{ URL::to('admin/withdraws/' . $withdraw->id . '/edit') }}@endif" autocomplete="off">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->
		<!-- username -->
		<div class="form-group {{{ $errors->has('username') ? 'has-error' : '' }}}">
			<label class="col-md-2 control-label" for="username">{{{ Lang::get('register/access.username') }}}</label>
			<div class="col-md-10">
				<input class="form-control" type="text" name="username" id="username" value="{{{ Input::old('username', isset($withdraw) ? $withdraw->user->username : null) }}}" />
				{{ $errors->first('username', '<span class="help-inline">:message</span>') }}
			</div>
		</div>
		<!-- ./ username -->

		<div class="form-group {{{ $errors->has('value') ? 'has-error' : '' }}}">
			<label for="value" class="col-sm-2 control-label">{{{ Lang::get('admin/points.value') }}}</label>
			
			<div class="col-md-10">
				<input type="text" class="form-control reais" id="value" name="value" value="{{{ Input::old('value', isset($withdraw) ? $withdraw->value : null) }}}">
				{{ $errors->first('value', '<span class="help-inline">:message</span>') }}
			</div>
		</div>

		<div class="form-group {{{ $errors->has('payment_method') ? 'has-error' : '' }}}">
			<label for="payment_method" class="col-sm-2 control-label">{{{ Lang::get('admin/withdrawal.payment_method') }}}</label>
			
			<div class="col-md-10">
				<input type="text" class="form-control" id="payment_method" name="payment_method" value="{{{ Input::old('payment_method', isset($withdraw) ? $withdraw->payment_method : null) }}}">
				{{ $errors->first('payment_method', '<span class="help-inline">:message</span>') }}
			</div>
		</div>

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-offset-2 col-md-10">
				<a href="javascript:;" class="btn btn-default close_popup" onclick="parent.oTable.fnReloadAjax(); parent.$.colorbox.close();">{{{ Lang::get('admin/register.cancel') }}}</a>
				<button type="submit" class="btn btn-success">OK</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop
