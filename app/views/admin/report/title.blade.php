@extends('admin.layouts.default')

@section('title')
{{{ Lang::get('admin/report/title.title_historic') }}} ::
@parent
@stop

@section('content')
	<div class="row">
		<div class="col-md-12">

			<h3>{{{ Lang::get('admin/report/title.title_historic') }}}</h3>

			<table class="table table-bordered datatable" id="users">
				<thead>
					<tr class="replace-inputs">
						<th>{{{ Lang::get('admin/report/table.username') }}}</th>
						<th>{{{ Lang::get('admin/report/table.title') }}}</th>
						<th>{{{ Lang::get('admin/report/table.create_at') }}}</th>
					</tr>
					<tr>
						<th>{{{ Lang::get('admin/report/table.username') }}}</th>
						<th>{{{ Lang::get('admin/report/table.title') }}}</th>
						<th>{{{ Lang::get('admin/report/table.create_at') }}}</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>{{{ Lang::get('admin/report/table.username') }}}</th>
						<th>{{{ Lang::get('admin/report/table.title') }}}</th>
						<th>{{{ Lang::get('admin/report/table.create_at') }}}</th>
					</tr>
				</tfoot>
			</table>

			<div class="clear: both;"></div>
			
			<div style="float:right;">
				<a href="javascript:window.print();" class="btn btn-primary btn-icon icon-left hidden-print">{{{ Lang::get('office/extract.print') }}}<i class="entypo-doc-text"></i>
				</a>
			</div>	
		</div>
	</div>
@stop


{{-- Scripts --}}
@section('scripts')
	<script type="text/javascript">
		var oTable;
		$(document).ready(function() {

			$.datepicker.regional[""].dateFormat = 'dd/mm/yy';
            $.datepicker.setDefaults($.datepicker.regional['']);
			
			oTable = $('#users').dataTable( {
				"sPaginationType": "bootstrap",
				"sDom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'<'export-data'T>f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
				"aaSorting": [[ 2, "desc"]],
				//"bServerSide": true,
				//"bProcessing": true,
		        "sAjaxSource": "{{ URL::action('AdminReportController@getDatatitle') }}",
		        "fnDrawCallback": function ( oSettings ) {
	           		$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
	     		}
			});

			oTable.columnFilter({
				"sPlaceHolder" : "head:after",
				aoColumns: [ "sPlaceHolder", "sPlaceHolder", { type: "date-range" }]
			});
		});
	</script>
	<style type="text/css">
	.dataTables_processing{
		display: none;
	}
	</style>
@stop