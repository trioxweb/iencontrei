@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')

	{{-- Create User Form --}}
	<form class="form-horizontal" method="post" action="@if (isset($point)){{ URL::to('admin/points/' . $point->id . '/edit') }}@endif" autocomplete="off">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<!-- username -->
		<div class="form-group {{{ $errors->has('username') ? 'has-error' : '' }}}">
			<label class="col-md-2 control-label" for="username">{{{ Lang::get('register/access.username') }}}</label>
			<div class="col-md-10">
				<input class="form-control" type="text" name="username" id="username" value="{{{ Input::old('username', isset($point) ? $point->user->username : null) }}}" />
				{{ $errors->first('username', '<span class="help-inline">:message</span>') }}
			</div>
		</div>
		<!-- ./ username -->

		<div class="form-group {{{ $errors->has('value') ? 'has-error' : '' }}}">
			<label for="value" class="col-sm-2 control-label">{{{ Lang::get('admin/points.value') }}}</label>
			
			<div class="col-md-10">
				<input type="text" class="form-control" id="value" name="value" value="{{{ Input::old('value', isset($point) ? $point->value : null) }}}">
			</div>
		</div>

		<!-- Status -->
		<div class="form-group {{{ $errors->has('status') ? 'has-error' : '' }}}">
			<label class="col-md-2 control-label" for="status">{{{ Lang::get('admin/points.points_active') }}}</label>
			<div class="col-md-6">
				@if ($mode == 'create')
					<select class="form-control" name="status" id="status">
						<option value="1"{{{ (Input::old('status', 0) === 1 ? ' selected="selected"' : '') }}}>{{{ Lang::get('general.yes') }}}</option>
						<option value="0"{{{ (Input::old('status', 0) === 0 ? ' selected="selected"' : '') }}}>{{{ Lang::get('general.no') }}}</option>
					</select>
				@else
					<select class="form-control" name="status" id="status">
						<option value="1"{{{ ($point->status ? ' selected="selected"' : '') }}}>{{{ Lang::get('general.yes') }}}</option>
						<option value="0"{{{ ( !$point->status ? ' selected="selected"' : '') }}}>{{{ Lang::get('general.no') }}}</option>
					</select>
				@endif
				{{ $errors->first('confirm', '<span class="help-inline">:message</span>') }}
			</div>
		</div>
		<!-- ./ status -->

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-offset-2 col-md-10">
				<a href="javascript:;" class="btn btn-default close_popup" onclick="parent.oTable.fnReloadAjax(); parent.$.colorbox.close();">{{{ Lang::get('admin/register.cancel') }}}</a>
				<button type="submit" class="btn btn-success">OK</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop
