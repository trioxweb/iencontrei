<!DOCTYPE html>

<html lang="en">

<head>

	@include('layouts/head', array('title'=>'Admin'))

</head>

<body class="page-body skin-default page-fade-only gray" data-url="{{{ URL::to('/') }}}">

<div class="page-container horizontal-menu">

	<header class="navbar navbar-fixed-top"><!-- set fixed position by adding class "navbar-fixed-top" -->
		
		<div class="navbar-inner">
		
			<!-- logo -->
			<div class="navbar-brand">
				<a href="{{{ URL::action('AdminDashboardController@getIndex') }}}">
					<img src="/assets/img/logo/logo_empresa.png" height="20px" alt="" />
				</a>
			</div>

			<!-- welcome -->
			<!--<div class="navbar-brand">
				{{{ Lang::get('admin/header.text_welcome') }}}, <b>{{{ Auth::user()->data->firstname }}}</b>
			</div>-->
			
			
			<!-- main menu -->
						
			<ul class="navbar-nav">

				<li class="root-level has-sub"> 
					<a href="{{{ URL::action('AdminDashboardController@getIndex') }}}">
						<i class="entypo-monitor" style="color:#701c1c"></i>
						<span class="title">Home</span>
					</a> 
				</li>

				<!-- SIGNUPS -->
				@if(Auth::user()->can("manage_users"))
				<li class="root-level has-sub"> 
					<a href="#">
						<i class="entypo-user-add" style="color:#701c1c"></i>
						<span class="title">{{{ Lang::get('admin/header.signups') }}}</span>
					</a> 

					<ul style="opacity: 0; transform: matrix(1, 0, 0, 1, -10, 0); visibility: hidden; display: none;"> 
						<li>
							<a href="{{{ URL::action('AdminUsersController@getIndex') }}}"><span>{{{ Lang::get('admin/header.signups') }}}</span></a>
						</li>

						<li>
							<a href="{{{ URL::action('RegisterController@getIndex') }}}"><span>{{{ Lang::get('admin/header.signups_add_on') }}}</span></a>
						</li>
					</ul>
				</li>
				@endif

				<!-- FINANCE -->
				@if(Auth::user()->can("manage_credits_withdraws") || Auth::user()->can("manage_points"))
				<li class="root-level has-sub"> 
					<a href="#">
						<i class="entypo-doc-text" style="color:#701c1c"></i>
						<span class="title">{{{ Lang::get('admin/header.finance') }}}</span>
					</a> 

					<ul style="opacity: 0; transform: matrix(1, 0, 0, 1, -10, 0); visibility: hidden; display: none;"> 
						@if(Auth::user()->can("manage_credits_withdraws"))
							<li>
							<a href="{{{ URL::action('AdminCreditsController@getIndex') }}}"><span>{{{ Lang::get('admin/header.finance_credits') }}}</span></a>
							</li>
							<li>
							<a href="{{{ URL::action('AdminWithdrawsController@getIndex') }}}"><span>{{{ Lang::get('admin/header.finance_withdrawal') }}}</span></a>
							</li>
							<li>
							<a href="{{{ URL::action('AdminCreditsController@getTransfers') }}}"><span>{{{ Lang::get('admin/header.finance_transfers') }}}</span></a>
							</li>
							<li>
							<a href="{{{ URL::action('AdminCreditsController@getGains') }}}"><span>{{{ Lang::get('admin/header.finance_gains') }}}</span></a>
							</li>
							<li>
							<a href="{{{ URL::action('AdminCreditsController@getAdditionals') }}}"><span>{{{ Lang::get('admin/header.finance_credits_additionals') }}}</span></a>
							</li>
						@endif
						@if(Auth::user()->can("manage_points"))
							<li>
							<a href="{{{ URL::action('AdminPointsController@getIndex') }}}"><span>{{{ Lang::get('admin/header.points') }}}</span></a>
							</li>
						@endif
						<li>
							<a href="{{{ URL::action('AdminFinancingsController@getIndex') }}}"><span>{{{ Lang::get('admin/header.financings') }}}</span></a>
						</li>
					</ul>
				</li>
				@endif

				<!-- REPORTS -->
				@if(Auth::user()->can("manage_reports"))
				<li class="root-level has-sub">
					<a href="#">
						<i class="entypo-archive" style="color:#701c1c"></i>
						<span>{{{ Lang::get('admin/report/title.report') }}}</span>
					</a>
				
					<ul style="opacity: 0; transform: matrix(1, 0, 0, 1, -10, 0); visibility: hidden; display: none;">
						<li>
							<a href="{{{ URL::to('admin/reports/binary') }}}"><span>{{{ Lang::get('admin/report/title.binary') }}}</span></a>
						</li>
						<li>
							<a href="{{{ URL::action('AdminReportController@getTitle') }}}"><span>{{{ Lang::get('admin/report/title.title') }}}</span></a>
						</li>
					</ul>
				</li>
				@endif

				<li>
					<a href="{{{ URL::to('admin/orders') }}}">
						<i class="entypo-clipboard" style="color:#701c1c"></i>
						<span>{{ Lang::get('office/orders/title.orders') }}</span>
					</a>
				</li>
				
				<!-- BLOG -->	
				@if(Auth::user()->can("manage_blogs"))
				<li>
					<a href="{{{ URL::to('admin/blogs') }}}">
						<i class="entypo-info" style="color:#701c1c"></i>
						<span>{{{ Lang::get('admin/blogs/title.blog_management') }}}</span>
					</a>
				</li>
				@endif
				
				<!-- SETTINGS -->
				@if(Auth::user()->can('manage_setting'))
				<li class="root-level has-sub">
					<a href="#"><i class="entypo-doc-text" style="color:#701c1c"></i><span>Configurações</span></a>
					
					<ul style="opacity: 0; transform: matrix(1, 0, 0, 1, -10, 0); visibility: hidden; display: none;">
						<li>
							<a href="{{{ URL::action('AdminPlansController@getIndex') }}}"><span>Plans Management</span></a>
						</li>
						<li>
							<a href="{{{ URL::action('AdminRolesController@getIndex') }}}"><span>Role Management</span></a>
						</li>
					</ul>
				</li>
				@endif
				
				@if(Auth::user()->can("manage_credits_withdraws"))
				<li>
				<a href="{{{ URL::to('admin/settings') }}}"><i class="entypo-tools" style="color:#701c1c"></i><span>{{{ Lang::get('admin/settings/title.settings') }}}</span></a>
				</li>
				@endif
			</ul>
						
			<!-- notifications and other links -->
			<ul class="nav navbar-right pull-right">

				<li class="dropdown"> 
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> 
						<i class="entypo-list"></i> 
						<span class="badge badge-info">Links</span> 
					</a> <!-- dropdown menu (tasks) --> 

						<ul class="dropdown-menu">
							<li> 
								<ul class="dropdown-menu-list scroller" tabindex="5001" style="overflow: hidden; outline: none;"> 									
									<li> 
										<a href="{{{ URL::to('office') }}}" target="_blank"> 
											<span>{{{ Lang::get('admin/header.backoffice') }}}</span> 
										</a> 
									</li>
									<li> 
										<a href="{{{ URL::to('admin/manual') }}}" target="_blank"> 
											<span>{{{ Lang::get('admin/manual/title.manual') }}}</span> 
										</a> 
									</li>
								</ul> 
							</li>
						</ul>
				
				<!-- language selector -->

				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-close-others="true">
						<img src="/assets/img/idioma/{{{ App::getLocale() }}}.png" />
					</a>
					
					<ul class="dropdown-menu pull-right">
						<li @if(App::getLocale() == 'en') class="active" @endif>
							<a href="/setlang/en">
								<img src="/assets/img/idioma/en.png" />
								<span>English</span>
							</a>
						</li>
						<li @if(App::getLocale() == 'pt') class="active" @endif>
							<a href="/setlang/pt">
								<img src="/assets/img/idioma/pt.png" />
								<span>Portugues</span>
							</a>
						</li>
						<li @if(App::getLocale() == 'es_ES') class="active" @endif>
							<a href="/setlang/es_ES">
								<img src="/assets/img/idioma/es_ES.png" />
								<span>Espanish</span>
							</a>
						</li>
					</ul>
						
				</li>

				<li class="sep"></li>
				
				<li>
					<a href="{{{ URL::to('user/logout') }}}">
						{{{ Lang::get('admin/header.logout') }}} <i class="entypo-logout right"></i>
					</a>
				</li>
				
				
				<!-- mobile only -->
				<li class="visible-xs">	
				
					<!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
					<div class="horizontal-mobile-menu visible-xs">
						<a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
							<i class="entypo-menu"></i>
						</a>
					</div>
					
				</li>
				
			</ul>
	
		</div>
</header>

	<div class="main-content">
	
	<div class="row"></div>

	<!-- ./ navbar -->

	<!-- Notifications -->
	@include('notifications')
	<!-- ./ notifications -->

	<!-- Content -->
	@yield('content')
	<!-- ./ content -->
	<div style="clear:both"></div>
	<!-- Footer -->
	<footer class="main">
	
	<div class="pull-right">
		<a href="https://trioxweb.com" target="_blank"><img src="{{ asset('assets/img/trioxweb.png') }}"></a>
	</div>
		
	&copy; 2015 <strong>{{ Setting::getSetting('name')}}</strong> - Todos os direitos reservados.
	
	</footer>
	<!-- ./ Footer -->

	</div>
	<!-- ./ container -->
	
	@yield('scripts_default')

	@yield('scripts')

	<script>
	$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
	</script>
</body>

</html>
