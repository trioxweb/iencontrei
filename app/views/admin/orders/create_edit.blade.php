@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')

	{{-- Create User Form --}}
	<form class="form-horizontal" method="post" action="@if (isset($order)){{ URL::to('admin/orders/' . $order->id . '/edit') }}@endif" autocomplete="off">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->
		
		
		<!-- username -->
		@if($mode == 'create')
			<div class="form-group {{{ $errors->has('username') ? 'has-error' : '' }}}">
				<label class="col-md-2 control-label" for="username">{{{ Lang::get('admin/orders/title.username') }}}</label>
				<div class="col-md-10">
					<input class="form-control" type="text" name="username" id="username" value="{{{ Input::old('username') }}}" />
					{{ $errors->first('username', '<span class="help-inline">:message</span>') }}
				</div>
			</div>
		@elseif($mode == 'edit' && $order->order_plan()->first())
			<div class="form-group">
				<label class="col-md-2 control-label" for="username">{{{ Lang::get('admin/orders/title.username') }}}</label>	
				<div class="col-md-10">
					<input type="text" class="form-control" id="username" value="{{{ $order->user->username }}}" disabled />
				</div>
			</div>
		@endif
		<!-- ./ username -->

		<!-- plan -->
		@if($mode == 'create')
			<div class="form-group {{{ $errors->has('network_plan_id') ? 'has-error' : '' }}}">
				<label class="col-md-2 control-label" for="network_plan_id">Plan</label>	
				<div class="col-md-10">
					{{ Form::select('network_plan_id', $plans->lists('name', 'id'),
							Input::old('network_plan_id'), 
							array('id' => 'network_plan_id', 'class' => 'form-control')) }}
					{{ $errors->first('network_plan_id', '<span class="help-inline">:message</span>') }}
				</div>
			</div>
		@elseif($mode == 'edit' && $order->order_plan()->first())
			<div class="form-group">
				<label class="col-md-2 control-label" for="network_plan_id">{{{ Lang::get('admin/orders/title.plan') }}}</label>	
				<div class="col-md-10">
					<input type="text" class="form-control" id="network_plan_id" value="{{{ $order->order_plan()->first()->name }}}" disabled />
				</div>
			</div>
		@endif
		<!-- ./ plan -->

		<!-- payment method -->
		<div class="form-group {{{ $errors->has('network_plan_id') ? 'has-error' : '' }}}">
			<label class="col-md-2 control-label" for="network_plan_id">{{{ Lang::get('admin/orders/title.payment_method') }}}</label>	
			<div class="col-md-10">
				{{ Form::select('network_plan_id', $payment_methods,
						Input::old('network_plan_id'), 
						array('id' => 'network_plan_id', 'class' => 'form-control')) }}
				{{ $errors->first('network_plan_id', '<span class="help-inline">:message</span>') }}
			</div>
		</div>
		<!-- ./ payment method -->

		<!-- total -->
		@if($mode == 'edit')
			<div class="form-group">
				<label for="total" class="col-sm-2 control-label">{{{ Lang::get('admin/orders/title.total') }}}</label>
				
				<div class="col-md-10">
					<input type="text" class="form-control" id="total" name="total" value="@currency($order->total, $order->currency_code)" disabled />
				</div>
			</div>
		@endif
		<!-- ./ total -->

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-offset-2 col-md-10">
				<a href="javascript:;" class="btn btn-default close_popup" onclick="parent.oTable.fnReloadAjax(); parent.$.colorbox.close();">{{{ Lang::get('admin/register.cancel') }}}</a>
				<button type="submit" class="btn btn-success">OK</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop
