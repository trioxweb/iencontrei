@extends('admin.layouts.default')

@section('title')
Orders ::
@parent
@stop

@section('content')

	<div class="row">
		<div class="col-md-12">

			<h3>{{{ Lang::get('admin/orders/title.orders') }}}</h3>
			<br>
			
			<table class="table table-bordered datatable" id="users">
				<thead>
					<tr class="replace-inputs">
						<th class="col-sm-1">#</th>
						<th>{{{ Lang::get('admin/orders/table.username') }}}</th>
						<th>{{{ Lang::get('admin/orders/table.name') }}}</th>
						<th>{{{ Lang::get('admin/orders/table.total') }}}</th>
						<th>{{{ Lang::get('admin/orders/table.created_at') }}}</th>
						<th>{{{ Lang::get('admin/orders/table.payment_method') }}}</th>
						<th>{{{ Lang::get('admin/orders/table.status') }}}</th>
						<th class="col-sm-4"></th>
					</tr>
					<tr>
						<th>#</th>
						<th>{{{ Lang::get('admin/orders/table.username') }}}</th>
						<th>{{{ Lang::get('admin/orders/table.name') }}}</th>
						<th>{{{ Lang::get('admin/orders/table.total') }}}</th>
						<th>{{{ Lang::get('admin/orders/table.created_at') }}}</th>
						<th>{{{ Lang::get('admin/orders/table.payment_method') }}}</th>
						<th>{{{ Lang::get('admin/orders/table.status') }}}</th>
						<th class="col-sm-4">{{{ Lang::get('admin/orders/table.actions') }}}</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>#</th>
						<th>{{{ Lang::get('admin/orders/table.username') }}}</th>
						<th>{{{ Lang::get('admin/orders/table.name') }}}</th>
						<th>{{{ Lang::get('admin/orders/table.total') }}}</th>
						<th>{{{ Lang::get('admin/orders/table.created_at') }}}</th>
						<th>{{{ Lang::get('admin/orders/table.payment_method') }}}</th>
						<th>{{{ Lang::get('admin/orders/table.status') }}}</th>
						<th class="col-sm-4">{{{ Lang::get('admin/orders/table.actions') }}}</th>
					</tr>
				</tfoot>
			</table>

			<div class="clear: both;"></div>

		
		</div>
	</div>

	<!-- Modal Dialog -->
	<div id="modal-dialog" class="modal fade">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title">{{ Lang::get('admin/register.are_you_sure') }}</h4>
	      </div>
	      <div class="modal-body">
	        <p>{{ Lang::get('admin/register.are_you_sure_about_this') }}?</p>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">{{{ Lang::get('admin/register.cancel') }}}</button>
	        <a href="#" class="btn btn-danger" id="confirm">{{{ Lang::get('office/home.active_true') }}}</a>
	      </div>
	    </div>
	  </div>
	</div>


@stop

{{-- Scripts --}}
@section('scripts')
	<script type="text/javascript">
		var oTable;
		$(document).ready(function() {
			oTable = $('#users').dataTable( {
				"sPaginationType": "bootstrap",
				"sDom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'<'export-data'T>f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
				"aaSorting": [[ 4, "desc"]],
				//"bServerSide": true,
				//"bProcessing": true,
		        "sAjaxSource": "{{ URL::action('AdminOrdersController@getData') }}",
		        "fnDrawCallback": function ( oSettings ) {
	           		$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
	     		}
			});

			oTable.columnFilter({
				"sPlaceHolder" : "head:after",
				aoColumns: [ "sPlaceHolder", "sPlaceHolder", "sPlaceHolder", "sPlaceHolder", "sPlaceHolder", "sPlaceHolder", { type: "select" }]
			});

			$('#modal-dialog').on('show.bs.modal', function (e) {
			      href = $(e.relatedTarget).attr('data-href');
			      $(this).find('.modal-footer #confirm').attr('href', href);
		    });
		});
	</script>
	<style type="text/css">
	.dataTables_processing{
		display: none;
	}
	</style>
@stop