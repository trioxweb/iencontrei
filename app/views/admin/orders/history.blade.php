@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')

	<div class="row">
		<div class="col-md-12">
			<table id="history" class="table table-striped table-hover">
				<thead>
					<tr>
						<th class="col-md-2">{{{ Lang::get('admin/orders/table.name') }}}</th>
						<th class="col-md-2">{{{ Lang::get('admin/orders/table.created_at') }}}</th>
						<th class="col-md-2">{{{ Lang::get('admin/orders/table.actions') }}}</th>
					</tr>
				</thead>
				<tbody>
					@foreach($order_historys as $order_history)
					<tr>
						<th class="col-md-2">{{$order_history->order_status()->first()->name}}</th>
						<th class="col-md-2">{{$order_history->created_at}}</th>
						<th><a href="{{{ URL::to('admin/orders/' . $order_history->id . '/history_delete' ) }}}" class="btn btn-danger btn-sm">{{{ Lang::get('admin/orders/button.delete') }}}</a>
						</th>
					</tr>
					@endforeach
					@if($order_historys->count() == 0)
					<tr>			
						<th colspan="6">{{ Lang::get('admin/orders/table.no_history_found') }}</th>
					</tr>
					@endif
				</tbody>
			</table>
		</div>
	</div>

	{{-- Create User Form --}}
	<form class="form-horizontal" method="post" action="@if (isset($order)){{ URL::to('admin/orders/' . $order->id . '/history') }}@endif" autocomplete="off">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<!-- payment method -->
		<div class="form-group {{{ $errors->has('order_status_id') ? 'has-error' : '' }}}">
			<div class="col-md-4">
				{{ Form::select('order_status_id', $order_status->lists('name', 'id'),
						Input::old('order_status_id'), 
						array('id' => 'order_status_id', 'class' => 'form-control')) }}
				{{ $errors->first('order_status_id', '<span class="help-inline">:message</span>') }}
			</div>

			<div class="col-md-4">
				<button type="submit" class="btn btn-success">{{{ Lang::get('admin/orders/title.add_to_history') }}}</button>
			</div>
		</div>
		<!-- ./ payment method -->
	</form>
@stop
