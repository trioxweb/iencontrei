			<h1>1. Admin</h1>

			<h2>1.1. Gerenciando os usuários</h2>
			<p>Para gerenciar os usuários na administração é necessário entrar no menu “Cadastros”. Dentro desta página, havera uma lista e todas as informações de cada usuário cadastrado.</p>
			<p>– Para realizar uma alteração no cadastro é preciso acessar o botão “Editar”.</p>
			<p>– Para posicionar um cadastro na rede, é preciso clivar em “Posicionar na rede”.</p>
			<p>– Para aprovar o cadastro e o mesmo começar a receber as bonificações (após a confirmação do pagamento do pacote), é necessario clicar em “Aprovar”.</p>
			<p>– O administrador também poderá acessar o backoffice do usuário, no botão “Acessar”.</p>
			
			<h2>1.2 Controle Financeiro</h2>
			
			<h2>1.2.1. Créditos</h2>
			<p>A página créditos dentro do menu "Financeiro" lista todas as operações de creditos para os usuários. Nela o Administrador poderá:</p>
			<p>- Adicionar um crédito (qualquer valor) para um usuario.</p>
			<p>- Adicionar crédito de divisão de lucro para um usuário.</p>

			<h2>1.2.1. Saques</h2>
			<p>Para o administrador visualizar todos os saques solicitador por seus usuários, é preciso acessar a página "Saques" dentro de "Financeiro".</p>
			
			<h2>1.3. Pedidos</h2>
			<p>Na página pedidos é onde ficara todos os pedidos (upgrade, pagamento de pacote ou ativação). Nesta página o administrador poderá:</p>
			<p>- No botão "Histórico": Alterar status do pedido</p>
			<p>- No botão "Excluir": Deletar pedido</p>

			<h2>1.4. Gerenciar Blog</h2>
			<p>Para se comunicar com os seus usuários, o administrador poderá escrever no blog interno, que será localizado na home page do backoffice.</p>
			<p>Para adicionar um comunicado é preciso apertar no botão "Criar" e inserir o título e a mensagem, após isso, é só clicar em "Publicar".</p>

			<h2>1.5 Link de compartilhamento</h2>
			<p>Nesta sessão o administrador poderá alterar o link que os usuários devem compartilhar diariamente.</p>