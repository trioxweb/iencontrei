			<h1>2. Backoffice</h1>

			<h2>2.1. Minha Conta</h2>
			<p>Para fazer alterações na conta do usuário, dele deverá acessar o menu <b>“Minha Conta”</b> para fazer qualquer tipo de alteração nos dados cadastrados.</p>
			
			<h2>2.2. Pedidos</h2>
			<p>Ná página pedidos o usuário poderá realizar pedidos de:</p>
			<p>- Upgrade</p>
			<p>- Compra de Pacote</p>
			<p>- Pagamento de ativação</p>

			<p>Para realizar um pedido o usuário deverá clicar no item desejado, após isso, dele será direcionado para a página de pagamento, onde poderá ser feito a escolha da forma de pagamento.</p>
			<p>Logo, irá gerar um pedido que se localizara na página pedidos e o usuário poderá visualizar o status do pedido.</p>

			<h2>2.3. Minha Rede</h2>
			
			<h2>2.4. Binário</h2>
			<p>Na página binário, o usuário terá a visualização da árvore binária.</p>

			<h2>2.5. Meus Diretos</h2>
			<p>Na página dos "Meus diretos" o usuário poderá visualizar todos os cadastros diretos.</p>

			<h2>2.6. Financeiro</h2>
			<p>A página financeiro é voltada para toda movimentação financeira do usuário. Onde ele poderá:</p>
			<p>- Solicitar saque</p>
			<p>- Transferir saldo para usuário</p>
			<p>- Transferir saldo para a loja</p>

			<h2>2.7. Tarefas<h2>
			<p>A página tarefas é onde todos os usuários deverão fazer o seu compartilhamento diário.</p>
			<p>Para realizar o compartilhamento, o usuário deverá fazer o login com o facebook, autorizar as publicações em modo público (se não o sistema não irá validar) e compartilhar os links que aparecerão em sua tela.</p>