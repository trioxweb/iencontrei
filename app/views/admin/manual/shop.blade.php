<h2>3. Loja Virtual</h2>

<p>Página Principal: www.neonnapp.com/shop Esta é a sua loja virtual, onde cada “associado” vai ter a sua após a aprovação (item Aprovando Cadastros) com o link http://usuario.seutrioxmatriz.com.br. E nela estarão os respectivos produtos a serem vendidos.</p>

<p>Administrador da Loja Virtual: www.neonnapp.com/shop/admin</p>
<p>Na página: Vendas > Pedidos. Haverá todos os pedidos feitos na loja virtual de todos os associados. Após entrar nesta lista de pedidos, para analisar o pedido e conferir as respectivas comissões dos associados, clique em “Detalhes”. Na sessão Detalhes do Pedido vai constar todas as informações sobre o mesmo, e no item Comissão vai conter a respectiva comissão e para qual associado gera a comissão, e o botão “Aprovar comissão” é que vai direcionar a comissão ao usuário.</p>
<p>Na sessão Situação do pedido, o administrador da loja poderá encaminhar (email) e deixar registrado em qual situação o produto esta, também, com uma caixa de texto para encaminhar o que desejar ao cliente.</p>