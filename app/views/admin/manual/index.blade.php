@extends('admin.layouts.default')

@section('title')
{{{ $title }}} ::
@parent
@stop

@section('content')

	<div class="row">
		<div class="col-md-12">

			<h3>{{{ $title }}}</h3>

			<div class="panel panel-primary" style="padding: 10px 20px 10px 20px">

				<a style="float:right" href="{{{ URL::to('assets/archive/fluxograma_triox-matriz_3.pdf') }}}" target="_blank" class="btn btn-blue"> Fluxograma Sistema</a><br />

				@include('admin/manual/admin')

				<hr />

				@include('admin/manual/office')

			</div>

		</div>
	</div>

@stop

{{-- Scripts --}}
@section('scripts')

@stop