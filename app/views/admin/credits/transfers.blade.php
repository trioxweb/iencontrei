@extends('admin.layouts.default')

@section('title')
{{{ Lang::get('admin/credit.transfers') }}} ::
@parent
@stop

@section('content')

	<div class="row">
		<div class="col-md-12">

			<h3>{{{ Lang::get('admin/credit.transfers') }}}</h3>
			<br>
			
			<table class="table table-bordered datatable" id="users">
				<thead>
					<tr class="replace-inputs">
						<th>#</th>
						<th>{{{ Lang::get('admin/credits/table.username') }}}</th>
						<th>{{{ Lang::get('admin/credits/table.create_at') }}}</th>
						<th>{{{ Lang::get('admin/credits/table.value') }}}</th>
						<th>{{{ Lang::get('admin/credits/table.description') }}}</th>
						<th></th>
					</tr>
					<tr>
						<th>#</th>
						<th>{{{ Lang::get('admin/credits/table.username') }}}</th>
						<th>{{{ Lang::get('admin/credits/table.create_at') }}}</th>
						<th>{{{ Lang::get('admin/credits/table.value') }}}</th>
						<th>{{{ Lang::get('admin/credits/table.description') }}}</th>
						<th>{{{ Lang::get('admin/credits/table.actions') }}}</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>#</th>
						<th>{{{ Lang::get('admin/credits/table.username') }}}</th>
						<th>{{{ Lang::get('admin/credits/table.create_at') }}}</th>
						<th>{{{ Lang::get('admin/credits/table.value') }}}</th>
						<th>{{{ Lang::get('admin/credits/table.description') }}}</th>
						<th>{{{ Lang::get('admin/credits/table.actions') }}}</th>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>

	<!-- Modal Dialog -->
	<div id="modal-dialog" class="modal fade">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title">{{ Lang::get('admin/register.are_you_sure') }}</h4>
	      </div>
	      <div class="modal-body">
	        <p>{{ Lang::get('admin/register.are_you_sure_about_this') }}?</p>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">{{{ Lang::get('admin/register.cancel') }}}</button>
	        <a href="#" class="btn btn-danger" id="confirm">{{{ Lang::get('office/home.active_true') }}}</a>
	      </div>
	    </div>
	  </div>
	</div>


@stop

{{-- Scripts --}}
@section('scripts')
	<script type="text/javascript">
		var oTable;
		$(document).ready(function() {
			$.datepicker.regional[""].dateFormat = 'dd/mm/yy';
            $.datepicker.setDefaults($.datepicker.regional['']);
			
			oTable = $('#users').dataTable( {
				"sPaginationType": "bootstrap",
				"sDom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'<'export-data'T>f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
				"aaSorting": [[ 0, "desc"]],
				//"bServerSide": true,
				//"bProcessing": true,
		        "sAjaxSource": "{{ URL::action('AdminCreditsController@getDatatransfers') }}",
		        "fnDrawCallback": function ( oSettings ) {
	           		$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
	     		}
			});

			oTable.columnFilter({
				"sPlaceHolder" : "head:after",
				aoColumns: [ "sPlaceHolder", "sPlaceHolder", { type: "date-range" }, "sPlaceHolder", "sPlaceHolder"]
			});

			$('#modal-dialog').on('show.bs.modal', function (e) {
			      href = $(e.relatedTarget).attr('data-href');
			      $(this).find('.modal-footer #confirm').attr('href', href);
		    });
		});
	</script>
	<style type="text/css">
	.dataTables_processing{
		display: none;
	}
	</style>
@stop