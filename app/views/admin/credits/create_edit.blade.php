@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')

	{{-- Create User Form --}}
	<form class="form-horizontal" method="post" action="@if (isset($credit)){{ URL::to('admin/credits/' . $credit->id . '/edit') }}@endif" autocomplete="off">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->
		<!-- username -->
		<div class="form-group {{{ $errors->has('username') ? 'has-error' : '' }}}">
			<label class="col-md-2 control-label" for="username">{{{ Lang::get('register/access.username') }}}</label>
			<div class="col-md-10">
				@if($mode == 'create')
					<input class="form-control" type="text" name="username" id="username" value="{{{ Input::old('username', isset($credit) ? $credit->user->username : null) }}}" />
				@else
					<input class="form-control" type="text" value="{{{ $credit->user->username }}}" disabled />
				@endif
				{{ $errors->first('username', '<span class="help-inline">:message</span>') }}
			</div>
		</div>
		<!-- ./ username -->

		<div class="form-group {{{ $errors->has('value') ? 'has-error' : '' }}}">
			<label for="value" class="col-sm-2 control-label">{{{ Lang::get('admin/points.value') }}}</label>
			
			<div class="col-md-10">
				@if($mode == 'create')
					<input type="text" class="form-control" id="value" name="value" value="{{{ Input::old('value', isset($credit) ? $credit->value : null) }}}">
				@else
					<input type="text" class="form-control" id="value" name="value" value="{{ Currency::format($credit->value) }}" disabled />
				@endif
				{{ $errors->first('value', '<span class="help-inline">:message</span>') }}
			</div>
		</div>

		<div class="form-group {{{ $errors->has('description') ? 'has-error' : '' }}}">
			<label for="description" class="col-sm-2 control-label">{{{ Lang::get('admin/credit.description') }}}</label>
			
			<div class="col-md-10">
				<input type="text" class="form-control" id="description" name="description" value="{{{ Input::old('description', isset($credit) ? $credit->description : null) }}}">
			</div>
		</div>

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-offset-2 col-md-10">
				<a href="javascript:;" class="btn btn-default close_popup" onclick="parent.oTable.fnReloadAjax(); parent.$.colorbox.close();">{{{ Lang::get('admin/register.cancel') }}}</a>
				<button type="submit" class="btn btn-success">OK</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop
