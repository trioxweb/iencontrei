@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')

	{{-- Create User Form --}}
	<form class="form-horizontal" method="post" action="{{ URL::action('AdminCreditsController@postDivideprofit') }}">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->
		<!-- value -->
		<div class="form-group {{{ $errors->has('value') ? 'has-error' : '' }}}">
			<label class="col-md-2 control-label" for="value">{{{ Lang::get('admin/credits/title.value') }}}</label>
			<div class="col-md-10">
				<input class="form-control" type="text" name="value" id="value" value="{{{ Input::old('value') }}}" data-mask="fdecimal" data-rad="," data-dec="." />
				{{ $errors->first('value', '<span class="help-inline">:message</span>') }}
			</div>
		</div>
		<!-- ./ value -->

		<!-- plan -->
		<div class="form-group {{{ $errors->has('network_plan_id') ? 'has-error' : '' }}}">
			<label class="col-md-2 control-label" for="network_plan_id">Plan</label>	
			<div class="col-md-4">
				{{ Form::select('network_plan_id', array_merge([0 => ''], Plan::orderby('order')->lists('name', 'id')),
					Input::old('network_plan_id'), 
					array('id' => 'network_plan_id', 'class' => 'form-control')) }}
				{{ $errors->first('network_plan_id', '<span class="help-inline">:message</span>') }}
			</div>
		</div>
		<!-- ./ plan -->

		<!-- titulo -->
		<div class="form-group {{{ $errors->has('network_title_id') ? 'has-error' : '' }}}">
			<label class="col-md-2 control-label" for="network_title_id">Ou Titulo</label>	
			<div class="col-md-4">
				{{ Form::select('network_title_id', array_merge([0 => ''], MlmTitle::lists('name', 'id')),
					Input::old('network_title_id'), 
					array('id' => 'network_title_id', 'class' => 'form-control')) }}
				{{ $errors->first('network_title_id', '<span class="help-inline">:message</span>') }}
			</div>
		</div>
		<!-- ./ titulo -->

		<!-- verify_post -->
		<div class="form-group {{{ $errors->has('verify_post') ? 'has-error' : '' }}}">
			<label class="col-md-2 control-label" for="verify_post">{{{ Lang::get('admin/credits/title.verify_post') }}}</label>
			<div class="col-md-10">
				<label><input type="radio" name="verify_post" value="1" checked="true" />Verificar Compartilhamentos </label><br>
				<label><input type="radio" name="verify_post" value="0" /> Não Verificar Compartilhamentos </label>
				{{ $errors->first('verify_post', '<span class="help-inline">:message</span>') }}
			</div>
		</div>
		<!-- ./ verify_post -->

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-offset-2 col-md-10">
				<a href="javascript:;" class="btn btn-default close_popup" onclick="parent.oTable.fnReloadAjax(); parent.$.colorbox.close();">{{{ Lang::get('admin/register.cancel') }}}</a>
				<button type="submit" class="btn btn-success">OK</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop