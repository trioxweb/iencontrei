@extends('admin.layouts.default')

@section('title')
{{{ Lang::get('admin/credit.credits_additionals') }}} ::
@parent
@stop

@section('content')

	<div class="row">
		<div class="col-md-12">

			<h3>{{{ Lang::get('admin/credit.credits_additionals') }}}</h3>
			<br>
			
			<table class="table table-bordered datatable" id="credits_additionals">
				<thead>
					<tr class="replace-inputs">
						<th>#</th>
						<th>{{{ Lang::get('register/access.username') }}}</th>
						<th>{{{ Lang::get('admin/points.create_at') }}}</th>
						<th>{{{ Lang::get('admin/points.value') }}}</th>
						<th>{{{ Lang::get('admin/credit.description') }}}</th>
					</tr>
					<tr>
						<th>#</th>
						<th>{{{ Lang::get('register/access.username') }}}</th>
						<th>{{{ Lang::get('admin/points.create_at') }}}</th>
						<th>{{{ Lang::get('admin/points.value') }}}</th>
						<th>{{{ Lang::get('admin/credit.description') }}}</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>#</th>
						<th>{{{ Lang::get('register/access.username') }}}</th>
						<th>{{{ Lang::get('admin/points.create_at') }}}</th>
						<th>{{{ Lang::get('admin/points.value') }}}</th>
						<th>{{{ Lang::get('admin/credit.description') }}}</th>
					</tr>
				</tfoot>
			</table>

			<div class="clear: both;"></div>
			
			<div style="float:right;">
				<a href="javascript:window.print();" class="btn btn-primary btn-icon icon-left hidden-print">{{{ Lang::get('office/extract.print') }}}<i class="entypo-doc-text"></i>
				</a>
			</div>	

		
		</div>
	</div>

@stop

{{-- Scripts --}}
@section('scripts')
	<script type="text/javascript">
		var oTable;
		$(document).ready(function() {

			$.datepicker.regional[""].dateFormat = 'dd/mm/yy';
            $.datepicker.setDefaults($.datepicker.regional['']);
			
			oTable = $('#credits_additionals').dataTable( {
				"sPaginationType": "bootstrap",
				"sDom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'<'export-data'T>f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
				"aaSorting": [[ 0, "desc"]],
				//"bServerSide": true,
				//"bProcessing": true,
		        "sAjaxSource": "{{ URL::action('AdminCreditsController@getDataadditionals') }}",
		        "fnDrawCallback": function ( oSettings ) {
	           		$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
	     		}
			});

			oTable.columnFilter({
				"sPlaceHolder" : "head:after",
				aoColumns: [ "sPlaceHolder", "sPlaceHolder", { type: "date-range" }, "sPlaceHolder", "sPlaceHolder"]
			});
		});
	</script>
	<style type="text/css">
	.dataTables_processing{
		display: none;
	}
	</style>
@stop