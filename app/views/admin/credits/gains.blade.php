@extends('admin.layouts.default')

@section('title')
{{{ Lang::get('admin/credit.extract_gains') }}} ::
@parent
@stop

@section('content')

	<div class="row">
		<div class="col-md-12">

			<h3>{{{ Lang::get('admin/credit.extract_gains') }}}</h3>
			<br>
			
			<table class="table table-bordered datatable" id="users">
				<thead>
					<tr class="replace-inputs">
						<th>{{{ Lang::get('admin/credits/table.username') }}}</th>
						<th>{{{ Lang::get('admin/credits/table.total_gains') }}}</th>
					</tr>
					<tr>
						<th>{{{ Lang::get('admin/credits/table.username') }}}</th>
						<th>{{{ Lang::get('admin/credits/table.total_gains') }}}</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>{{{ Lang::get('admin/credits/table.username') }}}</th>
						<th>{{{ Lang::get('admin/credits/table.total_gains') }}}</th>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>

@stop

{{-- Scripts --}}
@section('scripts')
	<script type="text/javascript">
		var oTable;
		$(document).ready(function() {
			oTable = $('#users').dataTable({
				"sPaginationType": "bootstrap",
				"sDom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'<'export-data'T>f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
				"aaSorting": [[ 1, "desc"]],
				//"bServerSide": true,
				//"bProcessing": true,
		        "sAjaxSource": "{{ URL::action('AdminCreditsController@getDatagains') }}",
		        "fnDrawCallback": function ( oSettings ) {
	           		$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
	     		}
			});

			oTable.columnFilter({
				"sPlaceHolder" : "head:after",
				aoColumns: [ "sPlaceHolder", "sPlaceHolder",]
			});
		});
	</script>
	<style type="text/css">
	.dataTables_processing{
		display: none;
	}
	</style>
@stop