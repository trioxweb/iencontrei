@extends('admin.layouts.default')

@section('title')
Credits ::
@parent
@stop

@section('content')

	<div class="row">
		<div class="col-md-12">

			<h3>{{{ Lang::get('admin/credit.credits') }}}

			<div class="pull-right">
				<a href="{{{ URL::action('AdminCreditsController@getDivideprofit') }}}" class="btn btn-small btn-info iframe"><span class="glyphicon glyphicon-plus-sign"></span> Divide Profit</a>
				<a href="{{{ URL::to('admin/credits/create') }}}" class="btn btn-small btn-info iframe"><span class="glyphicon glyphicon-plus-sign"></span> Create</a>
			</div>
			</h3>
			<br>
			
			<table class="table table-bordered datatable" id="users">
				<thead>
					<tr class="replace-inputs">
						<th>#</th>
						<th>{{{ Lang::get('register/access.username') }}}</th>
						<th>{{{ Lang::get('admin/points.create_at') }}}</th>
						<th>{{{ Lang::get('admin/points.value') }}}</th>
						<th>{{{ Lang::get('admin/credit.description') }}}</th>
						<th></th>
					</tr>
					<tr>
						<th>#</th>
						<th>{{{ Lang::get('register/access.username') }}}</th>
						<th>{{{ Lang::get('admin/points.create_at') }}}</th>
						<th>{{{ Lang::get('admin/points.value') }}}</th>
						<th>{{{ Lang::get('admin/credit.description') }}}</th>
						<th>{{{ Lang::get('admin/register.actions') }}}</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>#</th>
						<th>{{{ Lang::get('register/access.username') }}}</th>
						<th>{{{ Lang::get('admin/points.create_at') }}}</th>
						<th>{{{ Lang::get('admin/points.value') }}}</th>
						<th>{{{ Lang::get('admin/credit.description') }}}</th>
						<th>{{{ Lang::get('admin/register.actions') }}}</th>
					</tr>
				</tfoot>
			</table>

			<div class="clear: both;"></div>
			
			<div style="float:right;">
				<a href="javascript:window.print();" class="btn btn-primary btn-icon icon-left hidden-print">{{{ Lang::get('office/extract.print') }}}<i class="entypo-doc-text"></i>
				</a>
			</div>	

		
		</div>
	</div>

	<!-- Modal Dialog -->
	<div id="modal-dialog" class="modal fade">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title">{{ Lang::get('admin/register.are_you_sure') }}</h4>
	      </div>
	      <div class="modal-body">
	        <p>{{ Lang::get('admin/register.are_you_sure_about_this') }}?</p>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">{{{ Lang::get('admin/register.cancel') }}}</button>
	        <a href="#" class="btn btn-danger" id="confirm">{{{ Lang::get('office/home.active_true') }}}</a>
	      </div>
	    </div>
	  </div>
	</div>


@stop

{{-- Scripts --}}
@section('scripts')
	<script type="text/javascript">
		var oTable;
		$(document).ready(function() {

			$.datepicker.regional[""].dateFormat = 'dd/mm/yy';
            $.datepicker.setDefaults($.datepicker.regional['']);
			
			oTable = $('#users').dataTable( {
				"sPaginationType": "bootstrap",
				"sDom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'<'export-data'T>f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
				"aaSorting": [[ 0, "desc"]],
				//"bServerSide": true,
				//"bProcessing": true,
		        "sAjaxSource": "{{ URL::action('AdminCreditsController@getData') }}",
		        "fnDrawCallback": function ( oSettings ) {
	           		$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
	     		}
			});

			oTable.columnFilter({
				"sPlaceHolder" : "head:after",
				aoColumns: [ "sPlaceHolder", "sPlaceHolder", { type: "date-range" }, "sPlaceHolder", "sPlaceHolder"]
			});

			$('#modal-dialog').on('show.bs.modal', function (e) {
			      href = $(e.relatedTarget).attr('data-href');
			      $(this).find('.modal-footer #confirm').attr('href', href);
		    });
		});
	</script>
	<style type="text/css">
	.dataTables_processing{
		display: none;
	}
	</style>
@stop