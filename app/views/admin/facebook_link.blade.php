@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')

	{{-- Create User Form --}}
	<form class="form-horizontal" method="post" action="{{ URL::action('AdminDashboardController@postFacebooklink') }}">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->
		<!-- facebook_link -->
		<div class="form-group {{{ $errors->has('facebook_link') ? 'has-error' : '' }}}">
			<label class="col-md-2 control-label" for="facebook_link">Link</label>
			<div class="col-md-10">
				<input class="form-control" type="text" name="facebook_link" id="facebook_link" value="{{{ Input::old('facebook_link', $facebook_link) }}}" />
				{{ $errors->first('facebook_link', '<span class="help-inline">:message</span>') }}
			</div>
		</div>
		<!-- ./ facebook_link -->

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-offset-2 col-md-10">
				<button type="submit" class="btn btn-success">Salvar</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop