@extends('admin.layouts.default')

{{-- Web site Title --}}
@section('title')
	{{{ Lang::get('admin/settings/title.settings') }}} :: @parent
@stop

{{-- Content --}}
@section('content')
	<div class="page-header">
		<h3>{{{ Lang::get('admin/settings/title.settings') }}}</h3>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<form role="form" method="post" class="form-horizontal form-groups-bordered validate" action="{{{ URL::action('AdminSettingsController@postEdit') }}}">
				{{ Form::token() }}
			
				<div class="panel panel-primary" data-collapsed="0">
				
					<div class="panel-heading">
						<div class="panel-title">
							{{{ Lang::get('admin/settings/title.settings') }}}
						</div>
						
					</div>
					
					<div class="panel-body">
		
						<div class="form-group {{{ $errors->has('minimum_withdrawal') ? 'has-error' : '' }}}">
							<label for="minimum_withdrawal" class="col-sm-3 control-label">{{{ Lang::get('admin/settings/forms.minimum_withdrawal') }}}</label>
							
							<div class="col-sm-5">
								<input type="text" class="form-control" id="minimum_withdrawal" name="minimum_withdrawal" value="{{{ Input::old('minimum_withdrawal', isset($minimum_withdrawal) ? $minimum_withdrawal : '300.00') }}}">
								{{ $errors->first('minimum_withdrawal', '<span class="help-inline">:message</span>') }}
							</div>
						</div>

						<div class="form-group {{{ $errors->has('tax_withdrawal') ? 'has-error' : '' }}}">
							<label for="tax_withdrawal" class="col-sm-3 control-label">{{{ Lang::get('admin/settings/forms.tax_withdrawal') }}}</label>
							
							<div class="col-sm-5">
								<input type="text" class="form-control" id="tax_withdrawal" name="tax_withdrawal" value="{{{ Input::old('tax_withdrawal', isset($tax_withdrawal) ? $tax_withdrawal : '8') }}}">
								{{ $errors->first('tax_withdrawal', '<span class="help-inline">:message</span>') }}
							</div>
						</div>

						<div class="form-group {{{ $errors->has('minimum_transfer_oc') ? 'has-error' : '' }}}">
							<label for="minimum_transfer_oc" class="col-sm-3 control-label">{{{ Lang::get('admin/settings/forms.minimum_transfer_oc') }}}</label>
							
							<div class="col-sm-5">
								<input type="text" class="form-control" id="minimum_transfer_oc" name="minimum_transfer_oc" value="{{{ Input::old('minimum_transfer_oc', isset($minimum_transfer_oc) ? $minimum_transfer_oc : '300.00') }}}">
								{{ $errors->first('minimum_transfer_oc', '<span class="help-inline">:message</span>') }}
							</div>
						</div>

						<div class="form-group {{{ $errors->has('tax_transfer_oc') ? 'has-error' : '' }}}">
							<label for="tax_transfer_oc" class="col-sm-3 control-label">{{{ Lang::get('admin/settings/forms.tax_transfer_oc') }}}</label>
							
							<div class="col-sm-5">
								<input type="text" class="form-control" id="tax_transfer_oc" name="tax_transfer_oc" value="{{{ Input::old('tax_transfer_oc', isset($tax_transfer_oc) ? $tax_transfer_oc : '8') }}}">
								{{ $errors->first('tax_transfer_oc', '<span class="help-inline">:message</span>') }}
							</div>
						</div>

						<div class="form-group {{{ $errors->has('minimum_transfer_user') ? 'has-error' : '' }}}">
							<label for="minimum_transfer_user" class="col-sm-3 control-label">{{{ Lang::get('admin/settings/forms.minimum_transfer_user') }}}</label>
							
							<div class="col-sm-5">
								<input type="text" class="form-control" id="minimum_transfer_user" name="minimum_transfer_user" value="{{{ Input::old('minimum_transfer_user', isset($minimum_transfer_user) ? $minimum_transfer_user : '300.00') }}}">
								{{ $errors->first('minimum_transfer_user', '<span class="help-inline">:message</span>') }}
							</div>
						</div>

						<div class="form-group {{{ $errors->has('tax_transfer_user') ? 'has-error' : '' }}}">
							<label for="tax_transfer_user" class="col-sm-3 control-label">{{{ Lang::get('admin/settings/forms.tax_transfer_user') }}}</label>
							
							<div class="col-sm-5">
								<input type="text" class="form-control" id="tax_transfer_user" name="tax_transfer_user" value="{{{ Input::old('tax_transfer_user', isset($tax_transfer_user) ? $tax_transfer_user : '8') }}}">
								{{ $errors->first('tax_transfer_user', '<span class="help-inline">:message</span>') }}
							</div>
						</div>

						<div class="form-group {{{ $errors->has('tax_payment_with_balance') ? 'has-error' : '' }}}">
							<label for="tax_payment_with_balance" class="col-sm-3 control-label">{{{ Lang::get('admin/settings/forms.tax_payment_with_balance') }}}</label>
							
							<div class="col-sm-5">
								<input type="text" class="form-control" id="tax_payment_with_balance" name="tax_payment_with_balance" value="{{{ Input::old('tax_payment_with_balance', isset($tax_payment_with_balance) ? $tax_payment_with_balance : '8') }}}">
								{{ $errors->first('tax_payment_with_balance', '<span class="help-inline">:message</span>') }}
							</div>
						</div>

						<div class="form-group {{{ $errors->has('payment_methods') ? 'has-error' : '' }}}">
							<label for="payment_methods" class="col-sm-3 control-label">{{{ Lang::get('admin/settings/forms.payment_methods') }}}</label>
							
							<div class="col-sm-5">
								@foreach($payment_methods as $key => $payment_method)
									<input type="checkbox" name="payment_methods[]" value="{{$key}}" @if($payment_method['status']) checked="true" @endif> {{$payment_method['name']}} <br>
								@endforeach
								{{ $errors->first('payment_methods', '<span class="help-inline">:message</span>') }}
							</div>
						</div>

						<div class="form-group">
							<label for="submit_data" class="col-sm-3 control-label"></label>
							<div class="col-sm-5">
								<button type="submit" id="submit_data" class="btn btn-blue">{{{ Lang::get('admin/settings/forms.save') }}}</button>	
							</div>
						</div>
						
					</div>
				
				</div>

			</form>
		</div>
	</div>
@stop

{{-- Scripts --}}
@section('scripts')

@stop