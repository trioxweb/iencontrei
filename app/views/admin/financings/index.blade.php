@extends('admin.layouts.default')

@section('title')
{{{ $title }}} :: @parent
@parent
@stop

@section('content')
	<div class="row">
		<div class="col-md-12">

			<h3>{{{ $title }}}</h3>
			<br>
			
			<table class="table table-bordered datatable" id="users">
				<thead>
					<tr class="replace-inputs">
						<th>{{{ Lang::get('admin/financings/table.funder') }}}</th>
						<th>{{{ Lang::get('admin/financings/table.funded') }}}</th>
						<th>{{{ Lang::get('admin/financings/table.amount_financed') }}}</th>
						<th>{{{ Lang::get('admin/financings/table.amount_paid') }}}</th>
						<th>{{{ Lang::get('admin/financings/table.created_at') }}}</th>
						<th>{{{ Lang::get('admin/financings/table.updated_at') }}}</th>
					</tr>
					<tr>
						<th>{{{ Lang::get('admin/financings/table.funder') }}}</th>
						<th>{{{ Lang::get('admin/financings/table.funded') }}}</th>
						<th>{{{ Lang::get('admin/financings/table.amount_financed') }}}</th>
						<th>{{{ Lang::get('admin/financings/table.amount_paid') }}}</th>
						<th>{{{ Lang::get('admin/financings/table.created_at') }}}</th>
						<th>{{{ Lang::get('admin/financings/table.updated_at') }}}</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>{{{ Lang::get('admin/financings/table.funder') }}}</th>
						<th>{{{ Lang::get('admin/financings/table.funded') }}}</th>
						<th>{{{ Lang::get('admin/financings/table.amount_financed') }}}</th>
						<th>{{{ Lang::get('admin/financings/table.amount_paid') }}}</th>
						<th>{{{ Lang::get('admin/financings/table.created_at') }}}</th>
						<th>{{{ Lang::get('admin/financings/table.updated_at') }}}</th>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
@stop

{{-- Scripts --}}
@section('scripts')
	<script type="text/javascript">
		var oTable;
		$(document).ready(function() {

			$.datepicker.regional[""].dateFormat = 'dd/mm/yy';
            $.datepicker.setDefaults($.datepicker.regional['']);
			
			oTable = $('#users').dataTable( {
				"sPaginationType": "bootstrap",
				"sDom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'<'export-data'T>f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
				"aaSorting": [[ 0, "desc"]],
				//"bServerSide": true,
				//"bProcessing": true,
		        "sAjaxSource": "{{ URL::action('AdminFinancingsController@getData') }}",
		        "fnDrawCallback": function ( oSettings ) {
	           		$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
	     		}
			});

			oTable.columnFilter({
				"sPlaceHolder" : "head:after",
				aoColumns: [ "sPlaceHolder", "sPlaceHolder", "sPlaceHolder", "sPlaceHolder", { type: "date-range" }, { type: "date-range" }]
			});
		});
	</script>
@stop