@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')

	<!-- Tabs -->
		<ul class="nav nav-tabs">
			<li class="active"><a href="#tab-general" data-toggle="tab">{{{ Lang::get('admin/register.general') }}}</a></li>
			<li><a href="#tab-data" data-toggle="tab">{{{ Lang::get('admin/register.account') }}}</a></li>
			<li><a href="#tab-address" data-toggle="tab">{{{ Lang::get('register/personal_data.address') }}}</a></li>
			<li><a href="#tab-bank" data-toggle="tab">{{{ Lang::get('admin/register.bank') }}}</a></li>
		</ul>
	<!-- ./ tabs -->

	{{-- Create User Form --}}
	<form class="form-horizontal" method="post" action="@if (isset($user)){{ URL::to('admin/users/' . $user->id . '/edit') }}@endif" autocomplete="off">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<!-- Tabs Content -->
		<div class="tab-content">
			<!-- General tab -->
			<div class="tab-pane active" id="tab-general">
				<!-- parent -->
				<div class="form-group {{{ $errors->has('parent') ? 'has-error' : '' }}}">
					<label class="col-md-2 control-label" for="parent">{{{ Lang::get('register/access.sponsor') }}}</label>	
					<div class="col-md-10">
						@if(!$user->mlm->network_in)
							<input class="form-control" type="text" name="parent" id="parent" value="{{{ Input::old('parent', ($user->mlm->parent_user()->first()) ? $user->mlm->parent_user()->first()->username : null) }}}" />
						@else
							<input class="form-control" type="text" id="parent" value="{{{ ($user->mlm->parent_user()->first()) ? $user->mlm->parent_user()->first()->username : null }}}" disabled>
						@endif
						{{ $errors->first('parent', '<span class="help-inline">:message</span>') }}
					</div>
				</div>
				<!-- ./ parent -->

				<!-- side -->
				<div class="form-group {{{ $errors->has('side_network') ? 'has-error' : '' }}}">
					<label class="col-md-2 control-label" for="side_network">Lado na Rede</label>	
					<div class="col-md-10">
					@if(!$user->mlm->network_in)
						{{ Form::select('side_network', ['' => 'Definido Pelo Patrocinador', 1 => 'Lado Esquerdo', 2 => 'Lado Direito'],
							Input::old('side_network', isset($user->mlm->side_network) ? $user->mlm->side_network : null), 
							array('id' => 'side_network', 'class' => 'form-control')) }}
					@else
						<input class="form-control" type="text" id="side_network" value="@if($user->mlm->side_network == 1) Lado Esquerdo @elseif($user->mlm->side_network == 2) Lado Direito @endif" disabled>
					@endif
						{{ $errors->first('side_network', '<span class="help-inline">:message</span>') }}
					</div>
				</div>
				<!-- ./ side -->

				<!-- plan -->
				<div class="form-group {{{ $errors->has('network_plan_id') ? 'has-error' : '' }}}">
					<label class="col-md-2 control-label" for="network_plan_id">Plan</label>	
					<div class="col-md-10">
						{{ Form::select('network_plan_id', array_merge([0 => ''], Plan::orderby('order')->lists('name', 'id')),
							Input::old('network_plan_id', $user->mlm->network_plan_id), 
							array('id' => 'network_plan_id', 'class' => 'form-control')) }}
						{{ $errors->first('network_plan_id', '<span class="help-inline">:message</span>') }}
					</div>
				</div>
				<!-- ./ plan -->

				<!-- titulo -->
				<div class="form-group {{{ $errors->has('network_title_id') ? 'has-error' : '' }}}">
					<label class="col-md-2 control-label" for="network_title_id">Titulo</label>	
					<div class="col-md-10">
						{{ Form::select('network_title_id', array_merge([0 => ''], MlmTitle::lists('name', 'id')),
							Input::old('network_title_id', isset($user->mlm->network_title_id) ? $user->mlm->network_title_id : null), 
							array('id' => 'network_title_id', 'class' => 'form-control')) }}
						{{ $errors->first('network_title_id', '<span class="help-inline">:message</span>') }}
					</div>
				</div>
				<!-- ./ titulo -->

				<!-- username -->
				<div class="form-group {{{ $errors->has('username') ? 'has-error' : '' }}}">
					<label class="col-md-2 control-label" for="username">{{{ Lang::get('register/access.username') }}}</label>
					<div class="col-md-10">
						<input class="form-control" type="text" name="username" id="username" value="{{{ Input::old('username', isset($user) ? $user->username : null) }}}" />
						{{ $errors->first('username', '<span class="help-inline">:message</span>') }}
					</div>
				</div>
				<!-- ./ username -->

				<!-- Email -->
				<div class="form-group {{{ $errors->has('email') ? 'has-error' : '' }}}">
					<label class="col-md-2 control-label" for="email">Email</label>
					<div class="col-md-10">
						<input class="form-control" type="text" name="email" id="email" value="{{{ Input::old('email', isset($user) ? $user->email : null) }}}" />
						{{ $errors->first('email', '<span class="help-inline">:message</span>') }}
					</div>
				</div>
				<!-- ./ email -->

				<!-- Password -->
				<div class="form-group {{{ $errors->has('password') ? 'has-error' : '' }}}">
					<label class="col-md-2 control-label" for="password">{{{ Lang::get('register/access.password') }}}</label>
					<div class="col-md-10">
						<input class="form-control" type="password" name="password" id="password" value="" />
						{{ $errors->first('password', '<span class="help-inline">:message</span>') }}
					</div>
				</div>
				<!-- ./ password -->

				<!-- Password Confirm -->
				<div class="form-group {{{ $errors->has('password_confirmation') ? 'has-error' : '' }}}">
					<label class="col-md-2 control-label" for="password_confirmation">{{{ Lang::get('register/access.confirm_password') }}}</label>
					<div class="col-md-10">
						<input class="form-control" type="password" name="password_confirmation" id="password_confirmation" value="" />
						{{ $errors->first('password_confirmation', '<span class="help-inline">:message</span>') }}
					</div>
				</div>
				<!-- ./ password confirm -->
			</div>
			<!-- ./ general tab -->
			
			<!-- Account tab -->
			<div class="tab-pane" id="tab-data">
				<div class="form-group {{{ $errors->has('firstname') ? 'has-error' : '' }}}">
					<label for="firstname" class="col-sm-3 control-label">{{{ Lang::get('register/personal_data.firstname') }}}</label>
					
					<div class="col-sm-5">
						<input type="text" class="form-control" id="firstname" name="firstname" value="{{{ Input::old('firstname', isset($user->data->firstname) ? $user->data->firstname : null) }}}">
					</div>
					{{ $errors->first('firstname', '<span class="help-inline">:message</span>') }}
				</div>

				<div class="form-group {{{ $errors->has('lastname') ? 'has-error' : '' }}}">
					<label for="lastname" class="col-sm-3 control-label">{{{ Lang::get('register/personal_data.lastname') }}}</label>
					
					<div class="col-sm-5">
						<input type="text" class="form-control" id="lastname" name="lastname" value="{{{ Input::old('lastname', isset($user->data->lastname) ? $user->data->lastname : null) }}}">
					</div>
					{{ $errors->first('lastname', '<span class="help-inline">:message</span>') }}
				</div>

				<div class="cpf">
					<div class="form-group {{{ $errors->has('cpf') ? 'has-error' : '' }}}">
						<label for="cpf" class="col-sm-3 control-label">{{{ Lang::get('register/personal_data.cpf') }}}</label>
						
						<div class="col-sm-5">
							<input type="text" class="form-control" id="cpf" name="cpf" value="{{{ Input::old('cpf', isset($user->data->cpf) ? $user->data->cpf : null) }}}">
						</div>
						{{ $errors->first('cpf', '<span class="help-inline">:message</span>') }}
					</div>
				</div>

				<div class="form-group {{{ $errors->has('telephone') ? 'has-error' : '' }}}">
					<label for="telephone" class="col-sm-3 control-label">{{{ Lang::get('office/myaccount/forms.telephone') }}}</label>
					
					<div class="col-sm-4">
						<input type="text" class="form-control" id="telephone" name="telephones[0][0]" data-mask="(99) 999999999" value="{{{ Input::old('telephones.0.0', isset($user->data->telephones[0][0]) ? $user->data->telephones[0][0] : null) }}}">
					</div>
					<div class="col-sm-3">
						<input type="text" class="form-control" id="provider" name="telephones[0][1]" placeholder="Operadora" value="{{{ Input::old('telephones.0.1', isset($user->data->telephones[0][1]) ? $user->data->telephones[0][1] : null) }}}">
					</div>
				</div>
				<?php $last_key = null;?>
				@foreach ($user->data->telephones as $key => $telephone)
					<?php if($key == 0){ continue; }?>
					<div class="form-group {{{ $errors->has('telephone') ? 'has-error' : '' }}}">
						<label for="telephone" class="col-sm-3 control-label"></label>
						<div class="col-sm-4">
							<input type="text" class="form-control" id="telephone" name="telephones[{{$key}}][0]" data-mask="(99) 999999999" value="{{{ Input::old('telephones.'.$key.'.0', isset($telephone[0]) ? $telephone[0] : null) }}}">
						</div>
						<div class="col-sm-3">
							<input type="text" class="form-control" id="provider" name="telephones[{{$key}}][1]" placeholder="Operadora" value="{{{ Input::old('telephones.'.$key.'.1', isset($telephone[1]) ? $telephone[1] : null) }}}">
						</div>
						<div class="col-sm-1">
							<a class="btn btn-default rmTelephone" onclick="rmTelephone(this);"><i class="entypo-minus"></i></a>
						</div>
					</div>
					<?php $last_key = $key; ?>
				@endforeach
				@foreach (Input::old('telephones', []) as $key => $telephone)
				<?php if($key > $last_key){ ?>
					<div class="form-group {{{ $errors->has('telephone') ? 'has-error' : '' }}}">
						<label for="telephone" class="col-sm-3 control-label"></label>
						<div class="col-sm-4">
							<input type="text" class="form-control" id="telephone" name="telephones[{{$key}}][0]" data-mask="(99) 999999999" value="{{{ Input::old('telephones.'.$key.'.0', isset($telephone[0]) ? $telephone[0] : null) }}}">
						</div>
						<div class="col-sm-3">
							<input type="text" class="form-control" id="provider" name="telephones[{{$key}}][1]" placeholder="Operadora" value="{{{ Input::old('telephones.'.$key.'.1', isset($telephone[1]) ? $telephone[1] : null) }}}">
						</div>
						<div class="col-sm-1">
							<a class="btn btn-default rmTelephone" onclick="rmTelephone(this);"><i class="entypo-minus"></i></a>
						</div>
					</div>
				<?php }?>
				@endforeach
				@if(isset($key))
					<script>window.idTelephone = {{$key}}</script>
				@endif
				
				
				<div id="newtelephone" class="form-group">
					<div class="col-sm-offset-10 col-sm-1">
						<a href="javascript:addNewTelephone();" class="btn btn-default" style=""><i class="entypo-plus"></i></a>
					</div>
				</div>

			</div>
			<!-- ./ Account tab -->
			
			<!-- Address tab -->
			<div class="tab-pane" id="tab-address">
				<div class="form-group {{{ $errors->has('address') ? 'has-error' : '' }}}">
					<label for="address" class="col-sm-3 control-label">{{{ Lang::get('register/personal_data.address') }}}</label>
					
					<div class="col-sm-5">
						<input type="text" class="form-control" id="address" name="address" value="{{{ Input::old('address', isset($user->address->address) ? $user->address->address : null) }}}">
					</div>
					{{ $errors->first('address', '<span class="help-inline">:message</span>') }}
				</div>
				<div class="form-group {{{ $errors->has('address_2') ? 'has-error' : '' }}}">
					<label for="address_2" class="col-sm-3 control-label"></label>
					
					<div class="col-sm-5">
						<input type="text" class="form-control" id="address_2" name="address_2" value="{{{ Input::old('address_2', isset($user->address->address_2) ? $user->address->address_2 : null) }}}">
					</div>
				</div>

				<div class="form-group {{{ $errors->has('city') ? 'has-error' : '' }}}">
					<label for="city" class="col-sm-3 control-label">{{{ Lang::get('register/personal_data.city') }}}</label>
					
					<div class="col-sm-5">
						<input type="text" class="form-control" id="city" name="city" value="{{{ Input::old('city', isset($user->address->city) ? $user->address->city : null) }}}">
					</div>
				</div>

				<div class="form-group {{{ $errors->has('zone') ? 'has-error' : '' }}}">
					<label for="zone" class="col-sm-3 control-label">{{{ Lang::get('register/personal_data.zone') }}}</label>
					
					<div class="col-sm-5">
						<input type="text" class="form-control" id="zone" name="zone" value="{{{ Input::old('zone', isset($user->address->zone) ? $user->address->zone : null) }}}">
					</div>
				</div>

				<div class="form-group {{{ $errors->has('country') ? 'has-error' : '' }}}">
					<label for="country" class="col-sm-3 control-label">{{{ Lang::get('register/personal_data.country') }}}</label>
					
					<div class="col-sm-5">
						<input type="text" class="form-control" id="country" name="country" value="{{{ Input::old('country', isset($user->address->country) ? $user->address->country : null) }}}">
					</div>
				</div>

				<div class="form-group {{{ $errors->has('postcode') ? 'has-error' : '' }}}">
					<label for="postcode" class="col-sm-3 control-label">{{{ Lang::get('register/personal_data.postcode') }}}</label>
					
					<div class="col-sm-5">
						<input type="text" class="form-control" id="postcode" name="postcode" value="{{{ Input::old('postcode', isset($user->address->postcode) ? $user->address->postcode : null) }}}">
					</div>
					{{ $errors->first('postcode', '<span class="help-inline">:message</span>') }}
				</div>
			</div>
			<!-- ./ Address tab -->

			<!-- Bank tab -->
			<div class="tab-pane" id="tab-bank">
				<div class="form-group {{{ $errors->has('bank_name') ? 'has-error' : '' }}}">
					<label for="bank_name" class="col-sm-3 control-label">{{{ Lang::get('office/myaccount/forms.bank') }}}</label>
					
					<div class="col-sm-5">
						<input type="text" class="form-control" id="bank_name" name="bank_name" value="{{{ Input::old('bank_name', isset($user->bank->bank_name) ? $user->bank->bank_name : null) }}}">
					</div>
				</div>

				<div class="form-group {{{ $errors->has('agency') ? 'has-error' : '' }}}">
					<label for="agency" class="col-sm-3 control-label">{{{ Lang::get('office/myaccount/forms.agency') }}}</label>
					
					<div class="col-sm-5">
						<input type="text" class="form-control" id="agency" name="agency" value="{{{ Input::old('agency', isset($user->bank->agency) ? $user->bank->agency : null) }}}">
					</div>
				</div>

				<div class="form-group {{{ $errors->has('account_number') ? 'has-error' : '' }}}">
					<label for="account_number" class="col-sm-3 control-label">{{{ Lang::get('office/myaccount/forms.account') }}}</label>
					
					<div class="col-sm-5">
						<input type="text" class="form-control" id="account_number" name="account_number" value="{{{ Input::old('account_number', isset($user->bank->account_number) ? $user->bank->account_number : null) }}}">
					</div>
				</div>

				<div class="form-group {{{ $errors->has('account_type') ? 'has-error' : '' }}}">
					<label for="account_type" class="col-sm-3 control-label">{{{ Lang::get('office/myaccount/forms.account_type') }}}</label>
					
					<div class="col-sm-5">
						<input type="text" class="form-control" id="account_type" name="account_type" value="{{{ Input::old('account_type', isset($user->bank->account_type) ? $user->bank->account_type : null) }}}">
					</div>
				</div>

				<div class="form-group {{{ $errors->has('titular_name') ? 'has-error' : '' }}}">
					<label for="titular_name" class="col-sm-3 control-label">{{{ Lang::get('office/myaccount/forms.account_titular') }}}</label>
					
					<div class="col-sm-5">
						<input type="text" class="form-control" id="titular_name" name="titular_name" value="{{{ Input::old('titular_name', isset($user->bank->titular_name) ? $user->bank->titular_name : null) }}}">
					</div>
				</div>

				<div class="form-group {{{ $errors->has('titular_cpf') ? 'has-error' : '' }}}">
					<label for="titular_cpf" class="col-sm-3 control-label">{{{ Lang::get('office/myaccount/forms.titular_cpf') }}}</label>
					
					<div class="col-sm-5">
						<input type="text" class="form-control" id="titular_cpf" name="titular_cpf" value="{{{ Input::old('titular_cpf', isset($user->bank->titular_cpf) ? $user->bank->titular_cpf : null) }}}">
					</div>
				</div>

				<div class="form-group {{{ $errors->has('nib') ? 'has-error' : '' }}}">
					<label for="nib" class="col-sm-3 control-label">NIB</label>
					
					<div class="col-sm-5">
						<input type="text" class="form-control" id="nib" name="nib" value="{{{ Input::old('nib', isset($user->bank->nib) ? $user->bank->nib : null) }}}">
					</div>
				</div>

				<div class="form-group {{{ $errors->has('iban') ? 'has-error' : '' }}}">
					<label for="iban" class="col-sm-3 control-label">IBAN</label>
					
					<div class="col-sm-5">
						<input type="text" class="form-control" id="iban" name="iban" value="{{{ Input::old('iban', isset($user->bank->iban) ? $user->bank->iban : null) }}}">
					</div>
				</div>

				<div class="form-group {{{ $errors->has('swift') ? 'has-error' : '' }}}">
					<label for="swift" class="col-sm-3 control-label">SWIFT</label>
					
					<div class="col-sm-5">
						<input type="text" class="form-control" id="swift" name="swift" value="{{{ Input::old('swift', isset($user->bank->swift) ? $user->bank->swift : null) }}}">
					</div>
				</div>

				<div class="form-group {{{ $errors->has('skrill') ? 'has-error' : '' }}}">
					<label for="skrill" class="col-sm-3 control-label">SKRILL (Email)</label>
					
					<div class="col-sm-5">
						<input type="text" class="form-control" id="skrill" name="skrill" value="{{{ Input::old('skrill', isset($user->bank->skrill) ? $user->bank->skrill : null) }}}">
					</div>
				</div>

				<div class="form-group {{{ $errors->has('paypal') ? 'has-error' : '' }}}">
					<label for="paypal" class="col-sm-3 control-label">Paypal (Email)</label>
					
					<div class="col-sm-5">
						<input type="text" class="form-control" id="paypal" name="paypal" value="{{{ Input::old('paypal', isset($user->bank->paypal) ? $user->bank->paypal : null) }}}">
					</div>
				</div>

				<div class="form-group {{{ $errors->has('pagseguro') ? 'has-error' : '' }}}">
					<label for="pagseguro" class="col-sm-3 control-label">Pagseguro (Email)</label>
					
					<div class="col-sm-5">
						<input type="text" class="form-control" id="pagseguro" name="pagseguro" value="{{{ Input::old('pagseguro', isset($user->bank->pagseguro) ? $user->bank->pagseguro : null) }}}">
					</div>
				</div>
			</div>
			<!-- ./ Bank tab -->
		</div>
		<!-- ./ tabs content -->

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-offset-2 col-md-10">
				<a href="javascript:;" class="btn btn-default close_popup" onclick="parent.oTable.fnReloadAjax(); parent.$.colorbox.close();">{{{ Lang::get('admin/register.cancel') }}}</a>
				<button type="submit" class="btn btn-success">OK</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop
