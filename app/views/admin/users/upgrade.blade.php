@extends('admin.layouts.default')

@section('title')
Upgrade ::
@parent
@stop

@section('content')

	<div class="row">
		<div class="col-md-12">

			<h3>{{{ Lang::get('admin/register/upgrade.upgrade') }}}</h3>
			
			<br />

			<table class="table table-bordered datatable" id="extract">
				<thead>
					<tr>
						<th class="col-md-1">{{{ Lang::get('admin/register/upgrade.username') }}}</th>
						<th class="col-md-2">{{{ Lang::get('admin/register/upgrade.full_name') }}}</th>
						<th class="col-md-4">{{{ Lang::get('admin/register/upgrade.email') }}}</th>
						<th class="col-md-6">{{{ Lang::get('admin/register/upgrade.value') }}}</th>
					</tr>
				</thead>
			</table>

			<div class="clear: both;"></div>
			
			<div style="float:right;">
				<a href="javascript:window.print();" class="btn btn-primary btn-icon icon-left hidden-print">{{{ Lang::get('admin/register/upgrade.print') }}}<i class="entypo-doc-text"></i>
				</a>
			</div>	

		
		</div>
	</div>

@stop

{{-- Scripts --}}
@section('scripts')
	<script type="text/javascript">
		var oTable;
		$(document).ready(function() {
			oTable = $('#extract').dataTable( {
				"sPaginationType": "bootstrap",
				"sDom": "<'row'<'col-xs-6 col-left'l><'col-xs-6 col-right'<'export-data'T>f>r>t<'row'<'col-xs-6 col-left'i><'col-xs-6 col-right'p>>",
				"aaSorting": [[ 1, "desc"]],
		        "sAjaxSource": "{{ URL::action('OfficeFinanceController@getExtract') }}",
			});
		});
	</script>
@stop