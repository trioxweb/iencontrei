@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')
	{{-- Create User Form --}}
	<form class="form-horizontal" method="post" action="@if (isset($plan)){{ URL::to('admin/plans/' . $plan->id . '/edit') }}@endif" autocomplete="off">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->
		<!-- name --> 
		<div class="form-group {{{ $errors->has('name') ? 'has-error' : '' }}}">
			<label class="col-md-2 control-label" for="name">{{{ Lang::get('register/access.name') }}}</label>
			<div class="col-md-10">
				<input class="form-control" type="text" name="name" id="name" value="{{{ Input::old('name', isset($plan) ? $plan->name : null) }}}" />
				{{ $errors->first('name', '<span class="help-inline">:message</span>') }}
			</div>
		</div>

		<!-- ./ name -->

		<div class="form-group {{{ $errors->has('value') ? 'has-error' : '' }}}">
			<label for="value" class="col-sm-2 control-label">{{{ Lang::get('admin/plan.value') }}}</label>
			
			<div class="col-md-10">
				<input type="text" class="form-control reais" id="value" name="value" value="{{{ Input::old('value', isset($plan) ? $plan->value : null) }}}">
			</div>
		</div>

		<div class="form-group {{{ $errors->has('points') ? 'has-error' : '' }}}">
			<label for="points" class="col-sm-2 control-label">{{{ Lang::get('admin/plan.points') }}}</label>
			
			<div class="col-md-10">
				<input type="text" class="form-control" id="points" name="points" value="{{{ Input::old('points', isset($plan) ? $plan->points : null) }}}">
			</div>
		</div>

		<div class="form-group {{{ $errors->has('order') ? 'has-error' : '' }}}">
			<label for="order" class="col-sm-2 control-label">{{{ Lang::get('admin/plan.order') }}}</label>
			
			<div class="col-md-10">
				<input type="text" class="form-control" id="order" name="order" value="{{{ Input::old('order', isset($plan) ? $plan->order : null) }}}">
			</div>
		</div>

		<!-- Form Actions -->
		<div class="form-group">
			<div class="col-md-offset-2 col-md-10">
				<a href="javascript:;" class="btn btn-default close_popup" onclick="parent.oTable.fnReloadAjax(); parent.$.colorbox.close();">{{{ Lang::get('admin/register.cancel') }}}</a>
				<button type="submit" class="btn btn-success">OK</button>
			</div>
		</div>
		<!-- ./ form actions -->
	</form>
@stop
