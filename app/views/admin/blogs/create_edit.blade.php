@extends('admin.layouts.modal')

{{-- Content --}}
@section('content')
	
	{{-- Edit Blog Form --}}
	<form class="form-horizontal" method="post" action="@if (isset($post)){{ URL::to('admin/blogs/' . $post->id . '/edit') }}@endif" autocomplete="off">
		<!-- CSRF Token -->
		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		<!-- ./ csrf token -->

		<div class="row">
			<div class="col-sm-10">
				<input type="text" class="form-control input-lg" id="title" name="title" placeholder="Post title" value="{{{ Input::old('title', isset($post) ? $post->title : null) }}}" />
				{{ $errors->first('title', '<span class="help-block error">:message</span>') }}
			</div>
			<div class="col-sm-2 post-save-changes">
				<button type="submit" class="btn btn-green btn-lg btn-block btn-icon"> @if(isset($post)) Salvar @else Publicar @endif <i class="entypo-check"></i></button>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-sm-12">
				<textarea class="form-control wysihtml5" rows="18" name="content" id="content">{{{ Input::old('content', isset($post) ? $post->content : null) }}}</textarea>
				{{ $errors->first('content', '<span class="help-block error">:message</span>') }}
			</div>
		</div>
	</form>
@stop

@section('scripts')
	<link rel="stylesheet" href="{{asset('assets/admin/js/wysihtml5/bootstrap-wysihtml5.css')}}">

	<script src="{{asset('assets/admin/js/wysihtml5/wysihtml5-0.4.0pre.min.js')}}"></script>

	<script src="{{asset('assets/admin/js/wysihtml5/bootstrap-wysihtml5.js')}}"></script>
@stop
