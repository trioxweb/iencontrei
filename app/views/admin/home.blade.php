@extends('admin.layouts.default')

@section('title')
Home ::
@parent
@stop

@section('content')

<!--<div class="row">
	<div class="col-md-12">

	<h4><strong>Infos Bonus</strong></h4>
	
		<div class="tabs-vertical-env">
		
			<ul class="nav tabs-vertical">
				<li class="active"><a href="#v-fixo" data-toggle="tab">Pacotes</a></li>
				<li><a href="#v-indicacao" data-toggle="tab">Indicação</a></li>
				<li><a href="#v-divisaolucro" data-toggle="tab">Divisão de Lucro</a></li>
				<li><a href="#v-binario" data-toggle="tab">Binário</a></li>
				<li><a href="#v-venda" data-toggle="tab">Venda direta</a></li>
				<li><a href="#v-divisaovenda" data-toggle="tab">Divisão Vendas</a></li>
				<li><a href="#v-planodecarreira" data-toggle="tab">Plano de carreira</a></li>				
				<li><a href="#v-premiacoes" data-toggle="tab">Premiações</a></li>
				<li><a href="#v-residualativo" data-toggle="tab">Residual Ativo</a></li>
			</ul>
			
			<div class="tab-content">
				<div class="tab-pane active" id="v-fixo">
					<p>*Para todos os pacotes é obrigatório o pagamento da Adesão (R$100) - Pagamento somente com Boleto, ou seja, nã pode ativar outros cadastrados</p>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Plano</th>
								<th>Pacote</th>
								<th>Binário</th>
								<th>Indicação</th>
								<th>De Binário</th>
								<th>Venda</th>
								<th>% / Geração</th>
								<th>D. Lucro</th>
								<th>D. Vendas</th>
							</tr>
						</thead>
			
						<tbody>
							<tr>
								<td>ADESÃO</td>
								<td>R$ 100</td>
								<td>0 pontos</td>
								<td-></td>
								<td>-</td>
								<td>-</td>
								<td>-</td>
								<td>-</td>
								<td>-</td>
							</tr>
				
							<tr>
								<td>BRONZE</td>
								<td>R$ 699</td>
								<td>200 pontos</td>
								<td>30%</td>
								<td>10%</td>
								<td>5%</td>
								<td>1% / 3ª</td>
								<td>-</td>
								<td>-</td>
							</tr>
				
							<tr>
								<td>PRATA</td>
								<td>R$ 1499</td>
								<td>500 pontos</td>
								<td>30%</td>
								<td>10%</td>
								<td>5%</td>
								<td>1% / 3ª</td>
								<td>1 cota</td>
								<td>1%</td>
							</tr>
							<tr>
								<td>OURO</td>
								<td>R$ 2999</td>
								<td>1000 pontos</td>
								<td>50%</td>
								<td>10%</td>
								<td>5%</td>
								<td>1% / 3ª</td>
								<td>2 cotas</td>
								<td>3%</td>
							</tr>
						</tbody>
					</table>				
				</div>
				<div class="tab-pane" id="v-indicacao">
					<p>O Bonus de Indicação é pago quando Aprova o Cadastro.</p>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Pacote</th>
								<th>Porcentagem</th>
							</tr>
						</thead>
			
						<tbody>
							<tr>
								<td>Consultor</td>
								<td>5%</td>
							</tr>
							<tr>
								<td>Bronze</td>
								<td>10%</td>
							</tr>
							<tr>
								<td>Prata</td>
								<td>15%</td>
							</tr>
							<tr>
								<td>Ouro</td>
								<td>18%</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="tab-pane" id="v-divisaolucro">
					<table class="table table-striped">
					<thead>
							<tr>
								<th>Pacote</th>
								<th>Cota</th>
							</tr>
						</thead>
			
						<tbody>
							<tr>
								<td>Consultor</td>
								<td>-</td>
							</tr>
				
							<tr>
								<td>Bronze</td>
								<td>-</td>
							</tr>
				
							<tr>
								<td>Prata</td>
								<td>1 cota</td>
							</tr>
							<tr>
								<td>Ouro</td>
								<td>2 cotas</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="tab-pane" id="v-binario">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Pacote</th>
								<th>Pontos no binário</th>
							</tr>
						</thead>
			
						<tbody>
							<tr>
								<td>Consultor</td>
								<td>100</td>
							</tr>
							<tr>
								<td>Bronze</td>
								<td>200</td>
							</tr>
							<tr>
								<td>Prata</td>
								<td>700</td>
							</tr>
							<tr>
								<td>Ouro</td>
								<td>700</td>
							</tr>
						</tbody>
					</table>
					<p>- Pagamento: Todo dia às 00:01 (Fuso Horario de Brasília - Brasil)</p>
				</div>
				<div class="tab-pane" id="v-venda">
					<table class="table table-striped">
					<thead>
							<tr>
								<th>Pacote</th>
								<th>Venda direta</th>
								<th>% / Geração</th>
								<th>Disivão Vendas VO</th>
							</tr>
						</thead>
			
						<tbody>
							<tr>
								<td>Consultor</td>
								<td>5ª</td>
								<td>1% / 3º</td>
								<td>-</td>
							</tr>
				
							<tr>
								<td>Bronze</td>
								<td>5%</td>
								<td>1% / 3ª</td>
								<td>-</td>
							</tr>
				
							<tr>
								<td>Prata</td>
								<td>5%</td>
								<td>1% / 3ª</td>
								<td>1%</td>
							</tr>
							<tr>
								<td>Ouro</td>
								<td>5%</td>
								<td>1% / 3ª</td>
								<td>3%</td>
							</tr>
						</tbody>
					</table>
					<p>Pagamento: Diario às 00:01 (Fuso Horario de Brasília - Brasil)</p>
				</div>
				<div class="tab-pane" id="v-divisaovenda">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Pacote</th>
								<th>Porcentagem</th>
							</tr>
						</thead>
			
						<tbody>
							<tr>
								<td>Consultor</td>
								<td>-</td>
							</tr>
							<tr>
								<td>Bronze</td>
								<td>-</td>
							</tr>
							<tr>
								<td>Prata</td>
								<td>1%</td>
							</tr>
							<tr>
								<td>Ouro</td>
								<td>3%</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="tab-pane" id="v-planodecarreira">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Título</th>
								<th>Regra</th>
							</tr>
						</thead>
			
						<tbody>
							<tr>
								<td>Supervisor</td>
								<td>30.000 pontos na equipe menor / Passa a ganhar 30% do binário</td>
							</tr>
							<tr>
								<td>Gerente</td>
								<td>100.000 pontos na equipe menor / Passa a ganhar 35% do binário</td>
							</tr>
							<tr>
								<td>Diretor</td>
								<td>500.000 pontos na equipe menor / Passa a ganhar 40% do binário</td>
							</tr>
							<tr>
								<td>Vice Presidente</td>
								<td>1.900.000 pontos na equipe menor / Passa a ganhar 45% do binário</td>
							</tr>
							<tr>
								<td>Presidente</td>
								<td>7.000.000 pontos na equipe menor / Passa a ganhar  50% do binário</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="tab-pane" id="v-premiacao">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Título</th>
								<th>Premio</th>
							</tr>
						</thead>
			
						<tbody>
							<tr>
								<td>Supervisor</td>
								<td>1 Tablet</td>
							</tr>
							<tr>
								<td>Gerente</td>
								<td>1 Ultrabook</td>
							</tr>
							<tr>
								<td>Diretor</td>
								<td>1 carro 0 Km</td>
							</tr>
							<tr>
								<td>Vice Presidente</td>
								<td>1 Mercedes C180</td>
							</tr>
							<tr>
								<td>Presidente</td>
								<td>1 Evoque</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="tab-pane" id="v-residualativo">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>Geração</th>
								<th>Valor</th>
							</tr>
						</thead>
			
						<tbody>
							<tr>
								<td>1ª</td>
								<td>R$ 4,00</td>
							</tr>
							<tr>
								<td>2ª</td>
								<td>R$ 3,00</td>
							</tr>
							<tr>
								<td>3ª</td>
								<td>R$ 3,00</td>
							</tr>
							<tr>
								<td>4ª</td>
								<td>R$ 3,00</td>
							</tr>
							<tr>
								<td>5ª</td>
								<td>R$ 3,00</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			
		</div>		
	</div>	
</div>-->

<div class="row">
	<div class="col-sm-3">
		<div class="tile-title tile-plum">
				
			<div class="icon">
				<i class="glyphicon glyphicon-ok"></i>
			</div>
				
			<div class="title">
				<h3>{{ $users_actives }}</h3>
				<p>{{{ Lang::get('admin/home.users_active') }}}</p>
			</div>
		</div>
			
	</div>

	<div class="col-sm-3">
		
		<div class="tile-title tile-plum">
		
			<div class="icon">
				<i class="glyphicon glyphicon-time"></i>
			</div>
				
			<div class="title">
				<h3>{{ $users_inactives }}</h3>
				<p>{{{ Lang::get('admin/home.users_pending') }}}</p>
			</div>
		</div>
			
	</div>

	<div class="col-sm-3">
		
		<div class="tile-title tile-plum">
				
			<div class="icon">
				<i class="glyphicon glyphicon-map-marker"></i>
			</div>
				
			<div class="title">
				<h3>{{ $total_volume }}</h3>
				<p>{{{ Lang::get('admin/home.points_of_volume_in_the_month') }}}</p>
			</div>
		</div>
			
	</div>

	<div class="col-sm-3">
		
		<div class="tile-title tile-plum">
				
			<div class="icon">
				<i class="glyphicon glyphicon-fire"></i>
			</div>
				
			<div class="title">
				<h3>{{ $new_users }}</h3>
				<p>{{{ Lang::get('admin/home.new_consultants_in_month') }}}</p>
			</div>
		</div>
			
	</div>

</div>

<!-- FINANCE -->

<div class="row">

	<div class="col-sm-3">
	
		<div class="tile-stats tile-green" style="background:#1a2843">
			<div class="icon"><i class="entypo-volume"></i></div>
			<div class="num">@currency(Credit::getBalanceTotal())</div>
			
			<h3>{{{ Lang::get('admin/home.current_balance') }}}</h3>
		</div>
		
	</div>

	<div class="col-sm-3">
	
		<div class="tile-stats tile-green" style="background:#1a2843">
			<div class="icon"><i class="entypo-download"></i></div>
			<?php $receivable_balance = Order::select(DB::raw('SUM(total) as receivable_balance'))->where('order_status_id', '=', 1)->orWhere('order_status_id', '=', 3)->first()->receivable_balance; ?>
			<div class="num">@currency(($receivable_balance > 0) ? $receivable_balance : 0)</div>
			
			<h3>{{{ Lang::get('admin/home.receivable_balance') }}}</h3>
		</div>
		
	</div>

	<div class="col-sm-3">
	
		<div class="tile-stats tile-blue">
			<div class="icon"><i class="entypo-upload"></i></div>
			
			<div class="num">@currency(UserWithdraw::getBalancePayable())</div>
			
			<h3>{{{ Lang::get('admin/home.balance_payable') }}}</h3>
		</div>
		
	</div>

	<div class="col-sm-3">
	
		<div class="tile-stats tile-blue">
			<div class="icon"><i class="entypo-publish"></i></div>
			<div class="num">@currency(UserWithdraw::getBalanceTotalPay())</div>
			
			<h3>{{{ Lang::get('admin/home.total_pay') }}}</h3>
		</div>
		
	</div>

</div>

<!-- END FINANCE -->

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary">
			<table class="table table-bordered">
				<tbody>
					<tr>
						<td width="100%">
							<strong>{{{ Lang::get('admin/home.total_registers_titles') }}}</strong>
							<br />
							<div id="chart_titles"></div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
@stop

@section('scripts')
	<link rel="stylesheet" href="{{asset('assets/admin/js/rickshaw/rickshaw.min.css')}}">
	<script src="{{asset('assets/admin/js/rickshaw/vendor/d3.v3.js')}}" id="script-resource-8"></script>
	<script src="{{asset('assets/admin/js/rickshaw/rickshaw.min.js')}}" id="script-resource-9"></script>
	<script src="{{asset('assets/admin/js/raphael-min.js')}}" id="script-resource-10"></script>
	<script src="{{asset('assets/admin/js/morris.min.js')}}" id="script-resource-11"></script>
	<script src="{{asset('assets/admin/js/jquery.peity.min.js')}}" id="script-resource-12"></script>
	<script src="{{asset('assets/admin/js/neon-charts.js')}}" id="script-resource-13"></script>
	<script src="{{asset('assets/admin/js/jquery.sparkline.min.js')}}" id="script-resource-14"></script>
	
	<script>
		$.getJSON( "{{{ URL::action('AdminDashboardController@getCharts')}}}", function( data ) {
			Morris.Bar({
	            element: 'chart_titles',
	            data: data,
	            xkey: 'x',
	            ykeys: 'y',
	            labels: ['Quantidade'],
	            stacked: true,
	            barColors: ['#ffaaab', '#ff6264', '#d13c3e']
	        });
		});
	</script>
@stop