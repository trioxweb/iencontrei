@if (count($errors->all()) > 0)
<div class="alert alert-danger alert-block">
	<button type="button" class="close" data-dismiss="alert">&times;</button>
	<h4>{{{ Lang::get('messages.error') }}}</h4>
    {{{ Lang::get('messages.please_check_the_form_below_for_errors') }}}
</div>
@endif

@if ($message = Session::get('success'))
<div class="alert alert-success alert-block">
	<button type="button" class="close" data-dismiss="alert">&times;</button>
	<h4>{{{ Lang::get('messages.success') }}}</h4>
    @if(is_array($message))
        @foreach ($message as $m)
            {{ $m }}
        @endforeach
    @else
        {{ $message }}
    @endif
</div>
@endif

@if ($message = Session::get('error'))
<div class="alert alert-danger alert-block">
	<button type="button" class="close" data-dismiss="alert">&times;</button>
	<h4>{{{ Lang::get('messages.errors') }}}</h4>
    @if(is_array($message))
    @foreach ($message as $m)
    {{ $m }} <br>
    @endforeach
    @else
    {{ $message }}
    @endif
</div>
@endif

@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-block">
	<button type="button" class="close" data-dismiss="alert">&times;</button>
	<h4>{{{ Lang::get('messages.alerts') }}}</h4>
    @if(is_array($message))
    @foreach ($message as $m)
    {{ $m }}
    @endforeach
    @else
    {{ $message }}
    @endif
</div>
@endif

@if ($message = Session::get('info'))
<div class="alert alert-info alert-block">
	<button type="button" class="close" data-dismiss="alert">&times;</button>
	<h4>{{{ Lang::get('messages.info') }}}</h4>
    @if(is_array($message))
    @foreach ($message as $m)
    {{ $m }}
    @endforeach
    @else
    {{ $message }}
    @endif
</div>
@endif


@if ($message = Session::get('alert_order'))

@if($message == 'no_active_contract')

<h4><a href="{{ (Auth::user()->orders->count() > 0) ? URL::action('OfficeOrdersController@getIndex') : URL::action('OfficeOrdersController@getCreate', 'plan') }}" class="btn btn-green btn-lg btn-icon col-md-12" style="margin-bottom:15px"><b>{{{ Lang::get('office/orders/messages.payment_package') }}}</b> <i class="entypo-basket"></i></a></h4>

<!-- Modal Dialog -->
<div class="modal fade" id="modal-no-active-contract">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{{{ Lang::get('office/header.warning') }}}</h4>
            </div>
            
            <div class="modal-body" style="align-text:center">
                <h4>{{{ Lang::get('office/orders/messages.not_yet_pay_activation') }}}</h4>
            </div>
            
            <div class="modal-footer">
                <a href="{{ (Auth::user()->orders->count() > 0) ? URL::action('OfficeOrdersController@getIndex') : URL::action('OfficeOrdersController@getCreate', 'plan') }}" class="btn btn-green btn-icon">{{{ Lang::get('office/orders/messages.payment_package') }}} <i class="entypo-basket"></i></a>
            </div>
        </div>
    </div>
</div>

@section('scripts')
    <script type="text/javascript">
      $(document).ready(function() {
        $('#modal-no-active-contract').modal('show');
      });
    </script>
    <style>
        #modal-no-active {
        background: rgba(0, 0, 0, 0.9) !important;
        }
    </style>
@stop

@endif

@if($message == 'no_active_monthly')

<h4><a href="{{ (Auth::user()->orders->count() > 0) ? URL::action('OfficeOrdersController@getIndex') : URL::action('OfficeOrdersController@getCreate', 'active_monthly') }}" class="btn btn-green btn-lg btn-icon"><b>{{{ Lang::get('office/orders/buttons.payment_activation_monthly') }}}</b> <i class="entypo-basket"></i></a></h4>

<!-- Modal Dialog -->
<div class="modal fade" id="modal-no-active-monthly">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{{{ Lang::get('office/header.warning') }}}</h4>
            </div>
            
            <div class="modal-body" style="align-text:center">
                <h4>{{{ Lang::get('office/orders/messages.not_yet_pay_activation') }}}</h4>
            </div>
            
            <div class="modal-footer">
                <a href="{{ (Auth::user()->orders->count() > 0) ? URL::action('OfficeOrdersController@getIndex') : URL::action('OfficeOrdersController@getCreate', 'plan') }}" class="btn btn-green btn-icon">{{{ Lang::get('office/orders/buttons.payment_activation_monthly') }}} <i class="entypo-basket"></i></a>
            </div>
        </div>
    </div>
</div>
@section('scripts')
    <script type="text/javascript">
      $(document).ready(function() {
        $('#modal-no-active-monthly').modal('show');
      });
    </script>
    <style>
        #modal-no-active-monthly {
        background: rgba(0, 0, 0, 0.9) !important;
        }
    </style>
@stop

@endif

                    
@endif