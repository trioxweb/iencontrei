@extends('emails.layouts.default')

@section('content')

	  	<h2>Realizamos um upgrade no nosso sistema!</h2>

	  	<p>Olá {{ $user->data->firstname }}, seja bem-vindo ao novo sistema da empresa {{ Setting::getSetting('name') }}.</p><br />
	  	<p>Parabéns por mais esta conquista, ​chegamos a excelência da Plataforma 3.0, em seu BackOffice você se surpreenderá, acesse o link abaixo, para criar sua nova senha<p>
	  	<br />
	  	<a href='{{{ (Confide::checkAction('UserController@reset_password', array($token))) ? : URL::to('user/reset/'.$token)  }}}'>
		    {{{ (Confide::checkAction('UserController@reset_password', array($token))) ? : URL::to('user/reset/'.$token)  }}}
		</a>
	  	<p>O Site ja estara no ar, para vocês desfrutarem o melhor, tecnologia de ponta, desfrute, compartilhe, e gere resultados!</p>
	  	<br />
	  	<p>Sucesso!</p><br >
		<p>A Direção</p><br />
@stop