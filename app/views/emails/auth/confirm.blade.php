@extends('emails.layouts.default')

@section('content')
	  	<h2>{{ Lang::get('confide::confide.email.account_confirmation.subject') }}</h2>

		<p>{{ Lang::get('confide::confide.email.account_confirmation.greetings', array( 'name' => $user->username)) }},</p>

		<p>{{ Lang::get('confide::confide.email.account_confirmation.body') }}</p>
		<a href='{{{ URL::to("user/confirm/{$user->confirmation_code}") }}}'>
		    {{{ URL::to("user/confirm/{$user->confirmation_code}") }}}
		</a>

		<p>{{ Lang::get('confide::confide.email.account_confirmation.farewell') }}</p>
@stop