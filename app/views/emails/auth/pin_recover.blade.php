@extends('emails.layouts.default')

@section('content')
		<h2>Recuperar/Remover PIN</h2>

		<div>
			Para Recuperar/Remover seu PIN, clique neste link: {{ URL::action('UserController@getPinreset', array($token)) }}.
		</div>
@stop