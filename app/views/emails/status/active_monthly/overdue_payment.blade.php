@extends('emails.layouts.default')

@section('content')
	  	<h2>Olá, {{ $user->data->firstname }}!</h2>
	  	<h3>O seu ativo mensal venceu.</h3>
	  	<p>Efetue o pagamento <a href="{{{ URL::action('OfficeOrdersController@getIndex') }}}">neste link</a>.</p>
	  	<br />
		<p>A Direção</p><br />
@stop