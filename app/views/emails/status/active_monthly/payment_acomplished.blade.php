@extends('emails.layouts.default')

@section('content')

	  	<h2>Olá, {{ $user->data->firstname }}!</h2>
	  	<h3>Você esta ativo durante 30 dias.</h3>
	  	<br />
	  	<p>Sucesso!</p><br >
		<p>A Direção</p><br />
@stop