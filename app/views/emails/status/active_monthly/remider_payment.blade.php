@extends('emails.layouts.default')

@section('content')

	  	<h2>Olá, {{ $user->data->firstname }}!</h2>
	  	<h3>O seu ativo mensal esta vencendo.</h3>
	  	<p>Efetue o pagamento para não vencer o ativo <a href="{{{ URL::action('OfficeOrdersController@getIndex') }}}">neste link</a>.</p>
	  	<br />
		<p>A Direção</p><br />
@stop