@extends('emails.layouts.default')

@section('content')

	  	<h2>Transferência de {{ $user->data->firstname }} foi Recebida com Sucesso</h2>
		
		<p>Transferência de: {{ $user->username }}<br />
	  	Total Recebido: @currency($transfer_value)</p><br />

	  	<p>Acesse sua Backoffice para mais detalhes: <a href="{{{ URL::action('OfficeFinanceController@getIndex') }}}">Backoffice</a>.</p><br>

	  	<p>Desejamos sucesso em toda sua jornada!</p><br />
	  	
@stop