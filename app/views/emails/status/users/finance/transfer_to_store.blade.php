@extends('emails.layouts.default')

@section('content')

	  	<h2>Transferência para Loja Realizada com Sucesso</h2>
		
	  	<p>Total Transferido: @currency($transfer_value)</p><br />

	  	<p>Se você não se lembra de ter realizado está transferência, altere sua Senha e Verifique seus Dados em <a href="{{{ URL::action('OfficeUserController@getIndex') }}}">Minha Conta</a>.</p><br>
		
		<p>Desejamos sucesso em toda sua jornada!</p><br />

@stop