@extends('emails.layouts.default')

@section('content')

	  	<h2>Foram realizadas alterações no seu cadastro</h2>
		
		<p>Se você não se lembra de ter realizado alterações em seu cadastro, altere sua Senha e Verifique seus Dados em <a href="{{{ URL::action('OfficeUserController@getIndex') }}}">Minha Conta</a>.</p><br>	
		
	  	<p>Se foi você quem realizou as alterações desconsidere este E-mail.</p>

	  	<p>Desejamos sucesso em toda sua jornada!</p><br>	
		
@stop