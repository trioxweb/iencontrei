@extends('emails.layouts.default_admin')

@section('content')

  		<h2>Prezado Administrador!</h2>
	  	<p>Foi realizado um novo cadastro no seu sistema. Segue abaixo os dados do usuário:</p>
		
	  	<p>Nome: {{ $user->data->firstname }}</p>
  		Email: {{ $user->email }}<br />
  		Usuário: {{ $user->username }}<br />
  		Patrocinador: {{ $user->mlm->parent_user()->first()->data->firstname }} ({{ $user->mlm->parent_user()->first()->username }})</p><br />
		<br>	
		<a href="{{{ URL::action('AdminUsersController@getIndex') }}}">Ver mais detalhes...</a><br />

@stop