@extends('emails.layouts.default')

@section('content')

	  	<h2>Você foi posicionado na rede!</h2>

	  	<p>Agora você tem uma posição privilegiada, na Neonn Corporation, você é "PIONEIRO" nesta visionária ideia, lembra-se: "PIONEIROS FIZERAM FORTUNA".</p>
		
	  	<p>Acesse seu backoffice para ver as novidades: {{{ URL::action('OfficeDashboardController@getIndex') }}}</p>

	  	<p>Desejamos sucesso em toda sua jornada!</p>
		
@stop