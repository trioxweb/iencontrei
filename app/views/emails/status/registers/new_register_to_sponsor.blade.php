@extends('emails.layouts.default')

@section('content')

	  	<h2>Parabéns você tem um novo membro em sua equipe!</h2>

	  	<p>Ajude sua rede a alcançar resultados financeiros, oferecendo suporte e atendimento.</p>

	  	<p>Segue abaixo esta as informações de contato:</p>
		
	  	<p>Nome: {{ $user->data->firstname }}<br />
	  	Email: {{ $user->email }}<br />
	  	Usuario: {{ $user->username }}</p><br />

	  	<h3>Faça contato o mais breve possível, ensinando todo o "PADRÃO DE SUCESSO"!</h3>

	  	<p>Desejamos sucesso em toda sua jornada!</p>
		
@stop