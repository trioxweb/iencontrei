@extends('emails.layouts.default_admin')

@section('content')

	  	<h2>Novo Pedido de Upgrade</h2>

	  	<p>Foi realizado um novo pedido de upgrade de pacote em seu sistema, por favor, verifique o pagamento e aprove este pedido. Segue abaixo os dados do usuário que solicitou.</p>

	  	<p>Usuario: {{ $user->username }}<br />
	  	Pedido Nº: {{ $order_id }}</p><br />
	  	<br />
	  	<a href="{{{ URL::action('AdminOrdersController@getIndex') }}}">Ver mais detalhes...</a>

@stop