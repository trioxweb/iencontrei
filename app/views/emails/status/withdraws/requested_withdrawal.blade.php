@extends('emails.layouts.default_admin')

@section('content')

	  	<h2>Novo pedido de saque realizado</h2>

	  	<p>Foi realizado um novo pedido de saque no seu sistema. Por favor, verifique a solicitação e confirme a solicitação de saque.</p>
	  	<p>Segue abaixo os dados do usuário que solicitou o saque:</p>

	  	<p>Usuario: {{ $user->username }}<br />
	  	Valor: @currency($withdraw->value)</p><br />
	  	<br />
	  	<a href="{{{ URL::action('AdminWithdrawsController@getIndex') }}}">Ver mais detalhes...</a>

@stop