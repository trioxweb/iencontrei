@extends('emails.layouts.default')

@section('content')

	  	<h2>Seu pedido de saque está completo</h2>

	  	<p>O pedido foi processado com sucesso, por favor verifique o meio de pagamento selecionado.</p>
	  	<p>Caso você estiver com alguma dúvida, envie um e-mail para <a href="mailto:financeiro@iecontrei.com">financeiro@iecontrei.com</a>.</p>

	  	<p>Valor do Saque: @currency($withdraw->value) <br> 
	  	Saque Realizado por: {{ $withdraw->payment_method }}</p>

	  	<p>Desejamos sucesso em toda sua jornada!</p><br />

@stop