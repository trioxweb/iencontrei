@extends('emails.layouts.default_admin')

@section('content')

	  	<h2>Novo titulo conquistado</h2>

	  	<p>Foi registrado um novo título conquistado em seu sistema. Segue abaixo os dados do usuário:</p>

	  	<p>Usuario: {{ $user->username }}</p><br />
	  	<p>Titulo: {{ $user->mlm->title->name }}</p><br />

@stop