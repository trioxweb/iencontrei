@extends('emails.layouts.default')

@section('content')

  		<h1>Parabéns!</h1>

	  	<h3>Você Acaba-se de se Tornar um {{ $user->mlm->title->name }}.</h3>

	  	<p>Desejamos sucesso em toda sua jornada!</p><br />

@stop