<!DOCTYPE html>
<html lang="pt-BR">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<!--HEADER-->
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
		 		<td bgcolor="#ececec">
					<div align="center">
	  					<table border="0" cellpadding="0" cellspacing="0" width="550">
	    					<tr>
	     						 <td style="padding: 15px 0px 0px 0px;">
	        						<table border="0" cellpadding="0" cellspacing="0" width="100%">
	          							<tr>
	            							<td width="550" align="center">
	            								<a href="{{ URL::action('OfficeDashboardController@getIndex') }}">
	            									<img alt="{{ Setting::getSetting('name') }}" src="{{ Setting::getSetting('url') }}/assets/img/email/cabecalho.png" width="500" height="125" style="display: block; font-family: Arial; color: #ffffff; font-size: 14px;" border="0">
	            								</a>
	            							</td>
	          							</tr>
	        						</table>
								</td>
							</tr>
						</table>
	      			</div>
	      		</td>
	    	</tr>
	  	</table>

	  	<div style="padding: 0px 200px 0px 200px;">

	  	<!--CONTENT-->
	  	@yield('content')
	  	<!--END-CONTENT-->

	  	<!--FOOTER-->

	  	<br />
	  	<p>Agradecimentos!</p>
		<p>Grupo iEncontrei</p>

		<b>Suporte e Atendimento</b>
		<p>Email: <a href="mailto:marketing@icubes.com.br">marketing@icubes.com.br</a></p>
		<p>Web Site: <a href="http://www.icubes.com.br">www.icubes.com.br</a></p>
	  	</div>
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td bgcolor="#ececec">
					<div align="center">
						<table border="0" cellpadding="0" cellspacing="0" width="550">   
							<tr>
								<td align="center" style="font-family: Arial, sans-serif; color: #888888; font-size: 10px; padding: 10px 0 10px 0; line-height: 10px;">
			    					<a href="https://www.facebook.com/iencontrei" target="_blank">
			        					<img src="{{ Setting::getSetting('url') }}/assets/img/email/social_facebook.png" width="25" height="25">
			    					</a> 
			    					<a href="https://www.twitter.com/iencontrei" target="_blank">
			        					<img src="{{ Setting::getSetting('url') }}/assets/img/email/social_twitter.png" width="25" height="25">
			    					</a>
			    					<p>Você recebeu este email através do site {{ Setting::getSetting('url') }}</p> 
			    					<p><a href="http://www.iencontrei.com/#section-about-brooklyn" target="_blank">A Empresa</a> | <a href="http://www.iencontrei.net/" target="_blank">Produtos</a> | <a href="http://iencontrei.com/app" target="_blank">Chat</a></p>
									Copyright 2015 <a href="http://iecontrei.com/" target="_blank">iEncontrei</a>. Todos os Direitos Reservados</p>        
								</td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
		</table>
	</body>
</html>