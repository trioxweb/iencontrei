@extends('emails.layouts.default')

@section('content')

	  	<h2>Você foi Cadastrado com Sucesso</h2>

	  	<p>Parabens {{ $user->data->firstname }}, seja bem-vindo à empresa {{ Setting::getSetting('name') }}.</p><br />
	  	<br />
	  	<p>A partir de agora você terá todo suporte, treinamento e informações da empresa que mais paga no modelo de negócio marketing multinível.</p>
	  	<p>Deixaremos você informado de tudo que esta acontecendo, Eventos, Conferências, Palestras e Treinamentos.</p>
		<p>Estamos preparados com toda infra-estrutura física e online, para te ajudar a alcançar os maiores resultados nesta empresa.</p>
	  	<br />
	  	<p>Segue abaixo seus dados de acesso da sua página personalizada, espalhe para o maior número de pessoas, você será notificado quando alguém se cadastrar em sua organização.</p>
	  	<br/>
	  	<p>Nome de usuário: {{ $user->username }}</p><br />
	  	Acesse o seu Backoffice: <a href="{{ URL::action('OfficeDashboardController@getIndex') }}">{{ URL::action('OfficeDashboardController@getIndex') }}</a>
		<br/>
		<p>Construimos um sistema avançadp e exclusivo, para você ter acesso e controle de todo seu negócio, Na sua Área Vip, também temos eficazes ferramentas para prospecção e divulgação de uma forma profissional!</p>
		<br/>
		<p>Desejamos sucesso em toda sua jornada!</p>
@stop