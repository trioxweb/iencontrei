<?php

namespace Embrapag;
use DB;

class Embrapag {

    private $config = array(
        'email' => 'presidente@viagensdeouro.com', 
        'token' => 'CA0E8715-F26F-46F0-A139-05C3D09323B4'
    );

    public function getStatus($order_id){

        $data = array(
            'recebedor_email' => $this->config['email'],
            'recebedor_api_token' => $this->config['token'],
            'pedido_id' => $order_id
        );
        $this->utf8_encode_deep($data);

        $retorno = $this->curl("https://embrapag.syspag.com/payment/getStatus", $data);
        //$retorno = json_decode($retorno);

        echo var_dump($retorno); die();

        if(isset($retorno->url_pagamento)){
            DB::table('embrapag')->insert([
                'order_id' => $order->id,
                'url_pagamento' => $retorno->url_pagamento, 
                'status'    => 'Aguardando Pagamento'   
            ]);
        }
    }

    /*
     * @param Order $order Order Model
     * @param User $user
     * @param String $desc
     * @return Bollean
     */
    public function criaTransacao($order, $user, $desc) {

        //Token do Cliente
        $data_cliente = array(
            'recebedor_api_token' => $this->config['token'], 
            'recebedor_email'=> $this->config['email']
        );   

        //Dados do Pagador
        $data_pagador = array(
            'pagador_nome' => $order->payment_firstname.' '.$order->payment_lastname, 
            'pagador_email' => $order->email, 
            'pagador_cpf' => $user->data->cpf, 
            'pagador_telefone_ddd' => substr($order->telephone, 0, 2),
            'pagador_telefone' => substr($order->telephone, 2)
        ); 

        //Dados do Pedidos  
        $data_pedido  = array(
            'pedido_id' => $order->id, 
            'pedido_descricao' => $desc,
            'pedido_meio_pagamento' => 'boleto',
            'pedido_moeda' => 'BRL',
            'pedido_valor_total_original' => $order->total
        );     

        $data_entrega = array(
            'entrega_nome' => $order->shipping_firstname.' '.$order->shipping_lastname,
            'entrega_logradouro' => $order->shipping_address_1,
            'entrega_numero' => $order->shipping_address_2,
            'entrega_complemento' => $order->shipping_address_2,
            'entrega_bairro' => $order->shipping_address_1,
            'entrega_cep' => $order->shipping_postcode,
            'entrega_cidade' => $order->shipping_city,
            'entrega_estado' => strtoupper($order->shipping_zone),
            'entrega_pais' => 'BRA'
        ); 

        $retorno = PaybrasCriaTransacao::main(array_merge($data_cliente, $data_pedido, $data_pagador, $data_entrega));
        
        if(isset($retorno['url_pagamento'])){
            DB::table('embrapag')->insert([
                'order_id' => $order->id,
                'url_pagamento' => $retorno['url_pagamento'], 
                'status'    => 'Aguardando Pagamento'   
            ]);
        }
        return $retorno;
    }

}