<?php

namespace Embrapag;

class PaybrasConfig{
	
	private static $config;
	private static $data;
	const varName = 'PaybrasConfig';
	
	private function __construct() {
		$PaybrasConfig = array();

        $PaybrasConfig['ambiente'] = Array();

        //Serviço de consulta de status de Transação para ambiente de produção
        $PaybrasConfig['ambiente']['status']['producao'] = "https://embrapag.syspag.com/payment/getStatus";
        //Serviço de consulta de status de Transação para ambiente de sandbox
        $PaybrasConfig['ambiente']['status']['sandbox'] = "https://sandbox.embrapag.syspag.com/payment/getStatus";
        //Serviço de consulta de status de Transação para ambiente local - apenas para desenvolvedores paybras
        $PaybrasConfig['ambiente']['status']['local'] = "http://localhost/embrapag/payment/getStatus";

        //Serviço de consulta de parcelas para ambiente de produção
        $PaybrasConfig['ambiente']['parcelas']['producao'] = "https://embrapag.syspag.com/payment/getParcelas";
        //Serviço de consulta de parcelas para ambiente de sandbox
        $PaybrasConfig['ambiente']['parcelas']['sandbox'] = "https://sandbox.embrapag.syspag.com/payment/getParcelas";
        //Serviço de consulta de parcelas para ambiente local - apenas para desenvolvedores paybras
        $PaybrasConfig['ambiente']['parcelas']['local'] = "http://localhost/embrapag/payment/getParcelas";

        //Serviço de criação de transação para ambiente de produção
        $PaybrasConfig['ambiente']['criacao']['producao'] = "https://embrapag.syspag.com/payment/api/criaTransacao";
        //Serviço de criação de transação para ambiente de sandbox
        $PaybrasConfig['ambiente']['criacao']['sandbox'] = "https://sandbox.embrapag.syspag.com/payment/api/criaTransacao";
        //Serviço de criação de transação para ambiente local
        $PaybrasConfig['ambiente']['criacao']['local'] = "http://localhost/embrapag/payment/api/criaTransacao";

        //Serviço de envio de códigos de rastreio em lote para ambiente de produção
        $PaybrasConfig['ambiente']['rastreio']['producao'] = "https://embrapag.syspag.com.com/payment/transactions/enviaRastreio";
        //Serviço de envio de códigos de rastreio em lote para ambiente de sandbox
        $PaybrasConfig['ambiente']['rastreio']['sandbox'] = "https://sandbox.embrapag.syspag.com/payment/transactions/enviaRastreio";
        //Serviço de envio de códigos de rastreio em lote para ambiente local
        $PaybrasConfig['ambiente']['rastreio']['local'] = "http://localhost/embrapag/payment/transactions/enviaRastreio";


        $PaybrasConfig['lojista'] = Array();

        //E-mail do lojista usado como nome de usuário no Paybras
        $PaybrasConfig['lojista']['email'] = "presidente@viagensdeouro.com";

        //Token do lojista. Encontrada no menu "integração -> Token de segurança" do painel Paybras
        $PaybrasConfig['lojista']['token'] = "CA0E8715-F26F-46F0-A139-05C3D09323B4";

        //Ambiente que se deseja utilizar:
        // - sandbox: ambiente de teste; 
        // - producao: ambiente de produção; 
        // - local: ambiente local (apenas para desenvolvedores paybras)
        $PaybrasConfig['lojista']['ambiente'] = "producao";

		if (isset($PaybrasConfig)) {
			self::$data = $PaybrasConfig;
			unset($PaybrasConfig);
		} else {
			throw new \Exception("Configuração não definida.");
		}
	}

	public static function init() {
		if (self::$config == null) {
			self::$config = new PaybrasConfig();
		}
		return self::$config;
	}
	
	public static function getDadosLojista() {
		if (isset(self::$data['lojista']) && isset(self::$data['lojista']['email']) && isset(self::$data['lojista']['token'])) {
			return new PaybrasDadosLojista(self::$data['lojista']['email'], self::$data['lojista']['token']);
		} else {
			throw new \Exception("Dados de Lojista não adicionados ao arquivo de configuração");
		}
	}
	
	public static function getURL($servico) {
        if(isset(self::$data['lojista']['ambiente'])){
            $ambiente = self::$data['lojista']['ambiente'];
    		if (isset(self::$data['ambiente']) && isset(self::$data['ambiente'][$servico][$ambiente])) {
    			return self::$data['ambiente'][$servico][$ambiente];
    		} else {
    			throw new \Exception("Dados de conexão não adicionados ao arquivo de configuração");
    		}
        } else {
            throw new \Exception("Ambiente não setado no arquivo de configuração");
        }
	}

	public static function curl($url,$data){
        $ch = curl_init();

        $data_string = json_encode($data);
        
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );
        return curl_exec($ch);
    }

    public static function utf8_encode_deep(&$input) {
        if (is_string($input)) {
            $input = utf8_encode($input);
        } elseif (is_array($input)) {
            foreach ($input as &$value) {
                self::utf8_encode_deep($value);
            }
            unset($value);

        } elseif (is_object($input)) {
            $vars = array_keys(get_object_vars($input));
            foreach ($vars as $var) {
                self::utf8_encode_deep($input->$var);
            }
        }
    }
}

?>
