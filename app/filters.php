<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{	

	/**
	 * 
	 * Se Logado Gravar Cookie de acesso ao Opencart
	 * 
	 */ 
	if(Auth::user()){
		setcookie('opencart_access', 'free', time() + (10 * 365 * 24 * 60 * 60), '/', '.'.Setting::getSetting('url'));
    }else{
		setcookie('opencart_access', 'blocked', time() + (10 * 365 * 24 * 60 * 60), '/', '.'.Setting::getSetting('url'));
    }

	$language = Session::get('language', 'pt'); //en will be the default language.
    App::setLocale($language);

	/**
	 * 
	 * Transform Subdomain in User Refer
	 * 
	 */ 
	if($user_refer = UserRefer::where('ip', '=', $request->getClientIp())->first()){
		Session::put('refer_parent_id', $user_refer->user_id);
	}

	$setting = new Setting();
	$url_config = $setting->getSetting('url');
	$url_contine = (Request::path() != '/') ? Request::path() : '';
	$url = explode('.',Request::getHost());
	$url_t = count(explode('.', $url_config))+1;	
	$uri = explode( "/", $url_contine );

	// To StrLower
	$url[0] = strtolower($url[0]);

	if (count($url) == $url_t && $url[0] != 'www'){
		$user = User::where('username', '=', $url[0])->first();
		if($user){
			$user->username = strtolower($user->username);
			if($user->isActiveInNetwork()){
				if(!$user_refer){
					$user_refer = new UserRefer();
				}
				$user_refer->ip 	 = $request->getClientIp();
				$user_refer->user_id = $user->id;
				$user_refer->save();

				setcookie('tracking', $user->username, time() + (60*60*24*665), '/', $url_config);
				setcookie('tracking_email', $user->email, time() + (60*60*24*665), '/', $url_config);
				setcookie('tracking_firstname', $user->data->firstname, time() + (60*60*24*665), '/', $url_config);
				Session::put('refer_parent_id', $user->id);
			}
		}
		if(!isset($_COOKIE['tracking']) || ($user && $_COOKIE['tracking'] != $user->username)){
			return Redirect::to('http://'.$url_config.'/'.$url_contine);
		}
	}
	
	// Integra Wordpress
	if($uri[0] != 'user' && $uri[0] != 'cron' && $uri[0] != 'office' && $uri[0] != 'admin' && $uri[0] != 'register' && $uri[0] != 'facebook' && $uri[0] != 'cadastrar' && $uri[0] != 'setlang'){
		
		// Redireciona para Usuario
		if($user_refer){
			$user = User::find($user_refer->user_id);
			$user->username = strtolower($user->username);
			if($user->username != $url[0]){
				return Redirect::to('http://'.$user->username.'.'.$url_config.'/'.$url_contine);
			}
		}

		//define('WP_USE_THEMES', true);
		//return require( dirname( __FILE__ ) . '/../public/site/wp-blog-header.php' );
	}elseif($uri[0] != 'register'){
		if(count($url) == $url_t && $url[0] != 'www'){
			return Redirect::to('http://'.$url_config.'/'.$url_contine);
		}
	}elseif($uri[0] == 'register'){
		if($user_refer){
			$user = User::find($user_refer->user_id);
			$user->username = strtolower($user->username);
			if($user->username != $url[0]){
				if(!(Auth::user() && Auth::user()->username == $user->username)){
					return Redirect::to('http://'.$user->username.'.'.$url_config.'/'.$url_contine);
				}elseif(count($url) == $url_t && $url[0] != 'www'){
					return Redirect::to('http://'.$url_config.'/'.$url_contine);
				}
			}
		}
	}

	/**
	 * 
	 * /End Transform Subdomain in User Refer
	 * 
	 */ 

	/**
	 * 
	 * Redirect to Admin if User/admin not in Network
	 * 
	 */ 
	Session::forget('alert_order');

	if(Request::is('office*') && Auth::user()){

		if(Auth::user()->can('manage_admin')){
			if(!Auth::user()->mlm->network_in)
	    		return Redirect::to('/admin');
	    }

	    if(Auth::user()->mlm->contractNeedPay()){
	    	Session::flash('alert_order', 'no_active_contract');
	    }elseif(Auth::user()->mlm->activeMonthlyNeedPay()){
	    	Session::flash('alert_order', 'no_active_monthly');
	    }

    }
});


App::after(function($request, $response)
{
	
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if ( Auth::guest() ) // If the user is not logged in
	{
        	return Redirect::guest('user/login');
	}
});

Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('user/login/');
});

/*
|--------------------------------------------------------------------------
| Role Permissions
|--------------------------------------------------------------------------
|
| Access filters based on roles.
|
*/

// Check for permissions on admin actions
Entrust::routeNeedsPermission( 'admin', 'manage_admin', Redirect::to('/user/login') );
Entrust::routeNeedsPermission( 'admin/index', 'manage_admin', Redirect::to('/') );
Entrust::routeNeedsPermission( 'admin/blogs*', 'manage_blogs', Redirect::to('/admin') );
Entrust::routeNeedsPermission( 'admin/comments*', 'manage_comments', Redirect::to('/admin') );
Entrust::routeNeedsPermission( 'admin/users*', 'manage_users', Redirect::to('/admin') );
Entrust::routeNeedsPermission( 'admin/credits*', 'manage_credits_withdraws', Redirect::to('/admin') );
Entrust::routeNeedsPermission( 'admin/withdraws*', 'manage_credits_withdraws', Redirect::to('/admin') );
Entrust::routeNeedsPermission( 'admin/points*', 'manage_points', Redirect::to('/admin') );
Entrust::routeNeedsPermission( 'admin/reports*', 'manage_reports', Redirect::to('/admin') );
Entrust::routeNeedsPermission( 'admin/roles*', 'manage_roles', Redirect::to('/admin') );
Entrust::routeNeedsPermission( 'admin/settings*', 'manage_credits_withdraws', Redirect::to('/admin') );
Entrust::routeNeedsPermission( 'admin/plans*', 'manage_setting', Redirect::to('/admin') );

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::getToken() !== Input::get('csrf_token') &&  Session::getToken() !== Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});

/*
|--------------------------------------------------------------------------
| Language
|--------------------------------------------------------------------------
|
| Detect the browser language.
|
*/

Route::filter('detectLang',  function($route, $request, $lang = 'auto')
{

    if($lang != "auto" && in_array($lang , Config::get('app.available_language')))
    {
        Config::set('app.locale', $lang);
    }else{
        $browser_lang = !empty($_SERVER['HTTP_ACCEPT_LANGUAGE']) ? strtok(strip_tags($_SERVER['HTTP_ACCEPT_LANGUAGE']), ',') : '';
        $browser_lang = substr($browser_lang, 0,2);
        $userLang = (in_array($browser_lang, Config::get('app.available_language'))) ? $browser_lang : Config::get('app.locale');
        Config::set('app.locale', $userLang);
        App::setLocale($userLang);
    }
});

/*
|--------------------------------------------------------------------------
| PIN Protection Filter
|--------------------------------------------------------------------------
*/

Route::filter('pin', function()
{
	if(Auth::user()->pin){
		if(!isset($_COOKIE['pin'])){
			return Redirect::action('OfficeDashboardController@getPin');
		}else{
			if ($_COOKIE['pin'] != Auth::user()->pin){
				return Redirect::action('OfficeDashboardController@getPin');
			}
		}
	}
});